package br.com.moveltrack.cobranca.services

import br.com.moveltrack.persistence.util.Utils
import br.com.moveltrack.persistence.domain.Cliente
import br.com.moveltrack.persistence.domain.MBoleto
import br.com.moveltrack.persistence.domain.MBoletoStatus
import br.com.moveltrack.persistence.repositories.ContratoRepository
import br.com.moveltrack.persistence.repositories.MBoletoRepository
import br.com.moveltrack.persistence.util.BoletoUtils
import br.com.moveltrack.persistence.util.OSValidator
import br.com.moveltrack.persistence.util.Utils.DDMMYY_FORMAT
import br.com.moveltrack.persistence.util.Utils.DD_MM_YY_FORMAT
import br.com.moveltrack.persistence.util.Utils.moeda
import org.springframework.core.io.ByteArrayResource
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.io.IOException
import java.time.ZonedDateTime
import javax.mail.MessagingException


@Component
@EnableScheduling
class TaskAtualizaViagem(
        private val mBoletoRepository: MBoletoRepository,
        private val javaMailSender: JavaMailSender,
        private val contratoRepository: ContratoRepository

) {

    @Scheduled(cron = "0 38 20 * * ?")
    fun enviaEmailCobranca() {
        val list = mBoletoRepository.findAllBySituacaoAndDataVencimentoBefore(MBoletoStatus.VENCIDO, ZonedDateTime.now().minusDays(3))?.groupBy { it.contrato?.cliente }
        list?.keys?.forEach { cliente ->
            if (Utils.validEmail(cliente?.email)) {
                val boletos = mBoletoRepository.findAllByContratoAndSituacao(contratoRepository.findByCliente(cliente),MBoletoStatus.VENCIDO)
                sendEmailWithAttachment(cliente!!, boletos!!, TipoEmail.COBRANCA)
            }
        }
    }

    @Scheduled(cron = "0 50 20 * * ?")
    fun enviaEmailLembrete() {
        val list = mBoletoRepository.findAllBySituacaoAndDataVencimentoBefore(MBoletoStatus.EMITIDO, ZonedDateTime.now().plusDays(5))?.groupBy { it.contrato?.cliente }
        list?.forEach { cliente, boletos ->
            if (Utils.validEmail(cliente?.email)) {
                sendEmailWithAttachment(cliente!!, boletos, TipoEmail.LEMBRETE)
            }
        }
    }



    @Throws(MessagingException::class, IOException::class)
    fun sendEmailWithAttachment(cliente: Cliente, boletos: List<MBoleto>, tipoEmail: TipoEmail) {
        val msg = javaMailSender.createMimeMessage()
        val helper = MimeMessageHelper(msg, true)
        if(OSValidator.isUnix) helper.setTo(cliente.usuario?.email!!) else  helper.setTo("fb040974@gmail.com")
        when (tipoEmail) {
            TipoEmail.COBRANCA -> {
                helper.setSubject("Cobrança Moveltrack")
                helper.setText(getConteudoCobranca(cliente, boletos),true)
            }
            TipoEmail.LEMBRETE -> {
                helper.setSubject("Lembrete de Pagamento Moveltrack")
                helper.setText(getConteudoLembrete(cliente, boletos),true)
            }
            TipoEmail.AVISO_PROTEST0 -> {}
        }
        boletos.forEach {boleto->
            val bytes = BoletoUtils.getBoletoIuguInBytes(boleto)
            val fileName = "Boleto_CPF_${boleto.contrato?.cliente?.cpf?.filter {it->it.isDigit()}}_Vencimento_${boleto.dataVencimento?.format(DD_MM_YY_FORMAT)}.pdf"
            helper.addAttachment(fileName, ByteArrayResource(bytes) )
        }
        javaMailSender.send(msg)
    }
}


enum class TipoEmail{
    COBRANCA,
    LEMBRETE,
    AVISO_PROTEST0
}


fun getConteudoCobranca(cliente: Cliente,mBoletos: List<MBoleto>): String {
    val mensagem = StringBuffer("<p>Prezado(a) ${cliente.nome}, consta no nosso sistema que não ${if(mBoletos.size>1) "não foram pagos os boletos" else "foi pago o boleto"} com vencimento em:</p>")
    mBoletos.forEach {mBoleto->
        mensagem.append("<p><b>${mBoleto.dataVencimento?.format(DDMMYY_FORMAT)}</b>, no valor de <b> ${moeda(mBoleto.valor)} </b>;</p>")
    }
    mensagem.append("<br>")
    mensagem.append("<p>Caso estes boletos já tenha${if(mBoletos.size>1)"m" else ""} sido pago${if(mBoletos.size>1)"s" else ""}, desconsidere esta mensagem.</p>")
    mensagem.append("<p>Este e-mail é automático e não recebe respostas.</b>.</p>")
    mensagem.append("<p>Por favor, em caso de dúvida, ligue para <b>4105-0145</b>.</p>")
    mensagem.append("<p>Cordialmente, Dep. Financeiro</b>.</p>")
    mensagem.append("<p><b>Moveltrack Segurança & Tecnologia.</b></p>")
    return mensagem.toString()
}


fun getConteudoLembrete(cliente: Cliente,mBoletos: List<MBoleto>): String {
    val mensagem = StringBuffer("<p>Prezado(a) ${cliente.nome}, para sua comodidade, ${if(mBoletos.size>1)"seguem boletos" else "segue boleto"} para pagamento, com vencimento em:</p>")
    mBoletos.forEach {mBoleto->
        mensagem.append("<p><b>${mBoleto.dataVencimento?.format(DDMMYY_FORMAT)}</b>, no valor de <b> ${moeda(mBoleto.valor)} </b>;</p>")
    }
    mensagem.append("<br>")
    mensagem.append("<p>Este ${if(mBoletos.size>1)"podem ser pagos" else "pode ser pago"} em toda a rede bancária até o vencimento, ou por meio eletrônico.</b></p>")
    mensagem.append("<p>Caso ${if(mBoletos.size>1)"estes boletos já tenham sido pagos" else "este boleto já tenha sido pago"}, desconsidere esta mensagem.</p>")
    mensagem.append("<p>Este e-mail é automático e não recebe respostas.</b></p>")
    mensagem.append("<p>Por favor, em caso de dúvida, ligue para <b>4105-0145</b>.</p>")
    mensagem.append("<p>Cordialmente, Dep. Financeiro</b>.</p>")
    mensagem.append("<p><b>Moveltrack Segurança & Tecnologia.</b></p>")
    return mensagem.toString()
}
