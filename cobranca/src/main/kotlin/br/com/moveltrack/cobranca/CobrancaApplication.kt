package br.com.moveltrack.cobranca

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender

@SpringBootApplication
class CobrancaApplication

fun main(args: Array<String>) {
    runApplication<CobrancaApplication>(*args)
}





