package br.com.moveltrack.gateway.commands

import br.com.moveltrack.gateway.utils.*
import br.com.moveltrack.persistence.domain.Sms
import br.com.moveltrack.persistence.domain.SmsTipo
import br.com.moveltrack.persistence.repositories.SmsRepository
import java.util.*

// D    Y    D    ,    0    0    0    0    0    0    #
class CommandHandlerCRX1(smsRepository: SmsRepository) : CommandHandler(smsRepository) {
    var DEBUG_MODE = Constantes.DEBUG_CRX1

    override fun sendCommandToTracker(sms: Sms?) {
        logs(DEBUG_MODE, "enviando para o imei (sms):" + sms!!.imei)
        var command: ByteArray? = null
        val serial1 = HashMaps.instance?.getSerial1ByImei(sms.imei)
        val serial2 = HashMaps.instance?.getSerial2ByImei(sms.imei)
        val socket = HashMaps.instance?.getSocketByImei(sms.imei)
        when (sms.tipo) {
            SmsTipo.BLOQUEIO -> {
                command = buildCRX1Command(COMMAND_CUT_OIL, serial1!!, serial2!!)
                logs(DEBUG_MODE, "Comando de Bloqueio")
                printBuff(DEBUG_MODE, *command)
            }
            SmsTipo.DESBLOQUEIO -> {
                command = buildCRX1Command(COMMAND_CONNECT_OIL, serial1!!, serial2!!)
                logs(DEBUG_MODE, "Comando de Desbloqueio")
                printBuff(DEBUG_MODE, *command)
            }
            else -> {
            }
        }
        if (command != null)
            sendToTerminal(command, socket, sms)
    }

    companion object {
        @JvmField
		val COMMAND_LOCATION = byteArrayOf(0x44, 0x57, 0x58, 0x58, 0x2C, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x23)

        // D    W    X    X    ,    0    0    0    0    0    0    #
        val COMMAND_CONNECT_OIL = byteArrayOf(0x48, 0x46, 0x59, 0x44, 0x2C, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x23)

        // H    F    Y    D    ,    0    0    0    0    0    0    #
        val COMMAND_CUT_OIL = byteArrayOf(0x44, 0x59, 0x44, 0x2C, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x23)
        @JvmStatic
		fun buildCRX1Command(commandContent: ByteArray, ser1: Byte?, ser2: Byte?): ByteArray {
            val commandList: MutableList<Byte> = ArrayList()
            val informationContentSize = 4 + commandContent.size
            val packageSize = 6 + informationContentSize
            commandList.add(0x78.toByte())
            commandList.add(0x78.toByte()) //start
            commandList.add(packageSize.toByte()) //package size 
            commandList.add(0x80.toByte()) //protocolo de envio
            commandList.add(informationContentSize.toByte()) //information content size
            commandList.add(0x00.toByte())
            commandList.add(0x01.toByte())
            commandList.add(0xA9.toByte())
            commandList.add(0x63.toByte()) //server flag bit 00 01 A9 63 
            for (i in commandContent.indices) {
                commandList.add(commandContent[i]) //content
            }
            commandList.add(ser1!!)
            commandList.add(ser2!!) //serial
            commandList.add(0x00.toByte())
            commandList.add(0x00.toByte()) //crc
            commandList.add(0x0D.toByte())
            commandList.add(0x0A.toByte()) //stop bit
            val commandArray: Array<Any> = commandList.toTypedArray()
            var command = ByteArray(commandArray.size)
            for (i in command.indices) {
                command[i] = commandArray[i] as Byte
            }
            command = Crc16.setCrc(command)
            return command
        }
    }
}
