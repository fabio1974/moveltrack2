package br.com.moveltrack.gateway.utils

import br.com.moveltrack.persistence.domain.*

object GerenciaAlarmes {
    var SOS = "SOS!"
    var BATERIA_FRACA = "BATERIA FRACA!"
    var DESCONECTANDO_BATERIA = "DESCONECTANDO BATERIA!"
    var PARADA_BRUSCA = "PARADA BRUSCA!"
    var ALARME_NEUTRO = "ALARME NEUTRO!"
    var ENTROU_CERCA = "ENTROU NA CERCA VIRTUAL!"
    var SAIU_CERCA = "SAIU DA CERCA VIRTUAL!"
    fun gerenciaAlarme(alarme: Location?) {
        /*	Equipamento equipamento =  equipamentoDao.findByImei(alarme.getImei());
		Veiculo veiculo = EquipamentoDAO.getInstance().getVeiculoByEquipamento(equipamento.getId());
		Cliente cliente =  EquipamentoDAO.getInstance().getClienteByVeiculo(veiculo);

		if(veiculo.getTipo()== VeiculoTipo.MOTOCICLETA && (getAlarmeByCodigo(alarme).equals(DESCONECTANDO_BATERIA)||getAlarmeByCodigo(alarme).equals(BATERIA_FRACA)))
			sendEmail(alarme,equipamento,cliente,veiculo,"moveltrack@gmail.com");
		else	
			sendEmail(alarme,equipamento,cliente,veiculo,cliente.getEmailAlarme());  */
    }

    fun sendEmail(alarme: Location?, equipamento: Equipamento?, cliente: Cliente?, veiculo: Veiculo?, to: String?) {

        /*if(to==null)
			to = "fabio.barros1974@gmail.com";	   

		String from = "alarme@moveltrack.com.br";
		Properties props = System.getProperties();
		props.put("mail.smtp.host", "mail.moveltrack.com.br");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "587");
		//props.put("mail.debug", "true");
		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("alarme@moveltrack.com.br","mvt17547");
			}
		});
		try{
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));

			message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));
			message.setSubject(getAlarmeByCodigo(alarme));

			Multipart multipart = new MimeMultipart();
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(getConteudo(alarme,cliente,equipamento), "text/html; charset=ISO-8859-1");
			multipart.addBodyPart(messageBodyPart);
			message.setContent(multipart );
			Transport.send(message);
			logs(true,"Email de ALARME enviado para "+cliente.getNome()+ " e-mail:"+cliente.getEmailAlarme());
		}catch (MessagingException mex) {
			mex.printStackTrace();
		}*/
    }

    private fun getConteudo(alarme: Location, cliente: Pessoa, equipamento: Equipamento): String {
/*		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");

		StringBuffer mensagem = new StringBuffer("<p>Prezado cliente, um alarme foi enviado nesse momento do ve�culo "+ EquipamentoDAO.getInstance().getVeiculoByEquipamento(equipamento.getId())+":</p>");
		mensagem.append("<br>");
		mensagem.append("<p>Tipo do Alarme: "+getAlarmeByCodigo(alarme).toUpperCase()+"</p>");
		mensagem.append("<p>Data: "+sdf.format(alarme.getDateLocation())+"h</p>");
		mensagem.append("<p>Latitude: "+alarme.getLatitude()+"</p>");
		mensagem.append("<p>Longitude: "+alarme.getLongitude()+"</p>");
		mensagem.append("<p>Velocidade: "+alarme.getVelocidade()+"</p>");
		mensagem.append("<p>Cliente: "+cliente.getNome()+"</p>");
		mensagem.append("<p>Este e-mail � autom�tico e n�o recebe respostas.</b>.</p>");
		mensagem.append("<p>Por favor, em caso de d�vida, ligue para <b>4105-0145</b>.</p>");
		mensagem.append("<p><b>Moveltrack Rastreamento.</b></p>");
		return mensagem.toString();*/
        return ""
    }

    private fun getAlarmeByCodigo(loc: Location?): String? {
        if (loc != null && loc.alarmType != null) {
            if (loc.alarmType == "100") {
                return SOS
            } else if (loc.alarmType == "011") {
                return BATERIA_FRACA
            } else if (loc.alarmType == "010") {
                return DESCONECTANDO_BATERIA
            } else if (loc.alarmType == "001") {
                return PARADA_BRUSCA
            } else if (loc.alarmType == "000") {
                return ALARME_NEUTRO
            } else if (loc.alarmType == "110") {
                return ENTROU_CERCA
            } else if (loc.alarmType == "111") {
                return SAIU_CERCA
            }
        }
        return null
    } /*   private static void attach(Multipart multipart, byte[] pdfFile, String fileName) {
	   BodyPart messageBodyPart = new MimeBodyPart();
       DataSource source = new ByteArrayDataSource(pdfFile, "application/pdf");
       try {
		messageBodyPart.setDataHandler(new DataHandler(source));
	       messageBodyPart.setFileName(fileName);
	       multipart.addBodyPart(messageBodyPart);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
   }*/
}
