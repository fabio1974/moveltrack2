package br.com.moveltrack.gateway.protocols

import br.com.moveltrack.persistence.domain.Location
import br.com.moveltrack.persistence.domain.ModeloRastreador
import br.com.moveltrack.persistence.domain.toLocation2
import br.com.moveltrack.persistence.repositories.ChipRepository
import br.com.moveltrack.persistence.repositories.Location2Repository
import br.com.moveltrack.persistence.repositories.LocationRepository
import org.springframework.beans.factory.annotation.Autowired
import java.io.IOException
import java.io.Serializable
import java.net.Socket
import java.text.SimpleDateFormat
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit
import java.util.*
import kotlin.experimental.and

abstract class ReadingProtocolHandler(val locationRepository: LocationRepository?,
                                      val location2Repository: Location2Repository?): Serializable {

    @Throws(IOException::class)
    abstract fun startReading(clientSocket: Socket?)

    fun sendToTerminal(response: ByteArray?, socket: Socket?) {
        try {
            if (socket != null && socket.isConnected && !socket.isClosed) {
                val out = socket.getOutputStream()
                out.write(response)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    var lastLocation: Location? = null
    fun saveLocation(loc: Location,modeloRastreador: ModeloRastreador?) {

        val igualAnterior = lastLocation != null &&
                                    lastLocation!!.latitude == loc.latitude &&
                                    lastLocation!!.longitude == loc.longitude &&
                                    lastLocation!!.dateLocation!!.isEqual(loc.dateLocation)

        val valoresAbsurdos = loc.latitude == 0.0 || loc.velocidade > 200 || loc.latitude < -90

        val dataNoFuturo = ChronoUnit.MINUTES.between(ZonedDateTime.now(), loc.dateLocation) > 5

        if (!igualAnterior && !valoresAbsurdos && !dataNoFuturo){
            locationRepository?.save(loc)
                location2Repository?.save(loc.toLocation2())
                lastLocation = loc
        }
    }



    fun printBuff(DEBUG_MODE: Boolean, vararg buff: Byte) {
        var i = 0
        while (i < buff.size && DEBUG_MODE) {
            System.out.printf("%02x ", buff[i])
            i++
        }
        if (DEBUG_MODE) println()
    }

    fun fromByte(b: Byte): BitSet {
        var b = b
        val bits = BitSet(8)
        for (i in 0..7) {
            bits[i] = b and 1 == 1.toByte()
            b = (b.toInt() shr 1).toByte()
        }
        return bits
    }

    fun printSocketStatus(DEBUG_MODE: Boolean, title: String, socket: Socket?) {
        if (DEBUG_MODE) {
            println(title + " " + if (socket != null) "not null" else "null")
            if (socket != null) {
                println(title + " " + if (socket.isConnected) "connected" else "not conected")
                println(title + " " + if (socket.isClosed) "closed" else "not closed")
                println(title + " " + "Inet Address: " + socket.inetAddress)
            }
        }
    }


}

