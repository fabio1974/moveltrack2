package br.com.moveltrack.gateway.utils

import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

object Constantes {

    const val PORT_TK103 = 6214
    const val PORT_GT06 = 6215
    const val PORT_H02 = 6216
    const val PORT_GT06_B = 6217
    const val PORT_XT009 = 6218
    const val PORT_TR02 = 6219
    const val PORT_CRX1 = 6220
    const val PORT_JV200 = 6221
    const val PORT_TK06 = 6222
    const val PORT_CRXN = 6223
    const val PORT_CRX3 = 6224

    const val DEBUG_CRX1 = false
    const val DEBUG_CRX3 = false
    const val DEBUG_CRXN = false
    const val DEBUG_GT06 = false
    const val DEBUG_TK103 = false
    const val DEBUG_TR02 = false
    const val DEBUG_GT06B = false
    const val DEBUG_XT009 = false
    const val DEBUG_H02 = false
    const val DEBUG_JV200 = true
    const val DEBUG_TK06 = false
}

