package br.com.moveltrack.gateway.utils

import java.net.Socket
import java.util.*

class HashMaps private constructor() {
    @Synchronized
    fun getSocketByImei(imei: String?): Socket? {
        return socketsMap[imei]
    }

    @Synchronized
    fun getSerial1ByImei(imei: String?): Byte {
        return try {
            seriais[imei]!![0]
        } catch (e: Exception) {
            0x00.toByte()
        }
    }

    fun printStatus(DEBUG_MODE: Boolean) {
        if (!DEBUG_MODE) return
        val imeis: Set<String> = socketsMap.keys
        for (imei in imeis) {
            println(imei + " -> " + getSocketByImei(imei)!!.inetAddress)
        }
    }

    @Synchronized
    fun getSerial2ByImei(imei: String?): Byte {
        return try {
            seriais[imei]!![1]
        } catch (e: Exception) {
            0x00.toByte()
        }
    }

    @Synchronized
    fun setSocketInImei(imei: String, socket: Socket) {
        socketsMap[imei] = socket
    }

    @Synchronized
    fun setSeriais(imei: String, serial1: Byte, serial2: Byte) {
        val bt = arrayOf(serial1, serial2)
        //printBuff(imei,true,serial1,serial2);
        seriais[imei] = bt
    } /*public void printBuff(String imei,Boolean DEBUG_MODE,byte...buff){
		logs(DEBUG_MODE, "Seriais do imei "+imei);
		for (int i = 0; i < buff.length && DEBUG_MODE; i++) {
			System.out.printf("%02x ",buff[i]);
		}
		if(DEBUG_MODE)
			System.out.println();
	}*/

    companion object {
        var socketsMap: MutableMap<String, Socket> = HashMap()
        var seriais: MutableMap<String, Array<Byte>> = HashMap()
        private var hashMaps: HashMaps? = null

        @get:Synchronized
        val instance: HashMaps?
            get() {
                if (hashMaps == null) hashMaps = HashMaps()
                return hashMaps
            }
    }

    init {
        socketsMap = HashMap()
        seriais = HashMap()
    }
}
