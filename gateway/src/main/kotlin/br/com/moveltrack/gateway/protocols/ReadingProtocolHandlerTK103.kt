package br.com.moveltrack.gateway.protocols

import br.com.moveltrack.gateway.utils.Constantes
import br.com.moveltrack.gateway.utils.GpsCommand
import br.com.moveltrack.gateway.utils.LOCAL_ZONE
import br.com.moveltrack.gateway.utils.logs
import br.com.moveltrack.persistence.domain.Location
import br.com.moveltrack.persistence.domain.ModeloRastreador
import br.com.moveltrack.persistence.repositories.EquipamentoRepository
import br.com.moveltrack.persistence.repositories.Location2Repository
import br.com.moveltrack.persistence.repositories.LocationRepository
import br.com.moveltrack.persistence.repositories.SmsRepository
import org.springframework.stereotype.Component
import java.io.IOException
import java.io.PrintWriter
import java.net.Socket
import java.time.LocalDateTime
import java.time.ZonedDateTime
import java.util.*

@Component
class ReadingProtocolHandlerTK103(
        val smsRepository: SmsRepository?=null,
        val equipamentoRepository: EquipamentoRepository?=null,
        locationRepository: LocationRepository?=null,
        location2Repository: Location2Repository?=null
): ReadingProtocolHandler(locationRepository, location2Repository) {

    var DEBUG_MODE: Boolean = Constantes.DEBUG_TK103
    @Throws(IOException::class)
    override fun startReading(socket: Socket?) {
        val inputStream = socket!!.getInputStream()
        val outputStream = socket.getOutputStream()
        val pw = PrintWriter(outputStream, true)
        var completeMessage = ""
        var readByte = 0
        var imei = ""
        logs(DEBUG_MODE, " - START READING PROTOCOL TK103")
        while (inputStream.read().also { readByte = it } != -1) {
            val readChar = readByte.toChar()
            completeMessage += readChar
            if (readChar == ';') {
                logs(DEBUG_MODE, " - $completeMessage")
                if (completeMessage.startsWith("##,imei:")) {
                    pw.println("LOAD")
                    imei = completeMessage.substring("##,imei:".length, "##,imei:".length + 15)
                    logs(DEBUG_MODE, "->$imei<-")
                    logs(DEBUG_MODE, " - ENVIOU LOAD!")
                } else if (completeMessage == "$imei;") {
                    pw.println("ON")
                    logs(DEBUG_MODE, " - ENVIOU ON!")
                } else {
                    insertPoinOnDatabase(completeMessage)
                }
                completeMessage = ""
            }
        }
    }

    private fun insertPoinOnDatabase(completeMessage: String) {
        val gc = GpsCommand()
        val quebra = completeMessage.split(",".toRegex()).toTypedArray()
        try {
            gc.imei=quebra[0]
            gc.type=quebra[1]
            gc.datetime=quebra[2]
            gc.x1=quebra[3]
            gc.f=quebra[4]
            gc.time=quebra[5]
            gc.a=quebra[6]
            gc.lat=quebra[7]
            gc.ns=quebra[8]
            gc.lon=quebra[9]
            gc.ew=quebra[10]
            gc.speed=quebra[11]
            gc.x2=quebra[12]

            //EntityManager em = EntityProviderUtil.get().getEntityManager(Location.class);
            val loc = Location()
            loc.imei = gc.imei!!.substring("imei:".length)

            //sendComandToTracker(loc.getImei());
            val dt: String = gc.datetime!!
            val t: String = gc.time!!
            val calendar = GregorianCalendar()
            calendar[2000 + dt.substring(0, 2).toInt(), dt.substring(2, 4).toInt() - 1, dt.substring(4, 6).toInt(), dt.substring(6, 8).toInt(), dt.substring(8, 10).toInt()] = t.substring(4, 6).toInt()
            calendar[GregorianCalendar.MILLISECOND] = t.substring(7, 10).toInt()
            //val lat: String = gc.getLat()
            //val lon: String = gc.getLon()
            val latitude = gc.lat!!.substring(0, 2).toDouble() + gc.lat!!.substring(2).toDouble() / 60
            val longitude = gc.lon!!.substring(0, 3).toDouble() + gc.lon!!.substring(3).toDouble() / 60
            val dateLoc = ZonedDateTime.ofInstant(calendar.time.toInstant(),LOCAL_ZONE)
            loc.dateLocation=dateLoc
            loc.dateLocationInicio=dateLoc
            loc.latitude=if (gc.ns.equals("N")) latitude else -1 * latitude
            loc.longitude=if (gc.ew.equals("E")) longitude else -1 * longitude
            loc.velocidade=gc.speed!!.toFloat() * 3.6 / 1.852
            loc.comando=gc.type
            saveLocation(loc, ModeloRastreador.TK103A2)
        } catch (ex: StringIndexOutOfBoundsException) {
            logs(DEBUG_MODE, "Message:" + ex.message)
        } catch (e: Exception) {
            logs(DEBUG_MODE, e.stackTrace.toString())
        }
    } /*	private void sendComandToTracker(String imei) {
	try{
		int comando = EquipamentoDAO.getInstance().getComando(imei);
		if(comando==Comando.STOP_ENGINE){
			String senha = EquipamentoDAO.getInstance().getSenha(imei);
			String command = "Fix060s***n"+senha;
			Utils.log(DEBUG_MODE," - STOP_ENGINE"+command);
			pw.print(command);
			EquipamentoDAO.getInstance().update(imei, Comando.NOTHING);
		}
		if(comando==Comando.START_ENGINE){
			String senha = EquipamentoDAO.getInstance().getSenha(imei);
			String command = "Fix020s***n"+senha;
			Utils.log(DEBUG_MODE," - START_ENGINE"+command);
			pw.print(command);
			EquipamentoDAO.getInstance().update(imei, Comando.NOTHING);
		}
	}catch(Exception e){
		Utils.log(DEBUG_MODE,e.getStackTrace().toString());
	}
}*/
    //Location lastLocation;
}
