package br.com.moveltrack.gateway.utils

import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.sql.Timestamp
import java.text.*
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.*

fun logs(debug: Boolean, content: String?) {
    if (debug) println("${ZonedDateTime.now(LOCAL_ZONE).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)}: $content")
}

var LOCAL_ZONE = ZoneId.ofOffset("UTC", ZoneOffset.ofHours(-3))
var UTC_ZONE = ZoneId.ofOffset("UTC", ZoneOffset.ofHours(0))

fun localToZonedDateTime(localDateTimeString: String,pattern: String): ZonedDateTime{
    val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern(pattern)
    val dateTime = LocalDateTime.parse(localDateTimeString, formatter)
    return dateTime.atZone(LOCAL_ZONE)
}



object Utils {

    val FORMATO_DATA = SimpleDateFormat("dd/MM/yyyy")
    val FORMATO_HORA = SimpleDateFormat("HH:mm")
    val FORMATO_DATA_HORA = SimpleDateFormat("dd/MM/yyyy HH:mm")
    val FORMATO_DATA_SEGUNDOS = SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
    val numberFormat = NumberFormat.getInstance() //new Locale("pt","BR"));
    fun logImei(debug: Boolean, content: String, currentImei: String, standardImei: String) {
        if (debug && currentImei == standardImei) println(FORMATO_DATA_SEGUNDOS.format(Date()) + " - " + content)
    }

  /*  @JvmStatic
    fun logs(debug: Boolean, content: String?) {
        if (debug) println(FORMATO_DATA_SEGUNDOS.format(Date()) + " - " + content)
    }*/

 /*   fun logh(debug: Boolean, content: String) {
        if (debug) print(FORMATO_DATA_SEGUNDOS.format(Date()) + " - " + content)
    }*/

/*    fun logs(debug: Boolean, content: String?) {
        if (debug) println(content)
    }*/

    fun isBlank(obj: Any?): Boolean {
        return obj == null || obj.toString() == null || obj.toString().trim { it <= ' ' } == ""
    }

    fun BlankIfNull(texto: String?): String {
        return texto ?: ""
    }

    fun BlankIfNullOrEmpty(texto: String?): String {
        return if (texto == null || texto == "") "" else texto
    }

    fun truncate(valor: Double, casasDecimais: Int): Double {
        val inteiro = (valor * Math.pow(10.0, casasDecimais.toDouble())).toInt()
        return inteiro / Math.pow(10.0, casasDecimais.toDouble())
    }

    fun toUpperCase(texto: String): String {
        return texto.toUpperCase()
    }

    @Throws(ParseException::class)
    fun textToTime(texto: String?): Date {
        return FORMATO_HORA.parse(texto)
    }

    @Throws(ParseException::class)
    fun textToDate(texto: String?): Date {
        return FORMATO_DATA.parse(texto)
    }

    fun numberToText(numero: Double, decimais: Int): String {
        return numberToText(numero, decimais)
    }

    fun numberToText(numero: Number?, decimais: Int): String {
        if (numero == null) return ""
        numberFormat.isGroupingUsed = false
        numberFormat.maximumFractionDigits = decimais
        return numberFormat.format(numero)
    }

    @Throws(ParseException::class)
    fun textToNumber(texto: String): Number? {
        return if (texto.trim { it <= ' ' } == "") null else numberFormat.parse(texto)
    }



    fun lpad(valueToPad: String, filler: String, size: Int): String {
        var valueToPad = valueToPad
        while (valueToPad.length < size) {
            valueToPad = filler + valueToPad
        }
        return valueToPad
    }

    //Se estiver rodando no pda

    //return "";
    val currentPath: String
        get() {
            //Se estiver rodando no pda
            if (System.getProperty("os.arch") == "x86") return System.getProperty("user.dir") + File.separatorChar
            val javavm = System.getProperty("java.vm.name")
            return if (javavm == "J9" || javavm == "CVM") {
                val caminho = System.getProperty("java.class.path")
                val tmp = File(caminho)
                tmp.parent + File.separatorChar
            } else System.getProperty("user.dir") + File.separatorChar

            //return "";
        }


    fun getHoraByDataHora(dataHora: Date?): Date? {
        if (dataHora == null) return null
        val strhora = FORMATO_HORA.format(dataHora)
        try {
            return FORMATO_HORA.parse(strhora)
        } catch (ex: ParseException) {
            ex.printStackTrace()
        }
        return null
    }

    fun getHoraStringByDataHora(dataHora: Date?): String? {
        return if (dataHora == null) null else FORMATO_HORA.format(dataHora)
    }

    fun dataMaisXMinutos(data: Date?, x: Long): Date? {
        return if (data == null) null else Date(data.time + 1000 * 60 * x)
    }

    val j9Versao: String
        get() {
            var j9Versao = ""
            try {
                val f = File("/Application/J9/bin/j9.exe")
                val dataArq = f.length()
                val s = (dataArq.toString() + "").substring(0, 3)
                j9Versao = if (s == "563") "J91.0" else if (s == "778") "J91.1" else if (s == "409") "J91.2" else "J9New"
            } catch (e: Exception) {
                j9Versao = "J9?"
                e.printStackTrace()
            }
            return j9Versao
        }

    //563 - 10/11/2006  1.0
    //778 - 15/02/2008  1.1
    //409 - 26/09/2008  1.2
    fun dataMaisXSegundos(data: Date?, x: Long): Date? {
        return if (data == null) null else Date(data.time + 1000 * x)
    }

    fun dataMaisXDias(data: Date?, x: Long): Date? {
        return if (data == null) null else Date(data.time + 1000 * 60 * 60 * 24 * x)
    }

    fun dataAtualMenosXDias(x: Long): Timestamp {
        val c = Calendar.getInstance()
        return Timestamp(c.timeInMillis - 1000 * 60 * 60 * 24 * x)
    }

    fun dataMaisXDias(data: Timestamp, x: Long): Timestamp {
        return Timestamp(data.time + 1000 * 60 * 60 * 24 * x)
    }

    fun somaDataHora(data: Date?, hora: Date?): Date? {
        if (data == null) return hora
        if (hora == null) return data
        try {
            //Retira a hora da data
            val calData = Calendar.getInstance()
            val calHora = Calendar.getInstance()
            calData.time = data
            calHora.time = hora
            calData[Calendar.HOUR_OF_DAY] = calHora[Calendar.HOUR_OF_DAY]
            calData[Calendar.MINUTE] = calHora[Calendar.MINUTE]
            return calData.time

            /*
                        String strdata = FORMATO_DATA.format(data);
                        Date novadata = FORMATO_DATA.parse(strdata);
                         
                        String strhora = FORMATO_HORA.format(hora);
                        Date novahora = FORMATO_HORA.parse(strhora);
                        long soma = novadata.getTime() + novahora.getTime() - 10800000;
                        return new Date(soma);*/
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }

    fun convertData(value: String?): String {
        if (value == null || value == "") return ""
        return if (value.indexOf("/") != -1) value else value.substring(0, 2) + "/" + value.substring(2, 4) + "/" + value.substring(4)
        //   if(uf.equals("CE"))
        //       return value.substring(4) + "/" + value.substring(2,4) + "/" + value.substring(0,2);
        // + "=" + value;
    }

    fun getDataFromDataHora(dataHora: Date?): Date? {
        if (dataHora == null) return null
        val data = FORMATO_DATA.format(dataHora)
        try {
            return FORMATO_DATA.parse(data)
        } catch (ex: ParseException) {
            ex.printStackTrace()
        }
        return null
    }

    fun getDataStringByDataHora(dataHora: Date?): String? {
        return if (dataHora == null) null else FORMATO_DATA.format(dataHora)
    }

    fun isSegura(url: String): Boolean {
        return url.startsWith("https")
    }

    fun getHost(url: String): String {
        var host = ""
        try {
            val aux = url.substring(url.indexOf("://") + 3)
            host = aux.substring(0, aux.indexOf("/"))
        } catch (e: Exception) {
            println(e.message)
        }
        return host
    }

    fun getHostName(url: String): String {
        var aux = ""
        try {
            aux = url.substring(url.indexOf("://") + 3)
        } catch (e: Exception) {
            println(e.message)
        }
        return aux
    }

    fun stripCharsInBag(text: String?, bag: String): String {
        if (text == null) return ""
        val value = StringBuffer()
        for (ndx in 0 until text.length) {
            val cc = text[ndx]
            if (bag.indexOf(cc) == -1) value.append(cc)
        }
        return value.toString()
    }

    fun getMarcaFromMarcaModelo(marcaModelo: String): String {
        try {
            return marcaModelo.substring(marcaModelo.indexOf("/") + 1)
        } catch (e: Exception) {
            print(e.message)
        }
        return ""
    }


    fun usingDateFormatterWithTimeZone(input: Long): String {
        val date = Date(input)
        val cal: Calendar = GregorianCalendar(TimeZone.getTimeZone("GMT"))
        val sdf = SimpleDateFormat("yyyy/MMM/dd hh:mm:ss z")
        sdf.calendar = cal
        cal.time = date
        return sdf.format(date)
    }

    fun formatDate(format: String?, time: Long): String {
        val sdf = SimpleDateFormat(format)
        return sdf.format(Date(time))
    }

    /*public static void imprimeInputStream(InputStream in){
        try {
            BufferedReader read = new BufferedReader(new InputStreamReader(in));
            while (read.ready()) {
                System.out.println(read.readLine());
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }*/
    @Throws(IOException::class)
    fun fillFileHierarchy(folderFrom: File) {
        val files = folderFrom.listFiles()
        for (x in files.indices) {
            val f = files[x]
            if (f.isDirectory) {
                fillFileHierarchy(f)
            } else if (f.isFile) {
                val fNew = File(folderFrom.path + File.separator + f.name)
                val fOut = FileOutputStream(fNew)
                fOut.close()
            }
        }
    }

    @Throws(IOException::class)
    fun copyFileHierarchy(folderFrom: File, folderTo: File) {
        val buffer = ByteArray(8192)
        if (!folderTo.exists()) {
            folderTo.mkdirs()
        }
        val files = folderFrom.listFiles()
        for (x in files.indices) {
            val f = files[x]
            if (f.isDirectory) {
                copyFileHierarchy(f, File(folderTo.toString() + File.separator + f.name))
            } else if (f.isFile) {
                val fNew = File(folderTo.path + File.separator + f.name)
                val fOut = FileOutputStream(fNew)
                val fIn = FileInputStream(f)
                var bytes_read: Int
                while (fIn.read(buffer).also { bytes_read = it } != -1) // Read bytes until EOF
                    fOut.write(buffer, 0, bytes_read) //   write bytes
                fIn.close()
                fOut.close()
            }
        }
    }

    fun getListaConcatenada(lista: Array<String?>?): String {
        if (lista == null || lista.size == 0) return ""
        val sb = StringBuffer()
        for (x in lista.indices) if (x <= lista.size - 1) sb.append(lista[x]).append(", ")
        return sb.toString()
    }

    fun getDataFormatadaComBarras(data: String?, UF: String): String? {
        if (data == null && data!!.trim { it <= ' ' } == "" || data.length != 8) return data
        return if (UF == "CE") data.substring(6, 8) + "/" + data.substring(4, 6) + "/" + data.substring(0, 4) else data.substring(0, 2) + "/" + data.substring(2, 4) + "/" + data.substring(4, 8)
    }

    @Throws(ParseException::class)
    fun getDateFromStringFormat(data: String?, format: String?): Date {
        val sdf = SimpleDateFormat(format)
        return Date(sdf.parse(data).time)
    }


    fun dateStringToTimestampString(data: String?, format: String?): String? {
        if (data == null || data == "") return null
        val sdf = SimpleDateFormat(format)
        var date: Date? = null
        try {
            date = sdf.parse(data)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        val timestamp = Timestamp(date!!.time)
        return timestamp.toString()
    }

    fun dateTimestampStringToDateString(timestampString: String?, format: String?): String {
        return if (timestampString == null) "" else SimpleDateFormat(format).format(Timestamp.valueOf(timestampString))
    }

    fun dateTimestampStringToTimestamp(timestampString: String?): Timestamp? {
        if (timestampString == null) return null
        var result: Timestamp? = null
        try {
            result = Timestamp(SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(timestampString).time)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return result
    }





}
