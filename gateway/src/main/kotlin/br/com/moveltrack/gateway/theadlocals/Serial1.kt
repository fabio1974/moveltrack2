package br.com.moveltrack.gateway.theadlocals

object Serial1 {
    val instance = ThreadLocal<Byte>()

    var serial1: Byte?
        get() = instance.get()
        set(serial1) {
            instance.set(serial1)
        }
}
