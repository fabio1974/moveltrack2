package br.com.moveltrack.gateway.utils

class GpsCommand {

    //imei:359587010124900,tracker,0809231929,13554900601,F,112909.397,A,2234.4669,N,11354.3287,E,0.11,;
    var imei: String? = null
    var type: String? = null
    public var datetime: String? = null
    var x1: String? = null
    var f: String? = null
    var time: String? = null
    var a: String? = null
    var lat: String? = null
    var ns: String? = null
    var lon: String? = null
    var ew: String? = null
    var speed: String? = null
    var x2: String? = null
    var course: String? = null
    override fun toString(): String {
        return "Imei$imei - Type$type - DateTime$datetime - Lat$lat - NS-$ns - Lon$lon - EW$ew - Speed$speed - Time$time - X1$x1 - x2$x2<-"
    }
}
