package br.com.moveltrack.gateway

import br.com.moveltrack.gateway.protocols.ReadingProtocolHandler
import br.com.moveltrack.gateway.utils.logs
import java.io.IOException
import java.net.ServerSocket

internal class ThreadProtocol(var port: Int, var ph: ReadingProtocolHandler) : Thread() {
    override fun run() {
        var serverSocket: ServerSocket? = null
        try {
            serverSocket = ServerSocket(port)
            while (true) {
                logs(DEBUG_SERVICE,"Esperando ${ph.javaClass.name} na porta $port")
                ThreadForConnectedTracker(ph, serverSocket.accept()).start() //Uma thread iniciada para cada rastreador
            }
        } catch (e: Exception) {
            logs(DEBUG_SERVICE,"$port - ${e.message}")
            try {
                serverSocket!!.close()
            } catch (e1: IOException) {
                logs(DEBUG_SERVICE,e1.message);
            }
        }
    }

}
