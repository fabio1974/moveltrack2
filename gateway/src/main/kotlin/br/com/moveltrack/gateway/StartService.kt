package br.com.moveltrack.gateway

import br.com.moveltrack.gateway.StartService.compenion.chCRX1
import br.com.moveltrack.gateway.StartService.compenion.chCRX3
import br.com.moveltrack.gateway.StartService.compenion.chCRXN
import br.com.moveltrack.gateway.StartService.compenion.chJV200
import br.com.moveltrack.gateway.commands.*
import br.com.moveltrack.gateway.protocols.*
import br.com.moveltrack.gateway.utils.Constantes
import br.com.moveltrack.gateway.utils.Utils
import br.com.moveltrack.gateway.utils.logs
import br.com.moveltrack.persistence.domain.ModeloRastreador
import br.com.moveltrack.persistence.domain.Sms
import br.com.moveltrack.persistence.domain.SmsStatus
import br.com.moveltrack.persistence.repositories.EquipamentoRepository
import br.com.moveltrack.persistence.repositories.Location2Repository
import br.com.moveltrack.persistence.repositories.LocationRepository
import br.com.moveltrack.persistence.repositories.SmsRepository
import org.springframework.stereotype.Service
import java.time.ZonedDateTime

@Service
class StartService(
        val smsRepository: SmsRepository,
        val equipamentoRepository: EquipamentoRepository,
        val locationRepository: LocationRepository,
        val location2Repository: Location2Repository

){

    object compenion{
        var chCRX1: CommandHandler? = null
        var chCRXN: CommandHandler? = null
        var chCRX3: CommandHandler? = null
        var chJV200: CommandHandler? = null
    }



    fun initService(){
        logs(true, "Iniciando leitura dos rastreadores")

        ThreadProtocol(Constantes.PORT_CRX1, ReadingProtocolHandlerCRX1(smsRepository,equipamentoRepository,locationRepository,location2Repository)).start()
        ThreadProtocol(Constantes.PORT_CRX3, ReadingProtocolHandlerCRX3(smsRepository,equipamentoRepository,locationRepository,location2Repository)).start()
        ThreadProtocol(Constantes.PORT_CRXN, ReadingProtocolHandlerCRXN(smsRepository,equipamentoRepository,locationRepository,location2Repository)).start()
        ThreadProtocol(Constantes.PORT_JV200, ReadingProtocolHandlerJV200(smsRepository,equipamentoRepository,locationRepository,location2Repository)).start()
        ThreadProtocol(Constantes.PORT_TK103, ReadingProtocolHandlerTK103(smsRepository,equipamentoRepository,locationRepository,location2Repository)).start()

        chCRX1 = CommandHandlerCRX1(smsRepository)
        chCRX3 = CommandHandlerCRX3(smsRepository)
        chCRXN = CommandHandlerCRXN(smsRepository)
        chJV200 = CommandHandlerJV200(smsRepository)

        val threadCommand: ThreadCommand = ThreadCommand(smsRepository,equipamentoRepository)
        threadCommand.start()

    }

    internal class ThreadCommand(val smsRepository: SmsRepository,val equipamentoRepository: EquipamentoRepository) : Thread() {
        override fun run() {
            while (true) {
                try {
                    val ultimoMinuto = ZonedDateTime.now().minusMinutes(1)
                    val list = smsRepository.findAllByStatusAndDataUltimaAtualizacaoAfterOrderByDataUltimaAtualizacaoDesc(SmsStatus.ESPERANDO_SOCKET,ultimoMinuto)
                    val map = list?.groupBy { it.imei }
                    val ultimosSmss = map?.mapValues { it -> it.value.sortedBy { it.dataUltimaAtualizacao }.first() }

                         for (sms in ultimosSmss?.values!!) {
                            val mr: ModeloRastreador? = equipamentoRepository.findByImei(sms.imei).modelo
                            when (mr) {
                                ModeloRastreador.CRX1 -> chCRX1?.sendCommandToTracker(sms)
                                ModeloRastreador.CRX3 -> chCRX3?.sendCommandToTracker(sms)
                                ModeloRastreador.CRXN -> chCRXN?.sendCommandToTracker(sms)
                                ModeloRastreador.JV200 -> chJV200?.sendCommandToTracker(sms)
                                else -> {
                                }
                            }
                        }

                    sleep(5000)

                } catch (e: Exception) {
                    if (DEBUG_SERVICE) e.printStackTrace()
                }
            }
        }
    }


}

var DEBUG_SERVICE = true
