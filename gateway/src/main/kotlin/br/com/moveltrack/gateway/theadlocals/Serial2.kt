package br.com.moveltrack.gateway.theadlocals

object Serial2 {
    val instance = ThreadLocal<Byte>()

    var serial2: Byte?
        get() = instance.get()
        set(serial1) {
            instance.set(serial1)
        }
}
