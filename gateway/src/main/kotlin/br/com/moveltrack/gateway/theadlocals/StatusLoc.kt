package br.com.moveltrack.gateway.theadlocals

import br.com.moveltrack.persistence.domain.Location

object StatusLoc {
    val instance = ThreadLocal<Location>()

    var statusLoc: Location?
        get() = instance.get()
        set(statusLoc) {
            instance.set(statusLoc)
        }
}
