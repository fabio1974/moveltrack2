package br.com.moveltrack.gateway.commands

import br.com.moveltrack.gateway.utils.logs
import br.com.moveltrack.persistence.domain.Sms
import br.com.moveltrack.persistence.domain.SmsStatus
import br.com.moveltrack.persistence.repositories.SmsRepository
import java.net.Socket
import java.net.SocketTimeoutException
import java.util.*
import kotlin.experimental.and

abstract class CommandHandler(val smsRepository: SmsRepository) {
    abstract fun sendCommandToTracker(sms: Sms?)
    fun sendToTerminal(response: ByteArray?, socket: Socket?, sms: Sms) {
        try {
            printSocketStatus(true, "smsTipo=" + sms.tipo.toString(), socket)
            socket?.soTimeout = 15 * 1000

            if(isSocketOk(socket)) {

                logs(true, "Writing to the socket...")
                socket?.getOutputStream()?.write(response)
                sms.status = SmsStatus.ENVIADO
                smsRepository.save(sms)
                logs(true, "Wrote to the socket!")
                Thread.sleep(2000)

            }else{
                sms.status = SmsStatus.ESPERANDO
                smsRepository.save(sms)
            }

        } catch (ste: SocketTimeoutException) {
            ste.printStackTrace()
            sms.status = SmsStatus.ESPERANDO
            smsRepository.save(sms)

        } catch (e: Exception) {
            e.printStackTrace()
            sms.status = SmsStatus.ESPERANDO
            smsRepository.save(sms)
        }
    }

    private fun isSocketOk(socket: Socket?): Boolean {
        return socket!=null && !socket.isOutputShutdown && socket.isBound && !socket.isClosed && socket.isConnected
    }

    fun printBuff(DEBUG_MODE: Boolean, vararg buff: Byte) {
        var i = 0
        while (i < buff.size && DEBUG_MODE) {
            System.out.printf("%02x ", buff[i])
            i++
        }
        if (DEBUG_MODE) println()
    }

    fun fromByte(b: Byte): BitSet {
        var b = b
        val bits = BitSet(8)
        for (i in 0..7) {
            bits[i] = b and 1 == 1.toByte()
            b = (b.toInt() shr 1).toByte()
        }
        return bits
    }

    fun printSocketStatus(DEBUG_MODE: Boolean, title: String, socket: Socket?) {
        if (DEBUG_MODE) {
            println(title + " " + if (socket != null) "not null" else "null")
            if (socket != null) {
                println(title + " " + if (socket.isInputShutdown) "input shutdown" else "input ok")
                println(title + " " + if (socket.isOutputShutdown) "output shutdown" else "output ok")
                println(title + " " + if (socket.isBound) "bound" else "not bound")
                println(title + " " + if (socket.isConnected) "connected" else "not conected")
                println(title + " " + if (socket.isClosed) "closed" else "not closed")
                println(title + " " + "Inet Address: " + socket.inetAddress)
            }
        }
    }
}
