package br.com.moveltrack.gateway.theadlocals

object Imei {
    val instance = ThreadLocal<String>()

    var imei: String?
        get() = instance.get()
        set(imei) {
            instance.set(imei)
        }
}
