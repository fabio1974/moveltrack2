package br.com.moveltrack.gateway.controllers

import br.com.moveltrack.gateway.utils.logs
import br.com.moveltrack.persistence.domain.*
import br.com.moveltrack.persistence.repositories.ChipRepository
import br.com.moveltrack.persistence.repositories.EquipamentoRepository
import br.com.moveltrack.persistence.repositories.Location2Repository
import br.com.moveltrack.persistence.repositories.SmsRepository
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.lang.Thread.sleep
import java.time.ZonedDateTime
import java.util.*


@Controller
@EnableScheduling
class MainController(
        val chipRepository: ChipRepository,
        val equipamentoRepository: EquipamentoRepository,
        val smsRepository: SmsRepository,
        val location2Repository: Location2Repository
) {

    @ResponseBody
    @GetMapping("/chips")
    fun get(){
        val list = chipRepository.findAll().forEach {
            println(it.numero)
        }
    }


    @ResponseBody
    @GetMapping("/smsEsperandoDosUltimosMinutos/{minutos}")
    fun smsEsperandoDosUltimosMinutos(@PathVariable("minutos") minutos:Long): Sms?{

        val minutosAtras = ZonedDateTime.now().minusMinutes(minutos)
        val tipos = listOf<SmsTipo>(SmsTipo.BLOQUEIO,SmsTipo.DESBLOQUEIO)
        val sms =  smsRepository.findFirstByStatusAndDataUltimaAtualizacaoAfterAndTipoNotInOrderByDataUltimaAtualizacaoDesc(SmsStatus.ESPERANDO,minutosAtras,tipos)
        if(sms!=null) {
            sms.status = SmsStatus.ENVIADO
            smsRepository.save(sms)
        }
        logs(true,"LENDO SMS DO CELULAR...${sms?.id}")
        return sms
    }


    @PostMapping("/smsRecebido")
    fun smsRecebido(@RequestBody sms: Sms){
         sms.dataUltimaAtualizacao = ZonedDateTime.now()
         smsRepository.save(sms)
    }





    @ResponseBody
    @GetMapping("/redirecionar/{modelo}")
    fun redirecionarModelo(@PathVariable("modelo") modelo: String) {
        val m:ModeloRastreador = ModeloRastreador.valueOf(modelo)
        var mensagem:String?=null
        when(m){
            ModeloRastreador.CRX1-> mensagem="SERVER,1,moveltrack-gps.net,6220,0#"
            ModeloRastreador.CRXN-> mensagem="SERVER,1,moveltrack-gps.net,6223,0#"
            ModeloRastreador.CRX3-> mensagem="SERVER,1,moveltrack-gps.net,6224,0#"
        }
        geraSms(m, mensagem)
    }


    @ResponseBody
    @GetMapping("/testaSms")
    fun testaSms() {
        val list = arrayListOf<ModeloRastreador>(ModeloRastreador.CRX1,ModeloRastreador.CRX3,ModeloRastreador.CRXN)
        list.forEach {
            var mensagem:String?=null
            when(it){
                ModeloRastreador.CRX1-> mensagem="SERVER,1,moveltrack-gps.net,6220,0#"
                ModeloRastreador.CRXN-> mensagem="SERVER,1,moveltrack-gps.net,6223,0#"
                ModeloRastreador.CRX3-> mensagem="SERVER,1,moveltrack-gps.net,6224,0#"
            }
            geraSms(it, mensagem)
        }
    }





/*
    @Scheduled(cron = "0 0 0/1 * * *")
    fun configuraSMS(){

        val list = arrayListOf<ModeloRastreador>(ModeloRastreador.CRX1,ModeloRastreador.CRX3,ModeloRastreador.CRXN)
        list.forEach {
            var mensagem:String?=null
            when(it){
                ModeloRastreador.CRX1-> mensagem="SERVER,1,moveltrack-gps.net,6220,0#"
                ModeloRastreador.CRXN-> mensagem="SERVER,1,moveltrack-gps.net,6223,0#"
                ModeloRastreador.CRX3-> mensagem="SERVER,1,moveltrack-gps.net,6224,0#"
            }
            geraSms(it, mensagem)
        }

    }
*/

    private fun geraSms(m: ModeloRastreador, mensagem: String?) {
        val list: List<Equipamento>? = equipamentoRepository.findAllByModeloAndAtrasoGmtNotAndChipIsNotNull(m, -1)
        var count = 0
        list?.forEach {
            val lastLoc = location2Repository.findFirstByImeiOrderByDateLocationDesc(it.imei)
            if(lastLoc!=null && lastLoc.dateLocation!=null && lastLoc.dateLocation!!.isAfter(ZonedDateTime.now().minusMinutes(5))){
                val celular = "+55${it.chip?.numero?.filter { it.isDigit() }}"
                if(!celular.contains("999999")) {
                    val sms = Sms(celular = celular,
                            mensagem = mensagem,
                            imei = it.imei,
                            tipo = SmsTipo.DNS,
                            status = SmsStatus.ESPERANDO,
                            dataUltimaAtualizacao = ZonedDateTime.now()
                    )
                    smsRepository.save(sms)
                    /*val sms2 = Sms(celular = celular,
                            mensagem = "RESET#",
                            imei = it.imei,
                            tipo = SmsTipo.REINICIAR,
                            status = SmsStatus.ESPERANDO,
                            dataUltimaAtualizacao = ZonedDateTime.now()
                    )
                    smsRepository.save(sms2)*/
                    count++
                }
            }
        }
        logs(true, "INSERINDO LOTE DE SMS PARA TROCA DE DNS Modelo $m===========> size=${count}")
    }

    private fun geraSmsTeste(m: ModeloRastreador, mensagem: String?) {
        val list: List<Equipamento>? = equipamentoRepository.findAllByModeloAndAtrasoGmtNotAndChipIsNotNull(m, -1)
        val list2 = listOf<Equipamento>(list?.get(0)!!,list.get(1),list.get(2))
        list?.forEach {
            val sms = Sms(celular = "+5585997572919",
                    mensagem = mensagem,
                    imei = it.imei,
                    tipo = SmsTipo.DNS,
                    status = SmsStatus.ESPERANDO,
                    dataUltimaAtualizacao = ZonedDateTime.now()
            )
            smsRepository.save(sms)

            val sms2 = Sms(celular = "+5585997572919",
                    mensagem = "RESET#",
                    imei = it.imei,
                    tipo = SmsTipo.REINICIAR,
                    status = SmsStatus.ESPERANDO,
                    dataUltimaAtualizacao = ZonedDateTime.now()
            )
            smsRepository.save(sms2)
        }
        logs(true, "INSERINDO LOTE DE SMS PARA TROCA DE DNS Modelo $m===========> size=${list?.size}")
    }


}

/*fun main(args: Array<String>){
    println("(85) 99757-2919".filter { it.isDigit() })
}*/
