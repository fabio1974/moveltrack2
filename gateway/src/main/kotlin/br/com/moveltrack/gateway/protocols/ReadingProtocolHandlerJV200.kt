package br.com.moveltrack.gateway.protocols

import br.com.moveltrack.gateway.commands.CommandHandlerJV200
import br.com.moveltrack.gateway.commands.CommandHandlerJV200.Companion.buildJV200Command
import br.com.moveltrack.gateway.theadlocals.Imei
import br.com.moveltrack.gateway.theadlocals.Imei.imei
import br.com.moveltrack.gateway.theadlocals.Serial1.serial1
import br.com.moveltrack.gateway.theadlocals.Serial2.serial2
import br.com.moveltrack.gateway.theadlocals.StatusLoc.statusLoc
import br.com.moveltrack.gateway.utils.*
import br.com.moveltrack.gateway.utils.Crc16.isCrcOk
import br.com.moveltrack.gateway.utils.Crc16.setCrc
import br.com.moveltrack.gateway.utils.GerenciaAlarmes.gerenciaAlarme
import br.com.moveltrack.persistence.domain.*
import br.com.moveltrack.persistence.repositories.EquipamentoRepository
import br.com.moveltrack.persistence.repositories.Location2Repository
import br.com.moveltrack.persistence.repositories.LocationRepository
import br.com.moveltrack.persistence.repositories.SmsRepository
import org.springframework.stereotype.Component
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.math.BigInteger
import java.net.Socket
import java.time.LocalDateTime
import java.time.ZonedDateTime
import kotlin.experimental.and


@Component
class ReadingProtocolHandlerJV200(
        val smsRepository: SmsRepository?=null,
        val equipamentoRepository: EquipamentoRepository?=null,
        locationRepository: LocationRepository?=null,
        location2Repository: Location2Repository?=null
): ReadingProtocolHandler(locationRepository, location2Repository) {


    var DEBUG_MODE = Constantes.DEBUG_JV200
    @Throws(IOException::class)
    override fun startReading(socket: Socket?) {
        var buffer = ByteArrayOutputStream()
        var previous = 0
        var current = 0
        var array: ByteArray
        logs(DEBUG_MODE, " - START READING PROTOCOL JV200")
        val `in` = socket!!.getInputStream()
        while (`in`.read().also { current = it } != -1) {
            buffer.write(current)
            if (current == 0x0A && previous == 0x0D && buffer.size() > 3) {
                var len: Int
                array = buffer.toByteArray()
                len = if (array[0] == 0x79.toByte())
                    ((array[2] and 0xff.toByte()).toInt() shl 8) or (array[3] and 0xff.toByte()).toInt() + 6 //Por causa do comando retornado pelo JV200 que tem 2 bytes para o Packet Length ao inv�s de 1
                else
                    (array[2] and 0xFF.toByte()).toInt() + 5
                if (buffer.size() == len || buffer.size() > 255) {
                    transmissionAnalisys(array, len, socket)
                    buffer = ByteArrayOutputStream()
                } else previous = current
            } else {
                previous = current
            }
        }
        logs(DEBUG_MODE, " - STOP READING PROTOCOL JV200")
    }

    private fun transmissionAnalisys(byteArray: ByteArray, len: Int, socket: Socket?) {
        logs(DEBUG_MODE, "...receiving package with size = $len of type")
        when (if (byteArray[0] == 0x79.toByte()) byteArray[4].toInt() else byteArray[3].toInt()) {
            0x01 -> {
                logs(DEBUG_MODE, " LOGIN")
                sendPassword(byteArray, socket)
            }
            0x22 -> {
                logs(DEBUG_MODE, " LOCATION from " + imei + "->")
                val loc = getLocationFromLocationPackage(byteArray)
                saveLocation(loc, ModeloRastreador.JV200)
                logs(DEBUG_MODE, " SAVED LOCATION from $imei  com dateLocation ${loc.dateLocation}")
            }
            0x13 -> {
                logs(DEBUG_MODE, " HEARTBEAT from " + imei + "->")
                heartBeatResponse(byteArray, socket)
            }
            0x21 -> {
                logs(DEBUG_MODE, " COMMAND RESPONSE from " + imei + " - " + socket!!.inetAddress)
                printBuff(DEBUG_MODE, *byteArray)
                readCommand(byteArray)
            }
            0x26 -> {
                logs(DEBUG_MODE, " ALARM from " + imei + "->")
                val alarmLocation = getLocationFromAlarmPackage(byteArray)
                saveLocation(alarmLocation!!, ModeloRastreador.JV200)
                enviaEmailDoAlarme(alarmLocation)
                printBuff(true, *byteArray)
            }
            else -> {
                logs(DEBUG_MODE, " UNKNOW from " + imei + "->")
                printBuff(DEBUG_MODE, *byteArray)
            }
        }
        serial1 = byteArray[byteArray.size - 6]
        serial2 = byteArray[byteArray.size - 5]
        if (imei != null) {
            HashMaps.instance?.setSeriais(imei!!, serial1!!, serial2!!)
            val previousSocket = HashMaps.instance?.getSocketByImei(imei)
            if (previousSocket == null || !previousSocket.isConnected || previousSocket.isClosed) {
                HashMaps.instance?.setSocketInImei(imei!!, socket!!)
            }
        }
        //HashMaps.getInstance().printStatus(DEBUG_MODE);
    }

    fun enviaEmailDoAlarme(loc: Location?) {
        Thread { gerenciaAlarme(loc) }.start()
    }

    private fun sendPassword(login: ByteArray, socket: Socket?) {
        if (isCrcOk(login)) {
            setImei(login, socket)
            var password = byteArrayOf(
                    0x78, 0x78,  //start
                    0x05,  //length 
                    0x01,  //protocol
                    login[16], login[17],  //serial
                    0x00, 0x00,  //crc
                    0x0D, 0x0A //stop
            )
            password = setCrc(password)
            logs(DEBUG_MODE, "...sending password to imei: " + imei + "->")
            printBuff(DEBUG_MODE, *password)
            sendToTerminal(password, socket)
        } else {
            logs(DEBUG_MODE, "...discarding login package from " + imei + ": CRC error!")
        }
    }

    private fun heartBeatResponse(heartBeat: ByteArray, socket: Socket?) {
        if (isCrcOk(heartBeat)) {
            var heartBeatResponse = byteArrayOf(
                    0x78, 0x78,  //start
                    0x05,  //length 
                    0x13,  //protocol
                    heartBeat[9], heartBeat[10],  //serial
                    0x00, 0x00,  //crc
                    0x0D, 0x0A //stop
            )
            heartBeatResponse = setCrc(heartBeatResponse)
            logs(DEBUG_MODE, "...sending heartbeat response to " + imei + "->")
            printBuff(DEBUG_MODE, *heartBeatResponse)
            sendToTerminal(heartBeatResponse, socket)
            statusLoc = getHeartBeatStatus(heartBeat)
            val commandLocation: ByteArray = buildJV200Command(CommandHandlerJV200.COMMAND_LOCATION, serial1, serial2)
            if (isCrcOk(commandLocation)) {
                logs(DEBUG_MODE, "...SENDING COMMAND_LOCATION CRC OK. Sending to " + imei + "->")
                printBuff(DEBUG_MODE, *commandLocation)
                sendToTerminal(commandLocation, socket)
            } else logs(DEBUG_MODE, "...COMMAND_LOCATION BAD CRC to " + imei + "->")
        } else {
            logs(DEBUG_MODE, "...discarding heartBeat package from " + imei + ": CRC error!")
        }
    }

    fun getLocationFromAlarmPackage(alarm: ByteArray): Location? {
        val loc = Location()
        try {

            loc.imei = Imei.imei

            val dateLoc = LocalDateTime.of(2000 + alarm[4],
                    alarm[5].toInt(),
                    alarm[6].toInt(),
                    alarm[7].toInt(),
                    alarm[8].toInt(),
                    alarm[9].toInt(),
                    0).atZone(LOCAL_ZONE)

            loc.dateLocation = dateLoc
            loc.dateLocationInicio = dateLoc
            loc.satelites = (alarm[10] and 0x0F) as Int
            val courseStatus = fromByte(alarm[20])
            val isNorth = courseStatus[2]
            val isWest = courseStatus[3]
            val latitude = byteArrayOf(alarm[11], alarm[12], alarm[13], alarm[14])
            val longitude = byteArrayOf(alarm[15], alarm[16], alarm[17], alarm[18])
            val latitudeBI = BigInteger(latitude)
            val longitudeBI = BigInteger(longitude)
            val latitudeF = (if (isNorth) 1 else -1) * latitudeBI.toDouble() / 1800000
            val longitudeF = (if (isWest) -1 else 1) * longitudeBI.toDouble() / 1800000
            loc.latitude = latitudeF
            loc.longitude = longitudeF
            loc.velocidade = alarm[19].toInt().toDouble()
            if (loc.velocidade <= 3) loc.velocidade = 0.0
            val mcc = byteArrayOf(alarm[23], alarm[24])
            val mccBI = BigInteger(mcc)
            loc.mcc = mccBI.toInt()


            //Dados de Status do Alarme----------------------------------------------------------------------------------------
            //--------------------------------------------------------------------------------------------------------
            val terminalStatus = fromByte(alarm[31]) //terminal Information Content
            //logs(DEBUG_MODE,"...READING  HEARTBEAT STATUS FROM "+Imei.getImei());
            loc.alarm = if (terminalStatus[0]) "1" else "0"
            loc.ignition = if (terminalStatus[1]) "1" else "0"
            loc.battery = if (terminalStatus[2]) "1" else "0"
            val bit3 = terminalStatus[3]
            val bit4 = terminalStatus[4]
            val bit5 = terminalStatus[5]
            if (bit5 && !bit4 && !bit3) //100
                loc.alarmType = "100" //SOS");
            if (!bit5 && bit4 && bit3) //011
                loc.alarmType = "011" //Low Battery");
            if (!bit5 && bit4 && !bit3) //010
                loc.alarmType = "010" //Power Cut");
            if (!bit5 && !bit4 && bit3) //001
                loc.alarmType = "001" //Vibration Alarm");
            if (!bit5 && !bit4 && !bit3) //000
                loc.alarmType = "000" //Normal");
            loc.gps = if (terminalStatus[6]) "On" else "Off"
            loc.bloqueio = if (terminalStatus[7]) "Sim" else "Nao"
            val voltageLevel = alarm[32]
            if (voltageLevel.toInt() == 0x00) loc.volt = "0%" // - No Power");
            else if (voltageLevel.toInt() == 0x01) loc.volt = "<5%" // - Extremely Low Battery");
            else if (voltageLevel.toInt() == 0x02) loc.volt = "<10%" // - Very Low Battery");
            else if (voltageLevel.toInt() == 0x03) loc.volt = "<30%" // - Low Battery");
            else if (voltageLevel.toInt() == 0x04) loc.volt = "~50%" // - Medium");
            else if (voltageLevel.toInt() == 0x05) loc.volt = ">70%" // - High");
            else if (voltageLevel.toInt() == 0x06) loc.volt = ">90%" // - Very high");
            val gsm = alarm[33]
            if (gsm.toInt() == 0x00) loc.gsm = "0" //Sem sinal");// - No signal");
            else if (gsm.toInt() == 0x01) loc.gsm = "1" //Muito fraco");// - Extremely weak signal");
            else if (gsm.toInt() == 0x02) loc.gsm = "2" //Fraco");// - Very weak signal");
            else if (gsm.toInt() == 0x03) loc.gsm = "3" //Bom");// - Good signal");
            else if (gsm.toInt() == 0x04) loc.gsm = "4" //Muito Bom");// - Strong signal");
            val alarmTypeFormer = alarm[34]
            if (alarmTypeFormer.toInt() == 0x04) loc.alarmType = "004" //Fence In
            else if (alarmTypeFormer.toInt() == 0x05) loc.alarmType = "005" //Fence Out
            else if (alarmTypeFormer.toInt() == 0x06) loc.alarmType = "006" //Over-speed
            else if (alarmTypeFormer.toInt() == 0x09) loc.alarmType = "009" //Moving-alarm


            //Fim Status do Alarme----------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------
            logs(DEBUG_MODE, "...ALARM READING END")
        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }
        return loc
    }

    private fun getHeartBeatStatus(heartBeat: ByteArray): Location? {
        val loc = Location()
        try {
            val terminalStatus = fromByte(heartBeat[4]) //terminal Information Content
            logs(DEBUG_MODE, "...READING  HEARTBEAT STATUS FROM " + imei)
            loc.alarm = if (terminalStatus[0]) "1" else "0"
            loc.ignition = if (terminalStatus[1]) "1" else "0"
            loc.battery = if (terminalStatus[2]) "1" else "0"
            val bit3 = terminalStatus[3]
            val bit4 = terminalStatus[4]
            val bit5 = terminalStatus[5]
            if (bit5 && !bit4 && !bit3) //100
                loc.alarmType = "100" //SOS");
            if (!bit5 && bit4 && bit3) //011
                loc.alarmType = "011" //Low Battery");
            if (!bit5 && bit4 && !bit3) //010
                loc.alarmType = "010" //Power Cut");
            if (!bit5 && !bit4 && bit3) //001
                loc.alarmType = "001" //Shock Alarm");
            if (!bit5 && !bit4 && !bit3) //000
                loc.alarmType = "000" //Normal");
            loc.gps = if (terminalStatus[6]) "On" else "Off"
            loc.bloqueio = if (terminalStatus[7]) "Sim" else "Nao"
            val voltageLevel = heartBeat[5]
            if (voltageLevel.toInt() == 0x00) loc.volt = "0%" // - No Power");
            else if (voltageLevel.toInt() == 0x01) loc.volt = "<5%" // - Extremely Low Battery");
            else if (voltageLevel.toInt() == 0x02) loc.volt = "<10%" // - Very Low Battery");
            else if (voltageLevel.toInt() == 0x03) loc.volt = "<30%" // - Low Battery");
            else if (voltageLevel.toInt() == 0x04) loc.volt = "~50%" // - Medium");
            else if (voltageLevel.toInt() == 0x05) loc.volt = ">70%" // - High");
            else if (voltageLevel.toInt() == 0x06) loc.volt = ">90%" // - Very high");
            val gsm = heartBeat[6]
            if (gsm.toInt() == 0x00) loc.gsm = "0" //Sem sinal");// - No signal");
            else if (gsm.toInt() == 0x01) loc.gsm = "1" //Muito fraco");// - Extremely weak signal");
            else if (gsm.toInt() == 0x02) loc.gsm = "2" //Fraco");// - Very weak signal");
            else if (gsm.toInt() == 0x03) loc.gsm = "3" //Bom");// - Good signal");
            else if (gsm.toInt() == 0x04) loc.gsm = "4" //Muito Bom");// - Strong signal");
            logs(DEBUG_MODE, "...HEARTBEAT STATUS END")
        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }
        return loc
    }

    private fun readCommand(message: ByteArray) {
        /*		byte[] message = {0x79, 0x79  , 0x00 , 0x72 , 0x21 , 0x00 , 0x01 ,(byte)0xa9 , 0x63 , 0x01 , 0x43 , 0x75 , 0x72 , 0x72 , 0x65 ,       0x6e , 0x74 ,       0x20 , 0x70 , 0x6f 
		        , 0x73 , 0x69 , 0x74 , 0x69 , 0x6f , 0x6e , 0x21 ,      0x20 , 0x4c , 0x61 , 0x74 , 0x3a , 0x53 , 0x33 , 0x2e ,       0x37 , 0x34 ,       0x32 , 0x38 , 0x37 
		        , 0x38 , 0x2c , 0x4c , 0x6f , 0x6e , 0x3a , 0x57 ,      0x33 , 0x38 , 0x2e , 0x35 , 0x32 , 0x33 , 0x31 , 0x38 ,       0x36 , 0x2c ,       0x43 , 0x6f , 0x75 
		        , 0x72 , 0x73 , 0x65 , 0x3a , 0x32 , 0x38 , 0x30 ,      0x2e , 0x34 , 0x37 , 0x2c , 0x53 , 0x70 , 0x65 , 0x65 ,       0x64 , 0x3a ,       0x30 , 0x2e , 0x30 
		        , 0x30 , 0x4b , 0x6d , 0x2f , 0x68 , 0x2c , 0x44 ,      0x61 , 0x74 , 0x65 , 0x54 , 0x69 , 0x6d , 0x65 , 0x3a ,       0x32 , 0x30 ,       0x31 , 0x35 , 0x2d 
		        , 0x30 , 0x36 , 0x2d , 0x31 , 0x39 , 0x20 , 0x30 ,      0x32 , 0x3a , 0x34 , 0x30 , 0x3a , 0x33 , 0x39 , 0x00 , (byte)0x88 , 0x75 , (byte)0xb3 , 0x0d , 0x0a };
		 */
        if (isCrcOk(message)) {
            val len = ((message[2] and 0xff.toByte()).toInt() shl 8) or (message[3] and 0xff.toByte()).toInt() + 6
            val contentSize = len - 16
            val finalIndex = contentSize + 10
            val content = ByteArray(contentSize)
            for (i in 10 until finalIndex) {
                content[i - 10] = message[i]
            }
            val receivedCommand = String(content)
            receivedCommandAnalisys(receivedCommand)
        }
    }

    private fun receivedCommandAnalisys(receivedCommand: String) {
        logs(DEBUG_MODE, "Conteudo do comando: $receivedCommand")
        if (receivedCommand.contains("Lat:")) {
            val loc = getLocationFromReceivedCommand(receivedCommand)
            saveLocation(loc, ModeloRastreador.JV200)
            logs(DEBUG_MODE, "COMANDO INSERIDO =====>  Conteudo do comando: $receivedCommand")
        } else {

            val e = equipamentoRepository?.findByImei(Imei.imei)
            val numeroCelular = "+55${e?.chip?.numero?.filter { it.isDigit() }}"
            val sms = Sms(
                    celular=numeroCelular,
                    dataUltimaAtualizacao = ZonedDateTime.now(),
                    imei = imei,
                    mensagem = receivedCommand,
                    status = SmsStatus.RECEBIDO,
                    tipo = if (receivedCommand.contains("DYD=Success")) SmsTipo.BLOQUEIO
                    else if (receivedCommand.contains("HFYD=Success")) SmsTipo.DESBLOQUEIO
                    else if (receivedCommand.contains("Already in the state of fuel supply to resume") || receivedCommand.contains("Restore fuel supply: Success!")) SmsTipo.DESBLOQUEIO
                    else if (receivedCommand.contains("Already in the state of fuel supply cut off") || receivedCommand.contains("Cut off the fuel supply: Success!")) SmsTipo.BLOQUEIO
                    else SmsTipo.AVISO
            )
            logs(DEBUG_MODE,"Salvando SMS de BLOQUEIO ou DESBLOQUEIO")
            smsRepository?.save(sms)
            logs(DEBUG_MODE,"SMS de BLOQUEIO ou DESBLOQUEIO Salvo!")
        }
    }


    private fun getLocationFromReceivedCommand(receivedCommnad: String): Location {
        val loc = Location()
        try {
            val gc = GpsCommand()
            val quebra = receivedCommnad.substring(receivedCommnad.indexOf("Lat:")).split(",".toRegex()).toTypedArray()
            val latitude = quebra[0].substring(5).toFloat()
            val longitude = quebra[1].substring(5).toFloat()
            gc.ns = quebra[0].substring(4, 5)
            gc.ew = quebra[1].substring(4, 5)
            gc.datetime = quebra[4].substring("DateTime:".length)
            gc.speed = quebra[3].substring("Speed:".length, quebra[3].indexOf("Km/h"))
            loc.imei = imei
            loc.latitude = if (gc.ns == "N") latitude.toDouble() else -1 * latitude.toDouble()
            loc.longitude = if (gc.ew == "E") longitude.toDouble() else -1 * longitude.toDouble()
            loc.dateLocationInicio = localToZonedDateTime(gc.datetime!!,"yyyy-MM-dd HH:mm:ss")
            loc.dateLocation = loc.dateLocationInicio
            loc.velocidade = gc.speed!!.toDouble()
            if (loc.velocidade <= 3) loc.velocidade = 0.0
            if (statusLoc != null) {
                logs(DEBUG_MODE, "...STORING STATUS from " + imei.toString() + "!")
                loc.satelites = if (lastLocation != null) lastLocation!!.satelites else -1
                loc.mcc = if (lastLocation != null) lastLocation!!.mcc else -1
                loc.alarm = statusLoc!!.alarm
                loc.alarmType = statusLoc!!.alarmType
                loc.battery = statusLoc!!.battery
                loc.bloqueio = statusLoc!!.bloqueio
                loc.gps = statusLoc!!.gps
                loc.gsm = statusLoc!!.gsm
                loc.ignition = statusLoc!!.ignition
                loc.sos = statusLoc!!.sos
                loc.volt = statusLoc!!.volt
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return loc
    }

    private fun getLocationFromLocationPackage(location: ByteArray): Location {
        val loc = Location()
        try {
            if (isCrcOk(location)) {
                logs(DEBUG_MODE, "...getting good JV200 location package from " + imei.toString() + "!")
                loc.imei = Imei.imei
                val dateLoc = LocalDateTime.of(2000 + location[4].toInt(),
                        location[5].toInt(),
                        location[6].toInt(),
                        location[7].toInt(),
                        location[8].toInt(),
                        location[9].toInt(),0
                ).atZone(UTC_ZONE)
                loc.dateLocation = dateLoc
                loc.dateLocationInicio = dateLoc
                loc.satelites = (location[10] and 0x0F).toInt()
                val courseStatus = fromByte(location[20])
                val isNorth = courseStatus[2]
                val isWest = courseStatus[3]
                val latitude = byteArrayOf(location[11], location[12], location[13], location[14])
                val longitude = byteArrayOf(location[15], location[16], location[17], location[18])
                val latitudeBI = BigInteger(latitude)
                val longitudeBI = BigInteger(longitude)
                val latitudeF: Double = (if (isNorth) 1 else -1) * latitudeBI.toDouble() / 1800000
                val longitudeF: Double = (if (isWest) -1 else 1) * longitudeBI.toDouble() / 1800000
                loc.latitude = latitudeF
                loc.longitude = longitudeF
                loc.velocidade = location[19].toInt().toDouble()
                if (loc.velocidade <= 3) loc.velocidade = 0.0
                val mcc = byteArrayOf(location[22], location[23])
                loc.mcc = BigInteger(mcc).toInt()
                val acc = location[30].toInt()
                loc.ignition = if (acc == 0) "0" else "1"
            } else {
                logs(DEBUG_MODE, "...discarding location package from " + imei + ": CRC error!")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return loc
    }

    private fun setImei(login: ByteArray, socket: Socket?) {
        imei = String.format("%1x%02x%02x%02x%02x%02x%02x%02x", login[4], login[5], login[6], login[7], login[8], login[9], login[10], login[11])
        try {
            val ip = socket!!.localAddress.hostName
            val equipamento = equipamentoRepository?.findByImei(Imei.imei)
            equipamento?.atrasoGmt = if (ip=="45.55.45.234") 0 else -1
            equipamentoRepository?.save(equipamento!!)
        }catch (e: Exception){
            logs(true,"Imei SETADO =====>   ${Imei.imei}")
        }
    }



}

