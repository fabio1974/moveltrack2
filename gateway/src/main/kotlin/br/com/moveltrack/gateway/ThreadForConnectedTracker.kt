package br.com.moveltrack.gateway

import br.com.moveltrack.gateway.protocols.ReadingProtocolHandler
import br.com.moveltrack.gateway.utils.Utils
import br.com.moveltrack.gateway.utils.logs
import java.net.InetSocketAddress
import java.net.Socket

class ThreadForConnectedTracker(ph: ReadingProtocolHandler, var socket: Socket?) : Thread() {

    var DEBUG_MODE: Boolean
    var ph: ReadingProtocolHandler

    init {
        DEBUG_MODE = true
        this.ph = ph
    }

    override fun run() {
        try {
            logs(DEBUG_MODE," GOT CLIENT WITH IP:-> ${socket?.getInetAddress()}<- ON SERVER PORT  ${socket?.getPort()} -  ${socket?.getLocalPort()} - ${ph::class::simpleName}");
            socket!!.soTimeout = 10 * 60 * 1000
            //logs(DEBUG_MODE,"HOSTNAME ===> ${socket!!.localAddress.hostName}")
            ph.startReading(socket)
        } catch (e: Exception) {
            if (DEBUG_MODE) e.printStackTrace()
        } finally {
            try {
                logs(DEBUG_MODE," - STOP THREAD FOR SOCKET AT ${socket?.getInetAddress()} Closing socket...");
                if (socket != null) socket!!.close()
            } catch (e: Exception) {
                if (DEBUG_MODE) e.printStackTrace()
            }
        }
    }

}
