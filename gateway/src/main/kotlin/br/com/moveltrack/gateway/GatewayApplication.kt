package br.com.moveltrack.gateway



import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan
import org.springframework.context.ApplicationContext;


@ServletComponentScan
@SpringBootApplication
class GatewayApplication



fun main(args: Array<String>) {
    val applicationContext: ApplicationContext = SpringApplication.run(GatewayApplication::class.java, *args)
    val service = applicationContext.getBean(StartService::class.java)
    service.initService()
}


