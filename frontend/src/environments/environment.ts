export const environment = {
  production: false,
  apiUrl:'http://localhost:8080/api',
  name: 'DESENVOLVIMENTO',
  whitelistedDomains: ['localhost:8080'],
  blacklistedRoutes: ['localhost:4200/login'],
};
