export const environment = {
  production: true,
  //apiUrl:'http://138.197.235.221:8080',
  apiUrl:'/api',
  name: 'em construção...',
  //whitelistedDomains: ["138.197.235.221:8080"],
  whitelistedDomains: [new RegExp('/api')],
  blacklistedRoutes: [new RegExp('/login')],
};
