import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {LoginPayload} from './login/login.component';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';
import {Cliente, Contrato, Perfil, Pessoa, Usuario} from '../shared/model';


@Injectable({
  providedIn: 'root'
})
export class SegurancaService {

  pessoa: Pessoa
  decodedToken: any;

  constructor(private http: HttpClient, private router: Router) {
     var token = localStorage.getItem("token")
    if(token) {
      this.decodedToken = new JwtHelperService().decodeToken(token);
      console.log("decodedToken",this.decodedToken)
    }
  }

  login(loginPayload:LoginPayload):Observable<any>{
    return this.http.post<LoginPayload>(`${environment.apiUrl}/login`,loginPayload)
  }

  goSenha(pessoa: Pessoa) {
    this.pessoa = pessoa
    this.router.navigate(["/senha"])
  }

  goPermissoesUsuario(pessoa: Pessoa) {
    this.pessoa = pessoa
    this.router.navigate(["/permissoes"])
  }

  trocaSenha(usuario:Usuario):Observable<any>{
    return this.http.post<LoginPayload>(`${environment.apiUrl}/trocaSenha`,usuario)
  }


  isTokenExpired(){
    var token = localStorage.getItem("token")
    return new JwtHelperService().isTokenExpired(token)
  }

  logout(){
    localStorage.removeItem("token")
    this.decodedToken=null
    window.location.replace('/')
    //this.router.navigate(['/login']);
  }

  temPermissao(permissao:string){
    return this.decodedToken && this.decodedToken.roles.includes(permissao)
  }

  temUmaDasPermissoes(permissoes:string[]){
    return this.decodedToken && permissoes && this.decodedToken.roles.filter(it=> permissoes.includes(it)).length > 0
  }


  /*pessoaLogadaId(){
    return this.decodedToken.id
  }*/

  pessoaLogada(){
    let pessoa = new Pessoa()
    let usuario = new Usuario()
    usuario.nomeUsuario = this.decodedToken.sub
    usuario.perfil = new Perfil()
    usuario.perfil.tipo = this.decodedToken.perfil
    pessoa.nome = this.decodedToken.nome
    pessoa.cpf = this.decodedToken.cpf
    pessoa.usuario = usuario
    pessoa.id = this.decodedToken.id
    return pessoa
  }



  isLogado() {
    return this.decodedToken && this.decodedToken.sub
  }

  isCliente(){
    return this.decodedToken.perfil.toString().startsWith('CLIENTE')
  }


}
