import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, PRIMARY_OUTLET, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {SegurancaService} from './seguranca.service';
import {savePriviousRoute} from './saveHistoryNavigation';


@Injectable({
  providedIn: 'root'
})
export class PreventRefreshGuard implements CanActivate {

  constructor(private segurancaService: SegurancaService,private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (!this.router.navigated) {
      alert('Os dados não salvos do formulário atual foram perdidos e você será redirecionado para a página anterior...');
      this.router.navigate([localStorage.getItem("previousUrl")]);
      return false;
    }
      savePriviousRoute(state)

    return true;
  }

}
