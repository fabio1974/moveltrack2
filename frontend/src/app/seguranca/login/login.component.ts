import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import {SegurancaService} from '../seguranca.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import {DialogService} from '../../shared/dialog.service';


export class LoginPayload{
  username=null;
  password=null;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent{

  error:boolean = false

  constructor(
    private router: Router,
    public segurancaService: SegurancaService,
    public dialogService: DialogService) {
  }

  username
  password

  login(form: NgForm): void {
    if (form.valid) {

      var payload = new LoginPayload();
      payload.username = form.value.username;
      payload.password = form.value.password;

      this.dialogService.showWaitDialog("efetuando login...")

      this.segurancaService.login(payload).subscribe(
        resp => {

          this.segurancaService.decodedToken = new JwtHelperService().decodeToken(resp.token);
          console.log("decodedToken",this.segurancaService.decodedToken)
          localStorage.setItem('token', resp.token);
          this.dialogService.closeWaitDialog()
          this.router.navigate(['/home']);
        },
        error => {
          this.dialogService.closeWaitDialog()
          this.error = true;
          console.log('error', error);
      });
    }
  }

}
