import { Component, OnInit } from '@angular/core';
import {SegurancaService} from '../seguranca.service';
import {DialogService} from '../../shared/dialog.service';

@Component({
  selector: 'app-senha',
  templateUrl: './senha.component.html',
  styleUrls: ['./senha.component.css']
})
export class SenhaComponent implements OnInit {
  confirmaSenha: string;
  senha: string;

  constructor(public segurancaService:SegurancaService, private dialogService: DialogService) { }

  ngOnInit() {
  }

  submit() {
    if(!this.senha || !this.confirmaSenha){
      this.dialogService.showDialog("Atenção","Digite a senha e a confirmação")
      return
    }
    if(this.senha != this.confirmaSenha){
      this.dialogService.showDialog("Atenção","Senhas não conferem")
      return
    }

    if(this.senha.length < 4 || this.senha.length > 10){
      this.dialogService.showDialog("Atenção","A senha deve ter entre 4 e 10 caracteres.")
      return
    }


    this.dialogService.showWaitDialog("Aguarde...")
    this.segurancaService.pessoa.usuario.senha = this.senha
    this.segurancaService.trocaSenha(this.segurancaService.pessoa.usuario).subscribe(
      resp=>{
        this.dialogService.closeWaitDialog()
        this.dialogService.showDialog("OK","Senhas trocada com sucesso")
      },error => {
        this.dialogService.closeWaitDialog()
        this.dialogService.showDialog("Erro","Erro ao trocar senha. Tente novamente!")
        console.log("Erro troca de senha",error)
      }
    )

  }
}
