import { Component, OnInit } from '@angular/core';
import {Cliente, Perfil, Permissao} from '../../shared/model';
import {ControleService} from '../../controle/controle.service';
import {DialogService} from '../../shared/dialog.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {SegurancaService} from '../seguranca.service';
import {ContratoService} from '../../contrato/contrato.service';

@Component({
  selector: 'app-permissoes',
  templateUrl: './permissoes.component.html',
  styleUrls: ['./permissoes.component.css']
})
export class PermissoesComponent implements OnInit {

  perfis: Perfil[]
  permissoes: Permissao[];
  permissoesDisponiveis: Permissao[] = [];
  permissoesDoUsuario: Permissao[] = [];
  cliente: Cliente;
  permissoesParaRetirar: Permissao[];
  permissoesParaAtribuir: Permissao[];

  constructor(public contratoService: ContratoService,
              private segurancaService: SegurancaService,
              private dialogService: DialogService,
              private http: HttpClient) { }



  ngOnInit() {
    this.cliente = this.segurancaService.pessoa as Cliente
    this.http.get(`${environment.apiUrl}/permissoes`).subscribe(
      resp => {
        this.permissoes = resp as Permissao[]
        this.atualizaPermissoesDoUsuario()
      },
      error=>{
        this.dialogService.showDialog("Error",error)
      }
    );
  }


  atualizaPermissoesDoUsuario() {
    this.clearSelecteds()
    this.permissoesDoUsuario = this.cliente.usuario.permissoes
    this.ordenaPermissoesDoUsuario()
    this.permissoesDisponiveis = this.permissoes.filter(it=> !this.permissoesDoUsuario.includes(it))
  }

  atribuirPermissoes() {
    this.permissoesDoUsuario.push(...this.permissoesParaAtribuir)
    this.ordenaPermissoesDoUsuario()
    this.permissoesDisponiveis = this.permissoes.filter(it2=> !this.permissoesDoUsuario.find(it=>it.id==it2.id))
    this.clearSelecteds()
  }

  retirarPermissoes() {
    this.permissoesDoUsuario = this.permissoesDoUsuario.filter(it=> !this.permissoesParaRetirar.find(it2=> it.id==it2.id))
    this.permissoesDisponiveis = this.permissoes.filter(it2=> !this.permissoesDoUsuario.find(it=>it.id==it2.id))
    this.clearSelecteds()
  }

  ordenaPermissoesDoUsuario(){
    this.permissoesDoUsuario.sort((a, b) => a.descricao < b.descricao ? -1 : (a.descricao > b.descricao ? 1 : 0))
  }

  clearSelecteds(){
    this.permissoesParaRetirar=[];
    this.permissoesParaAtribuir=[];
  }





  submit() {

    this.cliente.usuario.permissoes = this.permissoesDoUsuario
    this.http.post(`${environment.apiUrl}/permissoesUsuario`,this.cliente.usuario).subscribe(
      resp => {
        this.dialogService.showDialog("Ok","Permissões do usuário atualizadas com sucesso!")
      },
      error=>{
        this.dialogService.showDialog("Error",error)
      }
    );

  }

}


