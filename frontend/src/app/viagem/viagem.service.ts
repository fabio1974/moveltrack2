import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Viagem, Contrato, Operacao, Page, Motorista, Veiculo, Cliente, Municipio, ViagemStatusOptions} from '../shared/model';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {MotoristaService} from '../motoristas/motorista.service';
import {VeiculoService} from '../veiculos/veiculo.service';
import {SegurancaService} from '../seguranca/seguranca.service';
import {SearchService} from '../shared/search.service';
import {DialogService} from '../shared/dialog.service';

@Injectable({
  providedIn: 'root'
})
export class ViagemService {

  viagem: Viagem
  operacao
  filtro: Viagem = new Viagem()
  motoristas: Motorista[]
  veiculos: Veiculo[]
  viagemStatusOptions = ViagemStatusOptions
  page:Page<Viagem> = new Page();
  cliente: Cliente

  constructor(private http: HttpClient,
              private router: Router,
              private searchService: SearchService,
              private dialogService: DialogService,
              private motoristaService: MotoristaService,
              private veiculoService: VeiculoService,
              private segurancaService: SegurancaService) {

    //this.loadMotoristas()
   // this.loadVeiculos()
     this.cliente   = this.segurancaService.pessoaLogada() as Cliente

    //this.filtro = new Viagem()
    this.filtro.cliente = this.cliente
    this.page.pageIndex = 1
    this.page.pageSize = 10
    this.listarPagina()


  }

  listarPagina() {
    this.dialogService.showWaitDialog("...processando. Aguarde!")
    this.getViagems(this.page,this.filtro)
      .subscribe(
        resp => {
          this.dialogService.closeWaitDialog()
          this.page.list = ((resp as any).content) as Viagem[];
          this.page.totalRecords = (resp as any).totalElements;

          console.log("this.page",this.page)

        },
        error =>{
          this.dialogService.closeWaitDialog()
          this.dialogService.showDialog(error.statusText,error.message)
        }
      );
  }

  goCreate(){
    this.operacao = Operacao.CREATE
    this.router.navigate(["/criarViagem"])
  }

  goUpdate(viagem){
    this.viagem = viagem
    this.operacao = Operacao.UPDATE
    this.router.navigate(["/criarViagem"])
  }

  goShow(viagem){
    this.viagem = viagem
    this.operacao = Operacao.SHOW
    this.router.navigate(["/criarViagem"])
  }


  goList(){
    this.listarPagina()
    this.router.navigate(["/listarViagens"])
  }

  getViagems(page: Page<Viagem>, filtro: Viagem): Observable<any>  {
    const query = `pageIndex=${page.pageIndex-1}&pageSize=${page.pageSize}&sortField=${page.sortField}&sortDirection=${page.sortDirection}`;
    return this.http.patch(`${environment.apiUrl}/viagems?${query}`,filtro);
  }

  postViagem(viagem) {
    return this.http.post(`${environment.apiUrl}/viagems`,viagem);
  }

  putViagem(viagem) {
    return this.http.put(`${environment.apiUrl}/viagems/${viagem.id}`,viagem);
  }

  delete(viagem) {
    return this.http.delete(`${environment.apiUrl}/viagems/${viagem.id}`)
  }
}
