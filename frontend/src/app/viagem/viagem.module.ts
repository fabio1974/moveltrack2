import { NgModule } from '@angular/core';
import {CommonModule, registerLocaleData} from '@angular/common';
import { ListarViagensComponent } from './listar-viagens/listar-viagens.component';
import { CriarViagensComponent } from './criar-viagens/criar-viagens.component';
import {SharedModule} from '../shared/shared.module';
import {AutoCompleteModule} from 'primeng/primeng';
import {RouterModule} from '@angular/router';

import ptBr from '@angular/common/locales/pt';
import {BsDatepickerModule, TypeaheadModule} from 'ngx-bootstrap';
registerLocaleData(ptBr)


@NgModule({
  declarations: [ListarViagensComponent, CriarViagensComponent],
  imports: [
    CommonModule,
    SharedModule,
    AutoCompleteModule,
    RouterModule,
    //TypeaheadModule,

  ]
})
export class ViagemModule { }
