import { Component, OnInit } from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {SearchService} from '../../shared/search.service';
import {ViagemService} from '../../viagem/viagem.service';
import {DialogService} from '../../shared/dialog.service';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {Cliente, Viagem, Municipio, Operacao, PessoaStatus, Motorista, Page, Veiculo, Contrato, Rota} from '../../shared/model';
import {markControlsAsTouched, Obrigatorio, setAllDisabled, TamanhoMaximo, TamanhoMinimo, ValidateCpf} from '../../shared/formUtils';
import {MotoristaService} from '../../motoristas/motorista.service';
import {VeiculoService} from '../../veiculos/veiculo.service';
import {bsDatepickerConfig} from '../../shared/bsDateTimePickerConfig';

import {Observable, of} from 'rxjs';
import {mergeMap} from 'rxjs/operators';
import {RotasService} from '../../mapas/rotas.service';

@Component({
  selector: 'app-criar-viagens',
  templateUrl: './criar-viagens.component.html',
  styleUrls: ['./criar-viagens.component.css']
})
export class CriarViagensComponent {

  viagem : Viagem




  constructor(private fb: FormBuilder,
              public searchService: SearchService,
              public viagemService: ViagemService,
              private dialogService: DialogService,
              private rotasService: RotasService
  )
  {




    this.viagem = this.viagemService.operacao==Operacao.CREATE? this.buildNewViagem() : this.viagemService.viagem

    this.formGroup.setValue(this.viagem)

    if(this.viagem && this.viagem.cidadeDestino)
      this.selecionaRotas()


    setAllDisabled(this.formGroup,this.viagemService.operacao==Operacao.SHOW)

    this.formGroup.controls.cidadeOrigem.disable()

    //console.log("this.formGroup.controls.status.value",this.formGroup.controls.status.value)

/*
    if(this.formGroup.controls.status.value =='ENCERRADA')
      this.formGroup.controls.chegadaReal.enable()
    else
      this.formGroup.controls.chegadaReal.disable()
*/


    this.formGroup.controls.status.disable()
    this.formGroup.controls.distanciaPercorrida.disable()
  }

  buildNewViagem(){
    let v = new Viagem()
    v.cidadeOrigem = new Municipio()
    v.cidadeOrigem.codigo = 7579
    v.cidadeOrigem.descricao = "CAXIAS"
    v.cidadeOrigem.label = "CAXIAS-MA"
    v.status = 'ABERTA'
    v.partida = new Date()
    v.cliente = this.viagemService.cliente
    return v
  }




  formGroup = this.fb.group({
    cidadeOrigem: [,[Obrigatorio]],
    cidadeDestino: [,[Obrigatorio]],
    veiculo: [,[Obrigatorio]],
    motorista: [,[Obrigatorio]],
    partida:[,[Obrigatorio]],
    chegadaReal:[],
    chegadaPrevista:[,[Obrigatorio]],
    status: [,Obrigatorio],
    qtdCidades:  [,Obrigatorio],
    qtdClientes: [,Obrigatorio],
    valorDaCarga: [,Obrigatorio],
    descricao:[],
    pesoDaCarga: [,],
    distanciaPercorrida:[],
    distanciaHodometro:  [,],
    valorDevolucao: [],
    id:[],
    numeroViagem:[],
    cliente:[this.viagemService.cliente],
    saiuDaCerca:[],
    entrouNaCerca:[],
    rota:[]
  });

  bsConfig = bsDatepickerConfig;
  rotas: Rota[] = [];






  submitForm() {

    if(this.formGroup.invalid) {
      markControlsAsTouched(this.formGroup)
      this.dialogService.showDialog("Atenção","Seu formulário contém erros")
      return
    }

    setAllDisabled(this.formGroup,false)
    let viagem = this.formGroup.value as Viagem

    if(viagem.chegadaReal){
      if (viagem.chegadaReal.getTime() > (new Date()).getTime()){
        this.dialogService.showDialog("Atenção","A chegada real só pode ser preenchida se for uma data passada. Não pode ser uma data futura")
        return
      }
      //Se chegou aqui, a data da Chegada Real foi corretamente preenchida, encerrando-se a viagem
      viagem.status = 'ENCERRADA'
    }else  if(viagem.status == 'ENCERRADA'){
      this.dialogService.showDialog("Atenção","Viagem com status 'ENCERRDA' precisa da data de Chegada Real preenchida")
      return
    }

    if(viagem.partida.getTime() > viagem.chegadaPrevista.getTime()){
      this.dialogService.showDialog("Atenção","A data da partida deve ser antes da data de chegada prevista")
      return
    }

    if(viagem.chegadaReal && viagem.partida.getTime() > viagem.chegadaReal.getTime()){
      this.dialogService.showDialog("Atenção","A data da partida deve ser antes da data de chegada real")
      return
    }





    this.dialogService.showWaitDialog("...aguarde, Processando")
    this.viagemService.postViagem(viagem).subscribe(
      resp=>{
        this.dialogService.closeWaitDialog()
        this.viagemService.listarPagina()
        this.viagemService.goList()
      },error1 => {
        let {error,message} = error1.error
        this.dialogService.closeWaitDialog()
        this.dialogService.showDialog(error,message)
      })
  }

  /*municipiosResult: Municipio[];
  searchMunicipios(event) {
    console.log("event.query",event.query)
    this.searchServices.searchMunicipios(event.query).subscribe(
      resp => {
        this.municipiosResult = (resp as Municipio[]).map(
          m=>{
            m.label = `${m.descricao}-${m.uf.sigla}`
            return m
          })
      },
      error => {
        console.log('error', error);
      }
    );
  }

  veiculosResult
  searchVeiculos(event) {
    this.veiculosResult = this.viagemService.veiculos.filter(v => v.placa.toString().includes(event.query.toUpperCase()))
  }

  motoristasResult
  searchMotoristas(event) {
    this.motoristasResult = this.viagemService.motoristas.filter(v => v.nome.toString().toUpperCase().includes(event.query.toUpperCase()))
  }*/


  selecionaRotas() {

    let filtro = new Rota()
    filtro.destino = this.formGroup.controls.cidadeDestino.value

    if(!filtro.destino)
      return

    let page = new Page<Rota>()
    page.pageSize = 10000
    page.pageIndex = 0


    this.dialogService.showWaitDialog("pesquisando rotas para o destino...")
    this.rotasService.getRotas(page,filtro).subscribe(
      resp =>{
        this.dialogService.closeWaitDialog()
        this.rotas = [null,...resp.content as Rota[]]
        this.rotas.map(rota=> {
          if(rota!=null)
            rota.label = rota.itinerario.map(m=> m.descricao).join("-")
          return rota
        })
      },error=>{
        this.dialogService.closeWaitDialog()
        this.dialogService.showDialog("Erro!",JSON.stringify(error))
      }
    )


  }

  getLabel(r: Rota) {
    if(r!=null)
      return r.itinerario.map(m=>m.descricao).join("-")
    else
      return ""
  }
}
