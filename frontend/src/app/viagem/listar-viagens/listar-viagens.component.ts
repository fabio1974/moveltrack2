import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {ViagemService} from '../../viagem/viagem.service';
import {DialogService} from '../../shared/dialog.service';
import {Router} from '@angular/router';
import {SearchService} from '../../shared/search.service';
import {Viagem, Contrato, Page, Cliente, Municipio, Veiculo} from '../../shared/model';
import {LazyLoadEvent} from 'primeng/api';
import {DespesaFrotaService} from '../../despesa-frota/despesa-frota.service';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {NgForm} from '@angular/forms';
import {AutoComplete} from 'primeng/components/autocomplete/autocomplete';
import {environment} from '../../../environments/environment';
import {MapaDataService} from '../../mapas/mapa-veiculo/mapa-data.service';
import {PageChangedEvent} from 'ngx-bootstrap';

@Component({
  selector: 'app-listar-viagens',
  templateUrl: './listar-viagens.component.html',
  styleUrls: ['./listar-viagens.component.css']
})
export class ListarViagensComponent  {

  constructor(
    public viagemService: ViagemService,
    private dialogService: DialogService,
    private router: Router,
    public searchService: SearchService,
    private despesaFrotaService: DespesaFrotaService,
    private segurancaService: SegurancaService,
    private mapDataService: MapaDataService
  ) {

  }


  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvents(event: KeyboardEvent) {
    console.log('event.key', event.key)
    switch (event.key) {
      case 'Enter':
        event.preventDefault();
        this.filtrar()
        break;
    }
  }


  filtrar() {
    this.viagemService.page.pageIndex = 1
      this.viagemService.listarPagina()
  }


  pageChanged($event: PageChangedEvent) {
    if(this.viagemService.page.pageIndex != $event.page){
      this.viagemService.page.pageIndex = $event.page;
      this.viagemService.listarPagina();
    }
  }


  changePageSize() {
    this.viagemService.page.pageIndex = 1
    this.viagemService.listarPagina()
  }


  delete(viagem) {

    if(confirm(`Deseja realmente delatar a viagem ${viagem.id} ?`)){
      this.dialogService.showWaitDialog("...deletando viagem...")
      this.viagemService.delete(viagem).subscribe(
        resp=>{
          this.dialogService.closeWaitDialog()
          this.viagemService.listarPagina()
        },
        error1 => {
          console.log("ERRO FDP", error1)
          this.dialogService.showDialog("Erro!",error1.error.message)
          this.dialogService.closeWaitDialog()
        }
      )
    };
  }


  viagemAtrasada(viagem: Viagem) {
    if(viagem.chegadaReal && viagem.chegadaReal.getTime() - viagem.chegadaPrevista.getTime() > 86400000)
      return true
    if(!viagem.chegadaReal && (new Date()).getTime() - viagem.chegadaPrevista.getTime() > 4 * 86400000)
      return true
    return false;
  }
}
