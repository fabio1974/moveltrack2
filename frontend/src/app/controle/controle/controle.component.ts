import { Component, OnInit } from '@angular/core';
import {ControleService} from '../controle.service';
import {Perfil, Permissao} from '../../shared/model';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {DialogService} from '../../shared/dialog.service';

@Component({
  selector: 'app-controle',
  templateUrl: './controle.component.html',
  styleUrls: ['./controle.component.css']
})
export class ControleComponent implements OnInit {

  perfis: Perfil[]
  permissoes: Permissao[];
  permissoesDisponiveis: Permissao[] = [];
  permissoesDoPerfil: Permissao[] = [];
  perfil: Perfil;
  permissoesParaRetirar: Permissao[];
  permissoesParaAtribuir: Permissao[];

  constructor(public controleService: ControleService,
              private dialogService: DialogService,
              private http: HttpClient) { }

  ngOnInit() {
    this.loadPerfis()
    this.loadPermissoes()
  }

  atualizaPermissoesDoPerfil() {
    this.clearSelecteds()
    this.permissoesDoPerfil = this.perfil.permissoes
    this.ordenaPermissoesDoPerfil()
    this.permissoesDisponiveis = this.permissoes.filter(it2=> !this.permissoesDoPerfil.find(it=>it.id==it2.id))
  }

  atribuirPermissoes() {
    this.permissoesDoPerfil.push(...this.permissoesParaAtribuir)
    this.ordenaPermissoesDoPerfil()
    this.permissoesDisponiveis = this.permissoes.filter(it2=> !this.permissoesDoPerfil.find(it=>it.id==it2.id))
    this.clearSelecteds()
  }

  retirarPermissoes() {
    this.permissoesDoPerfil = this.permissoesDoPerfil.filter(it=> !this.permissoesParaRetirar.find(it2=> it.id==it2.id))
    this.permissoesDisponiveis = this.permissoes.filter(it2=> !this.permissoesDoPerfil.find(it=>it.id==it2.id))
    this.clearSelecteds()
  }

  ordenaPermissoesDoPerfil(){
    this.permissoesDoPerfil.sort((a, b) => a.descricao < b.descricao ? -1 : (a.descricao > b.descricao ? 1 : 0))
  }

  clearSelecteds(){
    this.permissoesParaRetirar=[];
    this.permissoesParaAtribuir=[];
  }

  private loadPerfis() {
    this.http.get(`${environment.apiUrl}/perfis`).subscribe(
      resp => {
        this.perfis = resp as Perfil[]
      },
      error=>{
        this.dialogService.showDialog("Error",error)
      }
    );
  }

  private loadPermissoes() {
    this.http.get(`${environment.apiUrl}/permissoes`).subscribe(
      resp => {
        this.permissoes = resp as Permissao[]
      },
      error=>{
        this.dialogService.showDialog("Error",error)
      }
    );
  }


  submit() {

    if(!this.perfil){
      this.dialogService.showDialog("Error","Escolha um perfil para alterar")
    }

    this.perfil.permissoes = this.permissoesDoPerfil
    this.http.post(`${environment.apiUrl}/perfil`,this.perfil).subscribe(
      resp => {
        this.dialogService.showDialog("Ok","Perfil atualizado com sucesso!")
      },
      error=>{
        this.dialogService.showDialog("Error",error)
      }
    );

  }


}
