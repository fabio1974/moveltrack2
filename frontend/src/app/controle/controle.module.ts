import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ControleComponent } from './controle/controle.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [ControleComponent],
    imports: [
        CommonModule,
        SharedModule
    ]
})
export class ControleModule { }
