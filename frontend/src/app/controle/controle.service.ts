import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {Perfil, PerfilTipo} from '../shared/model';
import {HttpClient} from '@angular/common/http';
import {DialogService} from '../shared/dialog.service';

@Injectable({
  providedIn: 'root'
})
export class ControleService {

  constructor(private http: HttpClient,
              private dialogService: DialogService) {
    this.loadPerfis()
  }

  perfis: Perfil[]

  private loadPerfis() {
    this.http.get(`${environment.apiUrl}/perfis`).subscribe(
      resp => {
        this.perfis = resp as Perfil[]
      },
      error=>{
        this.dialogService.showDialog("Error",error)
      }
    );
  }


}
