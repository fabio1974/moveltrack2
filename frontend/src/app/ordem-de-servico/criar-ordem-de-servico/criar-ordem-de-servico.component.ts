import { Component, OnInit } from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {SearchService} from '../../shared/search.service';
import {DialogService} from '../../shared/dialog.service';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {Operacao, OrdemDeServico, OrdemDeServicoStatus} from '../../shared/model';
import {markControlsAsTouched, Obrigatorio, setAllDisabled, ValidatePlaca} from '../../shared/formUtils';
import {bsDatepickerConfig} from '../../shared/bsDateTimePickerConfig';
import {OrdemDeServicoService} from '../ordem-de-servico.service';

@Component({
  selector: 'app-criar-ordem-de-servico',
  templateUrl: './criar-ordem-de-servico.component.html',
  styleUrls: ['./criar-ordem-de-servico.component.css']
})
export class CriarOrdemDeServicoComponent implements OnInit {

  ordemDeServico: OrdemDeServico

  constructor(private fb: FormBuilder,
              public searchServices: SearchService,
              public ordemDeServicoService: OrdemDeServicoService,
              private dialogService: DialogService,
              private segurancaService: SegurancaService
  )
  {
    this.ordemDeServico = this.ordemDeServicoService.ordemDeServico || new OrdemDeServico()

    this.formGroup.setValue(this.ordemDeServico)

    setAllDisabled(this.formGroup,true)

    if(this.ordemDeServicoService.operacao==Operacao.CREATE)
      setAllDisabled(this.formGroup,false)
    else if(this.ordemDeServicoService.operacao==Operacao.SHOW)
      setAllDisabled(this.formGroup,true)
    else if(this.ordemDeServicoService.operacao==Operacao.UPDATE){
        setAllDisabled(this.formGroup, false)


    }
  }

  formGroup = this.fb.group({
    id:[],
    dataDoServico: [,Obrigatorio],
    cliente:[,Obrigatorio],
    veiculo:[,Obrigatorio],
    observacao:[,Obrigatorio],
    numero:[],
    operador:[,Obrigatorio],
    servico:[,Obrigatorio],
    valorDoServico:[,Obrigatorio],
    status:[,Obrigatorio]

  });

  OrdemDeServicoStatus = OrdemDeServicoStatus

  ngOnInit() {

  }

  submitForm() {



    if(this.formGroup.invalid) {
      markControlsAsTouched(this.formGroup)
      this.dialogService.showDialog("Atenção","Seu formulário contém erros")
      return
    }

    setAllDisabled(this.formGroup,false)
    let ordemDeServico = this.formGroup.value as OrdemDeServico


    this.dialogService.showWaitDialog("...aguarde, Processando")
    this.ordemDeServicoService.postOrdemDeServico(ordemDeServico).subscribe(
      resp=>{
        this.dialogService.closeWaitDialog()
        this.ordemDeServicoService.goList()
      },error1 => {
        let {error,message} = error1.error
        this.dialogService.closeWaitDialog()
        this.dialogService.showDialog(error,message)
      })
  }


  bsConfig =bsDatepickerConfig;

}
