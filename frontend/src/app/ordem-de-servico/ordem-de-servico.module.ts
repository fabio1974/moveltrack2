import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListarOrdemDeServicoComponent} from './listar-ordem-de-servico/listar-ordem-de-servico.component';
import {CriarOrdemDeServicoComponent} from './criar-ordem-de-servico/criar-ordem-de-servico.component';
import {SharedModule} from '../shared/shared.module';
import {AutoCompleteModule} from 'primeng/primeng';

@NgModule({
  declarations: [ListarOrdemDeServicoComponent, CriarOrdemDeServicoComponent],
  imports: [
    CommonModule,
    SharedModule,
    AutoCompleteModule
  ]
})
export class OrdemDeServicoModule { }
