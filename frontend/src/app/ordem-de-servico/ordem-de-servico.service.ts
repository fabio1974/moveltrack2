import { Injectable } from '@angular/core';
import {OrdemDeServico, Contrato, Operacao, Page, Cliente, OrdemDeServicoTipo, Veiculo} from '../shared/model';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {DialogService} from '../shared/dialog.service';

@Injectable({
  providedIn: 'root'
})
export class OrdemDeServicoService {

  page:Page<OrdemDeServico> = new Page();
  ordemDeServico: OrdemDeServico
  operacao = Operacao.SHOW
  filtro: OrdemDeServico = new OrdemDeServico()
  servicos = Object.keys(OrdemDeServicoTipo)
  clienteVeiculos: Veiculo[];

  constructor(private http: HttpClient,
              private router: Router,
              private dialogService: DialogService) {
    this.listarPagina()
  }

  listarPagina() {
    this.dialogService.showWaitDialog("...processando. Aguarde!")
    this.getOrdemDeServicos(this.page,this.filtro)
      .subscribe(
        resp => {
          this.dialogService.closeWaitDialog()
          this.page.list = ((resp as any).content) as OrdemDeServico[];
          //console.log("list",this.page.list)
          this.page.totalRecords = (resp as any).totalElements;
        },
        error =>{
          this.dialogService.closeWaitDialog()
          this.dialogService.showDialog(error.statusText,error.message)
        }
      );
  }

  veiculosDoCliente: Veiculo[]
  searchVeiculosCliente($event: Cliente, operacao: string = null) {
    this.findVeiculosCliente($event).subscribe(
      resp => {
        this.veiculosDoCliente = resp as Veiculo[]
        if(this.ordemDeServico.veiculo!=null && operacao!=Operacao.CREATE) {
          this.ordemDeServico.veiculo = this.veiculosDoCliente.find(it => it.id == this.ordemDeServico.veiculo.id)
          this.operacao = operacao
          this.router.navigate(["/criarOrdemDeServico"])
        }
      }, error => {
        console.log(error)
      }
    );
  }

  goCreate(cliente: Cliente = null){
    this.operacao = Operacao.CREATE
    this.ordemDeServico = new OrdemDeServico()
    this.ordemDeServico.cliente = cliente
    this.router.navigate(["/criarOrdemDeServico"])
  }

  goUpdate(ordemDeServico){
    this.ordemDeServico = ordemDeServico
    this.searchVeiculosCliente(ordemDeServico.cliente,Operacao.UPDATE)
  }

  goShow(ordemDeServico){
    this.ordemDeServico = ordemDeServico
    this.searchVeiculosCliente(ordemDeServico.cliente, Operacao.SHOW)
  }

  goList(){
    this.listarPagina()
    this.router.navigate(["/listarOrdemDeServico"])
  }

  getOrdemDeServicos(page: Page<OrdemDeServico>, filtro: OrdemDeServico): Observable<any>  {
    const query = `pageIndex=${page.pageIndex-1}&pageSize=${page.pageSize}&sortField=${page.sortField}&sortDirection=${page.sortDirection}`;
    return this.http.patch(`${environment.apiUrl}/ordemDeServicos?${query}`,filtro);
  }

  postOrdemDeServico(ordemDeServico) {
    return this.http.post(`${environment.apiUrl}/ordemDeServicos`,ordemDeServico);
  }

  putOrdemDeServico(ordemDeServico) {
    return this.http.put(`${environment.apiUrl}/ordemDeServicos/${ordemDeServico.id}`,ordemDeServico);
  }

  findVeiculosAtivosCliente(cliente: Cliente):  Observable<any> {
    return this.http.post(`${environment.apiUrl}/findVeiculosAtivosByCliente`,cliente);
  }

  findVeiculosCliente(cliente: Cliente):  Observable<any> {
    return this.http.post(`${environment.apiUrl}/findVeiculosByCliente`,cliente);
  }

}
