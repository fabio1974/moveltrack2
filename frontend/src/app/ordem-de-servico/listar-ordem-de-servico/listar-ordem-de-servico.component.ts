import {Component, HostListener} from '@angular/core';

import {DialogService} from '../../shared/dialog.service';
import {Router} from '@angular/router';
import {SearchService} from '../../shared/search.service';
import {DespesaFrotaService} from '../../despesa-frota/despesa-frota.service';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {AtivoInativo, OrdemDeServicoTipo} from '../../shared/model';
import {OrdemDeServicoService} from '../ordem-de-servico.service';
import {PageChangedEvent} from 'ngx-bootstrap';
import {LancamentoService} from '../../lancamento/lancamento.service';


@Component({
  selector: 'app-listar-ordem-de-servico',
  templateUrl: './listar-ordem-de-servico.component.html',
  styleUrls: ['./listar-ordem-de-servico.component.css'],
})
export class ListarOrdemDeServicoComponent  {

  constructor(
    public ordemDeServicoService: OrdemDeServicoService,
    private dialogService: DialogService,
    private router: Router,
    public searchService: SearchService,
    public segurancaService: SegurancaService,
    public despesaFrotaService: DespesaFrotaService,
    public lancamentoService: LancamentoService
  ) {

  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvents(event: KeyboardEvent) {
    console.log('event.key', event.key)
    switch (event.key) {
      case 'Enter':
        event.preventDefault();
        this.filtrar()
        break;
    }
  }

  filtrar() {
    this.ordemDeServicoService.page.pageIndex=1
    this.ordemDeServicoService.listarPagina()
  }

  changePageSize() {
    this.ordemDeServicoService.page.pageIndex=1
    this.ordemDeServicoService.listarPagina()
  }

  pageChanged($event: PageChangedEvent) {
    if(this.ordemDeServicoService.page.pageIndex != $event.page){
      this.ordemDeServicoService.page.pageIndex = $event.page;
      this.ordemDeServicoService.listarPagina();
    }
  }

  statuses = AtivoInativo;
  OrdemDeServicoTipo = OrdemDeServicoTipo
}
