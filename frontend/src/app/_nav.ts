export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: any;
  title?: boolean;
  children?: any;
  variant?: string;
  attributes?: object;
  divider?: boolean;
  class?: string;
}

export const navItems: NavData[] = [

  {
    name: 'Boletos',
    url: '/listarBoletos',
    icon: 'fa fa-file-invoice-dollar'
  },

  {
    name: 'Contratos',
    url: '/listarContratos',
    icon: 'fa fa-address-card'
  },

  {
    name: 'Motoristas',
    url: '/listarMotoristas',
    icon: 'fa fa-user-slash'
  },


  {
    name: 'Veículos',
    url: '/listarVeiculos',
    icon: 'fa fa-car'
  },

  {
    name: 'Rastreadores',
    url: '/listarEquipamento',
    icon: 'fa fa-satellite-dish'
  },

  {
    name: 'Chips',
    url: '/listarChip',
    icon: 'fa fa-sim-card'
  },

  {
    name: 'Ordem De Serviço',
    url: '/listarOrdemDeServico',
    icon: 'fa fa-edit'
  },

  {
    name: 'Lançamentos',
    url: '/listarLancamento',
    icon: 'fa fa-hand-holding-usd'
  },

  {
    name: 'Relatórios Internos',
    url: '/relatoriosInternos',
    icon: 'fa fa-file-pdf'
  },

  {
    name: 'Empregados',
    url: '/empregadoListar',
    icon: 'fa fa-users'
  },



  {
    name: 'Viagens',
    url: '/listarViagens',
    icon: 'fa fa-truck-moving'
  },

  {
    name: 'Mapa Viagem',
    url: '/mapaViagem',
    icon: 'fa fa-globe-americas'
  },

  {
    name: 'Despesas da Frota',
    url: '/listarDespesaFrota',
    icon: 'fa fa-file-invoice-dollar'
  },

  {
    name: 'Relatórios de Frota',
    url: '/relatorioView',
    icon: 'fa fa-file-pdf'
  },

  {
    name: 'Carnês',
    url: '/listarCarnes',
    icon: 'fa fa-file-alt'
  },

  {
    name: 'Mapa Veículo',
    url: '/mapaVeiculo',
    icon: 'fa fa-globe-americas'
  },

  {
    name: 'Mapa da Frota',
    url: '/mapaFrota',
    icon: 'fa fa-globe-americas'
  },


  {
    name: 'Rotas',
    url: '/rotas',
    icon: 'fa fa-route'
  },

  {
    name: 'Controle',
    url: '/controle',
    icon: 'fa fa-key'
  },


  /*
    {
      name: 'Radares',
      url: '/base',
      icon: 'icon-puzzle',
      /!*
          badge: {
            variant: 'info',
            text: 'NEW'
          },
      *!/
      children: [
        {
          name: 'Listar Radar',
          url: '/radar/list',
          icon: 'icon-puzzle'
        },
        {
          name: 'Novo Radar',
          url: '/radar/create',
          icon: 'icon-puzzle'
        },
        {
          name: 'Cadastrar Marca',
          url: '/base/carousels',
          icon: 'icon-puzzle'
        },
        {
          name: 'Cadastrat Modelo',
          url: '/base/collapses',
          icon: 'icon-puzzle'
        }
      ]
    },*/
  /*,
{
  divider: true
},
{
  name: 'SISCOM',
  url: 'http://coreui.io/angular/',
  icon: 'icon-cloud-download',
  class: 'mt-auto',
  variant: 'success',
  attributes: { target: '_blank', rel: 'noopener' }
}*/
];
