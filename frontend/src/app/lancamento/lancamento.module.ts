import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListarLancamentoComponent } from './listar-lancamento/listar-lancamento.component';
import { CriarLancamentoComponent } from './criar-lancamento/criar-lancamento.component';
import {SharedModule} from '../shared/shared.module';
import {AutoCompleteModule} from 'primeng/primeng';

@NgModule({
  declarations: [ListarLancamentoComponent, CriarLancamentoComponent],
  imports: [
    CommonModule,
    SharedModule,
    AutoCompleteModule
  ]
})
export class LancamentoModule { }
