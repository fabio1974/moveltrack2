import { Component, OnInit } from '@angular/core';
import {Empregado, Lancamento, LancamentoStatus, LancamentoTipo, Operacao, OrdemDeServico} from '../../shared/model';
import {FormBuilder} from '@angular/forms';
import {SearchService} from '../../shared/search.service';
import {OrdemDeServicoService} from '../../ordem-de-servico/ordem-de-servico.service';
import {DialogService} from '../../shared/dialog.service';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {markControlsAsTouched, Obrigatorio, setAllDisabled} from '../../shared/formUtils';
import {bsDatepickerConfig} from '../../shared/bsDateTimePickerConfig';
import {LancamentoService} from '../lancamento.service';

@Component({
  selector: 'app-criar-lancamento',
  templateUrl: './criar-lancamento.component.html',
  styleUrls: ['./criar-lancamento.component.css']
})
export class CriarLancamentoComponent {



  constructor(private fb: FormBuilder,
              public searchServices: SearchService,
              public lancamentoService: LancamentoService,
              private dialogService: DialogService,
              private segurancaService: SegurancaService
  )
  {


    this.formGroup.setValue(lancamentoService.lancamento)

    setAllDisabled(this.formGroup,true)

    if(this.lancamentoService.operacao==Operacao.CREATE || this.lancamentoService.operacao==Operacao.UPDATE) {
      setAllDisabled(this.formGroup, false)
      this.formGroup.controls.ordemDeServico.disable()
      if(this.formGroup.controls.operacao.value!=LancamentoTipo.RECEBIMENTO_DE_CLIENTE){
        this.formGroup.controls.houveBaixa.disable()
        this.formGroup.controls.formaPagamento.disable()
      }

    }

  }

  formGroup = this.fb.group({
    id:[],
    data: [,Obrigatorio],
    valor:[,Obrigatorio],
    operacao:[,Obrigatorio],
    status:[,Obrigatorio],
    houveBaixa:[,],
    formaPagamento:[],
    observacao:[,],
    ordemDeServico:[,],
    solicitante:[,Obrigatorio],
    operador:[,Obrigatorio],
  });




  submitForm() {

    if(this.formGroup.invalid) {
      markControlsAsTouched(this.formGroup)
      this.dialogService.showDialog("Atenção","Seu formulário contém erros")
      return
    }

    if(this.formGroup.controls.operacao.value==LancamentoTipo.RECEBIMENTO_DE_CLIENTE && !this.formGroup.controls.ordemDeServico.value){
      this.dialogService.showDialog("Atenção!","Recebimento só pode ser lançado mediante inclusão da respectiva Ordem de Servico!")
      return
    }

    if(this.formGroup.controls.operacao.value==LancamentoTipo.RECEBIMENTO_DE_CLIENTE && !this.formGroup.controls.formaPagamento.value){
      this.dialogService.showDialog("Atenção!","A forma de recebimento é obrigatória!")
      return
    }

    if(this.formGroup.controls.operacao.value==LancamentoTipo.RECEBIMENTO_DE_CLIENTE && !this.formGroup.controls.houveBaixa.value){
      this.dialogService.showDialog("Atenção!","Você precisa dizer se esse valor recebido foi ou não relacionado com a baixa de boleto")
      return
    }

    setAllDisabled(this.formGroup,false)
    let lancamento = this.formGroup.value as Lancamento


    this.dialogService.showWaitDialog("...aguarde, Processando")
    this.lancamentoService.postLancamento(lancamento).subscribe(
      resp=>{
        this.dialogService.closeWaitDialog()
        this.lancamentoService.goList()
      },error1 => {
        let {error,message} = error1.error
        this.dialogService.closeWaitDialog()
        this.dialogService.showDialog(error,message)
      })
  }


  bsConfig =bsDatepickerConfig;
  solicitanteLabel: string = "Titular";
  operadorLabel: string = "Registrado por...";

  setLabels(event: Event) {
    console.log("FAVUE",event.target["value"])
    let tipo = Object.values(LancamentoTipo).find(it=>it.toString() == event.target["value"])
    switch (tipo) {
      case LancamentoTipo.VALE:{
        this.solicitanteLabel = "Vale solicitado por...";
        this.operadorLabel = "Vale registrado por...";
        this.formGroup.controls.houveBaixa.disable()
        this.formGroup.controls.formaPagamento.disable()
        break;
      }
      case LancamentoTipo.RECEBIMENTO_DE_CLIENTE:{
        this.solicitanteLabel = "Dinheiro recebido por...";
        this.operadorLabel = "Recebimento registrado por...";
        this.formGroup.controls.houveBaixa.enable()
        this.formGroup.controls.formaPagamento.enable()
        break;
      }
      case LancamentoTipo.GASTO_DE_MATERIAL:{
        this.solicitanteLabel = "Gasto realizado por...";
        this.operadorLabel = "Gasto registrado por...";
        this.formGroup.controls.houveBaixa.disable()
        this.formGroup.controls.formaPagamento.disable()
        break;
      }
      case LancamentoTipo.DEVOLUCAO_DE_DINHEIRO:{
        this.solicitanteLabel = "Dinheiro devolvido à Moveltrack por...";
        this.operadorLabel = "Registrado por...";
        this.formGroup.controls.houveBaixa.disable()
        this.formGroup.controls.formaPagamento.disable()
        break;
      }
      default :{
        this.solicitanteLabel = "Titular";
        this.operadorLabel = "Registrado por...";
        this.formGroup.controls.houveBaixa.disable()
        this.formGroup.controls.formaPagamento.disable()
        break;
      }




    }


  }
}
