import {Component, HostListener} from '@angular/core';
import {Router} from '@angular/router';
import {SearchService} from '../../shared/search.service';
import {DespesaFrotaService} from '../../despesa-frota/despesa-frota.service';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {AtivoInativo, LancamentoTipo} from '../../shared/model';
import {LancamentoService} from '../lancamento.service';
import {PageChangedEvent} from 'ngx-bootstrap';

@Component({
  selector: 'app-listar-lancamento',
  templateUrl: './listar-lancamento.component.html',
  styleUrls: ['./listar-lancamento.component.css']
})
export class ListarLancamentoComponent {

  constructor(
    public lancamentoService: LancamentoService,
    private router: Router,
    public searchService: SearchService,
    public segurancaService: SegurancaService,
    public despesaFrotaService: DespesaFrotaService
  ) {
    console.log("OBJECT", Object.values(LancamentoTipo))
  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvents(event: KeyboardEvent) {
    console.log('event.key', event.key)
    switch (event.key) {
      case 'Enter':
        event.preventDefault();
        this.filtrar()
        break;
    }
  }

  filtrar() {
    this.lancamentoService.page.pageIndex = 1
    this.lancamentoService.listarPagina()
  }

  changePageSize() {
    this.lancamentoService.page.pageIndex = 1
    this.lancamentoService.listarPagina()
  }

  pageChanged($event: PageChangedEvent) {
    if (this.lancamentoService.page.pageIndex != $event.page) {
      this.lancamentoService.page.pageIndex = $event.page;
      this.lancamentoService.listarPagina();
    }
  }

  statuses = AtivoInativo;
  LancamentoTipo = LancamentoTipo


}
