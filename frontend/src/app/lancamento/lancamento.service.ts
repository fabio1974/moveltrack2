import {Injectable} from '@angular/core';
import {
  HouveBaixa,
  Lancamento,
  LancamentoFormaPagamento,
  LancamentoStatus,
  LancamentoTipo,
  Operacao, OrdemDeServico,
  OrdemDeServicoTipo,
  Page
} from '../shared/model';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {DialogService} from '../shared/dialog.service';
import {SegurancaService} from '../seguranca/seguranca.service';

@Injectable({
  providedIn: 'root'
})
export class LancamentoService {

  lancamento
  operacao = Operacao.SHOW
  filtro: Lancamento = new Lancamento()
  page:Page<Lancamento> = new Page();
  operacoes = Object.keys(LancamentoTipo)
  formasPagamento = Object.keys(LancamentoFormaPagamento)
  statuses = Object.keys(LancamentoStatus)
  houveBaixas= Object.keys(HouveBaixa)


  constructor(private http: HttpClient,
              private router: Router,
              private segurancaService: SegurancaService,
              private dialogService: DialogService) {
    this.listarPagina()
  }


  listarPagina() {
    this.dialogService.showWaitDialog("...processando. Aguarde!")
    this.getLancamentos(this.page,this.filtro)
      .subscribe(
        resp => {
          this.dialogService.closeWaitDialog()
          this.page.list = ((resp as any).content) as Lancamento[];
          //console.log("list",this.page.list)
          this.page.totalRecords = (resp as any).totalElements;
        },
        error =>{
          this.dialogService.closeWaitDialog()
          this.dialogService.showDialog(error.statusText,error.message)
        }
      );
  }

  goCreate(ordemDeServico: OrdemDeServico = null){
    this.operacao = Operacao.CREATE
    this.lancamento = new Lancamento()
    this.lancamento.operador = this.segurancaService.pessoaLogada()
    if(ordemDeServico){
      this.lancamento.ordemDeServico = ordemDeServico
      this.lancamento.operacao = LancamentoTipo.RECEBIMENTO_DE_CLIENTE
      this.lancamento.solicitante = ordemDeServico.operador
      this.lancamento.data = ordemDeServico.dataDoServico
    }

    this.router.navigate(["/criarLancamento"])
  }

  goUpdate(lancamento){
    this.lancamento = lancamento
    this.operacao = Operacao.UPDATE
    this.router.navigate(["/criarLancamento"])
  }

  goShow(lancamento){
    this.lancamento = lancamento
    this.operacao = Operacao.SHOW
    this.router.navigate(["/criarLancamento"])
  }

  goList(){
    this.listarPagina()
    this.router.navigate(["/listarLancamento"])
  }

  getLancamentos(page: Page<Lancamento>, filtro: Lancamento): Observable<any>  {
    const query = `pageIndex=${page.pageIndex-1}&pageSize=${page.pageSize}&sortField=${page.sortField}&sortDirection=${page.sortDirection}`;
    return this.http.patch(`${environment.apiUrl}/lancamentos?${query}`,filtro);
  }

  postLancamento(lancamento) {
    return this.http.post(`${environment.apiUrl}/lancamentos`,lancamento);
  }

  putLancamento(lancamento) {
    return this.http.put(`${environment.apiUrl}/lancamentos/${lancamento.id}`,lancamento);
  }




}
