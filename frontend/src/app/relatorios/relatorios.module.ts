import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RelatorioViewComponent} from './relatorio-view/relatorio-view.component';
import {SharedModule} from '../shared/shared.module';
import {SegurancaService} from '../seguranca/seguranca.service';
import {HttpClient} from '@angular/common/http';
import {RelatoriosService} from './relatorios.service';
import {DialogService} from '../shared/dialog.service';
import { MotoristaComponent } from './motorista/motorista.component';
import { ViagensComponent } from './viagens/viagens.component';
import {AutoCompleteModule} from 'primeng/primeng';
import {BsDatepickerModule, BsLocaleService} from 'ngx-bootstrap';
import { ViagensEstadoComponent } from './viagens-estado/viagens-estado.component';
import { ExcessoVelocidadeComponent } from './excesso-velocidade/excesso-velocidade.component';
import { RestricaoTrafegoComponent } from './restricao-trafego/restricao-trafego.component';
import { MotoristaCnhComponent } from './motorista-cnh/motorista-cnh.component';
import { DistanciaDiariaComponent } from './distancia-diaria/distancia-diaria.component';
import { ViagensVeiculoComponent } from './viagens-veiculo/viagens-veiculo.component';
//import { DatepickerModule } from 'ngx-bootstrap/datepicker';


declare const require: any;
export const jsPDF = require('jspdf');
require('jspdf-autotable');


@NgModule({
  declarations: [RelatorioViewComponent, MotoristaComponent, ViagensComponent, ViagensEstadoComponent, ExcessoVelocidadeComponent, RestricaoTrafegoComponent, MotoristaCnhComponent, DistanciaDiariaComponent, ViagensVeiculoComponent],
  imports: [
    CommonModule,
    SharedModule,
    AutoCompleteModule,
   // BsDatepickerModule.forRoot(),


  ],
  providers:[
    SegurancaService,
    HttpClient,
    RelatoriosService,
    DialogService,
    BsLocaleService
  ]
})
export class RelatoriosModule { }
