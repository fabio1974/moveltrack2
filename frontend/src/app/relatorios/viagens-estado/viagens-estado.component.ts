import {Component, OnInit} from '@angular/core';
import {DatepickerConfigService} from '../../shared/bsDateTimePickerConfig';
import {Cliente, RelatorioMotoristaPorViagem} from '../../shared/model';
import {RelatoriosService} from '../relatorios.service';
import {ViagemService} from '../../viagem/viagem.service';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {DialogService} from '../../shared/dialog.service';
import {buildCabecalho, fontSize, formatoDecimal0, formatoDecimal1, formatoDecimal2, formatoMoeda, getDoc} from '../relatoriosUtils';
import {dateToStringFormat, FORMAT_DATE_TIME_YY, FORMAT_DATE_YY} from '../../shared/Utils';
import {ExcelService} from '../../shared/excel-service';

@Component({
  selector: 'app-viagens-estado',
  templateUrl: './viagens-estado.component.html',
  styleUrls: ['./viagens-estado.component.css']
})
export class ViagensEstadoComponent implements OnInit {

  inicio: Date
  fim: Date
  cliente: Cliente
  uf: string = 'MA';

 // bsConfig


  constructor(
    private relatoriosService: RelatoriosService,
    private excelService: ExcelService,
    private segurancaService:SegurancaService,
    public datepickerConfigService: DatepickerConfigService,
    private dialogService: DialogService) {
 }

  ngOnInit() {
    this.fim = new Date()
    this.fim.setHours(23,59,59,999)
    this.inicio = new Date()
    this.inicio.setHours(0,0,0,0)
    this.inicio.setMonth(this.inicio.getMonth(),1)
    this.cliente = this.segurancaService.pessoaLogada() as Cliente
  }


  relatorioViagensPorEstado(formato) {
    if(!this.uf) {
      this.dialogService.showDialog("Atenção", "Escolha o estado de destino.")
    }else if(this.inicio.getTime() > this.fim.getTime() || this.fim.getTime() - this.inicio.getTime() > 86400000*31){
      this.dialogService.showDialog("Intervalo inválido", "Escolha um intervalo entre 1 e 31 dias.")
    }
    else{
      this.dialogService.showWaitDialog("...processando. Aguarde!")
      this.relatoriosService.relatorioViagensPorEstado(this.cliente, this.inicio, this.fim, this.uf).subscribe(
        resp => {
          if(formato=='pdf')
            this.geraRelatorioViagensPorEstadoPdf(this.cliente, this.inicio, this.fim, this.uf, resp as RelatorioMotoristaPorViagem[])
          else
            this.geraRelatorioViagensPorEstadoExcel(this.cliente, this.inicio, this.fim, this.uf, resp as RelatorioMotoristaPorViagem[])
          this.dialogService.closeWaitDialog()
        },
        error1 => {
          console.log("Error", error1)
          this.dialogService.closeWaitDialog()
        })
    }
  }






  geraRelatorioViagensPorEstadoPdf(cliente, inicio, fim, uf, rows: RelatorioMotoristaPorViagem[]) {

     let doc = getDoc('landscape','mm','a4')
    let total = rows.pop();
    let list = rows.map(
      row => {
        return [
          row.viagem.cidadeDestino.label,
          row.viagem.veiculo.placa,
          dateToStringFormat(row.viagem.partida,FORMAT_DATE_YY),

          `${(row.diasViagens as number).toLocaleString('pt-BR', formatoDecimal1)} `,
          `${(row.despesaCombustivel.litros as number).toLocaleString('pt-BR', formatoDecimal0)} l`,
          `${(row.viagem.distanciaHodometro as number).toLocaleString('pt-BR', formatoDecimal2)} km`,
          `${(row.viagem.distanciaHodometro as number/row.despesaCombustivel.litros as number).toLocaleString('pt-BR', formatoDecimal2)} km/l`,
          `${(row.despesaCombustivel.valor as number).toLocaleString('pt-BR', formatoMoeda)}`,
          `${(row.despesaEstivas as number).toLocaleString('pt-BR', formatoMoeda)}`,
          `${(row.despesaDiarias as number).toLocaleString('pt-BR', formatoMoeda)}`,
          `${(row.despesaDiversas as number).toLocaleString('pt-BR', formatoMoeda)}`,
          `${(row.despesaDiversas as number + row.despesaDiarias as number + row.despesaEstivas as number + row.despesaCombustivel.valor as number).toLocaleString('pt-BR', formatoMoeda)}`,
          `${(row.viagem.valorDaCarga as number).toLocaleString('pt-BR', formatoMoeda)}`,

        ];
      });
    doc.autoTable({
      startY: buildCabecalho(cliente,doc, 'Relatório das Viagens por Estado',inicio,fim, uf),
      styles: {halign: 'center', fontSize},
      head: [['Destino','Veículo', 'Partida', 'Dias','Litros','Odômetro', 'Média','Combustível','Estivas','Diárias','Outras','Desp.Total','Carga']],
      body: [...list],
      foot: [['Totalização',
        '','',
        `${(total.diasViagens as number).toLocaleString('pt-BR', formatoDecimal1)} `,
        `${(total.despesaCombustivel.litros as number).toLocaleString('pt-BR', formatoDecimal0)} l`,
        `${(total.viagem.distanciaHodometro as number).toLocaleString('pt-BR', formatoDecimal2)} km`,
        `${(total.viagem.distanciaHodometro as number/total.despesaCombustivel.litros as number).toLocaleString('pt-BR', formatoDecimal2)} km/l`,
        `${(total.despesaCombustivel.valor as number).toLocaleString('pt-BR', formatoMoeda)}`,
        `${(total.despesaEstivas as number).toLocaleString('pt-BR', formatoMoeda)}`,
        `${(total.despesaDiarias as number).toLocaleString('pt-BR', formatoMoeda)}`,
        `${(total.despesaDiversas as number).toLocaleString('pt-BR', formatoMoeda)}`,
        `${(total.despesaDiversas as number + total.despesaDiarias as number + total.despesaEstivas as number + total.despesaCombustivel.valor as number).toLocaleString('pt-BR', formatoMoeda)}`,
        `${(total.viagem.valorDaCarga as number).toLocaleString('pt-BR', formatoMoeda)}`,
      ]],
    });
    doc.save('relatorioViagensPorEstado.pdf');
  }

  private geraRelatorioViagensPorEstadoExcel(cliente: Cliente, inicio: Date, fim: Date, uf: string, rows: RelatorioMotoristaPorViagem[]) {
    let data : any = [];
    let total = rows.pop()
    rows.forEach(row=>{
      data.push({

        'Destino':row.viagem.cidadeDestino.label,
        'Veículo': row.viagem.veiculo.placa,
        'Partida': dateToStringFormat(row.viagem.partida,FORMAT_DATE_YY),
        'Dias':`${(row.diasViagens as number).toLocaleString('pt-BR', formatoDecimal1)} `,
        'Litros':`${(row.despesaCombustivel.litros as number).toLocaleString('pt-BR', formatoDecimal0)} l`,
        'Odômetro': `${(row.viagem.distanciaHodometro as number).toLocaleString('pt-BR', formatoDecimal2)} km`,
        'Média':`${(row.viagem.distanciaHodometro as number/row.despesaCombustivel.litros as number).toLocaleString('pt-BR', formatoDecimal2)} km/l`,
        'Combustível':`${(row.despesaCombustivel.valor as number).toLocaleString('pt-BR', formatoMoeda)}`,
        'Estivas': `${(row.despesaEstivas as number).toLocaleString('pt-BR', formatoMoeda)}`,
        'Diárias':`${(row.despesaDiarias as number).toLocaleString('pt-BR', formatoMoeda)}`,
        'Outras':`${(row.despesaDiversas as number).toLocaleString('pt-BR', formatoMoeda)}`,
        'Desp.Total':`${(row.despesaDiversas as number + row.despesaDiarias as number + row.despesaEstivas as number + row.despesaCombustivel.valor as number).toLocaleString('pt-BR', formatoMoeda)}`,
        'Carga': `${(row.viagem.valorDaCarga as number).toLocaleString('pt-BR', formatoMoeda)}`

      })
    })
    data.push({

      'Destino':'Totalização',
      'Veículo': '',
      'Partida': '',
      'Dias':`${(total.diasViagens as number).toLocaleString('pt-BR', formatoDecimal1)} `,
      'Litros':`${(total.despesaCombustivel.litros as number).toLocaleString('pt-BR', formatoDecimal0)} l`,
      'Odômetro': `${(total.viagem.distanciaHodometro as number).toLocaleString('pt-BR', formatoDecimal2)} km`,
      'Média':`${(total.viagem.distanciaHodometro as number/total.despesaCombustivel.litros as number).toLocaleString('pt-BR', formatoDecimal2)} km/l`,
      'Combustível':`${(total.despesaCombustivel.valor as number).toLocaleString('pt-BR', formatoMoeda)}`,
      'Estivas': `${(total.despesaEstivas as number).toLocaleString('pt-BR', formatoMoeda)}`,
      'Diárias':`${(total.despesaDiarias as number).toLocaleString('pt-BR', formatoMoeda)}`,
      'Outras':`${(total.despesaDiversas as number).toLocaleString('pt-BR', formatoMoeda)}`,
      'Desp.Total':`${(total.despesaDiversas as number + total.despesaDiarias as number + total.despesaEstivas as number + total.despesaCombustivel.valor as number).toLocaleString('pt-BR', formatoMoeda)}`,
      'Carga': `${(total.viagem.valorDaCarga as number).toLocaleString('pt-BR', formatoMoeda)}`

    })

    this.excelService.exportAsExcelFile(data, 'relatorioViagensPorEstado');

  }
}
