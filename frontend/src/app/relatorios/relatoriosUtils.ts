import {dateToStringFormat, FORMAT_DATE_TIME_MM} from '../shared/Utils';
import {Cliente} from '../shared/model';
import {logos} from '../shared/logos';
import {formatDate} from '@angular/common';

declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');

export const formatoMoeda = { minimumFractionDigits: 2 , style: 'currency', currency: 'BRL' }
export const formatoDecimal2 = { maximumFractionDigits: 2, minimumFractionDigits:2}
export const formatoDecimal1 = { maximumFractionDigits: 1, minimumFractionDigits:1}
export const formatoDecimal0 = { maximumFractionDigits: 0, minimumFractionDigits:0}
export const fontSize = 8
export const mes = ['','JAN','FEV','MAR','ABR','MAI','JUN','JUL','AGO','SET','OUT','NOV','DEZ']


export function getDoc(position?,unit?,size?){
  return  new jsPDF(position,unit,size)
}

export function buildCabecalho(cliente:Cliente,doc,titulo:string,inicio:Date,fim:Date, nome?:string) {
  let heigh = 30
  let width = 120

  console.log("clienteId",cliente.id)
  let logo =  logos.filter(it => it.clienteId == cliente.id)
  console.log("base64Img",logos)


  if(logo[0])
    doc.addImage(logo[0].base64Img, 'png', 5, 2);
  else
    doc.addImage(logos[0].base64Img, 'png', 5, 2);
  doc.text(titulo, width, 12);

  let periodo = `Período: ${dateToStringFormat(inicio,FORMAT_DATE_TIME_MM)} até ${dateToStringFormat(fim,FORMAT_DATE_TIME_MM)}`

  doc.setFontSize(8)


  if(nome) {
    doc.text(nome, width, 16);
    doc.setFontSize(8)
    doc.text(periodo, width, 20);
  }else{
    doc.setFontSize(8)
    doc.text(periodo, width, 16);
  }


  doc.line(5, heigh, 205, heigh);
  return heigh + 5
}


export function buildCabecalhoExpiracaoCnh(cliente:Cliente,doc,titulo:string,limite:Date) {
  let heigh = 30
  let width = 120

  console.log("clienteId",cliente.id)
  let logo =  logos.filter(it => it.clienteId == cliente.id)
  console.log("base64Img",logos)


  if(logo[0])
    doc.addImage(logo[0].base64Img, 'png', 5, 2);
  else
    doc.addImage(logos[0].base64Img, 'png', 5, 2);
  doc.text(titulo, width, 12);

  let periodo = `CNH's com expiração antes de : ${dateToStringFormat(limite,FORMAT_DATE_TIME_MM)} `

  doc.setFontSize(8)



    doc.setFontSize(8)
    doc.text(periodo, width, 16);



  doc.line(5, heigh, 205, heigh);
  return heigh + 5
}

export function buildCabecalhoFrota(cliente:Cliente,doc,titulo:string) {
  let heigh = 30
  let width = 120

  let chato = cliente.nome.indexOf("WELLFIELD")!=-1

  let logo =  logos.filter(it => it.clienteId == cliente.id)

  if(logo[0])
    doc.addImage(logo[0].base64Img, 'png', 5, 2);
  else
    doc.addImage(logos[0].base64Img, 'png', 5, 2);
  doc.text(titulo, width, 12);

  doc.setFontSize(8)

  doc.setFontSize(8)
  doc.text(`Data: ${formatDate(chato? cliente.dataCadastro : new Date(),"dd/MM/yy HH:mm:ss",'pt-BR')}`, width, 16);

  doc.line(5, heigh, 205, heigh);
  return heigh + 5
}



