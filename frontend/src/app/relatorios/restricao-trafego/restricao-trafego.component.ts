import { Component, OnInit } from '@angular/core';
import {DatepickerConfigService} from '../../shared/bsDateTimePickerConfig';
import {RelatoriosService} from '../relatorios.service';
import {SearchService} from '../../shared/search.service';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {DialogService} from '../../shared/dialog.service';
import {Cliente, LastLocation, RelatorioUsoIndevidoParam, Veiculo} from '../../shared/model';
import {buildCabecalho, fontSize, formatoDecimal0, formatoDecimal1, formatoDecimal2, formatoMoeda, getDoc} from '../relatoriosUtils';
import {formatDate, formatNumber} from '@angular/common';
import {MapaHttpService} from '../../mapas/mapa-http.service';
import {ExcelService} from '../../shared/excel-service';
import {dateToStringFormat, FORMAT_DATE_YY} from '../../shared/Utils';

@Component({
  selector: 'app-restricao-trafego',
  templateUrl: './restricao-trafego.component.html',
  styleUrls: ['./restricao-trafego.component.css']
})
export class RestricaoTrafegoComponent implements OnInit {
  hoursPlaceholder = 'hh';
  minutesPlaceholder = 'mm';
  veiculos: any;
  inicio: any;
  fim: any;
  relatorioUsoIndevidoParam : RelatorioUsoIndevidoParam


  disabled: boolean = true;

  cliente
  orderBy: string = "veiculo";
  veiculo: Veiculo = null;



  constructor(public datepickerConfigService: DatepickerConfigService,
              public relatoriosService: RelatoriosService,
              public searchService: SearchService,
              private excelService: ExcelService,
              private segurancaService:SegurancaService,
              private mapasHttpService: MapaHttpService,
              private dialogService: DialogService) { }

  ngOnInit() {
    var date = new Date();
    this.inicio = new Date(date.getFullYear(), date.getMonth(), 1,0,0,0,0);
    this.fim = new Date(date.getFullYear(),date.getMonth(),date.getDate(),23,59,59,999)


    this.cliente = this.segurancaService.pessoaLogada() as Cliente
    this.relatorioUsoIndevidoParam = new RelatorioUsoIndevidoParam()


    this.relatoriosService.getRelatorioUsoIndevidoParam(this.cliente).subscribe(
      resp=>{
        let list = resp as RelatorioUsoIndevidoParam[]
        if(list.length>0)
          this.relatorioUsoIndevidoParam = list[0]
          this.relatorioUsoIndevidoParam.cliente = this.cliente
          this.relatorioUsoIndevidoParam.inicio = this.inicio
          this.relatorioUsoIndevidoParam.fim = this.fim
          this.relatorioUsoIndevidoParam.orderBy = this.orderBy
          this.relatorioUsoIndevidoParam.veiculo = this.veiculo
      },error=>{
        console.log("Errro trazendo aprametros",error)
      },()=>{
        this.dialogService.closeWaitDialog()
      }
    )
  }

  editarExpediente() {
    this.disabled = false
  }

  salvarExpedientes() {
    this.disabled = true
    this.dialogService.showWaitDialog("salvando novos valores")
    this.relatoriosService.salvaRelatorioUsoIndevidoParam(this.relatorioUsoIndevidoParam).subscribe(
      resp=>{
        console.log("Deu certo",resp)
        this.dialogService.closeWaitDialog()
      },
      error1 => {
        console.log("Error",JSON.stringify(error1))
        this.dialogService.closeWaitDialog()
        this.dialogService.showDialog("Erro",JSON.stringify(error1))
      }
    )
  }


  relatorioUsoIndevido(formato) {
    this.dialogService.showWaitDialog("gerando relatório...")
    this.relatorioUsoIndevidoParam.orderBy = this.orderBy
    this.relatorioUsoIndevidoParam.veiculo = this.veiculo
    this.relatoriosService.getRelatorioUsoIndevido(this.relatorioUsoIndevidoParam).subscribe(
      resp=>{
        this.dialogService.closeWaitDialog()
        if(formato=='pdf')
          this.relatorioUsoIndevidoPdf(this.inicio, this.fim, resp as LastLocation[])
        else
          this.relatorioUsoIndevidoExcel(this.inicio, this.fim, resp as LastLocation[])

      },
      error1 => {
        console.log("Error",JSON.stringify(error1))
        this.dialogService.closeWaitDialog()
        this.dialogService.showDialog("Erro",JSON.stringify(error1))
      }
    )
  }

  relatorioUsoIndevidoPdf(inicio, fim, rows: LastLocation[]) {

    let doc = getDoc('portrait','mm','a4')

    let list1 = rows.map(
      row => {
        return [
          row.placa,
          row.marcaModelo,
          formatNumber(row.latitude,'pt-BR','1.6-6'),
          formatNumber(row.longitude,'pt-BR','1.6-6'),
          formatDate(row.dateLocation,"dd/MM/yy HH:mm:ss",'pt-BR'),
          `${formatNumber(row.velocidade as number,'pt-BR','1.2-2')}km/h`
        ];
      });

    doc.autoTable({
      startY: buildCabecalho(this.cliente,doc, 'Uso Fora do Expediente',this.inicio,this.fim),
      styles: {halign: 'center', fontSize},
      head: [['Placa','Marca/Modelo','Latitude','Longitude','Data/Hora','Velocidade']],
      body: [...list1]
    });
    doc.save('relatorioUsoDeVeiculosForaDoExpediente.pdf');
  }


  relatorioUsoIndevidoExcel(inicio, fim, rows: LastLocation[]) {

    let header = [
      { 'key': "Relatório:", 'value': 'Uso Fora do Expediente'},
      { 'key': "Cliente:", 'value': this.cliente.nome},
      { 'key': "Início:", 'value': formatDate(this.inicio, 'dd/MM/yy HH:mm:ss','pt_BR')},
      { 'key': "Fim:", 'value': formatDate(this.fim, 'dd/MM/yy HH:mm:ss','pt_BR')},
    ]

    let data : any = [];
    rows.forEach(row=>{
      data.push({
        'Placa':row.placa,
        'Marca/Modelo':row.marcaModelo,
        'Latitude':formatNumber(row.latitude,'pt-BR','1.6-6'),
        'Longitude': formatNumber(row.longitude,'pt-BR','1.6-6'),
        'Data/Hora':formatDate(row.dateLocation,"dd/MM/yy HH:mm:ss",'pt-BR'),
        'Velocidade':`${formatNumber(row.velocidade as number,'pt-BR','1.2-2')}km/h`
      })
    })

    this.excelService.exportAsExcelFile(data, 'relatorioUsoDeVeiculosForaDoExpediente',header);
  }






}


