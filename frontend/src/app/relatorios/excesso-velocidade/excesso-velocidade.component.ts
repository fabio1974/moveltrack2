import { Component, OnInit } from '@angular/core';
import {RelatoriosService} from '../relatorios.service';
import {SearchService} from '../../shared/search.service';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {DatepickerConfigService} from '../../shared/bsDateTimePickerConfig';
import {DialogService} from '../../shared/dialog.service';
import {Cliente, LastLocation, Location2, RelatorioMotoristaPorViagem, Veiculo} from '../../shared/model';
import {buildCabecalho, fontSize, formatoDecimal0, formatoDecimal1, formatoDecimal2, formatoMoeda, getDoc} from '../relatoriosUtils';
import {dateToStringFormat, FORMAT_DATE_YY} from '../../shared/Utils';
import * as moment from 'moment';
import {formatDate, formatNumber} from '@angular/common';
import {ExcelService} from '../../shared/excel-service';

@Component({
  selector: 'app-excesso-velocidade',
  templateUrl: './excesso-velocidade.component.html',
  styleUrls: ['./excesso-velocidade.component.css']
})
export class ExcessoVelocidadeComponent implements OnInit {



  inicio: Date
  fim: Date
  cliente: Cliente
  inferior: number = 110
  superior: number = null

  constructor(private relatoriosService: RelatoriosService,
              public searchService: SearchService,
              private excelService: ExcelService,
              private segurancaService:SegurancaService,
              public datepickerConfigService: DatepickerConfigService,
              private dialogService: DialogService) { }

  ngOnInit() {

    this.inicio = new Date(new Date().getTime() - 86400000*3);
    this.inicio.setHours(0,0,0,0)
    this.fim = new Date(new Date().getTime() - 86400000)
    this.fim.setHours(23,59,59,999)
    this.cliente = this.segurancaService.pessoaLogada() as Cliente

  }


  relatorioExcessoDeVelocidade(formato) {
    if(this.superior && this.superior <= this.inferior) {
      this.dialogService.showDialog("Atenção", "O limite de velocidade superior deve ser inexistente ou maior que o inferior.")
    }else if(!this.inicio || !this.fim){
      this.dialogService.showDialog("Data Inválida", "Coloque data de inicio e fim.")
    }else if(this.inicio.getTime() > this.fim.getTime() || this.fim.getTime() - this.inicio.getTime() > 86400000*3){
      this.dialogService.showDialog("Intervalo inválido", "O intervalo deve ser menor que 72 horas.")
    }
    else{
      this.dialogService.showWaitDialog("...processando. Aguarde!")
      this.relatoriosService.relatorioExcessoDeVelocidade(this.cliente, this.inicio, this.fim, this.inferior, this.superior).subscribe(
        resp => {
          if(formato=='pdf')
            this.relatorioExcessoDeVelocidadePdf(this.inicio, this.fim, resp as LastLocation[])
          else
            this.relatorioExcessoDeVelocidadeExcel(this.inicio, this.fim, resp as LastLocation[])
          this.dialogService.closeWaitDialog()
        },
        error1 => {
          console.log("Error1111", error1)
          this.dialogService.closeWaitDialog()
          this.dialogService.showDialog("Erro",error1.message?error1.message:error1.error)
        })
    }
  }


  relatorioExcessoDeVelocidadePdf(inicio, fim, rows: LastLocation[]) {


    let doc = getDoc('portrait','mm','a3')


    let list0 = rows


    let list1 = list0.map(
      row => {
        return [
          row.placa,
          row.marcaModelo,
          row.endereco,
          formatNumber(row.latitude,'pt-BR','1.6-6'),
          formatNumber(row.longitude,'pt-BR','1.6-6'),
          formatDate(row.dateLocation,"dd/MM/yy HH:mm:ss",'pt-BR'),
          `${formatNumber(row.velocidade as number,'pt-BR','1.2-2')}km/h`
        ];
      });

    let nome = `Vel. Inferior: ${this.inferior}km/h`
    if(this.superior)
      nome += ` - Vel. Superior: ${this.superior}km/h`

    doc.autoTable({
      startY: buildCabecalho(this.cliente,doc, 'Espectro de Velocidade',this.inicio,this.fim,nome),
      styles: {halign: 'center', fontSize},
      head: [['Placa','Marca/Modelo','Endereço','Latitude','Longitude','Data/Hora','Velocidade']],
      body: [...list1]
    });
    doc.save('relatorioExcessoDeVelocidade.pdf');
  }


  private relatorioExcessoDeVelocidadeExcel(inicio: Date, fim: Date, rows: LastLocation[]) {

    let header = [
      { 'key': "Relatório:", 'value': 'Espectro de Velocidade'},
      { 'key': "Cliente:", 'value': this.cliente.nome},
      { 'key': "Início:", 'value': formatDate(this.inicio, 'dd/MM/yy HH:mm:ss','pt_BR')},
      { 'key': "Fim:", 'value': formatDate(this.fim, 'dd/MM/yy HH:mm:ss','pt_BR')},
    ]

    let data : any = [];
    rows.forEach(row=>{
      data.push({
        'Placa':row.placa,
        'Marca/Modelo':row.marcaModelo,
        'Endereço':row.endereco,
        'Latitude':formatNumber(row.latitude,'pt-BR','1.6-6'),
        'Longitude': formatNumber(row.longitude,'pt-BR','1.6-6'),
        'Data/Hora':formatDate(row.dateLocation,"dd/MM/yy HH:mm:ss",'pt-BR'),
        'Velocidade':`${formatNumber(row.velocidade as number,'pt-BR','1.2-2')}km/h`
      })
    })

    this.excelService.exportAsExcelFile(data, 'relatorioExcessoDeVelocidade',header);
  }
}
