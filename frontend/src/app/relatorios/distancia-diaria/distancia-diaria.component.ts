import { Component, OnInit } from '@angular/core';
import {DatepickerConfigService} from '../../shared/bsDateTimePickerConfig';
import {Cliente, LastLocation, RelatorioFrota, Veiculo} from '../../shared/model';
import {RelatoriosService} from '../relatorios.service';
import {SearchService} from '../../shared/search.service';
import {ExcelService} from '../../shared/excel-service';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {DialogService} from '../../shared/dialog.service';
import {buildCabecalho, fontSize, getDoc} from '../relatoriosUtils';
import {formatDate, formatNumber} from '@angular/common';

@Component({
  selector: 'app-distancia-diaria',
  templateUrl: './distancia-diaria.component.html',
  styleUrls: ['./distancia-diaria.component.css']
})
export class DistanciaDiariaComponent implements OnInit {



  inicio: Date
  fim: Date
  cliente: Cliente
  veiculo: Veiculo
  orderBy: string = "distancia";


  constructor(public relatoriosService: RelatoriosService,
              public searchService: SearchService,
              private excelService: ExcelService,
              private segurancaService:SegurancaService,
              public datepickerConfigService: DatepickerConfigService,
              private dialogService: DialogService) { }

  ngOnInit() {

    var date = new Date();
    this.inicio = new Date(date.getFullYear(), date.getMonth(), 1,0,0,0,0);
    this.fim = new Date(date.getFullYear(),date.getMonth(),date.getDate(),0,0,0,0)
    this.cliente = this.segurancaService.pessoaLogada() as Cliente

  }


  relatorioDistanciaDiaria(formato) {
    if(!this.inicio || !this.fim){
      this.dialogService.showDialog("Data Inválida", "Coloque data de inicio e fim.")
      return
    }
    else{
      this.dialogService.showWaitDialog("...processando. Aguarde!")
      this.relatoriosService.relatorioDistanciaDiaria(this.cliente, this.veiculo, this.inicio, this.fim, this.orderBy).subscribe(
        resp => {
          if(formato=='pdf')
            this.relatorioDistanciaDiariaPdf(this.inicio, this.fim, resp as RelatorioFrota[])
          else
            this.relatorioDistanciaDiariaExcel(this.inicio, this.fim, resp as RelatorioFrota[])
          this.dialogService.closeWaitDialog()
        },
        error1 => {
          console.log("Error", error1)
          this.dialogService.closeWaitDialog()
        })
    }
  }


  relatorioDistanciaDiariaPdf(inicio, fim, rows: RelatorioFrota[]) {
    let doc = getDoc('landscape','mm','a4')
    let list0 = rows
    let list1 = list0.map(
      row => {
        return [
          row.placa,
          row.marcaModelo,
          `${formatNumber(row.distanciaPercorrida as number,'pt-BR','1.3-3')}km`
        ];
      });
    doc.autoTable({
      startY: buildCabecalho(this.cliente,doc, 'Distâncias Percorridas do Veículo no Período',this.inicio,this.fim),
      styles: {halign: 'center', fontSize},
      head: [['Placa','Marca/Modelo','Distância Percorrida']],
      body: [...list1]
    });
    doc.save('relatorioDistanciaPercorrida.pdf');
  }


  private relatorioDistanciaDiariaExcel(inicio: Date, fim: Date, rows: RelatorioFrota[]) {
    let header = [
      { 'key': "Relatório:", 'value': 'Distâncias Percorridas do Veículo no Período'},
      { 'key': "Cliente:", 'value': this.cliente.nome},
      { 'key': "Início:", 'value': formatDate(this.inicio, 'dd/MM/yy HH:mm:ss','pt_BR')},
      { 'key': "Fim:", 'value': formatDate(this.fim, 'dd/MM/yy HH:mm:ss','pt_BR')},
    ]

    let data : any = [];
    rows.forEach(row=>{
      data.push({
        'Placa':row.placa,
        'Marca/Modelo':row.marcaModelo,
        'Distância Percorrida':`${formatNumber(row.distanciaPercorrida as number,'pt-BR','1.3-3')}km`
      })
    })
    this.excelService.exportAsExcelFile(data, 'relatorioDistanciaPercorrida',header);
  }

}
