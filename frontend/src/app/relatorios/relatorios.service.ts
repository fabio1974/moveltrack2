import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {Cliente, Motorista, RelatorioFiltro, RelatorioUsoIndevidoParam, Veiculo} from '../shared/model';
import {SegurancaService} from '../seguranca/seguranca.service';
import {HttpClient} from '@angular/common/http';
import {MapaHttpService} from '../mapas/mapa-http.service';
import {DialogService} from '../shared/dialog.service';


@Injectable({
  providedIn: 'root'
})
export class RelatoriosService {

  veiculos: any;

  constructor(private segurancaService: SegurancaService,
              private mapasHttpService: MapaHttpService,
              private dialogService: DialogService,
              private http: HttpClient) {

    if(this.segurancaService.isCliente()) {
      this.dialogService.showWaitDialog("Carregando! aguarde...")
      this.mapasHttpService.findVeiculosAtivosCliente(this.segurancaService.pessoaLogada() as Cliente).subscribe(
        resp => {
          this.veiculos = resp as Veiculo[]
          this.dialogService.closeWaitDialog()
        }, error => {
          console.log("Erro carreganmdo veiculos ativos",error)
          this.dialogService.closeWaitDialog()
          this.dialogService.showDialog("Erro",error.message?error.message:error.error)
        }
      );
    }



  }


  relatorioProdutividade(cliente,inicio,fim) {
    let filtro = this.buildFiltro(cliente, inicio, fim);
    return this.http.post(`${environment.apiUrl}/relatorioProdutividadeVeiculo`,filtro);
  }

  relatorioConsumoVeiculo(cliente: Cliente, inicio: Date, fim: Date) {
    let filtro = this.buildFiltro(cliente, inicio, fim);
    return this.http.post(`${environment.apiUrl}/relatorioConsumoVeiculo`,filtro);
  }

  relatorioMotorista(cliente: Cliente, inicio: Date, fim: Date) {
    let filtro = this.buildFiltro(cliente, inicio, fim);
    return this.http.post(`${environment.apiUrl}/relatorioMotorista`,filtro);
  }

  relatorioDestino(cliente: Cliente, inicio: Date, fim: Date) {
    let filtro = this.buildFiltro(cliente, inicio, fim);
    return this.http.post(`${environment.apiUrl}/relatorioDestino`,filtro);
  }

  relatorioEstado(cliente: Cliente, inicio: Date, fim: Date) {
    let filtro = this.buildFiltro(cliente, inicio, fim);
    return this.http.post(`${environment.apiUrl}/relatorioEstado`,filtro);
  }

  relatorioProdutividadeMensal(cliente: Cliente, inicio: Date, fim: Date) {
    let filtro = this.buildFiltro(cliente, inicio, fim);
    return this.http.post(`${environment.apiUrl}/relatorioProdutividadeMensal`,filtro);
  }

  relatorioDespesaMensal(cliente: Cliente, inicio: Date, fim: Date, numeroViagem: number) {
    let filtro = this.buildFiltro(cliente, inicio, fim, numeroViagem);
    return this.http.post(`${environment.apiUrl}/relatorioDespesaMensal`,filtro);
  }

  relatorioViagensDoMotorista(cliente: Cliente, inicio: Date, fim: Date, motorista: Motorista) {
    let filtro = this.buildFiltro(cliente, inicio, fim, null,motorista);
    return this.http.post(`${environment.apiUrl}/relatorioViagensDoMotorista`,filtro);
  }

  relatorioViagensDoVeiculo(inicio: Date, fim: Date, veiculo: Veiculo) {
    let filtro = this.buildFiltro(null, inicio, fim, null,null,null,null,null,veiculo);
    return this.http.post(`${environment.apiUrl}/relatorioViagensDoVeiculo`,filtro);
  }


  getRelatorioUsoIndevidoParam(cliente: Cliente) {
    let filtro = this.buildFiltro(cliente,null,null,null,null);
    return this.http.post(`${environment.apiUrl}/getRelatorioUsoIndevidoParam`,filtro);
  }

  salvaRelatorioUsoIndevidoParam(relatorioUsoIndevidoParam: RelatorioUsoIndevidoParam) {
    return this.http.post(`${environment.apiUrl}/salvaRelatorioUsoIndevidoParam`,relatorioUsoIndevidoParam);
  }

  getRelatorioUsoIndevido(relatorioUsoIndevidoParam: RelatorioUsoIndevidoParam) {
    return this.http.post(`${environment.apiUrl}/getRelatorioUsoIndevido`,relatorioUsoIndevidoParam);
  }



  private buildFiltro(cliente, inicio, fim, numeroViagem?, motorista?, uf?, inferior?, superior?, veiculo?, orderBy?) {
    let filtro = new RelatorioFiltro()
    filtro.cliente = cliente
    filtro.inicio = inicio
    filtro.fim = fim
    filtro.numeroViagem = numeroViagem
    filtro.motorista = motorista
    filtro.uf = uf
    filtro.inferior = inferior
    filtro.superior = superior
    filtro.veiculo = veiculo
    filtro.orderBy = orderBy
    return filtro;
  }


  relatorioViagensPorEstado(cliente: Cliente, inicio: Date, fim: Date, uf: string) {
    let filtro = this.buildFiltro(cliente, inicio, fim, null,null,uf);
    return this.http.post(`${environment.apiUrl}/relatorioViagensPorEstado`,filtro);
  }

  relatorioExcessoDeVelocidade(cliente: Cliente,inicio: Date, fim: Date, inferior: number, superior:number) {
    let filtro = this.buildFiltro(cliente, inicio, fim, null,null,null,inferior,superior);
    return this.http.post(`${environment.apiUrl}/relatorioEspectroVelocidades`,filtro);
  }


  relatorioExpiracaoCnh(cliente: Cliente, limite: Date) {
    cliente.dataNascimento = limite
    return this.http.post(`${environment.apiUrl}/relatorioExpiracaoCnh`,cliente);
  }

  relatorioDistanciaDiaria(cliente: Cliente, veiculo: Veiculo, inicio: Date, fim: Date, orderBy: string) {
    let filtro = this.buildFiltro(cliente, inicio, fim, null,null,null,null,null, veiculo, orderBy);
    return this.http.post(`${environment.apiUrl}/relatorioDistanciaDiaria`,filtro);
  }


}
