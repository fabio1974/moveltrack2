import { Component, OnInit } from '@angular/core';
import {RelatoriosService} from '../relatorios.service';
import {SearchService} from '../../shared/search.service';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {DatepickerConfigService} from '../../shared/bsDateTimePickerConfig';
import {DialogService} from '../../shared/dialog.service';
import {Cliente, LastLocation, Motorista} from '../../shared/model';
import {buildCabecalho, buildCabecalhoExpiracaoCnh, fontSize, getDoc} from '../relatoriosUtils';
import {formatDate, formatNumber} from '@angular/common';
import {ExcelService} from '../../shared/excel-service';
import {dateToStringFormat, FORMAT_DATE_TIME_MM} from '../../shared/Utils';

@Component({
  selector: 'app-motorista-cnh',
  templateUrl: './motorista-cnh.component.html',
  styleUrls: ['./motorista-cnh.component.css']
})
export class MotoristaCnhComponent implements OnInit {

  limite = new Date();


  cliente: Cliente;

  constructor(private relatoriosService: RelatoriosService,
              public searchService: SearchService,
              private segurancaService:SegurancaService,
              private excelService: ExcelService,
              public datepickerConfigService: DatepickerConfigService,
              private dialogService: DialogService) { }



  ngOnInit() {
    this.limite.setMonth(this.limite.getMonth() + 3);
    this.cliente = this.segurancaService.pessoaLogada() as Cliente
  }


  relatorioExpiracaoCnh(formato) {


      this.dialogService.showWaitDialog("...processando. Aguarde!")
      this.relatoriosService.relatorioExpiracaoCnh(this.cliente, this.limite).subscribe(
        resp => {
          if(formato=='pdf')
            this.relatorioExpiracaoCnhPdf(resp as Motorista[])
          else
            this.relatorioExpiracaoCnhExcel(resp as Motorista[])
          this.dialogService.closeWaitDialog()
        },
        error1 => {
          console.log("Error", error1)
          this.dialogService.closeWaitDialog()
        })

  }


  relatorioExpiracaoCnhPdf(rows: Motorista[]) {

    let doc = getDoc('portrait','mm','a4')

    let list0 = rows

    let list1 = list0.map(
      row => {
        return [
          row.nome,
          row.categoriaCnh,
          formatDate(row.validadeCnh,"dd/MM/yy",'pt-BR'),
        ];
      });

    doc.autoTable({
      startY: buildCabecalhoExpiracaoCnh(this.cliente,doc,'Expiração de CNH',this.limite),
      styles: {halign: 'center', fontSize},
      head: [['Nome do Motorista','Categoria da Carteira','Data de Expiração']],
      body: [...list1]
    });
    doc.save('relatorioExpiracaoCNH.pdf');
  }


  private relatorioExpiracaoCnhExcel(rows: Motorista[]) {

    let periodo = `CNH's com expiração antes de : ${dateToStringFormat(this.limite,FORMAT_DATE_TIME_MM)} `

    let header = [
      { 'key': "Relatório:", 'value': periodo},
      { 'key': "Cliente:", 'value': this.cliente.nome},
    ]

    let data : any = [];
    rows.forEach(row=>{
      data.push({
        'Nome do Motorista':row.nome,
        'Categoria da Carteira':row.categoriaCnh,
        'Data de Expiração':formatDate(row.validadeCnh,"dd/MM/yy",'pt-BR')
      })
    })


    this.excelService.exportAsExcelFile(data, 'relatorioExpiracaoCNH',header);

  }
}
