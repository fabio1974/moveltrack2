import { Component, OnInit } from '@angular/core';
import {Cliente, RelatorioMotoristaPorViagem, Veiculo} from '../../shared/model';
import {RelatoriosService} from '../relatorios.service';
import {DatepickerConfigService} from '../../shared/bsDateTimePickerConfig';
import {buildCabecalho, fontSize, formatoDecimal0, formatoDecimal1, formatoDecimal2, formatoMoeda, getDoc} from '../relatoriosUtils';
import {dateToStringFormat, FORMAT_DATE_TIME_YY} from '../../shared/Utils';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {ExcelService} from '../../shared/excel-service';
import {DialogService} from '../../shared/dialog.service';

@Component({
  selector: 'app-viagens-veiculo',
  templateUrl: './viagens-veiculo.component.html',
  styleUrls: ['./viagens-veiculo.component.css']
})
export class ViagensVeiculoComponent implements OnInit {

  veiculo: Veiculo;
  inicio: Date
  fim: Date
  cliente: Cliente

  constructor(public relatoriosService: RelatoriosService,
              private segurancaService:SegurancaService,
              private excelService: ExcelService,
              private dialogService: DialogService,
              public datepickerConfigService: DatepickerConfigService) { }


  ngOnInit() {
    this.fim = new Date()
    this.fim.setHours(23,59,59,999)
    this.inicio = new Date()
    this.inicio.setHours(0,0,0,0)
    this.inicio.setMonth(this.inicio.getMonth()-3,1)
    this.cliente = this.segurancaService.pessoaLogada() as Cliente
  }


  relatorioViagensDoVeiculo(formato) {
    if(!this.veiculo) {
      this.dialogService.showDialog("Atenção", "Escolha um veículo.")
    }else if(this.inicio.getTime() > this.fim.getTime() || this.fim.getTime() - this.inicio.getTime() > 86400000*120){
      this.dialogService.showDialog("Intervalo inválido", "Escolha um intervalo entre 1 e 120 dias.")
    }
    else{
      this.dialogService.showWaitDialog("...processando. Aguarde!")
      this.relatoriosService.relatorioViagensDoVeiculo(this.inicio,this.fim,this.veiculo).subscribe(
        resp=>{
          if(formato=='pdf')
            this.geraRelatorioViagensDoVeiculoPdf(this.cliente,this.inicio,this.fim,this.veiculo,resp as RelatorioMotoristaPorViagem[])
          else
            this.geraRelatorioViagensDoVeiculoExcel(this.cliente,this.inicio,this.fim,this.veiculo,resp as RelatorioMotoristaPorViagem[])
          this.dialogService.closeWaitDialog()
        },
        error1 => {
          console.log("Error",error1)
          this.dialogService.closeWaitDialog()
        })
    }
  }



  geraRelatorioViagensDoVeiculoPdf(cliente, inicio, fim, veiculo: Veiculo, rows: RelatorioMotoristaPorViagem[]) {
    let doc = getDoc('landscape','mm','a4')
    let total = rows.pop();
    let list = rows.map(
      row => {
        return [
          row.viagem.cidadeDestino.label,
          row.viagem.motorista.nome,
          dateToStringFormat(row.viagem.partida,FORMAT_DATE_TIME_YY),
          dateToStringFormat(row.viagem.chegadaReal,FORMAT_DATE_TIME_YY),
          `${(row.diasViagens as number).toLocaleString('pt-BR', formatoDecimal1)} `,
          `${(row.despesaCombustivel.valor as number).toLocaleString('pt-BR', formatoMoeda)}`,
          `${(row.despesaCombustivel.litros as number).toLocaleString('pt-BR', formatoDecimal0)} l`,
          `${(row.viagem.distanciaHodometro as number).toLocaleString('pt-BR', formatoDecimal2)} km`,
          `${(row.viagem.distanciaHodometro as number/row.despesaCombustivel.litros as number).toLocaleString('pt-BR', formatoDecimal2)} km/l`,
          `${(row.despesaCombustivel.valor as number/row.despesaCombustivel.litros as number).toLocaleString('pt-BR', formatoMoeda)}/l`,
         // `${(row.despesaDiarias as number/row.diasViagens as number).toLocaleString('pt-BR', formatoMoeda)}/dia`,
         // `${(row.despesaEstivas as number/row.pesoDaCarga as number).toLocaleString('pt-BR', formatoMoeda)}/kg`,
        ];
      });
    doc.autoTable({
      startY: buildCabecalho(this.cliente, doc, 'Relatório das Viagens do Veículo',inicio,fim, `${veiculo.placa} - ${veiculo.marcaModelo}`),
      styles: {halign: 'center', fontSize},
      head: [['Destino','Motorista', 'Data Partida', 'Data Chegada','Dias', 'Vlr. Combst.','Lit. Combst.','Dist. Odômetro', 'Consumo Méd.','R$/litro']], //, ' Média Diárias', 'Média Estiva'
      body: [...list],
      foot: [['Totalização',
        '','','',
        `${(total.diasViagens as number).toLocaleString('pt-BR', formatoDecimal1)} `,
        `${(total.despesaCombustivel.valor as number).toLocaleString('pt-BR', formatoMoeda)}`,
        `${(total.despesaCombustivel.litros as number).toLocaleString('pt-BR', formatoDecimal0)} l`,
        `${(total.viagem.distanciaHodometro as number).toLocaleString('pt-BR', formatoDecimal2)} km`,
        `${(total.viagem.distanciaHodometro as number/total.despesaCombustivel.litros as number).toLocaleString('pt-BR', formatoDecimal2)} km/l`,
        `${(total.despesaCombustivel.valor as number/total.despesaCombustivel.litros as number).toLocaleString('pt-BR', formatoMoeda)}/l`,
       // `${(total.despesaDiarias as number/ total.diasViagens as number).toLocaleString('pt-BR', formatoMoeda)}/dia`,
       // `${(total.despesaEstivas as number/total.pesoDaCarga as number).toLocaleString('pt-BR', formatoMoeda)}/kg`,
      ]],
    });
    doc.save('relatorioViagensDoVeiculo.pdf');
  }


  geraRelatorioViagensDoVeiculoExcel(cliente, inicio, fim, veiculo:Veiculo, rows: RelatorioMotoristaPorViagem[]) {


    let data : any = [];
    let total = rows.pop()
    rows.forEach(row=>{
      data.push({

        'Destino':row.viagem.cidadeDestino.label,
        'Motorista': row.viagem.motorista.nome,
        'Data Partida': dateToStringFormat(row.viagem.partida,FORMAT_DATE_TIME_YY),
        'Data Chegada':dateToStringFormat(row.viagem.chegadaReal,FORMAT_DATE_TIME_YY),
        'Dias': `${(row.diasViagens as number).toLocaleString('pt-BR', formatoDecimal1)} `,
        'Vlr. Combst.':`${(row.despesaCombustivel.valor as number).toLocaleString('pt-BR', formatoMoeda)}`,
        'Lit. Combst.':`${(row.despesaCombustivel.litros as number).toLocaleString('pt-BR', formatoDecimal0)} l`,
        'Dist. Odômetro': `${(row.viagem.distanciaHodometro as number).toLocaleString('pt-BR', formatoDecimal2)} km`,
        'Consumo Méd.':`${(row.viagem.distanciaHodometro as number/row.despesaCombustivel.litros as number).toLocaleString('pt-BR', formatoDecimal2)} km/l`,
        'R$/litro': `${(row.despesaCombustivel.valor as number/row.despesaCombustivel.litros as number).toLocaleString('pt-BR', formatoMoeda)}/l`,
       // ' Média Diárias': `${(row.despesaDiarias as number/row.diasViagens as number).toLocaleString('pt-BR', formatoMoeda)}/dia`,
       // 'Média Estiva':`${(row.despesaEstivas as number/row.pesoDaCarga as number).toLocaleString('pt-BR', formatoMoeda)}/kg`,

      })
    })
    data.push({

      'Destino':'Totalização',
      'Motorista': '',
      'Data Partida':'',
      'Data Chegada':'',
      'Dias': `${(total.diasViagens as number).toLocaleString('pt-BR', formatoDecimal1)} `,
      'Vlr. Combst.':`${(total.despesaCombustivel.valor as number).toLocaleString('pt-BR', formatoMoeda)}`,
      'Lit. Combst.':`${(total.despesaCombustivel.litros as number).toLocaleString('pt-BR', formatoDecimal0)} l`,
      'Dist. Odômetro': `${(total.viagem.distanciaHodometro as number).toLocaleString('pt-BR', formatoDecimal2)} km`,
      'Consumo Méd.':`${(total.viagem.distanciaHodometro as number/total.despesaCombustivel.litros as number).toLocaleString('pt-BR', formatoDecimal2)} km/l`,
      'R$/litro': `${(total.despesaCombustivel.valor as number/total.despesaCombustivel.litros as number).toLocaleString('pt-BR', formatoMoeda)}/l`,
      //' Média Diárias': `${(total.despesaDiarias as number/ total.diasViagens as number).toLocaleString('pt-BR', formatoMoeda)}/dia`,
      //'Média Estiva':`${(total.despesaEstivas as number/total.pesoDaCarga as number).toLocaleString('pt-BR', formatoMoeda)}/kg`,

    })

    this.excelService.exportAsExcelFile(data, 'relatorioViagensDoVeiculo');


  }





}
