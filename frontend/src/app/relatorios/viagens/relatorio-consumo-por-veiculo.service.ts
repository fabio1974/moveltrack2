import { Injectable } from '@angular/core';
import {ConsumoPorVeiculo} from '../../shared/model';
import {buildCabecalho, fontSize, formatoDecimal1, formatoDecimal2, formatoMoeda, getDoc} from '../relatoriosUtils';
import {ExcelService} from '../../shared/excel-service';

@Injectable({
  providedIn: 'root'
})
export class RelatorioConsumoPorVeiculoService {

  constructor(private excelService: ExcelService) { }

  geraRelatorioConsumoPorVeiculoPdf(cliente,inicio,fim, rows: ConsumoPorVeiculo[]){
    let doc = getDoc('landscape','mm','a4')
    let total = rows.pop()
    let list = rows.map(
      row => {
        return [
          row.placa,
          row.marcaModelo,
          `${(row.kml2 as number).toLocaleString('pt-BR', formatoDecimal2)}km/L`,
          `${(row.kml as number).toLocaleString('pt-BR', formatoDecimal2)}km/L`,
          `${row.distanciaHodometro} km`,
          `${(row.distanciaPercorrida as number).toLocaleString('pt-BR', formatoDecimal2)} km`,
          `${(row.litros as number).toLocaleString('pt-BR', formatoDecimal2)} L`,
          row.qtdViagens,
          `${(row.erro as number * 100).toLocaleString('pt-BR', formatoDecimal2)}%`
        ];
      });
    doc.autoTable({
      startY: buildCabecalho(cliente,doc,"Relatório de Consumo por Veículo",inicio,fim),
      styles: {halign: 'center',fontSize},
      head: [['Placa', 'Veículo', 'Méd. Hodômetro','Méd. Rastr','Dist. Hod.','Dist. Rastr ','Litros','Q. Viagens', 'Erro']],
      body: [...list],
      foot: [['Totalização',
        '',
        `${(total.kml2 as number).toLocaleString('pt-BR', formatoDecimal2)}km/L`,
        `${(total.kml as number).toLocaleString('pt-BR', formatoDecimal2)}km/L`,
        `${(total.distanciaHodometro as number).toLocaleString('pt-BR', formatoDecimal2)} km`,
        `${(total.distanciaPercorrida as number).toLocaleString('pt-BR', formatoDecimal2)} km`,
        `${(total.litros as number).toLocaleString('pt-BR',formatoDecimal2)} L`,
        total.qtdViagens,
        '',//`${(total.erro as number * 100).toLocaleString('pt-BR', formatoLitros)}%`,
      ]],
    });
    doc.save('relatorioConsumoPorVeiculo.pdf');
  }

  geraRelatorioConsumoPorVeiculoExcel(cliente,inicio,fim, rows: ConsumoPorVeiculo[]){
    let data : any = [];
    let total = rows.pop()
    rows.forEach(row=>{
      data.push({

        'Placa': row.placa,
        'Veículo': row.marcaModelo,
        'Méd. Odômetro':`${(row.kml2 as number).toLocaleString('pt-BR', formatoDecimal2)}km/L`,
        'Méd. Rastr':`${(row.kml as number).toLocaleString('pt-BR', formatoDecimal2)}km/L`,
        'Dist. Odômetro':`${row.distanciaHodometro} km`,
        'Dist. Rastr ':`${(row.distanciaPercorrida as number).toLocaleString('pt-BR', formatoDecimal2)} km`,
        'Litros':`${(row.litros as number).toLocaleString('pt-BR', formatoDecimal2)} L`,
        'Q. Viagens':  row.qtdViagens,
        'Erro':`${(row.erro as number * 100).toLocaleString('pt-BR', formatoDecimal2)}%`


      })
    })
    data.push({

      'Placa': 'Totalização',
      'Veículo': total.marcaModelo,
      'Méd. Odômetro':`${(total.kml2 as number).toLocaleString('pt-BR', formatoDecimal2)}km/L`,
      'Méd. Rastr':`${(total.kml as number).toLocaleString('pt-BR', formatoDecimal2)}km/L`,
      'Dist. Odômetro':`${total.distanciaHodometro} km`,
      'Dist. Rastr ':`${(total.distanciaPercorrida as number).toLocaleString('pt-BR', formatoDecimal2)} km`,
      'Litros':`${(total.litros as number).toLocaleString('pt-BR', formatoDecimal2)} L`,
      'Q. Viagens':  total.qtdViagens,
      'Erro':'',

    })

    this.excelService.exportAsExcelFile(data, 'relatorioConsumoPorVeiculo');
  }

}
