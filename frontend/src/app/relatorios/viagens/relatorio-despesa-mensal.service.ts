import {Injectable} from '@angular/core';
import {RelatorioMensalDespesa} from '../../shared/model';
import {buildCabecalho, fontSize, formatoDecimal2, formatoMoeda, getDoc, mes} from '../relatoriosUtils';
import {ExcelService} from '../../shared/excel-service';

@Injectable({
  providedIn: 'root'
})
export class RelatorioDespesaMensalService {

  constructor(private excelService: ExcelService) { }


  geraRelatorioDespesaMensalPdf(cliente,inicio,fim, rows: RelatorioMensalDespesa[]){
    let doc = getDoc('landscape','mm','a4')
    let porcentagem = rows.pop()
    let total = rows.pop()
    console.log("ROWS",rows)
    let list = rows.map(
      row => {
        return [
          `${row.ano}-${mes[row.mes]}`,
          `${(row.carga as number).toLocaleString('pt-BR', formatoMoeda)}`,
          `${(row.combustivel as number).toLocaleString('pt-BR', formatoMoeda)}`,
          `${(row.manutencao as number).toLocaleString('pt-BR', formatoMoeda)}`,
          `${((row.estivas) as number).toLocaleString('pt-BR', formatoMoeda)}`,
          `${((row.diarias) as number).toLocaleString('pt-BR', formatoMoeda)}`,
          `${((row.ipva) as number).toLocaleString('pt-BR', formatoMoeda)}`,
          `${((row.transito) as number).toLocaleString('pt-BR', formatoMoeda)}`,
          `${((row.trabalhistas) as number).toLocaleString('pt-BR', formatoMoeda)}`,
          `${(row.outras  as number).toLocaleString('pt-BR', formatoMoeda)}`,
          `${(row.despesa  as number).toLocaleString('pt-BR', formatoMoeda)}`,
        ];
      });
    doc.autoTable({
      startY: buildCabecalho(cliente,doc,"Despesas Totais Mensais",inicio,fim),
      styles: {halign: 'center',fontSize},
      head: [['Mês','Carga Transportada','Combustível','Manutenções','Estivas','Diárias','IPVA','Taxas/Multas de Trânsito', 'Encargos Trabalhitas','Outras Despesas','Totais']],
      body: [...list],
      foot: [['Totalização',
        `${(total.carga as number).toLocaleString('pt-BR', formatoMoeda)}`,
        `${(total.combustivel as number).toLocaleString('pt-BR', formatoMoeda)}`,
        `${(total.manutencao as number).toLocaleString('pt-BR', formatoMoeda)}`,
        `${((total.estivas) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        `${((total.diarias) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        `${((total.ipva) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        `${((total.transito) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        `${((total.trabalhistas) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        `${(total.outras  as number).toLocaleString('pt-BR', formatoMoeda)}`,
        `${(total.despesa  as number).toLocaleString('pt-BR', formatoMoeda)}`,
      ],
        ['Porcentagem',
          `${(porcentagem.carga as number).toLocaleString('pt-BR', formatoDecimal2)}%`,
          `${(porcentagem.combustivel as number).toLocaleString('pt-BR', formatoDecimal2)}%`,
          `${(porcentagem.manutencao as number).toLocaleString('pt-BR', formatoDecimal2)}%`,
          `${((porcentagem.estivas) as number).toLocaleString('pt-BR', formatoDecimal2)}%`,
          `${((porcentagem.diarias) as number).toLocaleString('pt-BR', formatoDecimal2)}%`,
          `${((porcentagem.ipva) as number).toLocaleString('pt-BR', formatoDecimal2)}%`,
          `${((porcentagem.transito) as number).toLocaleString('pt-BR', formatoDecimal2)}%`,
          `${((porcentagem.trabalhistas) as number).toLocaleString('pt-BR', formatoDecimal2)}%`,
          `${(porcentagem.outras  as number).toLocaleString('pt-BR', formatoDecimal2)}%`,
          `${(porcentagem.despesa  as number).toLocaleString('pt-BR', formatoDecimal2)}%`,
        ]
      ],
    });
    doc.save('RelatorioDespesaMensal.pdf');
  }


  geraRelatorioDespesaMensalExcell(cliente,inicio,fim, rows: RelatorioMensalDespesa[]) {
    let data : any = [];
    let porcentagem = rows.pop()
    let total = rows.pop()
    rows.forEach(row=>{
      data.push({
        'Mês':`${row.ano}-${mes[row.mes]}`,
        'Carga Transportada':`${(row.carga as number).toLocaleString('pt-BR', formatoMoeda)}`,
        'Combustível':`${(row.combustivel as number).toLocaleString('pt-BR', formatoMoeda)}`,
        'Manutenções':`${(row.manutencao as number).toLocaleString('pt-BR', formatoMoeda)}`,
        'Estivas':`${((row.estivas) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        'Diárias':`${((row.diarias) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        'IPVA':`${((row.ipva) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        'Taxas/Multas de Trânsito':`${((row.transito) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        'Encargos Trabalhitas':`${((row.trabalhistas) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        'Outras Despesas':`${(row.outras  as number).toLocaleString('pt-BR', formatoMoeda)}`,
        'Totais':`${(row.despesa  as number).toLocaleString('pt-BR', formatoMoeda)}`
      })
    })
    data.push({
      'Mês':`Total`,
      'Carga Transportada':`${(total.carga as number).toLocaleString('pt-BR', formatoMoeda)}`,
      'Combustível':`${(total.combustivel as number).toLocaleString('pt-BR', formatoMoeda)}`,
      'Manutenções':`${(total.manutencao as number).toLocaleString('pt-BR', formatoMoeda)}`,
      'Estivas':`${((total.estivas) as number).toLocaleString('pt-BR', formatoMoeda)}`,
      'Diárias':`${((total.diarias) as number).toLocaleString('pt-BR', formatoMoeda)}`,
      'IPVA':`${((total.ipva) as number).toLocaleString('pt-BR', formatoMoeda)}`,
      'Taxas/Multas de Trânsito':`${((total.transito) as number).toLocaleString('pt-BR', formatoMoeda)}`,
      'Encargos Trabalhitas':`${((total.trabalhistas) as number).toLocaleString('pt-BR', formatoMoeda)}`,
      'Outras Despesas':`${(total.outras  as number).toLocaleString('pt-BR', formatoMoeda)}`,
      'Totais':`${(total.despesa  as number).toLocaleString('pt-BR', formatoMoeda)}`
    })

    data.push({
      'Mês':`Percentagem`,
      'Carga Transportada':`${(porcentagem.carga as number).toLocaleString('pt-BR', formatoMoeda)}%`,
      'Combustível':`${(porcentagem.combustivel as number).toLocaleString('pt-BR', formatoMoeda)}%`,
      'Manutenções':`${(porcentagem.manutencao as number).toLocaleString('pt-BR', formatoMoeda)}%`,
      'Estivas':`${((porcentagem.estivas) as number).toLocaleString('pt-BR', formatoMoeda)}%`,
      'Diárias':`${((porcentagem.diarias) as number).toLocaleString('pt-BR', formatoMoeda)}%`,
      'IPVA':`${((porcentagem.ipva) as number).toLocaleString('pt-BR', formatoMoeda)}%`,
      'Taxas/Multas de Trânsito':`${((porcentagem.transito) as number).toLocaleString('pt-BR', formatoMoeda)}%`,
      'Encargos Trabalhitas':`${((porcentagem.trabalhistas) as number).toLocaleString('pt-BR', formatoMoeda)}%`,
      'Outras Despesas':`${(porcentagem.outras  as number).toLocaleString('pt-BR', formatoMoeda)}%`,
      'Totais':`${(porcentagem.despesa  as number).toLocaleString('pt-BR', formatoMoeda)}%`
    })

    this.excelService.exportAsExcelFile(data, 'RelatorioDespesaMensal');
  }



}
