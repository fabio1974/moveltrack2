import { Injectable } from '@angular/core';
import {RelatorioFrota} from '../../shared/model';
import {buildCabecalho, fontSize, formatoDecimal1, formatoMoeda, getDoc} from '../relatoriosUtils';
import {ExcelService} from '../../shared/excel-service';

@Injectable({
  providedIn: 'root'
})
export class RelatorioProdutividadePorDestinoService {

  constructor(private excelService: ExcelService) { }

  geraRelatorioDestinoPdf(cliente,inicio,fim, rows: RelatorioFrota[]){
    let doc = getDoc('landscape','mm','a4')
    let total = rows.pop()
    let list = rows.map(
      row => {
        return [
          `${row.estado}-${row.destino}`,
          `${(row.qtdViagens)}`,
          `${(row.diasViagens as number).toLocaleString('pt-BR', formatoDecimal1)}`,
          `${((row.diasViagens/row.qtdViagens) as number).toLocaleString('pt-BR', formatoDecimal1)}`,
          `${(row.qtdCidades)}`,
          `${((row.qtdCidades/row.qtdViagens) as number).toLocaleString('pt-BR', formatoDecimal1)}`,
          `${((row.valorDaCarga) as number).toLocaleString('pt-BR', formatoMoeda)}`,
          `${((row.despesaCombustivel) as number).toLocaleString('pt-BR', formatoMoeda)}`,
          `${((row.valorDevolucao) as number).toLocaleString('pt-BR', formatoMoeda)}`,
          `${((row.valorDaCarga - row.despesaCombustivel -row.valorDevolucao) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        ];
      });
    doc.autoTable({
      startY: buildCabecalho(cliente,doc,"Relatório Produtividade p/ Destino",inicio,fim),
      styles: {halign: 'center',fontSize},
      head: [['Destino','Qtde. Viagens','Dias Viag.','Dias/Viagem','Qtd. Cidades','Cidades/Viagem','Valor da Carga','Despesas','Devolução','Carga Líq']],
      body: [...list],
      foot: [['Totalização',
        `${(total.qtdViagens)}`,
        `${(total.diasViagens as number).toLocaleString('pt-BR', formatoDecimal1)}`,
        `${((total.diasViagens/total.qtdViagens) as number).toLocaleString('pt-BR', formatoDecimal1)}`,
        `${(total.qtdCidades)}`,
        `${((total.qtdCidades/total.qtdViagens) as number).toLocaleString('pt-BR', formatoDecimal1)}`,
        `${((total.valorDaCarga) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        `${((total.despesaCombustivel) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        `${((total.valorDevolucao) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        `${((total.valorDaCarga - total.despesaCombustivel - total.valorDevolucao) as number).toLocaleString('pt-BR', formatoMoeda)}`,
      ]],
    });
    doc.save('relatorioProdutividadePorDestino.pdf');
  }


  geraRelatorioDestinoExcel(cliente,inicio,fim, rows: RelatorioFrota[]){
    let data : any = [];
    let total = rows.pop()
    rows.forEach(row=>{
      data.push({

        'Destino':`${row.estado}-${row.destino}`,
        'Qtde. Viagens':`${(row.qtdViagens)}`,
        'Dias Viag.': `${(row.diasViagens as number).toLocaleString('pt-BR', formatoDecimal1)}`,
        'Dias/Viagem':`${((row.diasViagens/row.qtdViagens) as number).toLocaleString('pt-BR', formatoDecimal1)}`,
        'Qtd. Cidades': `${(row.qtdCidades)}`,
        'Cidades/Viagem':`${((row.qtdCidades/row.qtdViagens) as number).toLocaleString('pt-BR', formatoDecimal1)}`,
        'Valor da Carga':`${((row.valorDaCarga) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        'Despesas':`${((row.despesaCombustivel) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        'Devolução':`${((row.valorDevolucao) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        'Carga Líq':`${((row.valorDaCarga - row.despesaCombustivel -row.valorDevolucao) as number).toLocaleString('pt-BR', formatoMoeda)}`,

      })
    })
    data.push({

      'Destino':`Totalização`,
      'Qtde. Viagens':`${(total.qtdViagens)}`,
      'Dias Viag.': `${(total.diasViagens as number).toLocaleString('pt-BR', formatoDecimal1)}`,
      'Dias/Viagem':`${((total.diasViagens/total.qtdViagens) as number).toLocaleString('pt-BR', formatoDecimal1)}`,
      'Qtd. Cidades':`${(total.qtdCidades)}`,
      'Cidades/Viagem':`${((total.qtdCidades/total.qtdViagens) as number).toLocaleString('pt-BR', formatoDecimal1)}`,
      'Valor da Carga':`${((total.valorDaCarga) as number).toLocaleString('pt-BR', formatoMoeda)}`,
      'Despesas':`${((total.despesaCombustivel) as number).toLocaleString('pt-BR', formatoMoeda)}`,
      'Devolução':`${((total.valorDevolucao) as number).toLocaleString('pt-BR', formatoMoeda)}`,
      'Carga Líq':`${((total.valorDaCarga - total.despesaCombustivel - total.valorDevolucao) as number).toLocaleString('pt-BR', formatoMoeda)}`


    })

    this.excelService.exportAsExcelFile(data, 'relatorioProdutividadePorDestino');
  }

}
