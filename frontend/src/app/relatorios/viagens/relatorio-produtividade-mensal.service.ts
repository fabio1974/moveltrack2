import { Injectable } from '@angular/core';
import {Cliente, RelatorioFrota} from '../../shared/model';
import {buildCabecalho, fontSize, formatoDecimal0, formatoDecimal1, formatoDecimal2, formatoMoeda, getDoc, mes} from '../relatoriosUtils';
import {ExcelService} from '../../shared/excel-service';

@Injectable({
  providedIn: 'root'
})
export class RelatorioProdutividadeMensalService {

  constructor(private excelService: ExcelService) { }


  geraRelatorioProdutividadeMensalPdf(cliente,inicio,fim, rows: RelatorioFrota[]){
    let doc = getDoc('landscape','mm','a4')
    let total = rows.pop()
    let list = rows.map(
      row => {
        return [
          `${row.ano}-${mes[row.mes]}`,
          `${(row.qtdViagens)}`,
          `${(row.diasViagens as number).toLocaleString('pt-BR', formatoDecimal1)}`,
          `${(row.qtdClientes)}`,
          `${((row.pesoDaCarga) as number).toLocaleString('pt-BR', formatoDecimal0)}kg`,
          `${((row.despesaCombustivel) as number).toLocaleString('pt-BR', formatoMoeda)}`,
          `${((row.distanciaPercorrida) as number).toLocaleString('pt-BR', formatoDecimal2)}`,
          `${(row.litros as number).toLocaleString('pt-BR', formatoDecimal2)}L`,
          `${(row.kml as number).toLocaleString('pt-BR', formatoDecimal2)}km/L`,
          `${((row.valorDaCarga) as number).toLocaleString('pt-BR', formatoMoeda)}`,
          `${((row.despesaTotal) as number).toLocaleString('pt-BR', formatoMoeda)}`,
          `${((row.valorDevolucao) as number).toLocaleString('pt-BR', formatoMoeda)}`,
          `${((row.valorDaCarga - row.valorDevolucao - row.despesaTotal) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        ];
      });

    doc.autoTable({
      startY: buildCabecalho(cliente,doc,"Produtividade Total Mensal",inicio,fim),
      styles: {halign: 'center',fontSize: fontSize -1},
      head: [['Mês','Qtde. Viag.','Dias Viag.','Qtd. Clientes','Peso Transp.','Combustível', 'Km Rodado','Litros','Consumo Médio','Valor da Carga','Despesa Total', 'Devolução','Carg. Líq.']],
      body: [...list],
      foot: [['Totalização',
        `${(total.qtdViagens)}`,
        `${(total.diasViagens as number).toLocaleString('pt-BR', formatoDecimal1)}`,
        `${(total.qtdClientes)}`,
        `${((total.pesoDaCarga) as number).toLocaleString('pt-BR', formatoDecimal0)}kg`,
        `${((total.despesaCombustivel) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        `${((total.distanciaPercorrida) as number).toLocaleString('pt-BR', formatoDecimal2)}`,
        `${(total.litros as number).toLocaleString('pt-BR', formatoDecimal2)}L`,
        `${(total.kml as number).toLocaleString('pt-BR', formatoDecimal2)}km/L`,
        `${((total.valorDaCarga) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        `${((total.despesaTotal) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        `${((total.valorDevolucao) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        `${((total.valorDaCarga - total.valorDevolucao - total.despesaTotal) as number).toLocaleString('pt-BR', formatoMoeda)}`,
      ]],
    });
    doc.save('RelatorioProdutividadeMensal.pdf');
  }


  geraRelatorioProdutividadeMensalExcel(cliente: Cliente, inicio: Date, fim: Date, rows: RelatorioFrota[]) {
    let data : any = [];
    let total = rows.pop()
    rows.forEach(row=>{
      data.push({
        'Mês':`${row.ano}-${mes[row.mes]}`,
        'Qtde. Viag.': `${(row.qtdViagens)}`,
        'Dias Viag.':`${(row.diasViagens as number).toLocaleString('pt-BR', formatoDecimal1)}`,
        'Qtd. Clientes':`${(row.qtdClientes)}`,
        'Peso Transp.':`${((row.pesoDaCarga) as number).toLocaleString('pt-BR', formatoDecimal0)}kg`,
        'Combustível':`${((row.despesaCombustivel) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        'Km Rodado':`${((row.distanciaPercorrida) as number).toLocaleString('pt-BR', formatoDecimal2)}`,
        'Litros':`${(row.litros as number).toLocaleString('pt-BR', formatoDecimal2)}L`,
        'Consumo Médio':`${(row.kml as number).toLocaleString('pt-BR', formatoDecimal2)}km/L`,
        'Valor da Carga':`${((row.valorDaCarga) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        'Despesa Total':`${((row.despesaTotal) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        'Devolução':`${((row.valorDevolucao) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        'Carg. Líq.':`${((row.valorDaCarga - row.valorDevolucao - row.despesaTotal) as number).toLocaleString('pt-BR', formatoMoeda)}`
      })
    })
    data.push({
      'Mês':`Total`,
      'Qtde. Viag.': `${(total.qtdViagens)}`,
      'Dias Viag.':`${(total.diasViagens as number).toLocaleString('pt-BR', formatoDecimal1)}`,
      'Qtd. Clientes':`${(total.qtdClientes)}`,
      'Peso Transp.':`${((total.pesoDaCarga) as number).toLocaleString('pt-BR', formatoDecimal0)}kg`,
      'Combustível':`${((total.despesaCombustivel) as number).toLocaleString('pt-BR', formatoMoeda)}`,
      'Km Rodado':`${((total.distanciaPercorrida) as number).toLocaleString('pt-BR', formatoDecimal2)}`,
      'Litros':`${(total.litros as number).toLocaleString('pt-BR', formatoDecimal2)}L`,
      'Consumo Médio':`${(total.kml as number).toLocaleString('pt-BR', formatoDecimal2)}km/L`,
      'Valor da Carga':`${((total.valorDaCarga) as number).toLocaleString('pt-BR', formatoMoeda)}`,
      'Despesa Total':`${((total.despesaTotal) as number).toLocaleString('pt-BR', formatoMoeda)}`,
      'Devolução':`${((total.valorDevolucao) as number).toLocaleString('pt-BR', formatoMoeda)}`,
      'Carg. Líq.':`${((total.valorDaCarga - total.valorDevolucao - total.despesaTotal) as number).toLocaleString('pt-BR', formatoMoeda)}`
    })

    this.excelService.exportAsExcelFile(data, 'RelatorioProdutividadeMensal');
  }


}
