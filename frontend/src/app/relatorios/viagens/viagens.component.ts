import {Component, OnInit} from '@angular/core';
import {buildCabecalho, fontSize, formatoDecimal1, formatoDecimal2, formatoMoeda, getDoc,} from '../relatoriosUtils';
import {Cliente, ConsumoPorVeiculo, RelatorioFrota, RelatorioMensalDespesa} from '../../shared/model';
import {RelatoriosService} from '../relatorios.service';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {DialogService} from '../../shared/dialog.service';
import {bsDatepickerConfig} from '../../shared/bsDateTimePickerConfig';
import {ExcelService} from '../../shared/excel-service';
import {RelatorioDespesaMensalService} from './relatorio-despesa-mensal.service';
import {RelatorioProdutividadeMensalService} from './relatorio-produtividade-mensal.service';
import {RelatorioProdutividadePorEstadoService} from './relatorio-produtividade-por-estado.service';
import {RelatorioProdutividadePorDestinoService} from './relatorio-produtividade-por-destino.service';
import {RelatorioProdutividadeMotoristaService} from './relatorio-produtividade-motorista.service';
import {RelatorioProdutividadePorVeiculoService} from './relatorio-produtividade-por-veiculo.service';
import {RelatorioConsumoPorVeiculoService} from './relatorio-consumo-por-veiculo.service';

@Component({
  selector: 'app-viagens',
  templateUrl: './viagens.component.html',
  styleUrls: ['./viagens.component.css']
})
export class ViagensComponent implements OnInit {

  inicio: Date
  fim: Date
  cliente: Cliente
  numeroViagem: number;
  bsConfig = bsDatepickerConfig

  constructor(
    private relatoriosService: RelatoriosService,
    private relatorioDespesaMensalService: RelatorioDespesaMensalService,
    private relatorioProdutividadeMensalService: RelatorioProdutividadeMensalService,
    private relatorioProdutividadePorEstadoService: RelatorioProdutividadePorEstadoService,
    private relatorioProdutividadePorDestinoService: RelatorioProdutividadePorDestinoService,
    private relatorioProdutividadeMotoristaService: RelatorioProdutividadeMotoristaService,
    private relatorioProdutividadePorVeiculoService: RelatorioProdutividadePorVeiculoService,
    private relatorioConsumoPorVeiculoService: RelatorioConsumoPorVeiculoService,
    private segurancaService:SegurancaService,
    private excelService:ExcelService,
    private dialogService: DialogService) { }

  ngOnInit() {
    this.fim = new Date()
    this.fim.setHours(23,59,59,999)
    this.inicio = new Date()
    this.inicio.setHours(0,0,0,0)
    this.inicio.setMonth(this.inicio.getMonth()-3,1)
    this.cliente = this.segurancaService.pessoaLogada() as Cliente
  }


  relatorioProdutividadePorVeiculo(formato) {
    this.dialogService.showWaitDialog("...processando. Aguarde!")
    this.relatoriosService.relatorioProdutividade(this.cliente,this.inicio,this.fim).subscribe(
      resp=>{
        if(formato=='pdf')
          this.relatorioProdutividadePorVeiculoService.geraRelatorioProdutividadePorVeiculoPdf(this.cliente,this.inicio,this.fim,resp as RelatorioFrota[])
        else
          this.relatorioProdutividadePorVeiculoService.geraRelatorioProdutividadePorVeiculoExcel(this.cliente,this.inicio,this.fim,resp as RelatorioFrota[])
        this.dialogService.closeWaitDialog()
      },
      error1 => {
        console.log("Error",error1)
        this.dialogService.closeWaitDialog()
      })
  }


  relatorioConsumoPorVeiculo(formato) {
    this.dialogService.showWaitDialog("...processando. Aguarde!")
    this.relatoriosService.relatorioConsumoVeiculo(this.cliente,this.inicio,this.fim).subscribe(
      resp=>{
        if(formato=='pdf')
          this.relatorioConsumoPorVeiculoService.geraRelatorioConsumoPorVeiculoPdf(this.cliente,this.inicio,this.fim,resp as ConsumoPorVeiculo[])
        else
          this.relatorioConsumoPorVeiculoService.geraRelatorioConsumoPorVeiculoExcel(this.cliente,this.inicio,this.fim,resp as ConsumoPorVeiculo[])
        this.dialogService.closeWaitDialog()
      },
      error1 => {
        console.log("Error",error1)
        this.dialogService.closeWaitDialog()
      })
  }


  relatorioProdutividadeDoMotorista(formato) {
    this.dialogService.showWaitDialog("...processando. Aguarde!")
    this.relatoriosService.relatorioMotorista(this.cliente,this.inicio,this.fim).subscribe(
      resp=>{
        if(formato=='pdf')
          this.relatorioProdutividadeMotoristaService.geraRelatorioProdutividadeMotoristaPdf(this.cliente,this.inicio,this.fim,resp as RelatorioFrota[])
        else
          this.relatorioProdutividadeMotoristaService.geraRelatorioProdutividadeMotoristaExcel(this.cliente,this.inicio,this.fim,resp as RelatorioFrota[])
        this.dialogService.closeWaitDialog()
      },
      error1 => {
        this.dialogService.closeWaitDialog()
        console.log("Error",error1)
      })
  }


  relatorioProdutividadePorDestino(formato) {
    this.dialogService.showWaitDialog("...processando. Aguarde!")
    this.relatoriosService.relatorioDestino(this.cliente,this.inicio,this.fim).subscribe(
      resp=>{
        if(formato=='pdf')
          this.relatorioProdutividadePorDestinoService.geraRelatorioDestinoPdf(this.cliente,this.inicio,this.fim,resp as RelatorioFrota[])
        else
          this.relatorioProdutividadePorDestinoService.geraRelatorioDestinoExcel(this.cliente,this.inicio,this.fim,resp as RelatorioFrota[])
        this.dialogService.closeWaitDialog()
      },
      error1 => {
        this.dialogService.closeWaitDialog()
        console.log("Error",error1)
      })
  }


  relatorioProdutividadePorEstado(formato) {
    this.dialogService.showWaitDialog("...processando. Aguarde!")
    this.relatoriosService.relatorioEstado(this.cliente,this.inicio,this.fim).subscribe(
      resp=>{
        if(formato=='pdf')
          this.relatorioProdutividadePorEstadoService.relatorioProdutividadePorEstadoPdf(this.cliente,this.inicio,this.fim,resp as RelatorioFrota[])
        else
          this.relatorioProdutividadePorEstadoService.relatorioProdutividadePorEstadoExcel(this.cliente,this.inicio,this.fim,resp as RelatorioFrota[])

        this.dialogService.closeWaitDialog()
      },
      error1 => {
        this.dialogService.closeWaitDialog()
        console.log("Error",error1)
      })
  }



  relatorioProdutividadeMensal(formato) {
    this.dialogService.showWaitDialog("...processando. Aguarde!")
    this.relatoriosService.relatorioProdutividadeMensal(this.cliente,this.inicio,this.fim).subscribe(
      resp=>{
        if(formato=='pdf')
          this.relatorioProdutividadeMensalService.geraRelatorioProdutividadeMensalPdf(this.cliente,this.inicio,this.fim,resp as RelatorioFrota[])
        else
          this.relatorioProdutividadeMensalService.geraRelatorioProdutividadeMensalExcel(this.cliente,this.inicio,this.fim,resp as RelatorioFrota[])

        this.dialogService.closeWaitDialog()
      },
      error1 => {
        this.dialogService.closeWaitDialog()
        console.log("Error",error1)
      })
  }



  relatorioDespesaMensal(formato) {
    this.dialogService.showWaitDialog("...processando. Aguarde!")
    this.relatoriosService.relatorioDespesaMensal(this.cliente,this.inicio,this.fim,this.numeroViagem).subscribe(
      resp=>{
        if(formato=='pdf')
          this.relatorioDespesaMensalService.geraRelatorioDespesaMensalPdf(this.cliente,this.inicio,this.fim,resp as RelatorioMensalDespesa[])
        else
          this.relatorioDespesaMensalService.geraRelatorioDespesaMensalExcell(this.cliente,this.inicio,this.fim,resp as RelatorioMensalDespesa[])
        this.dialogService.closeWaitDialog()
      },
      error1 => {
        this.dialogService.closeWaitDialog()
        console.log("Error",error1)
      })
  }

}
