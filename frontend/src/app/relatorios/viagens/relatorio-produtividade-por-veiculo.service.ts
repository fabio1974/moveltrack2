import { Injectable } from '@angular/core';
import {RelatorioFrota} from '../../shared/model';
import {buildCabecalho, fontSize, formatoDecimal1, formatoMoeda, getDoc} from '../relatoriosUtils';
import {ExcelService} from '../../shared/excel-service';

@Injectable({
  providedIn: 'root'
})
export class RelatorioProdutividadePorVeiculoService {

  constructor(private excelService: ExcelService) { }

  geraRelatorioProdutividadePorVeiculoPdf(cliente, inicio, fim, rows: RelatorioFrota[]) {
    let doc = getDoc()
    let total = rows.pop();
    let list = rows.map(
      row => {
        return [
          row.placa,
          row.qtdViagens,
          row.qtdCidades,
          row.qtdClientes,
          (row.valorDaCarga as number).toLocaleString('pt-BR', formatoMoeda),
          `${(row.pesoDaCarga as number).toLocaleString('pt-BR', formatoMoeda)} kg`
        ];
      });
    doc.autoTable({
      startY: buildCabecalho(cliente, doc, 'Relatório de Produtividade por Veículo',inicio,fim),
      styles: {halign: 'center', fontSize},
      head: [['Placa', 'Q. Viagens', 'Q. Cidades', 'Q. Clientes', 'Vl. da Carga', 'Peso Carga']],
      body: [...list],
      foot: [['Totalização',
        total.qtdViagens,
        total.qtdCidades,
        total.qtdClientes,
        (total.valorDaCarga as number).toLocaleString('pt-BR', formatoMoeda),
        `${(total.pesoDaCarga as number).toLocaleString('pt-BR', formatoMoeda)} kg`
      ]],
    });
    doc.save('relatorioProdutividadePorVeiculo.pdf');
  }

  geraRelatorioProdutividadePorVeiculoExcel(cliente, inicio, fim, rows: RelatorioFrota[]) {
    let data : any = [];
    let total = rows.pop()
    rows.forEach(row=>{
      data.push({

        'Placa':row.placa,
        'Q. Viagens':row.qtdViagens,
        'Q. Cidades':row.qtdCidades,
        'Q. Clientes':row.qtdClientes,
        'Vl. da Carga':(row.valorDaCarga as number).toLocaleString('pt-BR', formatoMoeda),
        'Peso Carga':`${(row.pesoDaCarga as number).toLocaleString('pt-BR', formatoMoeda)} kg`

      })
    })
    data.push({

      'Placa':'Totalização',
      'Q. Viagens':total.qtdViagens,
      'Q. Cidades':total.qtdCidades,
      'Q. Clientes':total.qtdClientes,
      'Vl. da Carga':(total.valorDaCarga as number).toLocaleString('pt-BR', formatoMoeda),
      'Peso Carga':`${(total.pesoDaCarga as number).toLocaleString('pt-BR', formatoMoeda)} kg`

    })

    this.excelService.exportAsExcelFile(data, 'relatorioProdutividadePorVeiculo');
  }

}
