import { Injectable } from '@angular/core';
import {RelatorioFrota} from '../../shared/model';
import {buildCabecalho, fontSize, formatoDecimal1, formatoDecimal2, formatoMoeda, getDoc} from '../relatoriosUtils';
import {ExcelService} from '../../shared/excel-service';

@Injectable({
  providedIn: 'root'
})
export class RelatorioProdutividadeMotoristaService {

  constructor(private excelService: ExcelService) { }

  geraRelatorioProdutividadeMotoristaPdf(cliente,inicio,fim, rows: RelatorioFrota[]){
    let doc = getDoc('landscape','mm','a4')
    let total = rows.pop()
    let list = rows.map(
      row => {
        return [
          row.motorista,
          `${(row.kml as number).toLocaleString('pt-BR', formatoDecimal2)}km/L`,
          `${(row.diasViagens as number).toLocaleString('pt-BR', formatoDecimal1)}`,
          `${(row.distanciaPercorrida as number).toLocaleString('pt-BR', formatoDecimal1)}km`,
          `${(row.litros as number).toLocaleString('pt-BR', formatoDecimal2)} L`,
          `${(row.despesaCombustivel as number).toLocaleString('pt-BR', formatoMoeda)}`,
          `${((row.despesaEstiva) as number).toLocaleString('pt-BR', formatoMoeda)}`,
          `${((row.despesaDiaria) as number).toLocaleString('pt-BR', formatoMoeda)}`,
          `${((row.despesaOutras) as number).toLocaleString('pt-BR', formatoMoeda)}`
        ];
      });
    doc.autoTable({
      startY: buildCabecalho(cliente,doc,"Relatório dos Motoristas",inicio,fim),
      styles: {halign: 'center',fontSize},
      head: [['Nome','Méd. Consumo','Dias Viag.','Dist. Percorrida','Litros Abastecidos','Desp. Combustível', 'Despesas Estiva', 'Despesas Diarias','Outras']],
      body: [...list],
      foot: [['Totalização',
        `${(total.kml as number).toLocaleString('pt-BR', formatoDecimal2)}km/L`,
        `${(total.diasViagens as number).toLocaleString('pt-BR', formatoDecimal1)}`,
        `${(total.distanciaPercorrida as number).toLocaleString('pt-BR', formatoDecimal1)}km`,
        `${(total.litros as number).toLocaleString('pt-BR', formatoDecimal2)}`,
        `${(total.despesaCombustivel as number).toLocaleString('pt-BR', formatoMoeda)}`,
        `${((total.despesaEstiva) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        `${((total.despesaDiaria) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        `${((total.despesaOutras) as number).toLocaleString('pt-BR', formatoMoeda)}`
      ]],
    });
    doc.save('relatorioProdutividadeMotorista.pdf');
  }

  geraRelatorioProdutividadeMotoristaExcel(cliente,inicio,fim, rows: RelatorioFrota[]){

    let data : any = [];
    let total = rows.pop()
    rows.forEach(row=>{
      data.push({

        'Nome':row.motorista,
        'Méd. Consumo':`${(row.kml as number).toLocaleString('pt-BR', formatoDecimal2)}km/L`,
        'Dias Viag.':`${(row.diasViagens as number).toLocaleString('pt-BR', formatoDecimal1)}`,
        'Dist. Percorrida':`${(row.distanciaPercorrida as number).toLocaleString('pt-BR', formatoDecimal1)}km`,
        'Litros Abastecidos':`${(row.litros as number).toLocaleString('pt-BR', formatoDecimal2)} L`,
        'Desp. Combustível':`${(row.despesaCombustivel as number).toLocaleString('pt-BR', formatoMoeda)}`,
        'Despesas Estiva': `${((row.despesaEstiva) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        'Despesas Diarias':`${((row.despesaDiaria) as number).toLocaleString('pt-BR', formatoMoeda)}`,
        'Outras':`${((row.despesaOutras) as number).toLocaleString('pt-BR', formatoMoeda)}`,

      })
    })
    data.push({

      'Nome':'Totalização',
      'Méd. Consumo':`${(total.kml as number).toLocaleString('pt-BR', formatoDecimal2)}km/L`,
      'Dias Viag.':`${(total.diasViagens as number).toLocaleString('pt-BR', formatoDecimal1)}`,
      'Dist. Percorrida':`${(total.distanciaPercorrida as number).toLocaleString('pt-BR', formatoDecimal1)}km`,
      'Litros Abastecidos':`${(total.litros as number).toLocaleString('pt-BR', formatoDecimal2)} L`,
      'Desp. Combustível':`${(total.despesaCombustivel as number).toLocaleString('pt-BR', formatoMoeda)}`,
      'Despesas Estiva': `${((total.despesaEstiva) as number).toLocaleString('pt-BR', formatoMoeda)}`,
      'Despesas Diarias':`${((total.despesaDiaria) as number).toLocaleString('pt-BR', formatoMoeda)}`,
      'Outras':`${((total.despesaOutras) as number).toLocaleString('pt-BR', formatoMoeda)}`


    })

    this.excelService.exportAsExcelFile(data, 'relatorioProdutividadeMotorista');

  }


}
