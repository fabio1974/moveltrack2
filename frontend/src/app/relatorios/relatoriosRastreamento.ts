declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');

var formato = { minimumFractionDigits: 2 , style: 'currency', currency: 'BRL' }

export function   geraRelatorioParadasPdf(locations){
  let doc = new jsPDF()
  let rows = locations.map(loc=>{
    return [ loc.id,
      loc.latitude,
      loc.dateLocationInicio,
      loc.dateLocation,
      loc.velocidade
    ]
  })
  doc.autoTable({
    head: [['Ordem', 'Endereço', 'Chegada','Saida','Permanencia' ]],
    body: [...rows],
    foot: [['Total1', 'Total2', 'Total3','Total4','Total5' ]],
  });
  doc.save('relatorioParadasList.pdf');
}



