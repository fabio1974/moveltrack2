import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Chip, Cliente, Contrato, Empregado, Equipamento, Motorista, Municipio, Veiculo, Viagem} from './model';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient) { }


  contratosResult
  searchContratos(event) {
    if(event.trim().length<3) return
    const query = `nome=${event}`;
    this.http.get(`${environment.apiUrl}/searchContratos?${query}`).subscribe(
      resp=>{
        this.contratosResult = (resp as Contrato[]).map(
          m=>{
            m.label = `${m.cliente.nome}-${m.numeroContrato}`
            return m
          })
      },
      error1 => {
        console.log('error', error1);
      }
    )
  }


  viagemResult
  searchViagens(event) {
    if(event.trim().length<2) return
    const query = `numeroViagem=${event}`;
    this.http.get(`${environment.apiUrl}/searchViagens?${query}`).subscribe(
      resp=>{
        this.viagemResult = (resp as Viagem[]).map(
          m=>{
            m.label = `${m.numeroViagem}-${m.cidadeDestino.descricao}`
            return m
          })
      },
      error1 => {
        console.log('error', error1);
      }
    )
  }




  equipamentosResult:Equipamento[]
  searchEquipamentos(event) {

    if(event.trim().length<3) return

    this.http.get(`${environment.apiUrl}/searchEquipamentos?imei=${event}`).subscribe(
      resp=>{
        this.equipamentosResult = (resp as Equipamento[])
      },
      error1 => {
        console.log('error', error1);
      }
    )
  }


  imeisResult:string[]
  searchImeis(event) {

    if(event.trim().length<3) return

    this.http.get(`${environment.apiUrl}/searchEquipamentos?imei=${event}`).subscribe(
      resp=>{
        this.imeisResult = (resp as Equipamento[]).map(e=>e.imei)
      },
      error1 => {
        console.log('error', error1);
      }
    )
  }







  chipsResult
  searchChips(event) {
    if(event.trim().length<3) return
    const query = `iccid=${event}`;
    this.http.get(`${environment.apiUrl}/searchChips?${query}`).subscribe(
      resp=>{
        this.chipsResult = resp as Chip[]
      },
      error1 => {
        console.log('error', error1);
      }
    )
  }


  chipsIccidResult
  searchChipsIccid(event) {
    if(event.trim().length<3) return
    const query = `iccid=${event}`;
    this.http.get(`${environment.apiUrl}/searchChips?${query}`).subscribe(
      resp=>{
        this.chipsIccidResult = (resp as Chip[]).map(chip=>chip.iccid)
      },
      error1 => {
        console.log('error', error1);
      }
    )
  }




  veiculosResult
  searchVeiculos(event) {
    if(event.trim().length<3) return
    const query = `placa=${event}`;
    this.http.get(`${environment.apiUrl}/searchVeiculos?${query}`).subscribe(
      resp=>{
        this.veiculosResult = resp as Veiculo[]
      },
      error1 => {
        console.log('error', error1);
      }
    )
  }

  clientesResult
  searchClientes(event) {

    if(event.trim().length<3) return
    const query = `nome=${event}`;

    this.http.get(`${environment.apiUrl}/searchClientes?${query}`).subscribe(
      resp=>{
        this.clientesResult = resp as Cliente[]
        console.log('this.clientesResult', this.clientesResult);
      },
      error1 => {
        console.log('error', error1);
      }
    )
  }

  empregadosResult:Empregado[]
  searchEmpregados(event) {
    if(event.trim().length<3) return
    const query = `nome=${event}`;
    this.http.get(`${environment.apiUrl}/searchEmpregados?${query}`).subscribe(
      resp=>{
        this.empregadosResult = resp as Empregado[]
        console.log("this.empregadosResult",this.empregadosResult)
      },
      error1 => {
        console.log('error', error1);
      }
    )
  }


  motoristasResult:Motorista[]
  searchMotoristas(event) {
    if(event.trim().length<3)  return
    const query = `nome=${event}`;
    this.http.get(`${environment.apiUrl}/searchMotoristas?${query}`).subscribe(
      resp=>{
        this.motoristasResult = resp as Motorista[]
      },
      error1 => {
        console.log('error', error1);
      }
    )
  }



  municipiosResult: Municipio[] = null;
  searchMunicipios(event) {
    if(event.trim().length<3) return
    const query = `nome=${event}`;
    this.http.get(`${environment.apiUrl}/searchMunicipios?${query}`).subscribe(
      resp => {
        this.municipiosResult = (resp as Municipio[]).map(
          m=>{
            m.label = `${m.descricao}-${m.uf.sigla}`
            return m
          })
        console.log('MunicipioResult', this.municipiosResult);
      },
      error => {
        console.log('error', error);
      }
    );
  }


  findVeiculosAtivosCliente(cliente: Cliente):  Observable<any> {
    return this.http.post(`${environment.apiUrl}/findVeiculosAtivosByCliente`,cliente);
  }



}
