import {BsDatepickerConfig} from 'ngx-bootstrap';
import {Injectable} from '@angular/core';

export const bsDatepickerConfig: Partial<BsDatepickerConfig> = Object.assign({},
  {
    containerClass: 'theme-dark-blue',
    dateInputFormat: 'DD/MM/YYYY HH:mm'
  });




@Injectable({
  providedIn: 'root'
})
export class DatepickerConfigService {
  setConfig(showTime:boolean): Partial<BsDatepickerConfig> {
    return Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-dark-blue',
        dateInputFormat: `DD/MM/YYYY${showTime?' HH:mm':''}`
      });
  }

  //funcao para corrigir bug do componente, que muda de horario quando se escolhe um mês diferente
  bsValueChange(event: Date, value: Date) {
    if(value && event && event.getMonth() != value.getMonth())
      event.setHours(value.getHours(),value.getMinutes(),value.getSeconds(),value.getMilliseconds())
  }
}
