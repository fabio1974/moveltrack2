import {ChangeDetectorRef, Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DialogService {





  constructor( ) {  }

  public messageTitle
//  private messageTitleWait
  public messageText
  public displayDialog=false
  public displayWaitDialog=false
  public displayViagemDialog=false
  public displayBloqueioDialog= false
  public displayConfirmDialog = false
  public displayChartDialog = false
  public displayTelimetria = false

  showDialog(title, text){
    Promise.resolve(null).then(() => {
      this.displayDialog = true
      this.messageText = text
      this.messageTitle= title
    })
  }

  showBloqueioDialog(title, text){
    Promise.resolve(null).then(() => {
      this.displayBloqueioDialog = true
    })
  }

  showConfirmDialog(title, text){
    Promise.resolve(null).then(() => {
      this.displayConfirmDialog = true
      this.messageText = text
      this.messageTitle= title

    })
  }

  showWaitDialog(title){
    Promise.resolve(null).then(() =>{
        this.displayWaitDialog = true
        this.messageTitle= title
    })
  }

  showChartDialog(title){
    Promise.resolve(null).then(() =>{
      this.displayChartDialog = true
      this.messageTitle= title
    })
  }

  showViagemDialog(title){
    Promise.resolve(null).then(() =>{
      this.displayViagemDialog = true
      this.messageTitle= title
    })
  }

  showTelemetriaDialog(){
    Promise.resolve(null).then(() =>{
      this.displayTelimetria = true
    })
  }



  closeWaitDialog(){
    this.displayWaitDialog = false
  }

  closeBloqueioDialog(){
    this.displayBloqueioDialog = false
  }

  closeTelemetriaDialog(){
    this.displayTelimetria = false
  }

  closeConfirmDialog(){
    this.displayConfirmDialog = false
  }


  closeChartDialog() {
    this.displayChartDialog = false
  }

  closeViagemDialog() {
    this.displayViagemDialog = false
  }

}
