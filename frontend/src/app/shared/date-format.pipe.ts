import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'dateFormat'
})
export class DateFormatPipe implements PipeTransform {
  transform(value: any, args?: any): any {
    if(!value) return ''
    var myDate = new Date(value[0],value[1]-1,value[2],value[3],value[4],value[5],value[6]/1000000)
    return moment(myDate).format(args)
  }
}
