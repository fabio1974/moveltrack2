import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {DialogService} from '../dialog.service';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.css']
})
export class ConfirmComponent implements OnInit {

  @Output() doEmitter = new EventEmitter();

  constructor(public dialogService: DialogService) { }

  ngOnInit() {
  }

  naoMethod() {
      this.dialogService.closeConfirmDialog()
  }

  doMethod(){
    this.doEmitter.emit("mensagemDoFilhoe")
    this.dialogService.closeConfirmDialog()
  }





}
