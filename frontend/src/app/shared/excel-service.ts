import {Injectable} from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import {formatDate, formatNumber} from '@angular/common';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';


@Injectable({
  providedIn: 'root'
})
export class ExcelService {

  constructor() { }

  fitToColumn = data => { const columnWidths = []; for (const property in data[0]) { columnWidths.push({ wch: Math.max( property ? property.toString().length : 0, ...data.map(obj => obj[property] ? obj[property].toString().length : 0 ) ) }); } return columnWidths; };

  public exportAsExcelFile(json: any[], excelFileName: string, header:any[]=[]): void {
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(header, {header: ['key', 'value'], skipHeader: true});
    ws['!cols'] = this.fitToColumn(json);
    XLSX.utils.sheet_add_json(ws, json, {skipHeader: false, origin: `A${header.length==0?1:header.length+2}`});
    const workbook: XLSX.WorkBook = { Sheets: { 'data': ws }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }



  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
    FileSaver.saveAs(data, fileName + '_' + new  Date().getTime() + EXCEL_EXTENSION);
  }


  public exportTableToSheet(table)  {
    console.log("table",table)
    const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(table);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    /* save to file */
    XLSX.writeFile(wb, 'SheetJS.xlsx');
  }


}
