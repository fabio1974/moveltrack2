import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'padStart'
})
export class PadStartFormatPipe implements PipeTransform {
  transform(value: number, maxLength?: number, fillString?: string): any {
    //console.log("ARGS",args)
    if(!value)
      return null
    return value.toString().padStart(maxLength,fillString)
  }
}
