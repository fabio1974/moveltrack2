export class Page<T> {
  pageIndex = 1;
  pageSize = 10;
  list: T[] = [];
  totalRecords = 0;
  sortField="id";
  sortDirection="DESC"  //"0" = ASC , "1" = DESC
}




export class Boleto {
  id=null;
  dataVencimento= new Date();
  dataBase=null;
  dataPagamento=null;
  dataRegistroPagamento=null;
  valor=null;
  multa=null;
  juros=null;
  situacao: MBoletoStatus = null// MBoletoStatus.EMITIDO;
  tipoDeCobranca = null//'COM_REGISTRO';
  tipo= null//'AVULSO';
  dataEmissao=null// new Date();
  mensagem34=null;
  observacao=null;
  nossoNumero=null;
  carne=null;
  contrato = null;
  emissor=null;
  iugu=null;
}

export enum MBoletoStatus {
  EMITIDO = "EMITIDO",
  VENCIDO = "VENCIDO",
  PAGAMENTO_EFETUADO = "PAGAMENTO_EFETUADO",
  CANCELADO = "CANCELADO",
  PROTESTADO = "PROTESTADO"
}

export class Contrato  {
  id = null;
  numeroContrato = null;
  inicio = new Date();
  fim = null;
  diaVencimento = 0;
  vendedor = null;
  pagamentoAnual = false;
  contratoGeraCarne = 'PENDENTE';
  contratoTipo = 'COMODATO';
  mensalidade = 0;
  entrada = 0;
  status: ContratoStatus = ContratoStatus.ATIVO;
  ultimaAlteracao = null;
  dataBase=null
  veiculos = null
  cliente: Cliente = new Cliente()
  label = null
}

export enum ContratoStatus {
  ATIVO="ATIVO",
  SUSPENSO="SUSPENSO",
  ENCERRADO="ENCERRADO",
  CANCELADO="CANCELADO"
}


export class Veiculo {

  constructor(id:number=null){
    this.id = id
  }

  id = null
  placa: string = null
  marcaModelo = null
  motorista = null
  descricao = null
  velocidadeMaxima = null
  equipamento: Equipamento = null
  cor = null
  dataInstalacao = new Date()
  dataDesinstalacao = null
  instalador = null
  contrato: Contrato = new Contrato()
  botaoPanico: Boolean = false
  demo: Boolean = false
  comEscuta: Boolean = false
  comCercaVirtual: Boolean = false
  comBloqueio: Boolean = false
  consumoCombustivel= 0
  tipo: VeiculoTipo = null
  status: VeiculoStatus = null
  lastLocation: Location = null
}

export enum  VeiculoStatus {
  ATIVO="ATIVO",
  INATIVO="INATIVO"
}


export enum VeiculoTipo{
  MOTOCICLETA="MOTOCICLETA",
  AUTOMOVEL="AUTOMOVEL",
  CAMINHAO="CAMINHAO",
  TRATOR="TRATOR",
  PICKUP="PICKUP"
}

export class Equipamento {
  id: number = null
  dataCadastro: Date = new Date()
  inicioSpot: Date = null
  vencimentoSpot: Date = null
  situacao: string = null
  proprietario: Cliente = null
  modelo: ModeloRastreador = null
  senha: string = null
  imei: string = null
  atrasoGmt: number = 0
  chip: Chip = null
  comando: number = 0
  observacao: string = null
  ultimaAlteracao: Date = null
  possuidor: Empregado = null
  label = null
  lastLocation: Location = null
  veiculo: Veiculo = null
}

export enum ModeloRastreador {
  GT06="GT06",
  H08="H08",
  TK103A2="TK103A2",
  GT06B="GT06B",
  XT009="XT009",
  GT06N="GT06N",
  GT06B2="GT06B2",
  TR02="TR02",
  CRX1="CRX1",
  JV200="JV200",
  TK06="TK06",
  ST350_LC2="ST350_LC2",
  ST350_LC4="ST350_LC4",
  ST940="ST940",
  CRXN="CRXN",
  SPOT_TRACE="SPOT_TRACE",
  CRX3="CRX3"
}

export class SelectOption{
  constructor(valor:string=null,descricao:string=null){
    this.valor  = valor
    this.descricao = descricao
  }
  valor: string
  descricao: string
}


export enum EquipamentoSituacao{
  AGUARDANDO_TESTE = "AGUARDANDO_TESTE",
  ATIVO = "ATIVO",
  COM_DEFEITO = "COM_DEFEITO",
  EM_TESTE = 'EM_TESTE',
  NAO_ENCONTRADO = 'NAO_ENCONTRADO',
  NOVO_CONFIGURADO = 'NOVO_CONFIGURADO',
  NOVO_NAO_CONFIGURADO = 'NOVO_NAO_CONFIGURADO',
  OUTRA = 'OUTRA',
  PRONTO_PARA_REUSO = 'PRONTO_PARA_REUSO',
  SUSPENSO = 'SUSPENSO'
}



export class Chip {
  id = null
  numero: string = null
  iccid: string = null
  dataCadastro: Date = null
  operadora: Operadora = null
  status = null
  label: string=null;
}

export enum Operadora {
  CLARO="CLARO",
  OI="OI",
  TIM="TIM",
  VIVO="VIVO",
  LINK_CLARO="LINK_CLARO",
  LINK_TIM="LINK_TIM",
  LINK_VIVO="LINK_VIVO",
  VODAFONE="VODAFONE",
  TRANSMEET_VIVO="TRANSMEET_VIVO"
}

export class Sms {
  id
  celular: string
  mensagem: string
  imei: string
  tipo: SmsTipo
  status: SmsStatus
  dataUltimaAtualizacao
}

export enum  SmsStatus {
  ESPERANDO="ESPERANDO",
  ENVIADO="ENVIADO",
  RECEBIDO="RECEBIDO",
  DESCARTADO="DESCARTADO",
  ESPERANDO_SOCKET="ESPERANDO_SOCKET"
 // ESPERANDO_SOCKET2="ESPERANDO_SOCKET2"
}

export enum SmsTipo {
  BLOQUEIO, DESBLOQUEIO, REINICIAR, CHECAR, AVISO, DNS
}



export const PessoaStatus = {
  ATIVO: 'ATIVO',
  INATIVO: 'INATIVO'
}


export const DespesaFrotaCategoria = [
  {label: 'MOTORISTA', value: 'MOTORISTA'},
  {label: 'VEÍCULO', value: 'VEICULO'},
  {label: 'VIAGEM', value: 'VIAGEM'}
]

export const DespesaFrotaEspecie = [
  {value: 'COMBUSTIVEL',label: 'COMBUSTÍVEL'},
  {value: 'ESTIVA',label: 'SERV. DE ESTIVA'},
  {value: 'DIARIA',label: 'DIÁRIA'},
  {value: 'IPVA',label: 'IPVA'},
  {value: 'LICENCIAMENTO',label: 'LICENCIAMENTO'},
  {value: 'MANUTENCAO',label: 'MANUTENÇÃO - PEÇAS E SERVIÇOS'},
  {value: 'MULTA_DE_TRANSITO',label: 'MULTA DE TRANSITO'},
  {value: 'OUTROS',label: 'OUTROS'},
  {value: 'SEGURO_OBRIGATORIO',label: 'SEGURO OBRIGATORIO'},
  {value: 'TRABALHISTAS',label: 'SALARIOS E ADICIONAIS TRAB.'}
]

export class Carne {
  id ;
  contrato = new Contrato();
  tipoDeCobranca=null;
  dataVencimentoInicio=null;
  dataVencimentoFim=null;
  postagem=null;
  dataEmissao=null;
  valorPrimeiroBoleto=null
  quantidadeBoleto: number;
}

export class Pessoa {
  id = null
  cpf = null
  cnpj = null
  status = PessoaStatus.ATIVO;
  nome: string = null
  email = null
  logoFile = 'logo_azul_221_57.png'
  ultimaAlteracao = null
  dataCadastro = null
  dataNascimento = null
  telefoneFixo = null
  celular1 = null
  celular2 = null
  endereco = null
  numero = null
  complemento = null
  municipio: Municipio = null
  cep = null
  bairro = null
  usuario: Usuario = new Usuario()
}



export class Cliente extends Pessoa{
  nomeFantasia = null
  observacao = null
  emailAlarme = null
  cerca = null
  ultimaCobranca = null
  ultimoLembrete = null

}

export class Motorista extends Pessoa {

  constructor(dataCadastro: Date = new Date(),id:number=null){
    super()
    this.dataCadastro = dataCadastro
    this.id = id
  }

  patrao = null
  validadeCnh = null;
  categoriaCnh = null;
}

export class Usuario{
  id = null
  perfil: Perfil = new Perfil()
  nomeUsuario = null
  ativo = null
  ultimoAcesso = null
  permissoes: Permissao[] = []
  senha = null
  email = null
}

export class Permissao {
  id = null
  descricao: string = null
}


export const caxias = {
  "codigo" : 7579,
  "descricao" : "CAXIAS",
  "latitude" : -4.85,
  "longitude" : -43.35,
  "uf" : {
    "id" : 10,
    "descricao" : "Maranhao",
    "sigla" : "MA",
    "horarioVerao" : false,
    "gmt" : -3
  },
  "label" : "CAXIAS-MA",
  "label$backend" : null
}

export class Municipio{
  codigo = null
  descricao = null
  uf = null
  label = `${this.descricao}-${this.uf}`
  latitude: number=null;
  longitude:number=null
}

export class Perfil{
  tipo: PerfilTipo = null
  permissoes: Permissao[]
}

export enum  PerfilTipo {
  ADMINISTRADOR="ADMINISTRADOR",
  FRANQUEADO="FRANQUEADO",
  CLIENTE_PJ="CLIENTE_PJ",
  CLIENTE_PF="CLIENTE_PF",
  CLIENTE_DEMONSTRACAO="CLIENTE_DEMONSTRACAO",
  VENDEDOR="VENDEDOR",
  FINANCEIRO="FINANCEIRO",
  GERENTE_GERAL="GERENTE_GERAL",
  GERENTE_ADM="GERENTE_ADM",
  INSTALADOR="INSTALADOR"
}





export class Viagem {
  id = null
  numeroViagem = null
  descricao = null
  cliente = null
  veiculo = null
  motorista = null
  cidadeOrigem = null
  cidadeDestino = null
  status = null
  partida: Date = null
  saiuDaCerca = null
  entrouNaCerca = null
  chegadaReal: Date = null
  chegadaPrevista: Date = null
  valorDaCarga = 0
  valorDevolucao = 0
  pesoDaCarga = 0
  distanciaPercorrida = 0
  distanciaHodometro = 0
  qtdCidades = 0
  qtdClientes = 0
  rota: Rota = null
  label
}


export class DespesaFrota {
  id = null;
  dataDaDespesa = null;
  descricao = null;
  veiculo  ;
  motorista;
  cliente = null;
  viagem ;
  categoria = null;
  especie = null;
  valor = null;
  litros = null;
  dataDePagamento = null;
}

export class Lancamento{
  id = null
  data: Date = new Date()
  valor:number = 0
  operacao: LancamentoTipo = null //Tipo de Lancamento
  status: LancamentoStatus = LancamentoStatus.ATIVO
  houveBaixa: HouveBaixa = null
  formaPagamento: LancamentoFormaPagamento = null
  observacao:string=null
  ordemDeServico: OrdemDeServico=null
  solicitante: Empregado=null
  operador:Empregado=null
}

export enum LancamentoTipo {
  RECEBIMENTO_DE_CLIENTE = "RECEBIMENTO_DE_CLIENTE",
  DEVOLUCAO_DE_DINHEIRO="DEVOLUCAO_DE_DINHEIRO" ,
  GASTO_DE_MATERIAL="GASTO_DE_MATERIAL",
  VALE="VALE"
}

export enum LancamentoFormaPagamento {
  DINHEIRO="DINHEIRO",
  CARTAO="CARTAO",
  CHEQUE="CHEQUE"
}

export enum HouveBaixa {
  SIM="SIM", NAO="NAO"
}

export enum LancamentoStatus{
  ATIVO="ATIVO",
  CANCELADO="CANCELADO"
}

class Option{
  constructor(public key: string,public label:string) {
  }
  static list():Option[]{
    return Object.values(this).filter(it=> it.key)
  }
  static findByKey(key: string): Option {
    return Object.values(this).find(it => it.key == key)
  }
}




export class OrdemDeServico{
  id=null
  dataDoServico:Date=new Date()
  cliente: Cliente =null
  veiculo: Veiculo=null
  observacao:string=null
  numero:number=null
  operador:Empregado=null
  servico: OrdemDeServicoTipo =null  //OrdemDeServicoTipo
  valorDoServico:number=0
  status:OrdemDeServicoStatus=OrdemDeServicoStatus.FECHADA
}

export enum OrdemDeServicoTipo{
  COBRANCA="COBRANCA",
  INSTALACAO="INSTALACAO",
  RETIRADA="RETIRADA",
  MANUTENCAO="MANUTENCAO",
}

export enum OrdemDeServicoStatus {
  FECHADA="FECHADA",
  CANCELADA = "CANCELADA"
}


export class RelatorioFiltro{
  inicio
  fim
  cliente: Cliente
  numeroViagem
  motorista: Motorista
  uf: string;
  inferior: number
  superior: number
  veiculo:Veiculo
  orderBy: string;
  boletoStatus: MBoletoStatus;
  fimCobranca: Date
  instalador:Empregado=null
}


export class RelatorioSinalFiltro {
  equipamento: Equipamento = null
  chip: Chip = null
  operadora: Operadora= null
  placa: String=  null
  veiculoTipo: VeiculoTipo=  null
  cliente: Cliente= null
  modeloRastreador: ModeloRastreador=  null
  equipamentoSituacao: EquipamentoSituacao=  null
  veiculoStatus: VeiculoStatus=  null
  contratoStatus: ContratoStatus=  null
  atrasoMinimo: number=  null
  atrasoMinimoUnidade: AtrasoUnidade=  null
  atrasoMaximo: number=  null
  atrasoMaximoUnidade: AtrasoUnidade=  null
}



export enum AtrasoUnidade{
  MINUTOS="MINUTOS",HORAS="HORAS",DIAS="DIAS"
}

export class LitrosValor {
  litros
  valor
}

export class RelatorioMotoristaPorViagem {
  viagem: Viagem
  despesaCombustivel: LitrosValor
  despesaDiarias
  despesaEstivas
  despesaDiversas
  pesoDaCarga
  diasViagens
}

export class RelatorioCobranca{
  cliente: Cliente
  boletos: Boleto[]
}

export const Operacao = {
  CREATE: 'CREATE',
  UPDATE: 'UPDATE',
  SHOW: 'SHOW'
}

export const ContratoGeraCarneStatusOptions = [
  {label:'GERADO', value:'GERADO'},
  {label:'PENDENTE', value:'PENDENTE'}
]

export const AtivoInativo = [
  {label:'ATIVO', value:'ATIVO'},
  {label:'INATIVO', value:'INATIVO'}
]

export const ContratoStatusOptions = [
  {label:'ATIVO', value:'ATIVO'},
  {label:'ENCERRADO', value:'ENCERRADO'},
  {label:'CANCELADO', value:'CANCELADO'},
  {label:'SUSPENSO', value:'SUSPENSO'}
]

export const ViagemStatusOptions = [
  {value:'ABERTA', label:'ABERTA'},
  {value:'SAINDO', label:'SAINDO'},
  {value:'SAIU_DA_CERCA', label:'SAIU DA CERCA'},
  {value:'NA_ESTRADA', label:'NA ESTRADA'},
  {value:'ENTROU_NA_CERCA', label:'ENTROU NA CERCA'},
  {value:'SE_APROXIMANDO', label:'SE APROXIMANDO'},
  {value:'CHEGADA_EM_TEMPO', label:'CHEGADA EM TEMPO'},
  {value:'CHEGADA_EM_ATRASO', label:'CHEGADA EM ATRASO'},
  {value:'ENCERRADA', label:'ENCERRADA'},
]




export class Empregado extends Pessoa{
  salario: number = null
}


export class Rota{
  origem = caxias
  destino = null
  label: string = null
  itinerario: Municipio[] = []
  polyline: string = null
  id: any;
  distancia: number;
  cliente: Cliente;
}



export class Location2 {
  id
  dateLocation: Date
  dateLocationInicio: Date
  latitude
  longitude
  velocidade
  imei
  comando
  satelites
  endereco
  mcc
  bloqueio
  gps
  gsm
  sos
  battery
  volt
  ignition
  alarm
  alarmType
}


export class LastLocation {
  placa
  marcaModelo
  cor
  tipo
  status
  dateLocation
  latitude
  longitude
  velocidade
  imei
  comando
  battery
  gps
  gsm
  ignition
  satelites
  endereco
}


export class ConsumoPorVeiculo{
 id
 placa
 marcaModelo
 qtdViagens
 distanciaPercorrida
 distanciaHodometro
 litros
 kml
 kml2
 erro
}

export class RelatorioFrota {
  id
  placa
  motorista
  ano
  mes
  data
  destino
  estado
  qtdViagens
  diasViagens
  qtdCidades
  qtdClientes
  valorDaCarga
  pesoDaCarga
  despesaEstiva
  despesaDiaria
  despesaOutras
  despesaCombustivel
  despesaTotal
  distanciaPercorrida
  litros
  kml
  valorDevolucao
  marcaModelo
}


export class RelatorioMensalDespesa {
   ano
   mes
   data
   carga
   combustivel
   manutencao
   estivas
   diarias
   ipva
   transito
   trabalhistas
   outras
   despesa
}

export class RelatorioUsoIndevidoParam {
  cliente
  velocidade
  segundaInicio: Date =  new Date(1970,1,1,6,0,0,0);
  segundaFim: Date =     new Date(1970,1,1,18,0,0,0);
  tercaInicio: Date =  new Date(1970,1,1,6,0,0,0);
  tercaFim: Date =     new Date(1970,1,1,18,0,0,0);
  quartaInicio: Date =  new Date(1970,1,1,6,0,0,0);
  quartaFim: Date =     new Date(1970,1,1,18,0,0,0);
  quintaInicio: Date =  new Date(1970,1,1,6,0,0,0);
  quintaFim: Date =     new Date(1970,1,1,18,0,0,0);
  sextaInicio: Date =  new Date(1970,1,1,6,0,0,0);
  sextaFim: Date =     new Date(1970,1,1,18,0,0,0);
  sabadoInicio: Date =  new Date(1970,1,1,6,0,0,0);
  sabadoFim: Date =     new Date(1970,1,1,18,0,0,0);
  domingoInicio: Date =  new Date(1970,1,1,6,0,0,0);
  domingoFim: Date =     new Date(1970,1,1,18,0,0,0);
  inicio: Date
  fim: Date
  orderBy: string
  veiculo: Veiculo
}


export class Chart {
  title
  type
  data
  columnNames
  options
}
