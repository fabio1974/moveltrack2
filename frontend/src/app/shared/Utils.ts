import * as moment from 'moment';

export const FORMAT_TIME_STAMP = "YYYY-MM-DD HH:mm:ss"
export const FORMAT_DATE = "DD/MM/YYYY"
export const FORMAT_DATE_YY = "DD/MM/YY"
export const FORMAT_DATE_TIME_MM = "DD/MM/YYYY HH:mm"
export const FORMAT_DATE_TIME_YY = "DD/MM/YY HH:mm"

export function dateToStringFormat(date,format){
  return date? moment(date).format(format):null
}
