import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ToastModule} from 'primeng/toast';
import {HttpClientModule} from '@angular/common/http';
import {DialogModule} from 'primeng/dialog';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SliderModule} from 'primeng/slider';
import {ButtonModule} from 'primeng/button';
import {DateFormatPipe} from './date-format.pipe';
import {TableModule} from 'primeng/table';
import {CalendarModule, DropdownModule, InputSwitchModule, ProgressSpinnerModule, ToolbarModule} from 'primeng/primeng';
import {PadStartFormatPipe} from './padStart-format.pipe';
import {CurrencyMaskModule} from 'ng2-currency-mask';
import {CURRENCY_MASK_CONFIG, CurrencyMaskConfig} from 'ng2-currency-mask/src/currency-mask.config';
import {NgxMaskModule} from 'ngx-mask';
import {InfoButtonComponent} from './info-button/info-button.component';
import {
  BsDatepickerModule,
  BsDropdownModule,
  CollapseModule,
  DatepickerModule,
  PaginationModule,
  TabsModule,
  TimepickerModule
} from 'ngx-bootstrap';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CpfValidator} from './info-button/cpf-validator';
import {AgmPolylineIcon} from './polyline-icon';
import {ConfirmComponent} from './confirm/confirm.component';
import {GoogleChartsModule} from 'angular-google-charts';
import {ChartContainerComponent} from './chart-container/chart-container.component';
import {NgSelectModule} from '@ng-select/ng-select';


export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
  align: "right",
  allowNegative: true,
  decimal: ",",
  precision: 2,
  prefix: "R$",
  suffix: "",
  thousands: "."
};



@NgModule({
  declarations: [DateFormatPipe,
    AgmPolylineIcon,
    PadStartFormatPipe,
    InfoButtonComponent,CpfValidator, ConfirmComponent, ChartContainerComponent
],

  imports: [
    BrowserAnimationsModule,
    CommonModule,
    ToastModule,
    HttpClientModule,
    DialogModule,
    FormsModule,
    SliderModule,
    ButtonModule,
    TableModule,
    ToolbarModule,
    CalendarModule,
    DropdownModule,
    CurrencyMaskModule,
    InputSwitchModule,
    NgxMaskModule.forRoot(),
    ReactiveFormsModule,
    DatepickerModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    BsDropdownModule.forRoot(),
    GoogleChartsModule.forRoot(),
    PaginationModule.forRoot(),
    NgSelectModule,
    TabsModule.forRoot(),
    ProgressSpinnerModule,
    CollapseModule.forRoot()


  ],
  exports: [
    CommonModule,
    ToastModule,
    HttpClientModule,
    DialogModule,
    FormsModule,
    SliderModule,
    ButtonModule,
    DateFormatPipe,
    PadStartFormatPipe,
    TableModule,
    ToolbarModule,
    CalendarModule,
    DropdownModule,
    CurrencyMaskModule,
    NgxMaskModule,
    InputSwitchModule,
    ReactiveFormsModule,
    InfoButtonComponent,
    BsDatepickerModule,
    DatepickerModule,
    TimepickerModule,
    AgmPolylineIcon,
    BsDropdownModule,
    ConfirmComponent,
    GoogleChartsModule,
    ChartContainerComponent,
    PaginationModule,
    NgSelectModule,
    TabsModule,
    CollapseModule

  ],

  providers:[
    { provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig }
  ]

})
export class SharedModule { }
