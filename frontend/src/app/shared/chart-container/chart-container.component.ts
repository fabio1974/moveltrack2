import {Component, Input, OnInit} from '@angular/core';
import {DialogService} from '../dialog.service';
import {Chart, SmsStatus, SmsTipo} from '../model';

@Component({
  selector: 'app-chart-container',
  templateUrl: './chart-container.component.html',
  styleUrls: ['./chart-container.component.css']
})
export class ChartContainerComponent implements OnInit {

  @Input()  chart: Chart;

  constructor(public dialogService: DialogService) { }

  ngOnInit() {
  }

}



