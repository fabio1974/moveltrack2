import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-info-button',
  templateUrl: './info-button.component.html',
  styleUrls: ['./info-button.component.css']
})
export class InfoButtonComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }

  @Input() control

}
