import {Directive} from '@angular/core';
import {AbstractControl, FormControl, NG_VALIDATORS, Validator, ValidatorFn} from '@angular/forms';


// validation function
function validateCpf(): ValidatorFn {
    return (c: AbstractControl) => {

        let isValid = c.value === '28272738880';

        if (isValid) {
            return null;
        } else {
            return {
                juriName: {
                    valid: false
                }
            };
        }
    };
}

@Directive({
    selector: '[cpfValidator][ngModel]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: CpfValidator, multi: true }
    ]
})

export class CpfValidator implements Validator {

    validator: ValidatorFn;

    constructor() {
        let v = validateCpf();
        console.log("resultado da funcao",v)
        this.validator = v
    }

    validate(c: FormControl) {
        console.log("campo",c)
        return this.validator(c);
    }
}
