import {AbstractControl, FormArray, FormGroup, ValidatorFn} from '@angular/forms';

export function ValidateCpf(control: AbstractControl) {
  let cpf = control.value
  if(!cpf)
    return {cpfInvalido:true, message: ' campo obrigatório'};
  else {
    cpf = cpf.replace(/[^0-9]+/g, "")
    if (cpf.length != 11)
      return {cpfInvalido: true, message: ' deve ter 11 digitos'};
    else if (!isValid(cpf))
      return {cpfInvalido: true, message: ' CPF inválido'};
  }
  return null;
}


export function ValidatePlaca(control: AbstractControl) {
  let placa = control.value
  if(!placa)
    return null
  else {
    placa = placa.replace(/[^A-Za-z0-9]+/g, "")
    console.log("PLACA",placa)
    if (placa.length != 7)
      return {cpfInvalido: true, message: ' deve ter 7 caracteres'};
  }
  return null;
}

export function ValidaDespesaMotorista(control: AbstractControl) {
  let placa = control.value
  if(!placa)
    return null
  else {
    placa = placa.replace(/[^A-Za-z0-9]+/g, "")
    console.log("PLACA",placa)
    if (placa.length != 7)
      return {cpfInvalido: true, message: ' deve ter 7 caracteres'};
  }
  return null;
}

export function Obrigatorio(control: AbstractControl) {
  if (!control.value)
    return {obrigatorio: true, message: ' campo obrigatório'};
}



export function VeiculoObrigatorio(categoria: AbstractControl) {
  if (categoria.value == 'VEICULO' )
    return {obrigatorio: true, message: ' campo obrigatório'};
}

export function MotoristaObrigatorio(categoria: AbstractControl) {
  if (categoria.value == 'MOTORISTA' )
    return {obrigatorio: true, message: ' campo obrigatório'};
}




export function TamanhoMinimo(tamanho:number): ValidatorFn {
  return (control => {
    const campo = control.value
    if (!campo)
      return {cpfInvalido: true, message: ' campo obrigatório'};
    else if (campo.toString().length < tamanho)
      return {cpfInvalido: true, message: ` deve ter ${tamanho} caracteres`};
  })
}

export function TamanhoMaximo(tamanho:number): ValidatorFn {
  return (control => {
    const campo = control.value
    if (!campo)
      return {cpfInvalido: true, message: ' campo obrigatório'};
    else if (campo.toString().length > tamanho)
      return {cpfInvalido: true, message: ` deve ter máx. ${tamanho} caracteres`};
  })
}

function isValid(strCPF) {
  var Soma;
  var Resto;
  Soma = 0;
  if (strCPF == "00000000000") return false;

  for (let i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
  Resto = (Soma * 10) % 11;

  if ((Resto == 10) || (Resto == 11))  Resto = 0;
  if (Resto != parseInt(strCPF.substring(9, 10)) ) return false;

  Soma = 0;
  for (let i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
  Resto = (Soma * 10) % 11;

  if ((Resto == 10) || (Resto == 11))  Resto = 0;
  if (Resto != parseInt(strCPF.substring(10, 11) ) ) return false;
  return true;
}


/**
 * Iterates over a FormGroup or FormArray and mark all controls as
 * touched, including its children.
 *
 * @param {(FormGroup | FormArray)} rootControl - Root form
 * group or form array
 * @param {boolean} [visitChildren=true] - Specify whether it should
 * iterate over nested controls
 */
export function markControlsAsTouched(rootControl: FormGroup | FormArray, visitChildren: boolean = true) {
  let stack: (FormGroup | FormArray)[] = [];
  // Stack the root FormGroup or FormArray
  if (rootControl && (rootControl instanceof FormGroup || rootControl instanceof FormArray)) {
    stack.push(rootControl);
  }
  while (stack.length > 0) {
    let currentControl = stack.pop();
    (<any>Object).values(currentControl.controls).forEach((control) => {
      // If there are nested forms or formArrays, stack
      // them to visit later
      if ((control instanceof FormGroup ||  control instanceof FormArray) &&  visitChildren) {
        stack.push(control);
      } else {
        control.markAsTouched();
      }
    });
  }
}

export function setAllDisabled(rootControl: FormGroup | FormArray,disabled: boolean = true,visitChildren: boolean = true) {
  let stack: (FormGroup | FormArray)[] = [];
  // Stack the root FormGroup or FormArray
  if (rootControl && (rootControl instanceof FormGroup || rootControl instanceof FormArray)) {
    stack.push(rootControl);
  }
  while (stack.length > 0) {
    let currentControl = stack.pop();
    (<any>Object).values(currentControl.controls).forEach((control) => {
      // If there are nested forms or formArrays, stack
      // them to visit later
      if ((control instanceof FormGroup ||  control instanceof FormArray) &&  visitChildren) {
        stack.push(control);
      } else {
        if(disabled)
          control.disable();
        else
          control.enable();
      }
    });
  }
}

