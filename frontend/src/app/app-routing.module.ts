import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './core/layout/layout.component';
import { LoginComponent } from './seguranca/login/login.component';
import { HomeComponent } from './core/home/home.component';


import {AuthGuard} from './seguranca/auth.guard';
import {ListarBoletosComponent} from './boleto/listar-boletos/listar-boletos.component';
import {CriarBoletoComponent} from './boleto/criar-boletos/criar-boleto.component';
import {ListarContratosComponent} from './contrato/listar-contratos/listar-contratos.component';
import {ListarCarnesComponent} from './carne/listar-carnes/listar-carnes.component';
import {MapaVeiculoComponent} from './mapas/mapa-veiculo/mapa-veiculo.component';
import {ListarViagensComponent} from './viagem/listar-viagens/listar-viagens.component';
import {ListarDespesaFrotaComponent} from './despesa-frota/listar-despesa-frota/listar-despesa-frota.component';
import {CriarDespesaFrotaComponent} from './despesa-frota/criar-despesa-frota/criar-despesa-frota.component';
import {ListarMotoristasComponent} from './motoristas/listar-motoristas/listar-motoristas.component';
import {ListarVeiculosComponent} from './veiculos/listar-veiculos/listar-veiculos.component';
import {RelatorioViewComponent} from './relatorios/relatorio-view/relatorio-view.component';
import {CriarVeiculoComponent} from './veiculos/criar-veiculo/criar-veiculo.component';
import {CriarMotoristaComponent} from './motoristas/criar-motorista/criar-motorista.component';
import {CriarViagensComponent} from './viagem/criar-viagens/criar-viagens.component';
import {PreventRefreshGuard} from './seguranca/prevent-refresh.guard';
import {CriarContratoComponent} from './contrato/criar-contrato/criar-contrato.component';
import {ListarEquipamentoComponent} from './equipamento/listar-equipamento/listar-equipamento.component';
import {ListarChipComponent} from './chip/listar-chip/listar-chip.component';
import {CriarEquipamentoComponent} from './equipamento/criar-equipamento/criar-equipamento.component';
import {CriarChipComponent} from './chip/criar-chip/criar-chip.component';
import {ListarOrdemDeServicoComponent} from './ordem-de-servico/listar-ordem-de-servico/listar-ordem-de-servico.component';
import {ListarLancamentoComponent} from './lancamento/listar-lancamento/listar-lancamento.component';
import {CriarOrdemDeServicoComponent} from './ordem-de-servico/criar-ordem-de-servico/criar-ordem-de-servico.component';
import {CriarLancamentoComponent} from './lancamento/criar-lancamento/criar-lancamento.component';
import {RotasComponent} from './mapas/rotas/rotas.component';
import {MapaFrotaComponent} from './mapas/mapa-frota/mapa-frota.component';
import {HomePageComponent} from './core/home-page/home-page.component';
import {AtualizarCarneComponent} from './carne/atualizar-carne/atualizar-carne.component';
import {MainTabsComponent} from './relatorios-internos/main-tabs/main-tabs.component';
import {EmpregadoListarComponent} from './empregado/empregado-listar/empregado-listar.component';
import {EmpregadoCriarComponent} from './empregado/empregado-criar/empregado-criar.component';
import {ControleComponent} from './controle/controle/controle.component';
import {PermissoesComponent} from './seguranca/permissoes/permissoes.component';
import {SenhaComponent} from './seguranca/senha/senha.component';



const routes: Routes = [
  {
    path: '',
    redirectTo: 'homePage',
    pathMatch: 'full'
  },

  {
    path: 'homePage',
    component: HomePageComponent,
  },


  {
    path: 'login',
    component: LoginComponent
  },

  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'home',
        component: HomeComponent,
        canActivate: [AuthGuard]

      },

      {
        path: 'listarDespesaFrotas',
        component: ListarDespesaFrotaComponent,
        canActivate: [AuthGuard],
        data:{expectedRoles: ['MENU_DESPESA_DE_FROTA']}
      },

      {
        path: 'listarVeiculos',
        component: ListarVeiculosComponent,
        canActivate: [AuthGuard],
        data:{expectedRoles: ['MENU_VEICULO']}

      },

      {
        path: 'criarVeiculo',
        component: CriarVeiculoComponent,
        canActivate: [PreventRefreshGuard],
        data:{expectedRoles: ['VEICULO_INSERIR']}
      },

      {
        path: 'criarMotorista',
        component: CriarMotoristaComponent,
        canActivate: [PreventRefreshGuard],
        data:{expectedRoles: ['MENU_MOTORISTA']}
      },

      {
        path: 'criarViagem',
        component: CriarViagensComponent,
        canActivate: [PreventRefreshGuard],
        data:{expectedRoles: ['MENU_VIAGENS']}
      },


      {
        path: 'relatorioView',
        component: RelatorioViewComponent,
        canActivate: [AuthGuard],
        data:{expectedRoles: ['MENU_RELATORIOS_FROTA']}
      },

      {
        path: 'relatoriosInternos',
        component: MainTabsComponent,
        canActivate: [AuthGuard],
        data:{expectedRoles: ['MENU_RELATORIOS_INTERNOS']}
      },

      {
        path: 'listarMotoristas',
        component: ListarMotoristasComponent,
        canActivate: [AuthGuard],
        data:{expectedRoles: ['MENU_MOTORISTA']}
      },

      {
        path: 'empregadoListar',
        component: EmpregadoListarComponent,
        canActivate: [AuthGuard],
        data:{expectedRoles: ['MENU_EMPREGADOS']}
      },

      {
        path: 'empregadoCriar',
        component: EmpregadoCriarComponent,
        canActivate: [PreventRefreshGuard],
        data:{expectedRoles: ['MENU_EMPREGADOS']}
      },

      {
        path: 'controle',
        component: ControleComponent,
        canActivate: [AuthGuard],
        data:{expectedRoles: ['MENU_ACESSO']}
      },

      {
        path: 'criarDespesaFrota',
        component: CriarDespesaFrotaComponent,
        canActivate: [PreventRefreshGuard],
        data:{expectedRoles: ['MENU_DESPESA_DE_FROTA']}
      },

      {
        path: 'mapaVeiculo',
        component: MapaVeiculoComponent,
        canActivate: [AuthGuard],
        data:{expectedRoles: ['MENU_MAPA']}
      },

      {
        path: 'mapaFrota',
        component: MapaFrotaComponent,
        canActivate: [AuthGuard],
        data:{expectedRoles: ['MAPA_DA_FROTA']}

      },

      {
        path: 'rotas',
        component: RotasComponent,
        canActivate: [AuthGuard],
        data:{expectedRoles: ['MENU_VIAGENS']}
      },

      {
        path: 'listarViagens',
        component: ListarViagensComponent,
        canActivate: [AuthGuard],
        data:{expectedRoles: ['MENU_VIAGENS']}
      },

      {
        path: 'listarDespesaFrota',
        component: ListarDespesaFrotaComponent,
        canActivate: [AuthGuard],
        data:{expectedRoles: ['MENU_DESPESA_DE_FROTA']}
      },

      {
        path: 'listarContratos',
        component: ListarContratosComponent,
        canActivate: [AuthGuard],
        data:{expectedRoles: ['MENU_CONTRATO']}
      },

      {
        path: 'listarEquipamento',
        component: ListarEquipamentoComponent,
        canActivate: [AuthGuard],
        data:{expectedRoles: ['MENU_RASTREADOR']}
      },

      {
        path: 'listarChip',
        component: ListarChipComponent,
        canActivate: [AuthGuard],
        data:{expectedRoles: ['MENU_CHIP']}
      },

      {
        path: 'listarOrdemDeServico',
        component: ListarOrdemDeServicoComponent,
        canActivate: [AuthGuard],
        data:{expectedRoles: ['MENU_OS']}
      },

      {
        path: 'listarLancamento',
        component: ListarLancamentoComponent,
        canActivate: [AuthGuard],
        data:{expectedRoles: ['MENU_OS']}
      },

      {
        path: 'criarContrato',
        component: CriarContratoComponent,
        canActivate: [PreventRefreshGuard],
        data:{expectedRoles: ['CONTRATO_INSERIR']}
      },

      {
        path: 'senha',
        component: SenhaComponent,
        canActivate: [PreventRefreshGuard],
        data:{expectedRoles: ['SENHA_PROPRIA','SENHA_QUALQUER']}
      },

      {
        path: 'permissoes',
        component: PermissoesComponent,
        canActivate: [PreventRefreshGuard],
        data:{expectedRoles: ['MENU_ACESSO']}
      },


      {
        path: 'criarOrdemDeServico',
        component: CriarOrdemDeServicoComponent,
        canActivate: [PreventRefreshGuard],
        data:{expectedRoles: ['OS_INSERIR']}
      },

      {
        path: 'criarLancamento',
        component: CriarLancamentoComponent,
        canActivate: [PreventRefreshGuard],
        data:{expectedRoles: ['MENU_OS']}
      },

      {
        path: 'criarEquipamento',
        component: CriarEquipamentoComponent,
        canActivate: [PreventRefreshGuard],
        data:{expectedRoles: ['RASTREADOR_INSERIR']}
      },

      {
        path: 'criarChip',
        component: CriarChipComponent,
        canActivate: [PreventRefreshGuard],
        data:{expectedRoles: ['CHIP_INSERIR_ATUALIZAR']}
      },

      {
        path: 'listarCarnes',
        component: ListarCarnesComponent,
        canActivate: [AuthGuard],
        data:{expectedRoles: ['FATURA_INSERIR']}
      },

      {
        path: 'atualizarCarne',
        component: AtualizarCarneComponent,
        canActivate: [PreventRefreshGuard],
        data:{expectedRoles: ['FATURA_INSERIR']}
      },

      {
        path: 'listarBoletos',
        component: ListarBoletosComponent,
        canActivate: [AuthGuard],
        data:{expectedRoles: ['FATURA_INSERIR']}
      },

      {
        path: 'criarBoleto',
        component: CriarBoletoComponent,
        canActivate: [PreventRefreshGuard],
        data:{expectedRoles: ['FATURA_INSERIR']}
      }

    ]
  },
  {
    path: '**',
    redirectTo: 'home'
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
