import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CriarVeiculoComponent } from './criar-veiculo/criar-veiculo.component';
import { ListarVeiculosComponent } from './listar-veiculos/listar-veiculos.component';
import {SharedModule} from '../shared/shared.module';
import {SegurancaService} from '../seguranca/seguranca.service';
import {AutoCompleteModule} from 'primeng/primeng';

@NgModule({
  declarations: [CriarVeiculoComponent, ListarVeiculosComponent],
  imports: [
    CommonModule,
    SharedModule,
    AutoCompleteModule
  ],
  providers:[
    SegurancaService
  ]
})
export class VeiculosModule { }
