import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Cliente, Contrato, Equipamento, Operacao, Page, Veiculo, VeiculoStatus} from '../shared/model';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {DialogService} from '../shared/dialog.service';
import {SegurancaService} from '../seguranca/seguranca.service';

@Injectable({
  providedIn: 'root'
})
export class VeiculoService {

  veiculo
  operacao = Operacao.SHOW
  filtro: Veiculo = new Veiculo()

  page = new Page()


  constructor(private http: HttpClient,
              private router: Router,
              private segurancaService: SegurancaService,
              private dialogService:DialogService) {
    this.filtro.equipamento = new Equipamento()
    this.filtro.equipamento.atrasoGmt = 100
    this.page.pageSize = 10
    if(this.segurancaService.isCliente()) {
      this.filtro.contrato.cliente = this.segurancaService.pessoaLogada() as Cliente
      this.filtro.status = VeiculoStatus.ATIVO

    }

    this.listarPagina();
  }

  getContratoByCliente(cliente: Cliente)   {
    let contrato = new Contrato()
    contrato.cliente = cliente
  }

  listarPagina() {
    this.dialogService.showWaitDialog("...processando. Aguarde!")
    this.getVeiculos(this.filtro)
      .subscribe(
        resp => {
          this.dialogService.closeWaitDialog()
          this.page.list = ((resp as any).content) as Veiculo[];
          this.page.totalRecords = (resp as any).totalElements;
        },
        error =>{
          this.dialogService.closeWaitDialog()
          this.dialogService.showDialog(error.statusText,error.message)
        }
      );
  }


  goCreate(contrato: Contrato = null){
    this.operacao = Operacao.CREATE
    this.veiculo = new Veiculo()
    this.veiculo.contrato = contrato
    this.router.navigate(["/criarVeiculo"])
  }

  goUpdate(veiculo){
    this.veiculo = veiculo
    this.operacao = Operacao.UPDATE
    this.router.navigate(["/criarVeiculo"])
  }

  goShow(veiculo){
    this.veiculo = veiculo
    this.operacao = Operacao.SHOW
    this.router.navigate(["/criarVeiculo"])
  }

  goList(){
    this.listarPagina()
    this.router.navigate(["/listarVeiculos"])
  }

  getVeiculos(filtro: Veiculo): Observable<any>  {
    const query = `pageIndex=${this.page.pageIndex-1}&pageSize=${this.page.pageSize}&sortField=${this.page.sortField}&sortDirection=${this.page.sortDirection}`;
    return this.http.patch(`${environment.apiUrl}/veiculos?${query}`,filtro);
  }

  postVeiculo(veiculo) {
    return this.http.post(`${environment.apiUrl}/veiculos`,veiculo);
  }

  putVeiculo(veiculo) {
    return this.http.put(`${environment.apiUrl}/veiculos/${veiculo.id}`,veiculo);
  }

}
