import {Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {Contrato, Equipamento, Operacao, Veiculo} from '../../shared/model';
import {SearchService} from '../../shared/search.service';
import {markControlsAsTouched, Obrigatorio, setAllDisabled, ValidatePlaca} from '../../shared/formUtils';
import {VeiculoService} from '../veiculo.service';
import {DialogService} from '../../shared/dialog.service';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {bsDatepickerConfig} from '../../shared/bsDateTimePickerConfig';

@Component({
  selector: 'app-criar-veiculo',
  templateUrl: './criar-veiculo.component.html',
  styleUrls: ['./criar-veiculo.component.css']
})
export class CriarVeiculoComponent implements OnInit {

  veiculo

  constructor(private fb: FormBuilder,
              public searchServices: SearchService,
              public veiculoService: VeiculoService,
              private dialogService: DialogService,
              private segurancaService: SegurancaService
  )
  {
    this.veiculo = this.veiculoService.veiculo || new Veiculo()
    this.formGroup.setValue(this.veiculo)

    setAllDisabled(this.formGroup,)

    if(this.veiculoService.operacao==Operacao.CREATE)
      setAllDisabled(this.formGroup,false)
    else if(this.veiculoService.operacao==Operacao.SHOW)
      setAllDisabled(this.formGroup,true)
    else if(this.veiculoService.operacao==Operacao.UPDATE){
      if(!segurancaService.temPermissao('VEICULO_INSERIR')) {
        setAllDisabled(this.formGroup, true)
        this.formGroup.controls.consumoCombustivel.enable()
      }else{
        setAllDisabled(this.formGroup, false)
      }
    }
  }

  formGroup = this.fb.group({
    contrato: [,Obrigatorio],
    placa: [,[ValidatePlaca]],
    id: [],
    marcaModelo: [,Obrigatorio],
    motorista: [],
    descricao: [],
    velocidadeMaxima: [],
    equipamento: [],
    cor: [,Obrigatorio],
    dataInstalacao: [,Obrigatorio],
    dataDesinstalacao: [],
    instalador: [,Obrigatorio],
    botaoPanico: [],
    demo: [],
    comEscuta: [],
    comCercaVirtual: [],
    comBloqueio: [],
    consumoCombustivel: [],
    tipo: [,Obrigatorio],
    status: ['ATIVO',Obrigatorio],
    lastLocation:[]
  });


  ngOnInit() {

  }

  submitForm() {

    console.log("VEICULO",this.formGroup.value)

    if(this.formGroup.invalid) {
      markControlsAsTouched(this.formGroup)
      this.dialogService.showDialog("Atenção","Seu formulário contém erros")
      return
    }

    setAllDisabled(this.formGroup,false)
    let veiculo = this.formGroup.value as Veiculo
    if(veiculo.placa){
      veiculo.placa = veiculo.placa.toUpperCase()
      if(veiculo.placa.indexOf('-')==-1)
        veiculo.placa = `${veiculo.placa.substr(0,3)}-${veiculo.placa.substr(3,7)}`
    }

    this.dialogService.showWaitDialog("...aguarde, Processando")
    this.veiculoService.postVeiculo(veiculo).subscribe(
      resp=>{
        this.dialogService.closeWaitDialog()
        this.veiculoService.goList()
      },error1 => {
        let {error,message} = error1.error
        this.dialogService.closeWaitDialog()
        if(message.toString().indexOf("ConstraintViolationException")!=-1)
          message = `Placa ou Rastreador já existe! Faça uma consulta pra checar ambos`
        this.dialogService.showDialog("Erro",message)
      })
  }


  bsConfig =bsDatepickerConfig;


}
