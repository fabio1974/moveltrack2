import {Component, HostListener, OnInit} from '@angular/core';
import {DialogService} from '../../shared/dialog.service';
import {Router} from '@angular/router';
import {SearchService} from '../../shared/search.service';
import {DespesaFrotaService} from '../../despesa-frota/despesa-frota.service';
import {AtivoInativo, Cliente, Contrato, Page, Veiculo} from '../../shared/model';
import {LazyLoadEvent} from 'primeng/api';
import {VeiculoService} from '../veiculo.service';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {MapaDataService} from '../../mapas/mapa-veiculo/mapa-data.service';
import {PageChangedEvent} from 'ngx-bootstrap';
import {EquipamentoService} from '../../equipamento/equipamento.service';

@Component({
  selector: 'app-listar-veiculos',
  templateUrl: './listar-veiculos.component.html',
  styleUrls: ['./listar-veiculos.component.css']
})
export class ListarVeiculosComponent  {

  constructor(
    public veiculoService: VeiculoService,
    private dialogService: DialogService,
    private router: Router,
    private searchService: SearchService,
    private despesaFrotaService: DespesaFrotaService,
    public segurancaService: SegurancaService,
    private mapDataService: MapaDataService,
    public equipamentoService: EquipamentoService
  ) {

  }

  tipos  = [
    {label: 'AUTOMOVEL', value: 'AUTOMOVEL'},
    {label: 'MOTOCICLETA', value: 'MOTOCICLETA'},
    {label: 'CAMINHAO', value: 'CAMINHAO'},
    {label: 'TRATOR', value: 'TRATOR'},
    {label: 'PICKUP', value: 'PICKUP'},
  ];
  statuses = AtivoInativo;


  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvents(event: KeyboardEvent) {
    console.log('event.key', event.key)
    switch (event.key) {
      case 'Enter':
        event.preventDefault();
        this.filtrar()
        break;
    }
  }


  filtrar() {
    this.veiculoService.page.pageIndex=1
    this.veiculoService.listarPagina()
  }

  changePageSize() {
    this.veiculoService.page.pageIndex=1
    this.veiculoService.listarPagina()
  }

  pageChanged($event: PageChangedEvent) {
    if(this.veiculoService.page.pageIndex != $event.page){
      this.veiculoService.page.pageIndex = $event.page;
      this.veiculoService.listarPagina();
    }
  }


}
