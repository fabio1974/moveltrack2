import {Component} from '@angular/core';
import {Boleto, Empregado, MBoletoStatus, Operacao} from '../../shared/model';
import {FormBuilder} from '@angular/forms';
import {BoletoService} from '../boleto.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DialogService} from '../../shared/dialog.service';
import {SearchService} from '../../shared/search.service';
import {markControlsAsTouched, Obrigatorio, setAllDisabled} from '../../shared/formUtils';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {DatepickerConfigService} from '../../shared/bsDateTimePickerConfig';


@Component({
  selector: 'app-criar-boletos',
  templateUrl: './criar-boleto.component.html',
  styleUrls: ['./criar-boleto.component.css']
})
export class CriarBoletoComponent {

  constructor(
    private fb: FormBuilder,
    private searchServices: SearchService,
    private segurancaService: SegurancaService,
    public boletoService: BoletoService,
    private route: ActivatedRoute,
    private router: Router,
    private dialogService: DialogService,
    public searchService: SearchService,
    public datepickerConfigService: DatepickerConfigService
  ) {

    this.formGroup.setValue(this.boletoService.boleto)
    setAllDisabled(this.formGroup,true  )
    if(this.boletoService.operacao==Operacao.UPDATE)
      this.formGroup.controls.situacao.enable()

    if(this.boletoService.operacao==Operacao.CREATE) {
      this.formGroup.controls.contrato.enable()
      this.formGroup.controls.valor.enable()
      this.formGroup.controls.dataVencimento.enable()
      this.formGroup.controls.mensagem34.enable()
    }
  }

  formGroup = this.fb.group({
    id:[],
    dataVencimento:[],
    dataBase:[],
    dataPagamento:[],
    dataRegistroPagamento:[],
    valor:[,Obrigatorio],
    multa:[],
    juros:[],
    situacao:[],
    tipoDeCobranca:[],
    tipo:[],
    dataEmissao:[],
    mensagem34:[,Obrigatorio],
    observacao:[],
    nossoNumero:[],
    carne:[],
    contrato:[,Obrigatorio],
    emissor:[],
    iugu:[]
  });



  submitForm() {

    if(this.formGroup.invalid) {
      markControlsAsTouched(this.formGroup)
      this.dialogService.showDialog("Atenção","Seu formulário contém erros")
      return
    }

    setAllDisabled(this.formGroup,false)
    let boleto = this.formGroup.value as Boleto

    if(boleto.situacao==MBoletoStatus.PAGAMENTO_EFETUADO && !boleto.dataRegistroPagamento){
      this.dialogService.showDialog("Atenção","É necessário informar a data do pagamento!")
      return
    }

    boleto.emissor = this.segurancaService.pessoaLogada() as Empregado

    this.dialogService.showWaitDialog("...aguarde, Processando")

    if(this.boletoService.operacao == Operacao.CREATE) {
    this.boletoService.postBoleto(boleto).subscribe(
      resp=>{
        this.dialogService.closeWaitDialog()
        this.boletoService.goList()
      },error1 => {
        let {error,message} = error1.error
        this.dialogService.closeWaitDialog()
        this.dialogService.showDialog(error,message)
      })
    }else if(this.boletoService.operacao == Operacao.UPDATE) {
      this.dialogService.showWaitDialog("...processando. Aguarde!")
      this.boletoService.putBoleto(boleto).subscribe(
        resp => {
          this.dialogService.closeWaitDialog()
          this.dialogService.showDialog("Ok","Boleto Alterado")
          this.boletoService.goList()
        },
        error1 => {
          this.dialogService.closeWaitDialog()
          this.dialogService.showDialog("Erro","Na alteração do Boleto")
          console.log("error", error1)
        }
      )
    }
  }

  listarBoletos() {
    this.router.navigate(['/listarBoletos']);
  }

  changeStatus() {
    if(this.formGroup.controls.situacao.value=='PAGAMENTO_EFETUADO') {
      this.formGroup.controls.dataRegistroPagamento.enable()
    }else {
      this.formGroup.controls.dataRegistroPagamento.setValue(null)
      this.formGroup.controls.dataRegistroPagamento.disable()
    }
  }

}
