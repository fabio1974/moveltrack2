import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Boleto, Contrato, MBoletoStatus, Operacao, Page} from '../shared/model';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';
import {DialogService} from '../shared/dialog.service';
import {SearchService} from '../shared/search.service';


@Injectable({
  providedIn: 'root'
})
export class BoletoService {


  boleto
  operacao = Operacao.SHOW
  filtro: Boleto = new Boleto()
  page:Page<Boleto> = new Page();
  statuses: MBoletoStatus[]



  constructor(private http: HttpClient,
              private router: Router,
              private dialogService: DialogService,
              private searchService: SearchService
              ) {

    this.page.pageSize = 20
    this.page.sortField = 'id'
    this.statuses =  Object.values(MBoletoStatus);
    this.listarPagina();

  }


  listarPagina() {
    this.dialogService.showWaitDialog("...processando. Aguarde!")
    console.log("Filtro",this.filtro)
    this.getBoletos(this.page,this.filtro)
      .subscribe(
        resp => {
          this.dialogService.closeWaitDialog()
          this.page.list = (resp as any).content;
          this.page.totalRecords = (resp as any).totalElements;
        },
        error =>{
          this.dialogService.closeWaitDialog()
          this.dialogService.showDialog(error.statusText,error.message)
        }
      );
  }


  sortBy(field,direction) {
    this.page.sortField = field
    this.page.sortDirection = direction
    this.page.pageIndex=1
    this.listarPagina()
    this.page.sortDirection = direction
  }


  goCreate(){
    //this.filtro = new Boleto()

    this.boleto = new Boleto()
    this.boleto.situacao = MBoletoStatus.EMITIDO;
    this.boleto.tipoDeCobranca = 'COM_REGISTRO';
    this.boleto.tipo= 'AVULSO';
    this.boleto.dataEmissao=  new Date();

    this.operacao = Operacao.CREATE
    this.router.navigate(["/criarBoleto"])
  }

  goUpdate(boleto){
    this.boleto = boleto
    this.operacao = Operacao.UPDATE
    this.router.navigate(["/criarBoleto"])
  }

  goShow(boleto){
    this.boleto = boleto
    this.operacao = Operacao.SHOW
    this.router.navigate(["/criarBoleto"])
  }

  goList(){
    this.listarPagina()
    this.router.navigate(["/listarBoletos"])
  }

  getBoletos(page: Page<Boleto>, filtro: Boleto): Observable<any>  {
    const query = `pageIndex=${page.pageIndex-1}&pageSize=${page.pageSize}&sortField=${page.sortField}&sortDirection=${page.sortDirection}`;
    return this.http.patch(`${environment.apiUrl}/boletos?${query}`,filtro);
  }

  postBoleto(boleto) {
    return this.http.post(`${environment.apiUrl}/boletos`,boleto);
  }

  getPdfFile(boleto: any) : Observable<any>  {
    return this.http.get(`${environment.apiUrl}/getBoletoPdf/${boleto.id}`, {responseType: 'json' });
  }

  putBoleto(boleto) {
    return this.http.put(`${environment.apiUrl}/boletos/${boleto.id}`,boleto);
  }




  /*  goCreate(){
      this.boleto = new Boleto()
      this.boleto.dataEmissao =  new Date()
      this.boleto.dataVencimento =  new Date()
      this.boleto.contrato = new Contrato()
      this.boleto.tipoDeCobranca = 'COM_REGISTRO'
      this.operacao = Operacao.CREATE
      this.router.navigate(["/criarBoletos"])
    }

    goListar(){
      this.router.navigate(["/listarBoletos"])
    }

    goUpdate(boleto){
      this.boleto = boleto
      this.operacao = Operacao.UPDATE
      this.router.navigate(["/criarBoletos"])
    }

    goShow(boleto){
      this.boleto = boleto
      this.operacao = Operacao.SHOW
      this.router.navigate(["/criarBoletos"])
    }


    getBoletos(page: Page, filtro: Boleto): Observable<any>  {
        const query = `pageIndex=${page.pageIndex}&pageSize=${page.pageSize}&sortField=${page.sortField}&sortDirection=${page.sortDirection}`;
        return this.http.patch(`${environment.apiUrl}/boletos?${query}`,filtro);
    }

    postBoleto(boleto) {
      console.log("Boleto",boleto)
      return this.http.post(`${environment.apiUrl}/boletos`,boleto);
    }

    putBoleto(boleto) {
      return this.http.put(`${environment.apiUrl}/boletos/${boleto.id}`,boleto);
    }
  1

  */
}
