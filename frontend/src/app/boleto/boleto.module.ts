import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListarBoletosComponent} from './listar-boletos/listar-boletos.component';

import {DialogService} from '../shared/dialog.service';
import {BoletoService} from './boleto.service';
import {SharedModule} from '../shared/shared.module';
import {AutoCompleteModule} from 'primeng/primeng';
import {RouterModule} from '@angular/router';
import {SearchService} from '../shared/search.service';
import {PdfViewerModule} from 'ng2-pdf-viewer';
import {CriarBoletoComponent} from './criar-boletos/criar-boleto.component';

@NgModule({
  declarations: [ListarBoletosComponent, CriarBoletoComponent],
  imports: [
    CommonModule,
    SharedModule,
    AutoCompleteModule,
    RouterModule,
    PdfViewerModule

  ],
  providers: [
    BoletoService,
    DialogService,
    SearchService
  ]
})
export class BoletoModule { }

