import {Component, HostListener, OnInit} from '@angular/core';
import {DialogService} from '../../shared/dialog.service';
import {LazyLoadEvent} from 'primeng/api';
import {BoletoService} from '../boleto.service';
import {Router} from '@angular/router';
import {Contrato, Boleto, Operacao, Page, Cliente, MBoletoStatus} from '../../shared/model';
import {environment} from '../../../environments/environment';
import {SearchService} from '../../shared/search.service';
import {PageChangedEvent} from 'ngx-bootstrap';




@Component({
  selector: 'app-listar-boletos',
  templateUrl: './listar-boletos.component.html',
  styleUrls: ['./listar-boletos.component.css']
})
export class ListarBoletosComponent  {


  constructor(
    public boletoService: BoletoService,
    public searchService: SearchService,
    private dialogService: DialogService
  ) {}


  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvents(event: KeyboardEvent) {
    console.log('event.key', event.key)
    switch (event.key) {
      case 'Enter':
        event.preventDefault();
        this.filtrar()
        break;
    }
  }


  filtrar() {
    this.boletoService.page.pageIndex=1
    this.boletoService.listarPagina()
  }

  changePageSize() {
    this.boletoService.page.pageIndex=1
    this.boletoService.listarPagina()
  }

  pageChanged($event: PageChangedEvent) {
    if(this.boletoService.page.pageIndex != $event.page){
      this.boletoService.page.pageIndex = $event.page;
      this.boletoService.listarPagina();
    }
  }


  showBoletoPdf(boleto, download: HTMLAnchorElement) {
       this.boletoService.getPdfFile(boleto).subscribe(
         data => {
           console.log("DATA",data)
           download.href = 'data:application/octet-stream;base64,'+data
           download.download = `${boleto.contrato.cliente.nome}.pdf`
           download.click()
         },
         error=> {
           console.log(error)//error
         })
  }


  showNumeroIugu(boleto: Boleto) {
    this.dialogService.showDialog("Número Iugu",boleto.iugu?boleto.iugu.invoiceId: "Não disponível para este boleto")
  }
}
