import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmpregadoListarComponent } from './empregado-listar/empregado-listar.component';
import { EmpregadoCriarComponent } from './empregado-criar/empregado-criar.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PaginationModule} from 'ngx-bootstrap';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [EmpregadoListarComponent, EmpregadoCriarComponent],
  imports: [
    CommonModule,
    FormsModule,
    PaginationModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class EmpregadoModule { }
