import {Component, HostListener, OnInit} from '@angular/core';
import {EmpregadoService} from '../../empregado/empregado.service';
import {DialogService} from '../../shared/dialog.service';
import {Router} from '@angular/router';
import {SearchService} from '../../shared/search.service';
import {DespesaFrotaService} from '../../despesa-frota/despesa-frota.service';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {PageChangedEvent} from 'ngx-bootstrap';

@Component({
  selector: 'app-empregado-listar',
  templateUrl: './empregado-listar.component.html',
  styleUrls: ['./empregado-listar.component.css']
})
export class EmpregadoListarComponent {

  constructor(
    public empregadoService: EmpregadoService,
    private dialogService: DialogService,
    private router: Router,
    private searchService: SearchService,
    private despesaFrotaService: DespesaFrotaService,
    public segurancaService: SegurancaService
  ) {

  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvents(event: KeyboardEvent) {
    console.log('event.key', event.key)
    switch (event.key) {
      case 'Enter':
        event.preventDefault();
        this.filtrar()
        break;
    }
  }

  filtrar() {
    this.empregadoService.page.pageIndex=1
    this.empregadoService.listarPagina()
  }

  changePageSize() {
    this.empregadoService.page.pageIndex=1
    this.empregadoService.listarPagina()
  }

  pageChanged($event: PageChangedEvent) {
    if(this.empregadoService.page.pageIndex != $event.page){
      this.empregadoService.page.pageIndex = $event.page;
      this.empregadoService.listarPagina();
    }
  }

}
