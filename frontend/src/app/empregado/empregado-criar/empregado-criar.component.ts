import { Component, OnInit } from '@angular/core';
import {EmpregadoService} from '../../empregado/empregado.service';
import {FormBuilder} from '@angular/forms';
import {DialogService} from '../../shared/dialog.service';
import {markControlsAsTouched, Obrigatorio, setAllDisabled, TamanhoMaximo, TamanhoMinimo, ValidateCpf} from '../../shared/formUtils';
import {Empregado, Operacao, PerfilTipo} from '../../shared/model';
import {DatepickerConfigService} from '../../shared/bsDateTimePickerConfig';
import {SearchService} from '../../shared/search.service';

@Component({
  selector: 'app-empregado-criar',
  templateUrl: './empregado-criar.component.html',
  styleUrls: ['./empregado-criar.component.css']
})
export class EmpregadoCriarComponent implements OnInit {

  constructor(public empregadoService: EmpregadoService,
              public datepickerConfigService: DatepickerConfigService,
              public searchServices: SearchService,
              private fb: FormBuilder,
              private dialogService: DialogService) { }

  ngOnInit() {
    this.formGroup.setValue(this.empregadoService.empregado)
    setAllDisabled(this.formGroup)
    if(this.empregadoService.operacao==Operacao.CREATE)
      setAllDisabled(this.formGroup,false)
    else if(this.empregadoService.operacao==Operacao.SHOW)
      setAllDisabled(this.formGroup,true)
    else if(this.empregadoService.operacao==Operacao.UPDATE){
      setAllDisabled(this.formGroup,false)
    }
  }

  formGroup = this.fb.group({
    id:[],
    cpf:[,[ValidateCpf]],
    cnpj:[,],
    status:[],
    nome:[,[Obrigatorio]],
    email:[,[Obrigatorio]],
    logoFile:[,],
    ultimaAlteracao:[],
    dataCadastro:[],
    dataNascimento:[,[Obrigatorio]],
    telefoneFixo:[],
    celular1:[,[Obrigatorio]],
    celular2:[,[Obrigatorio]],
    endereco:[,[Obrigatorio]],
    numero:[,[TamanhoMaximo(5)]],
    complemento:[],
    municipio:[,[Obrigatorio]],
    cep:[,[Obrigatorio]],
    bairro:[,[Obrigatorio]],
    usuario:this.fb.group({
      id:[],
      ultimoAcesso:[],
      senha:[],
      email:[],
      perfil:[],
      nomeUsuario:[,Obrigatorio],
      ativo:[],
      permissoes:[]
    }),
    salario:[]
  })


  submitForm() {

    if(this.empregadoService.operacao==Operacao.CREATE)
      this.formGroup.controls.dataCadastro.setValue(new Date())


    if(this.formGroup.invalid) {
      console.log("this.formGroup.controls",this.formGroup.controls)
      markControlsAsTouched(this.formGroup)
      this.dialogService.showDialog("Atenção","Seu formulário contém erros")
      return
    }
    //setAllDisabled(this.formGroup,false)
    let empregado = this.formGroup.value as Empregado

    this.dialogService.showWaitDialog("...aguarde, Processando")
    this.empregadoService.postEmpregado(empregado).subscribe(
      resp=>{
        this.empregadoService.empregado = empregado
        this.dialogService.closeWaitDialog()
        this.empregadoService.goList()
      },error1 => {
        let {error,message} = error1.error
        this.dialogService.closeWaitDialog()
        if(message.toString().indexOf("ConstraintViolationException")!=-1)
          message = `Esse CPF já existe no banco de dados!`
        this.dialogService.showDialog("Erro",message)
      })
  }

}
