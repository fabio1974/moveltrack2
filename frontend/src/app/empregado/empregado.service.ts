import {Injectable} from '@angular/core';
import {AtivoInativo, Empregado, Operacao, Page, Perfil, PerfilTipo} from '../shared/model';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {DialogService} from '../shared/dialog.service';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmpregadoService {

  empregado
  page:Page<Empregado> = new Page();
  operacao = Operacao.SHOW
  filtro: Empregado = new Empregado()
  statuses = AtivoInativo
  perfis: Perfil[]

  constructor(private http: HttpClient,private router: Router, private dialogService: DialogService) {
    this.filtro.status = null
    this.loadPerfis()
    this.listarPagina()
  }


  listarPagina() {
    this.dialogService.showWaitDialog("...processando. Aguarde!")
    this.getEmpregados(this.page,this.filtro)
      .subscribe(
        resp => {
          this.dialogService.closeWaitDialog()
          this.page.list = ((resp as any).content) as Empregado[];
          //console.log("list",this.page.list)
          this.page.totalRecords = (resp as any).totalElements;
        },
        error =>{
          this.dialogService.closeWaitDialog()
          this.dialogService.showDialog(error.statusText,error.message)
        }
      );
  }


  goCreate(){
    this.operacao = Operacao.CREATE
    this.empregado = new Empregado()
    this.empregado.dataCadastro = new Date()
    this.router.navigate(["/empregadoCriar"])
  }

  goUpdate(empregado){
    this.empregado = empregado
    this.empregado.usuario.perfil = this.perfis.find(it=> it.tipo == this.empregado.usuario.perfil.tipo)
    this.operacao = Operacao.UPDATE
    this.router.navigate(["/empregadoCriar"])
  }

  goShow(empregado){
    this.empregado = empregado
    this.empregado.usuario.perfil = this.perfis.find(it=> it.tipo == this.empregado.usuario.perfil.tipo)
    this.operacao = Operacao.SHOW
    this.router.navigate(["/empregadoCriar"])
  }

  goList(){
    this.listarPagina()
    this.router.navigate(["/empregadoListar"])
  }

  getEmpregados(page: Page<Empregado>, filtro: Empregado): Observable<any>  {
    const query = `pageIndex=${page.pageIndex-1}&pageSize=${page.pageSize}&sortField=${page.sortField}&sortDirection=${page.sortDirection}`;
    return this.http.patch(`${environment.apiUrl}/empregados?${query}`,filtro);
  }

  postEmpregado(empregado) {
    return this.http.post(`${environment.apiUrl}/empregados`,empregado);
  }

  putEmpregado(empregado) {
    return this.http.put(`${environment.apiUrl}/empregados/${empregado.id}`,empregado);
  }


  private loadPerfis() {
    this.http.get(`${environment.apiUrl}/perfis`).subscribe(
      resp=> this.perfis = (resp as Perfil[]).filter(it=> it.tipo!=PerfilTipo.CLIENTE_PF && it.tipo!=PerfilTipo.CLIENTE_PJ)
    )
  }
}
