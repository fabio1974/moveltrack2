import { Injectable } from '@angular/core';
import {Contrato, Operacao, Page, Chip, AtivoInativo} from '../shared/model';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {DialogService} from '../shared/dialog.service';

@Injectable({
  providedIn: 'root'
})
export class ChipService {

  chip
  page:Page<Chip> = new Page();
  operacao = Operacao.SHOW
  filtro: Chip = new Chip()

  constructor(private http: HttpClient,private router: Router, private dialogService: DialogService) {
    this.listarPagina()
  }


  listarPagina() {
    this.dialogService.showWaitDialog("...processando. Aguarde!")
    this.getChips(this.page,this.filtro)
      .subscribe(
        resp => {
          this.dialogService.closeWaitDialog()
          this.page.list = ((resp as any).content) as Chip[];
          //console.log("list",this.page.list)
          this.page.totalRecords = (resp as any).totalElements;
        },
        error =>{
          this.dialogService.closeWaitDialog()
          this.dialogService.showDialog(error.statusText,error.message)
        }
      );
  }


  goCreate(contrato: Contrato = null){
    this.operacao = Operacao.CREATE
    this.chip = new Chip()
    this.chip.contrato = contrato
    this.router.navigate(["/criarChip"])
  }

  goUpdate(chip){
    this.chip = chip
    this.operacao = Operacao.UPDATE
    this.router.navigate(["/criarChip"])
  }

  goShow(chip){
    this.chip = chip
    this.operacao = Operacao.SHOW
    this.router.navigate(["/criarChip"])
  }

  goList(){
    this.listarPagina()
    this.router.navigate(["/listarChip"])
  }

  getChips(page: Page<Chip>, filtro: Chip): Observable<any>  {
    const query = `pageIndex=${page.pageIndex-1}&pageSize=${page.pageSize}&sortField=${page.sortField}&sortDirection=${page.sortDirection}`;
    return this.http.patch(`${environment.apiUrl}/chips?${query}`,filtro);
  }

  postChip(chip) {
    return this.http.post(`${environment.apiUrl}/chips`,chip);
  }

  putChip(chip) {
    return this.http.put(`${environment.apiUrl}/chips/${chip.id}`,chip);
  }

  operadoras = [
    {label: 'OI', value: 'OI'},
    {label: 'TIM', value: 'TIM'},
    {label: 'VIVO', value: 'VIVO'},
    {label: 'LINK_TIM', value: 'LINK_TIM'},
    {label: 'LINK_CLARO', value: 'LINK_CLARO'},
    {label: 'LINK_VIVO', value: 'LINK_VIVO'},
    {label: 'VODAFONE', value: 'VODAFONE'},
    {label: 'TRANSMEET_VIVO', value: 'TRANSMEET_VIVO'},
  ];

  statuses = AtivoInativo;

}
