import { Component, OnInit } from '@angular/core';
import {ChipService} from '../chip.service';
import {markControlsAsTouched, Obrigatorio, setAllDisabled, TamanhoMinimo} from '../../shared/formUtils';
import {Chip, Operacao, Veiculo} from '../../shared/model';
import {FormBuilder} from '@angular/forms';
import {DialogService} from '../../shared/dialog.service';

@Component({
  selector: 'app-criar-chip',
  templateUrl: './criar-chip.component.html',
  styleUrls: ['./criar-chip.component.css']
})
export class CriarChipComponent implements OnInit {

  constructor(public chipService: ChipService,
              private fb: FormBuilder,
              private dialogService: DialogService) { }

  ngOnInit() {
    this.formGroup.setValue(this.chipService.chip)
    setAllDisabled(this.formGroup)
    if(this.chipService.operacao==Operacao.CREATE)
      setAllDisabled(this.formGroup,false)
    else if(this.chipService.operacao==Operacao.SHOW)
      setAllDisabled(this.formGroup,true)
    else if(this.chipService.operacao==Operacao.UPDATE){
      setAllDisabled(this.formGroup,false)
    }
  }

  formGroup = this.fb.group({
    id: [],
    numero:  [,[Obrigatorio, TamanhoMinimo(11)]],
    iccid:   [,[Obrigatorio, TamanhoMinimo(20)]],
    dataCadastro:   [,],
    operadora:   [,Obrigatorio],
    status:   [,Obrigatorio]
  })


  submitForm() {

    if(this.chipService.operacao==Operacao.CREATE)
      this.formGroup.controls.dataCadastro.setValue(new Date())


    if(this.formGroup.invalid) {
      console.log("this.formGroup.controls",this.formGroup.controls)
      markControlsAsTouched(this.formGroup)
      this.dialogService.showDialog("Atenção","Seu formulário contém erros")
      return
    }
    //setAllDisabled(this.formGroup,false)
    let chip = this.formGroup.value as Chip

    this.dialogService.showWaitDialog("...aguarde, Processando")
    this.chipService.postChip(chip).subscribe(
      resp=>{
        this.dialogService.closeWaitDialog()
        this.chipService.goList()
      },error1 => {
        let {error,message} = error1.error
        this.dialogService.closeWaitDialog()
        this.dialogService.showDialog(error,message)
      })
  }

}
