import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListarChipComponent } from './listar-chip/listar-chip.component';
import { CriarChipComponent } from './criar-chip/criar-chip.component';
import {SharedModule} from '../shared/shared.module';
import {AutoCompleteModule} from 'primeng/primeng';

@NgModule({
  declarations: [ListarChipComponent, CriarChipComponent],
  imports: [
    CommonModule,
    SharedModule,
    AutoCompleteModule
  ]
})
export class ChipModule { }
