import {Component, HostListener} from '@angular/core';
import {DialogService} from '../../shared/dialog.service';
import {Router} from '@angular/router';
import {SearchService} from '../../shared/search.service';
import {DespesaFrotaService} from '../../despesa-frota/despesa-frota.service';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {AtivoInativo} from '../../shared/model';
import {ChipService} from '../chip.service';
import {PageChangedEvent} from 'ngx-bootstrap';

@Component({
  selector: 'app-listar-chip',
  templateUrl: './listar-chip.component.html',
  styleUrls: ['./listar-chip.component.css']
})
export class ListarChipComponent  {

  constructor(
    public chipService: ChipService,
    private dialogService: DialogService,
    private router: Router,
    private searchService: SearchService,
    private despesaFrotaService: DespesaFrotaService,
    public segurancaService: SegurancaService
  ) {

  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvents(event: KeyboardEvent) {
    console.log('event.key', event.key)
    switch (event.key) {
      case 'Enter':
        event.preventDefault();
        this.filtrar()
        break;
    }
  }

  filtrar() {
    this.chipService.page.pageIndex=1
    this.chipService.listarPagina()
  }

  changePageSize() {
    this.chipService.page.pageIndex=1
    this.chipService.listarPagina()
  }

  pageChanged($event: PageChangedEvent) {
    if(this.chipService.page.pageIndex != $event.page){
      this.chipService.page.pageIndex = $event.page;
      this.chipService.listarPagina();
    }
  }

}
