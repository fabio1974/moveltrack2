import {Component, HostListener, OnInit, ViewEncapsulation} from '@angular/core';
import {CarneService} from '../../carne/carne.service';
import {DialogService} from '../../shared/dialog.service';
import {Router} from '@angular/router';
import {SearchService} from '../../shared/search.service';
import {Carne, Contrato, Page} from '../../shared/model';
import {PageChangedEvent} from 'ngx-bootstrap';

@Component({
  selector: 'app-listar-carnes',
  templateUrl: './listar-carnes.component.html',
  styleUrls: ['./listar-carnes.component.css'],
})
export class ListarCarnesComponent{

  constructor(
    public carneService: CarneService,
    private dialogService: DialogService,
    private router: Router,
    public searchService: SearchService
  ) {

  }


  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvents(event: KeyboardEvent) {
    console.log('event.key', event.key)
    switch (event.key) {
      case 'Enter':
        event.preventDefault();
        this.filtrar()
        break;
    }
  }

  showEndereco: boolean = false
  carne


  showCarnePdf(carne, download: HTMLAnchorElement) {

    this.carneService.getPdfFile(carne).subscribe(
      data => {
        download.href = 'data:application/octet-stream;base64,'+data
        download.download = `${carne.contrato.cliente.nome}.pdf`
        download.click()
      },
      error=> {
          this.dialogService.showDialog("Erro",JSON.stringify(error))
      })
  }

  showEnderecoMethod(carne) {
    console.log("carne", carne)
    Promise.resolve(null).then(() =>{
      this.carne = carne
      this.showEndereco = true
    })
  }

  pageChanged($event: PageChangedEvent) {
    this.carneService.page.pageIndex = $event.page;
    this.carneService.listarPagina();
  }


  changePageSize() {
    this.carneService.page.pageIndex = 1
    this.carneService.listarPagina()
  }

  filtrar() {
    this.carneService.page.pageIndex = 1
    this.carneService.listarPagina()
  }

}
