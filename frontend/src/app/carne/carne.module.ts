import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CriarCarneComponent } from './criar-carne/criar-carne.component';
import { ListarCarnesComponent } from './listar-carnes/listar-carnes.component';
import {SharedModule} from '../shared/shared.module';
import {AutoCompleteModule, CalendarModule, DropdownModule} from 'primeng/primeng';
import {CarneService} from './carne.service';
import {RouterModule} from '@angular/router';
import {PdfViewerModule} from 'ng2-pdf-viewer';
import { AtualizarCarneComponent } from './atualizar-carne/atualizar-carne.component';

@NgModule({
  declarations: [CriarCarneComponent, ListarCarnesComponent, AtualizarCarneComponent],
  imports: [
    CommonModule,
    SharedModule,
    DropdownModule,
    AutoCompleteModule,
    CalendarModule,
    RouterModule,
    PdfViewerModule
  ],
  exports:[
    CriarCarneComponent
  ],
  providers:[
    CarneService
  ]
})
export class CarneModule { }
