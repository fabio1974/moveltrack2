import {Component, OnInit} from '@angular/core';
import {Carne} from '../../shared/model';
import {CarneService} from '../carne.service';
import {DialogService} from '../../shared/dialog.service';
import {DatepickerConfigService} from '../../shared/bsDateTimePickerConfig';

@Component({
  selector: 'app-atualizar-carne',
  templateUrl: './atualizar-carne.component.html',
  styleUrls: ['./atualizar-carne.component.css']
})
export class AtualizarCarneComponent implements OnInit {

  carne: Carne



  constructor(
    private carneService: CarneService,
    private dialogService: DialogService,
    public datepickerConfigService: DatepickerConfigService

    ) { }

  ngOnInit() {
    this.carne = this.carneService.carne
    console.log(this.carne,this.carne)
  }

  atualizarCarne() {
    this.dialogService.showWaitDialog("Atualizando Carnê")
    this.carneService.atualizarCarne(this.carne).subscribe(
      resp=>{
        this.dialogService.closeWaitDialog()
        this.carneService.goList()
        //this.dialogService.showDialog("Ok","Carnê atualizado com sucesso")
      },
        error=>{
          this.dialogService.closeWaitDialog()
          this.dialogService.showDialog("Erro",JSON.stringify(error))
      }
    )

  }

  voltar() {
    this.carneService.goList()
  }
}
