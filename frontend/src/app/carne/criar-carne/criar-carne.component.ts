import {Component, Input, OnInit} from '@angular/core';
import {Contrato} from '../../shared/model';
import {ContratoService} from '../../contrato/contrato.service';
import {DialogService} from '../../shared/dialog.service';
import {DatepickerConfigService} from '../../shared/bsDateTimePickerConfig';

interface City {
  name: string;
  code: string;
}


@Component({
  selector: 'app-criar-carne',
  templateUrl: './criar-carne.component.html',
  styleUrls: ['./criar-carne.component.css']
})
export class CriarCarneComponent implements OnInit {


  constructor(
    public contratoService: ContratoService,
    public datepickerConfigService: DatepickerConfigService,
    public dialogService: DialogService,
  ) {
  }

  @Input() contratoSelected: Contrato ;

  quantidadeBoleto
  dataPostagem: any;
  dataReferencia: any;
  mensalidade: any;
  nome: any;
  veiculos: any;


  ngOnInit() {
    this.quantidadeBoleto = 12;

  }


  makeCarne() {
    this.dialogService.showWaitDialog("...processando. Aguarde!")
    this.contratoService.makeCarne(this.quantidadeBoleto).subscribe(
      resp=>{
        this.dialogService.closeWaitDialog()
        this.dialogService.showDialog("Ok", resp)
        this.contratoService.closeMakeCarne()
        this.contratoService.contrato.contratoGeraCarne = 'GERADO'
    },
      error=>{
        this.dialogService.closeWaitDialog()
        this.dialogService.showDialog("Erro", error.error.message)
        this.contratoService.closeMakeCarne()
    })

  }
}
