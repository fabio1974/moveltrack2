import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Contrato, Carne, Operacao, Page} from '../shared/model';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';
import {DialogService} from '../shared/dialog.service';


@Injectable({
  providedIn: 'root'
})
export class CarneService {

  carne
  operacao

  page:Page<Carne> = new Page()
  filtro = new Carne()

  constructor(private http: HttpClient,private router: Router, private dialogService: DialogService) {
    this.page.pageSize = 20
    this.listarPagina()
  }


  listarPagina() {
    this.dialogService.showWaitDialog("...processando. Aguarde!")
    this.getCarnes(this.page,this.filtro)
      .subscribe(
        resp => {
          this.dialogService.closeWaitDialog()
          this.page.list = (resp as any).content;
          this.page.totalRecords = (resp as any).totalElements;
        },
        error =>{
          this.dialogService.closeWaitDialog()
          this.dialogService.showDialog(error.statusText,error.message)
        }
      );
  }


  goCreate(){
    this.carne = new Carne()
    this.carne.dataEmissao = new Date()
    this.carne.contrato = new Contrato()
    this.carne.tipoDeCobranca = 'COM_REGISTRO'
    this.operacao = Operacao.CREATE
    this.router.navigate(["/criarCarnes"])
  }

  goUpdate(carne){
    this.carne = carne
    this.convertDates()
    this.operacao = Operacao.UPDATE
    this.router.navigate(["/atualizarCarne"])
  }

  goShow(carne){
    this.carne = carne
    this.convertDates()
    this.operacao = Operacao.SHOW
    this.router.navigate(["/criarCarnes"])
  }

  convertDates(){
    this.carne.dataEmissao = new Date(this.carne.dataEmissao)
    this.carne.dataVencimento = new Date(this.carne.dataVencimento)
    this.carne.dataRegistroPagamento = new Date(this.carne.dataRegistroPagamento)
  }

  getCarnes(page: Page<Carne>, filtro: Carne): Observable<any>  {
      const query = `pageIndex=${page.pageIndex-1}&pageSize=${page.pageSize}&sortField=${page.sortField}&sortDirection=${page.sortDirection}`;
      return this.http.patch(`${environment.apiUrl}/carnes?${query}`,filtro);
  }

  postCarne(carne) {
    return this.http.post(`${environment.apiUrl}/carnes`,carne);
  }

  atualizarCarne(carne) {
    return this.http.post(`${environment.apiUrl}/atualizarCarne`,carne);
  }
1

  getPdfFile(carne: any) : Observable<any>  {
    return this.http.get(`${environment.apiUrl}/getCarnePdf/${carne.id}`, {responseType: 'json' });
  }

  goList(){
    this.listarPagina()
    this.router.navigate(["/listarCarnes"])
  }
}
