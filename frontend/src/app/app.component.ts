import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import {Calendar} from 'primeng/primeng';

@Component({
  // tslint:disable-next-line
  selector: 'body',
  template: '<router-outlet></router-outlet>'

  //template: "<p-calendar id='dataEmissao'  dateFormat='dd/mm/yy' showTime='true' hourFormat='24' inputStyleClass='form-control'  name='dataEmissao'></p-calendar>"
})
export class AppComponent implements OnInit {
  constructor(private router: Router) { }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });
  }
}

