import { Component, OnInit } from '@angular/core';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private segurancaService: SegurancaService, private router: Router) { console.log("Aqui") }

  ngOnInit() {

    let homePage = 'listarVeiculos'
    if(this.segurancaService.temPermissao("MAPA_VER_PROPRIO"))
      homePage = 'mapaVeiculo'

    this.router.navigate([homePage])


  }

}
