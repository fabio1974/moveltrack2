import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CoreService {
  environment: any;
  constructor() {
    this.environment = environment.production
  }
}
