import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LayoutComponent} from './layout/layout.component';
import {SharedModule} from '../shared/shared.module';
import {AppAsideModule, AppFooterModule, AppHeaderModule, AppSidebarModule} from '@coreui/angular';
import {AppRoutingModule} from '../app-routing.module';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {BsDropdownModule} from 'ngx-bootstrap';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DialogService} from '../shared/dialog.service';
import {CalendarModule, Galleria, GalleriaModule, ProgressSpinnerModule} from 'primeng/primeng';
import {CoreService} from './core.service';
import { InfoButtonComponent } from '../shared/info-button/info-button.component';
import { HomePageComponent } from './home-page/home-page.component';


@NgModule({
  declarations: [
    LayoutComponent,
    HomePageComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    AppAsideModule,
    AppHeaderModule,
    AppFooterModule,
    AppSidebarModule,
    AppRoutingModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    BrowserAnimationsModule,
    ProgressSpinnerModule,
    CalendarModule,
    GalleriaModule
  ],
  exports: [
    AppRoutingModule,
/*    PerfectScrollbarModule,*/
  ],
  providers: [
    DialogService,
    CoreService
  ]

})
export class CoreModule { }
