import { Component, OnInit } from '@angular/core';
import { navItems } from '../../_nav';
import { Router } from '@angular/router';
import { routerNgProbeToken } from '@angular/router/src/router_module';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {DialogService} from '../../shared/dialog.service';
import {CoreService} from '../core.service';
import {environment} from '../../../environments/environment';


/*export function minimizeSidebar(minimize){
  this.sidebarMinimized = minimize
}*/

@Component({
  selector: 'app-dashboard',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  public navItems = navItems;
  public sidebarMinimized = false;
  private changes: MutationObserver;
  public element: HTMLElement = document.body;
  environment: any;

  constructor(
    private router: Router,
    public segurancaService: SegurancaService,
    public dialogService: DialogService,
    public coreService: CoreService,
  ) {
    this.environment = environment
  }

  isMenuPermitido(navData, roles): boolean {
    console.log(navData.name=='Mapa Veículo')
    return navData.name==='Boletos' && roles.includes('MENU_FATURA') ||
      navData.name === 'Contratos' && roles.includes('MENU_CONTRATO') ||
      navData.name === 'Chips' && roles.includes('MENU_CHIP') ||
      navData.name === 'Rastreadores' && roles.includes('MENU_RASTREADOR') ||
      navData.name === 'Viagens' && roles.includes('MENU_VIAGENS') ||
      navData.name === 'Despesas da Frota' && roles.includes('MENU_VIAGENS') ||
      navData.url === '/relatoriosInternos' && roles.includes('MENU_RELATORIOS_INTERNOS') ||
      navData.url === '/relatorioView' && roles.includes('MENU_RELATORIOS_FROTA') ||
      navData.url === '/empregadoListar' && roles.includes('MENU_EMPREGADOS') ||
      navData.name === 'Carnês' && roles.includes('MENU_CARNE') ||
      navData.url === '/mapaVeiculo' && roles.includes('MENU_MAPA') ||
      navData.url === '/mapaFrota' && roles.includes('MAPA_DA_FROTA') ||
      navData.name === 'Motoristas' && roles.includes('MENU_MOTORISTA') ||
      navData.name === 'Ordem De Serviço' && roles.includes('MENU_OS') ||
      navData.name === 'Lançamentos' && roles.includes('MENU_OS') ||
      navData.url === '/controle' && roles.includes('MENU_ACESSO') ||
      navData.name === 'Veículos' ||
      navData.name === 'Rotas' && roles.includes('MENU_VIAGENS')
  }

  ngOnInit() {

    const roles = this.segurancaService.decodedToken.roles
    this.navItems = navItems.filter(navData=>this.isMenuPermitido(navData,roles))
    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = document.body.classList.contains('sidebar-minimized');
    });

    this.changes.observe(<Element>this.element, {
      attributes: true
    });
  }



  logout(): void {
    this.router.navigate(['/login']);
  }

}
