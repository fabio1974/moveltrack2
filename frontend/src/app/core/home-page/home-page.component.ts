import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  //styleUrls: ['./home-page.component.css']
  styles: [`
    p {
      background-color: red;
    }
  `],
})
export class HomePageComponent implements OnInit {

  constructor(private router: Router) { }

  images: any[];
  isCollapsed: boolean =true;

  ngOnInit() {
    this.images = [];
    this.images.push({source:'assets/images/galleria/handshake.jpg', alt:'Temos rastreadores 100% via satélite para frotas remotas!', title:'Bem-vindo à Moveltrack!'});
    this.images.push({source:'assets/images/galleria/pc-landing.jpg', alt:'Mais leve e interativa', title:'Nova Plataforma!'});
    this.images.push({source:'assets/images/galleria/smarttrack.jpg', alt:'Monitore seu veículo 24 horas por dia com nosso aplicativo. Disponível para Android e iOS', title:'Mais tecnologia para sua segurança!'});

  }

  goLogin() {
    this.router.navigate(['/home'])
  }


}
