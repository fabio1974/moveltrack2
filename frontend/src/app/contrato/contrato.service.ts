import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Contrato, Operacao, Page, Perfil, PerfilTipo} from '../shared/model';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';
import {DialogService} from '../shared/dialog.service';
import {SegurancaService} from '../seguranca/seguranca.service';


@Injectable({
  providedIn: 'root'
})
export class ContratoService {

  contrato: Contrato = new Contrato()
  filtro: Contrato = new Contrato()
  page = new Page<Contrato>()

  operacao
  showCriaCarne: boolean;
  showCriaContrato: boolean;
  perfis: Perfil[];

  constructor(private http: HttpClient,
              private dialogService: DialogService,
              private segurancaService: SegurancaService,
              private router: Router) {

    this.filtro.contratoGeraCarne = null
    this.loadPerfis()
    this.listarPagina()
  }

  listarPagina() {

    this.dialogService.showWaitDialog("...processando. Aguarde!")
    this.getList()
      .subscribe(
        resp => {
          this.dialogService.closeWaitDialog()
          this.page.list = (resp as any).content;
          this.page.totalRecords = (resp as any).totalElements;
        },
        error =>{
          this.dialogService.closeWaitDialog()
          this.dialogService.showDialog(error.statusText,error.message)
        }
      );
  }




  goCreate(){
    this.contrato = new Contrato()
    this.operacao = Operacao.CREATE
    this.router.navigate(["/criarContrato"])
  }

  goUpdate(contrato){
    this.contrato = contrato
    this.contrato.cliente.usuario.perfil = this.perfis.find(it=> it.tipo == this.contrato.cliente.usuario.perfil.tipo)
    this.operacao = Operacao.UPDATE
    this.router.navigate(["/criarContrato"])
  }

  goShow(contrato){
    this.contrato = contrato
    this.contrato.cliente.usuario.perfil = this.perfis.find(it=> it.tipo == this.contrato.cliente.usuario.perfil.tipo)
    this.operacao = Operacao.SHOW
    this.router.navigate(["/criarContrato"])
  }




  goList(){
    this.closeCriarContrato()
    this.listarPagina()
    this.router.navigate(["/listarContratos"])
  }

  goMakeCarne(contrato){
    this.contrato = contrato
    Promise.resolve(null).then(() => {
      this.showCriaCarne = true
    })
  }


  closeCriarContrato(){
    Promise.resolve(null).then(() => {
      this.showCriaContrato = false
    })
  }

  closeMakeCarne(){
    Promise.resolve(null).then(() => {
      this.showCriaCarne = false
    })
  }

  makeCarne(quantidadeBoleto): Observable<any>{
    return this.http.post(`${environment.apiUrl}/carnes/${quantidadeBoleto}`,this.contrato);
  }


  getList(): Observable<any>  {
    const query = `pageIndex=${this.page.pageIndex-1}&pageSize=${this.page.pageSize}&sortField=${this.page.sortField}&sortDirection=${this.page.sortDirection}`;
      return this.http.patch(`${environment.apiUrl}/contratos?${query}`,this.filtro);
  }




  postContrato(contrato) {
    return this.http.post(`${environment.apiUrl}/contratos`,contrato);
  }


  private loadPerfis() {
    this.http.get(`${environment.apiUrl}/perfis`).subscribe(
      resp => {
        this.perfis = (resp as Perfil[]).filter(it => it.tipo == PerfilTipo.CLIENTE_PF || it.tipo == PerfilTipo.CLIENTE_PJ);
      },
      error=>{
        this.dialogService.showDialog("Error",error)
      }
    );
  }


}
