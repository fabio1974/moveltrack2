import { Component, OnInit } from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {SearchService} from '../../shared/search.service';
import {DialogService} from '../../shared/dialog.service';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {Cliente, Contrato, Empregado, Municipio, Operacao, Perfil, PerfilTipo, PessoaStatus, Usuario} from '../../shared/model';
import {markControlsAsTouched, Obrigatorio, setAllDisabled, TamanhoMaximo, TamanhoMinimo, ValidateCpf} from '../../shared/formUtils';
import {ContratoService} from '../contrato.service';
import {bsDatepickerConfig, DatepickerConfigService} from '../../shared/bsDateTimePickerConfig';

@Component({
  selector: 'app-criar-contrato',
  templateUrl: './criar-contrato.component.html',
  styleUrls: ['./criar-contrato.component.css']
})
export class CriarContratoComponent implements OnInit  {

  //contrato
  //bsConfig = bsDatepickerConfig;

  constructor(private fb: FormBuilder,
              public datepickerConfigService: DatepickerConfigService,
              public searchServices: SearchService,
              public contratoService: ContratoService,
              private dialogService: DialogService,
              private segurancaService: SegurancaService
  ){
    console.log("CONTRATO==>",this.contratoService.contrato)
  }

  formGroup = this.fb.group({
    cliente: this.fb.group({
      id:[],
      cpf:[,[ValidateCpf]],
      cnpj:[,[]],
      status:[],
      nome:[,[Obrigatorio]],
      email:[,[Obrigatorio]],
      logoFile:[,[Obrigatorio]],
      cerca:[],
      ultimaAlteracao:[],
      dataCadastro:[],
      dataNascimento:[,],
      telefoneFixo:[],
      celular1:[,[Obrigatorio]],
      celular2:[,[Obrigatorio]],
      endereco:[,[Obrigatorio]],
      numero:[,[Obrigatorio,TamanhoMaximo(5)]],
      complemento:[],
      municipio:[,[Obrigatorio]],
      cep:[,[Obrigatorio]],
      bairro:[,[Obrigatorio]],
      usuario:this.fb.group({
        id:[],
        ultimoAcesso:[],
        senha:[],
        email:[],
        perfil:[],
        nomeUsuario:[,Obrigatorio],
        ativo:[],
        permissoes:[]
      }),
      nomeFantasia:[,],
      observacao:[],
      emailAlarme:[],
      ultimaCobranca:[],
      ultimoLembrete:[]
    }) ,
    id: [],
    numeroContrato: [],
    inicio: [,[Obrigatorio]],
    fim: [],
    diaVencimento: [,[Obrigatorio]],
    vendedor: [,[Obrigatorio]],
    pagamentoAnual: [],
    contratoGeraCarne: [],
    contratoTipo: [,[Obrigatorio]],
    mensalidade: [,[Obrigatorio]],
    entrada: [],
    status: [,[Obrigatorio]],
    ultimaAlteracao: [],
    dataBase: [],
    veiculos: [],
    label: []
  });


  ngOnInit(): void {
    //this.contrato = this.contratoService.contratoSelected
    this.formGroup.setValue(this.contratoService.contrato)

    if(this.contratoService.operacao==Operacao.SHOW)
        setAllDisabled(this.formGroup,true)
    else{
      setAllDisabled(this.formGroup,false)
      if(this.contratoService.contrato.cliente.usuario.perfil.tipo == PerfilTipo.CLIENTE_PF){
        this.formGroup.controls.cliente['controls'].nomeFantasia.disable()
        this.formGroup.controls.cliente['controls'].cnpj.disable()
        this.formGroup.controls.cliente['controls'].cpf.enable()
      }else{
        this.formGroup.controls.cliente['controls'].nomeFantasia.enable()
        this.formGroup.controls.cliente['controls'].cnpj.enable()
        this.formGroup.controls.cliente['controls'].cpf.disable()
      }
    }
  }

  selectTipoPessoa() {

    let contrato = this.formGroup.value as Contrato

    if(contrato.cliente.usuario.perfil.tipo == PerfilTipo.CLIENTE_PF) {
      this.formGroup.controls.cliente['controls'].cnpj.disable()
      this.formGroup.controls.cliente['controls'].cnpj.setValue(null)
      this.formGroup.controls.cliente['controls'].nomeFantasia.disable()
      this.formGroup.controls.cliente['controls'].nomeFantasia.setValue(null)
      this.formGroup.controls.cliente['controls'].cpf.enable()

    }else{
      this.formGroup.controls.cliente['controls'].cpf.disable()
      this.formGroup.controls.cliente['controls'].cpf.setValue(null)
      this.formGroup.controls.cliente['controls'].cnpj.enable()
      this.formGroup.controls.cliente['controls'].nomeFantasia.enable()
    }
  }


  submitForm() {
    if(this.formGroup.invalid) {
      markControlsAsTouched(this.formGroup)
      this.dialogService.showDialog("Atenção","Seu formulário contém erros")
      return
    }

    let contrato = this.formGroup.value as Contrato

    if(contrato.cliente.usuario.perfil.tipo == PerfilTipo.CLIENTE_PJ && (!contrato.cliente.cnpj || !contrato.cliente.nomeFantasia) ){
      this.dialogService.showDialog("Atenção","CNPJ e Nome Fantasia São Obrigatórios")
      return
    }

    setAllDisabled(this.formGroup,false)

    this.dialogService.showWaitDialog("...aguarde, Processando")
    this.contratoService.postContrato(contrato).subscribe(
      resp=>{
        this.dialogService.closeWaitDialog()
        this.contratoService.closeCriarContrato()
        this.contratoService.contrato = contrato
        this.contratoService.goList()
      },error1 => {
        let {error,message} = error1.error
        this.dialogService.closeWaitDialog()
        if(message.toString().indexOf("ConstraintViolationException")!=-1)
          message = `Esse ${contrato.cliente.usuario.perfil.tipo == PerfilTipo.CLIENTE_PJ?'CNPJ':'CPF'} já existe no banco de dados!`
        this.dialogService.showDialog("Erro",message)
      })
  }






}
