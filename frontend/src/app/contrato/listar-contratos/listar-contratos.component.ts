import {Component, HostListener, OnInit} from '@angular/core';
import {DialogService} from '../../shared/dialog.service';
import {SearchService} from '../../shared/search.service';
import {ContratoGeraCarneStatusOptions, ContratoStatusOptions} from '../../shared/model';
import {ContratoService} from '../contrato.service';
import {VeiculoService} from '../../veiculos/veiculo.service';
import {MapaDataService} from '../../mapas/mapa-veiculo/mapa-data.service';
import {PageChangedEvent} from 'ngx-bootstrap';
import {SegurancaService} from '../../seguranca/seguranca.service';

@Component({
  selector: 'app-listar-contratos',
  templateUrl: './listar-contratos.component.html',
  styleUrls: ['./listar-contratos.component.css'],

})
export class ListarContratosComponent implements OnInit {

  contratoStatusOptions = ContratoStatusOptions
  contratoGeraCarneStatus = ContratoGeraCarneStatusOptions

  constructor(
    public contratoService: ContratoService,
    public dialogService: DialogService,
    public searchService: SearchService,
    public mapaDataService: MapaDataService,
    public veiculoService: VeiculoService,
    public segurancaService: SegurancaService

  ) {
  }

  showCriaCarne;

  items = []

  ngOnInit(): void {
    this.showCriaCarne = false;
  }


  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvents(event: KeyboardEvent) {
    console.log('event.key', event.key)
    switch (event.key) {
      case 'Enter':
        event.preventDefault();
        this.filtrar()
        break;
    }
  }


  filtrar() {
    this.contratoService.page.pageIndex=1
    this.contratoService.listarPagina()
  }

  changePageSize() {
    this.contratoService.page.pageIndex=1
    this.contratoService.listarPagina()
  }

  pageChanged($event: PageChangedEvent) {
    if(this.contratoService.page.pageIndex != $event.page){
      this.contratoService.page.pageIndex = $event.page;
      this.contratoService.listarPagina();
    }
  }


}
