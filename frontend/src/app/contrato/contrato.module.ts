import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListarContratosComponent } from './listar-contratos/listar-contratos.component';
import {SharedModule} from '../shared/shared.module';
import {AutoCompleteModule} from 'primeng/primeng';
import {CarneModule} from '../carne/carne.module';
import {DropdownModule} from 'primeng/dropdown';
import {MapaHttpService} from '../mapas/mapa-http.service';
import { CriarContratoComponent } from './criar-contrato/criar-contrato.component';
import { SenhaComponent } from '../seguranca/senha/senha.component';
import { PermissoesComponent } from '../seguranca/permissoes/permissoes.component';



@NgModule({
  declarations: [ListarContratosComponent, CriarContratoComponent, SenhaComponent, PermissoesComponent],
  imports: [
    CommonModule,
    SharedModule,
    AutoCompleteModule,
    CarneModule



  ],

  providers:[
    MapaHttpService
  ]



})
export class ContratoModule { }
