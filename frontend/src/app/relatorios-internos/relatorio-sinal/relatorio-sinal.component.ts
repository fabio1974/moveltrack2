import {Component, OnInit} from '@angular/core';
import {
  AtrasoUnidade,
  ContratoStatus,
  EquipamentoSituacao,
  ModeloRastreador,
  Operadora,
  RelatorioSinalFiltro,
  VeiculoStatus,
  VeiculoTipo
} from '../../shared/model';
import {SearchService} from '../../shared/search.service';
import {RelatoriosInternosService} from '../relatorios-internos.service';
import {DialogService} from '../../shared/dialog.service';

@Component({
  selector: 'app-relatorio-sinal',
  templateUrl: './relatorio-sinal.component.html',
  styleUrls: ['./relatorio-sinal.component.css']
})
export class RelatorioSinalComponent implements OnInit {

  constructor(public searchService: SearchService,
              private relatoriosInternosService: RelatoriosInternosService,
              private dialogService: DialogService
              ) { }

  filtro = new RelatorioSinalFiltro()

  operadoras = Object.keys(Operadora)
  veiculoTipos= Object.keys(VeiculoTipo)
  modelosRastreador= Object.keys(ModeloRastreador)
  equipamentoSituacoes= Object.keys(EquipamentoSituacao)
  veiculoStatuses= Object.keys(VeiculoStatus)
  contratoStatuses= Object.keys(ContratoStatus)
  minutos = Array.from(Array(60).keys())
  atrasoUnidades = Object.keys(AtrasoUnidade)

  ngOnInit() {
    this.filtro.contratoStatus = ContratoStatus.ATIVO
    this.filtro.veiculoStatus = VeiculoStatus.ATIVO
  }

  showRelatorio(download: HTMLAnchorElement) {
    this.dialogService.showWaitDialog("aguarde...")
    this.relatoriosInternosService.getRelatorioSinais(this.filtro).subscribe(
      data => {
        this.dialogService.closeWaitDialog()
        download.href = 'data:application/octet-stream;base64,'+data
        download.download = `RelatorioSinais${new Date().getTime()}.pdf`
        download.click()

      },
      error=> {
        this.dialogService.closeWaitDialog()
        console.log(error)//error
      })
  }
}
