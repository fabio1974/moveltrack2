import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MainTabsComponent} from './main-tabs/main-tabs.component';
import {BsDatepickerModule, TabsModule} from 'ngx-bootstrap';
import { RelatorioCobrancaComponent } from './relatorio-cobranca/relatorio-cobranca.component';
import {FormsModule} from '@angular/forms';
import { RelatorioSinalComponent } from './relatorio-sinal/relatorio-sinal.component';
import {NgSelectModule} from '@ng-select/ng-select';
import { RelatorioInstaladorComponent } from './relatorio-instalador/relatorio-instalador.component';


@NgModule({
  declarations: [MainTabsComponent, RelatorioCobrancaComponent, RelatorioSinalComponent, RelatorioInstaladorComponent],
    imports: [
        CommonModule,
        TabsModule,
        BsDatepickerModule,
        FormsModule,
        NgSelectModule
    ]
})
export class RelatoriosInternosModule { }
