import { Component, OnInit } from '@angular/core';
import {SearchService} from '../../shared/search.service';
import {RelatoriosInternosService} from '../relatorios-internos.service';
import {DialogService} from '../../shared/dialog.service';
import {RelatorioFiltro} from '../../shared/model';
import {DatepickerConfigService} from '../../shared/bsDateTimePickerConfig';

@Component({
  selector: 'app-relatorio-instalador',
  templateUrl: './relatorio-instalador.component.html',
  styleUrls: ['./relatorio-instalador.component.css']
})
export class RelatorioInstaladorComponent implements OnInit {

  constructor(public searchService: SearchService,
              private relatoriosInternosService: RelatoriosInternosService,
              public datepickerConfigService: DatepickerConfigService,
              private dialogService: DialogService) { }

  filtro = new RelatorioFiltro()


  ngOnInit() {
    this.filtro.inicio = new Date()
    this.filtro.inicio.setHours(0,0,0,0)
    this.filtro.inicio.setMonth(this.filtro.inicio.getMonth()-1)
    this.filtro.inicio.setDate(1)

    this.filtro.fim = new Date()
    this.filtro.fim.setHours(0,0,0,0)
    this.filtro.fim.setDate(1)
    this.filtro.fim.setMilliseconds(this.filtro.fim.getMilliseconds()-1)

  }

  showRelatorio(download: HTMLAnchorElement) {

    if(!this.filtro.instalador || !this.filtro.inicio || !this.filtro.fim){
      this.dialogService.showDialog("Atencao","Prenecha as datas e o nome do instalador")
      return
    }

    this.dialogService.showWaitDialog("aguarde...")
    this.relatoriosInternosService.getRelatorioInstalador(this.filtro).subscribe(
      data => {
        this.dialogService.closeWaitDialog()
        download.href = 'data:application/octet-stream;base64,'+data
        download.download = `RelatorioCobranca${new Date().getTime()}.pdf`
        download.click()
      },
      error=> {
        this.dialogService.closeWaitDialog()
        console.log(error)//error
      })
  }
}
