import {Component, OnInit} from '@angular/core';
import {DatepickerConfigService} from '../../shared/bsDateTimePickerConfig';
import {MBoletoStatus} from '../../shared/model';
import {RelatoriosInternosService} from '../relatorios-internos.service';
import {DialogService} from '../../shared/dialog.service';

@Component({
  selector: 'app-relatorio-cobranca',
  templateUrl: './relatorio-cobranca.component.html',
  styleUrls: ['./relatorio-cobranca.component.css']
})
export class RelatorioCobrancaComponent implements OnInit {
  fim: Date;
  status: MBoletoStatus = MBoletoStatus.VENCIDO ;

  constructor(public datepickerConfigService: DatepickerConfigService,
              public relatoriosInternosService: RelatoriosInternosService,
              private dialogService: DialogService
              ) { }

  ngOnInit() {

    this.fim = new Date()
    this.fim.setHours(0,0,0,0)
    this.fim.setDate(this.fim.getDate()-15)


  }

  showRelatorio(download: HTMLAnchorElement) {
    this.dialogService.showWaitDialog("aguarde...")
    this.relatoriosInternosService.getRelatorioCobranca(this.status,this.fim).subscribe(
      data => {
        this.dialogService.closeWaitDialog()
        download.href = 'data:application/octet-stream;base64,'+data
        download.download = `RelatorioCobranca${new Date().getTime()}.pdf`
        download.click()
      },
      error=> {
        this.dialogService.closeWaitDialog()
        console.log(error)//error
      })
  }


}
