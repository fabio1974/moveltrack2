import { Component, OnInit } from '@angular/core';
import {SegurancaService} from '../../seguranca/seguranca.service';

@Component({
  selector: 'app-main-tabs',
  templateUrl: './main-tabs.component.html',
  styleUrls: ['./main-tabs.component.css']
})
export class MainTabsComponent implements OnInit {

  constructor(public segurancaService: SegurancaService) { }

  ngOnInit() {
  }

}
