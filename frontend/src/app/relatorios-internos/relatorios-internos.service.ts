import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {MBoletoStatus, RelatorioFiltro, RelatorioSinalFiltro} from '../shared/model';

@Injectable({
  providedIn: 'root'
})
export class RelatoriosInternosService {

  constructor(private http: HttpClient,) { }


  getRelatorioCobranca(status:MBoletoStatus, fim: Date) : Observable<any>  {
    let filtro = new RelatorioFiltro()
    filtro.fimCobranca = fim
    filtro.boletoStatus = status

    return this.http.post(`${environment.apiUrl}/getRelatorioCobranca`,filtro, {responseType: 'json' });
  }


  getRelatorioSinais(filtro: RelatorioSinalFiltro)  : Observable<any>  {
    return this.http.post(`${environment.apiUrl}/getRelatorioSinais`,filtro, {responseType: 'json' });
  }

  getRelatorioInstalador(filtro: RelatorioFiltro)  : Observable<any>  {
    return this.http.post(`${environment.apiUrl}/getRelatorioInstalador`,filtro, {responseType: 'json' });
  }

}
