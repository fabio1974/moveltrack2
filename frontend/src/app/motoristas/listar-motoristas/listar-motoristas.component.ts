import {Component, HostListener, OnInit} from '@angular/core';

import {DialogService} from '../../shared/dialog.service';
import {DespesaFrotaService} from '../../despesa-frota/despesa-frota.service';
import {AtivoInativo, Cliente, Motorista, Page, PessoaStatus} from '../../shared/model';
import {LazyLoadEvent} from 'primeng/api';
import {MotoristaService} from '../motorista.service';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {Obrigatorio, TamanhoMaximo, TamanhoMinimo, ValidateCpf} from '../../shared/formUtils';
import {FormBuilder} from '@angular/forms';
import {PageChangedEvent} from 'ngx-bootstrap';


@Component({
  selector: 'app-listar-motoristas',
  templateUrl: './listar-motoristas.component.html',
  styleUrls: ['./listar-motoristas.component.css']
})
export class ListarMotoristasComponent  {

  constructor(
    public motoristaService: MotoristaService,
    public despesaFrotaService: DespesaFrotaService,
    public segurancaService: SegurancaService,
  ) {

  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvents(event: KeyboardEvent) {
    console.log('event.key', event.key)
    switch (event.key) {
      case 'Enter':
        event.preventDefault();
        this.filtrar()
        break;
    }
  }


  statuses = [
    {value: 'ATIVO', label: 'ATIVO' },
    {value: 'INATIVO',label: 'INATIVO'}
  ];

  filtrar() {
    this.motoristaService.page.pageIndex=1
    this.motoristaService.listarPagina()
  }

  changePageSize() {
    this.motoristaService.page.pageIndex=1
    this.motoristaService.listarPagina()
  }

  pageChanged($event: PageChangedEvent) {
    if(this.motoristaService.page.pageIndex != $event.page){
      this.motoristaService.page.pageIndex = $event.page;
      this.motoristaService.listarPagina();
    }
  }
}
