import {Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {Cliente, Motorista, Municipio, Operacao, PessoaStatus} from '../../shared/model';
import {SearchService} from '../../shared/search.service';
import {
  markControlsAsTouched,
  Obrigatorio,
  setAllDisabled,
  TamanhoMaximo,
  TamanhoMinimo,
  ValidateCpf
} from '../../shared/formUtils';
import {MotoristaService} from '../motorista.service';
import {DialogService} from '../../shared/dialog.service';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {DatepickerConfigService} from '../../shared/bsDateTimePickerConfig';

@Component({
  selector: 'app-criar-motorista',
  templateUrl: './criar-motorista.component.html',
  styleUrls: ['./criar-motorista.component.css']
})
export class CriarMotoristaComponent implements OnInit {

  motorista

  constructor(private fb: FormBuilder,
              public searchServices: SearchService,
              public motoristaService: MotoristaService,
              private dialogService: DialogService,
              private segurancaService: SegurancaService,
              public datepickerConfigService: DatepickerConfigService
              )
  {

    this.motorista = this.motoristaService.motorista || new Motorista()
    this.formGroup.setValue(this.motorista)
    setAllDisabled(this.formGroup,this.motoristaService.operacao==Operacao.SHOW)
    this.formGroup.controls.dataCadastro.disable()
  }


  formGroup = this.fb.group({
    dataCadastro: [],
    cpf: [,[ValidateCpf]],
    nome: [,[TamanhoMinimo(5)]],
    validadeCnh: [,Obrigatorio],
    categoriaCnh: [,Obrigatorio],
    status: [PessoaStatus.ATIVO,Obrigatorio],
    dataNascimento: [],
    patrao:[],
    id:[],
    cnpj:[],
    ultimaAlteracao:[],
    usuario:[],
    telefoneFixo: [],
    celular1: [,TamanhoMinimo(11)],
    celular2: [],
    endereco: [,[TamanhoMinimo(5)]],
    numero: [,[TamanhoMaximo(5)]],
    complemento: [],
    municipio: [,[Obrigatorio]],
    cep: [],
    bairro: [,[TamanhoMinimo(5)]],
    email:[],
    logoFile:[]
  });


  ngOnInit() {

  }

  submitForm() {

    if(this.formGroup.invalid) {
      markControlsAsTouched(this.formGroup)
      this.dialogService.showDialog("Atenção","Seu formulário contém erros")
      return
    }

    setAllDisabled(this.formGroup,false)
    let motorista = this.formGroup.value as Motorista
    motorista.patrao = this.segurancaService.pessoaLogada() as Cliente

    this.dialogService.showWaitDialog("...aguarde, Processando")
    this.motoristaService.postMotorista(motorista).subscribe(
      resp=>{
        this.dialogService.closeWaitDialog()
        this.motoristaService.goList()
    },error1 => {
        let {error,message} = error1.error
        this.dialogService.closeWaitDialog()
        this.dialogService.showDialog(error,message)
    })
  }

/*
  results: Municipio[];
  searchMunicipios(event) {
    console.log("event.query",event.query)
    this.searchServices.searchMunicipios(event.query).subscribe(
      resp => {
        this.results = (resp as Municipio[]).map(
          m=>{
            m.label = `${m.descricao}-${m.uf.sigla}`
            return m
        })
      },
      error => {
        console.log('error', error);
      }
    );
  }
*/

}
