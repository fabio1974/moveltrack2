import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Motorista, Contrato, Operacao, Page, Cliente} from '../shared/model';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {DialogService} from '../shared/dialog.service';
import {SegurancaService} from '../seguranca/seguranca.service';

@Injectable({
  providedIn: 'root'
})
export class MotoristaService {

  page:Page<Motorista> = new Page();
  filtro: Motorista = new Motorista()
  motorista: Motorista
  operacao

  constructor(private http: HttpClient,
              private dialogService: DialogService,
              private segurancaService: SegurancaService,
              private router: Router) {

    this.page.pageSize = 10
    if(this.segurancaService.temPermissao("MENU_MOTORISTA"))
      this.filtro.patrao = this.segurancaService.pessoaLogada() as Cliente
    this.listarPagina();
  }


  listarPagina() {
    this.dialogService.showWaitDialog("...processando. Aguarde!")
    this.getMotoristas(this.page,this.filtro)
      .subscribe(
        resp => {
          this.dialogService.closeWaitDialog()
          this.page.list = ((resp as any).content) as Motorista[];
          this.page.totalRecords = (resp as any).totalElements;
        },
        error =>{
          this.dialogService.closeWaitDialog()
          this.dialogService.showDialog(error.statusText,error.message)
        }
      );
  }

  goCreate(){
    this.operacao = Operacao.CREATE
    this.motorista = new Motorista()
    this.router.navigate(["/criarMotorista"])
  }

  goUpdate(motorista){
    this.motorista = motorista
    this.operacao = Operacao.UPDATE
    this.router.navigate(["/criarMotorista"])
  }

  goShow(motorista){
    this.motorista = motorista
    this.operacao = Operacao.SHOW
    this.router.navigate(["/criarMotorista"])
  }

  goList(){
    this.listarPagina()
    this.router.navigate(["/listarMotoristas"])
  }

  getMotoristas(page: Page<Motorista>, filtro: Motorista): Observable<any>  {
    const query = `pageIndex=${page.pageIndex-1}&pageSize=${page.pageSize}&sortField=${page.sortField}&sortDirection=${page.sortDirection}`;
    return this.http.patch(`${environment.apiUrl}/motoristas?${query}`,filtro);
  }

  postMotorista(motorista) {
    return this.http.post(`${environment.apiUrl}/motoristas`,motorista);
  }

  putMotorista(motorista) {
    return this.http.put(`${environment.apiUrl}/motoristas/${motorista.id}`,motorista);
  }

}
