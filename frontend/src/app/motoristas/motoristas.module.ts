import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListarMotoristasComponent } from './listar-motoristas/listar-motoristas.component';
import { CriarMotoristaComponent } from './criar-motorista/criar-motorista.component';
import {SharedModule} from '../shared/shared.module';
import {AutoCompleteModule} from 'primeng/primeng';

@NgModule({
  declarations: [ListarMotoristasComponent, CriarMotoristaComponent],
  imports: [
    CommonModule,
    SharedModule,
    AutoCompleteModule
  ]
})
export class MotoristasModule { }
