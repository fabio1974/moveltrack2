import {Component, HostListener, OnInit} from '@angular/core';

import {DialogService} from '../../shared/dialog.service';
import {Router} from '@angular/router';
import {SearchService} from '../../shared/search.service';
import {Contrato, Page, DespesaFrota, Cliente} from '../../shared/model';
import {LazyLoadEvent} from 'primeng/api';
import {DespesaFrotaService} from '../despesa-frota.service';
import {SegurancaService} from '../../seguranca/seguranca.service';

@Component({
  selector: 'app-listar-despesa-frota',
  templateUrl: './listar-despesa-frota.component.html',
  styleUrls: ['./listar-despesa-frota.component.css']
})
export class ListarDespesaFrotaComponent implements OnInit {

  constructor(
    private despesaFrotaService: DespesaFrotaService,
    private dialogService: DialogService,
    private router: Router,
    private searchService: SearchService,
    private segurancaService: SegurancaService
  ) {

  }


  page:Page<DespesaFrota> = null;
  inited:boolean = false

  ngOnInit(): void {
    this.page = new Page()
    this.listarPagina();
    this.inited = true
  }


  listarPagina() {
    this.dialogService.showWaitDialog("...processando. Aguarde!")
    this.despesaFrotaService.getDespesaFrotas(this.page,this.despesaFrotaService.filtro)
      .subscribe(
        resp => {
          this.dialogService.closeWaitDialog()
          this.loaded = true;
          this.page.list = (resp as any).content;
          console.log("list",this.page.list)
          this.page.totalRecords = (resp as any).totalElements;
        },
        error =>{
          this.dialogService.closeWaitDialog()
          this.dialogService.showDialog(error.statusText,error.message)
        }
      );
  }

  onLazyLoad(event: LazyLoadEvent) {
    this.page.pageIndex = event.first / event.rows;
    if(!this.inited)
      this.listarPagina();
    this.inited = false
  }


  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvents(event: KeyboardEvent) {
    console.log('event.key', event.key)
    switch (event.key) {
      case 'Enter':
        event.preventDefault();
        this.filtrar()
        break;
    }
  }

  filtrar() {
    if(this.despesaFrotaService.filtro.veiculo == '')
      this.despesaFrotaService.filtro.veiculo = null
    if(this.despesaFrotaService.filtro.motorista == '')
      this.despesaFrotaService.filtro.motorista = null
    if(this.despesaFrotaService.filtro.viagem == '')
      this.despesaFrotaService.filtro.viagem = null

    this.page.pageIndex = 0

    this.listarPagina()
  }

  results: any;
  loaded: boolean = false;

  delete(despesaFrota: DespesaFrota) {
    if(confirm("Deseja realmente deletar esse registro?")){


    this.dialogService.showWaitDialog("...processando. Aguarde!")
    this.despesaFrotaService.deleteDespesaFrota(despesaFrota)
      .subscribe(
        resp=>{
          this.listarPagina()
            this.dialogService.closeWaitDialog()
            this.dialogService.showDialog("Aviso","Registro deletado com sucesso")
        },error1 => {
          this.dialogService.closeWaitDialog()
          this.dialogService.showDialog("Erro",JSON.stringify(error1))
        }
      )

    }else
      return

  }
}
