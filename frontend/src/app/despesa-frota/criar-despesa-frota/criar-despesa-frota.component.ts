import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DialogService} from '../../shared/dialog.service';
import {SearchService} from '../../shared/search.service';
import {FormBuilder} from '@angular/forms';
import {
  Cliente,
  Contrato,
  DespesaFrota,
  DespesaFrotaCategoria,
  DespesaFrotaEspecie,
  Motorista,
  Operacao,
  Page,
  Veiculo, Viagem
} from '../../shared/model';
import {DespesaFrotaService} from '../despesa-frota.service';
import {MotoristaService} from '../../motoristas/motorista.service';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {VeiculoService} from '../../veiculos/veiculo.service';
import {ViagemService} from '../../viagem/viagem.service';
import {markControlsAsTouched, MotoristaObrigatorio, Obrigatorio, setAllDisabled, VeiculoObrigatorio} from '../../shared/formUtils';
import {bsDatepickerConfig} from '../../shared/bsDateTimePickerConfig';

@Component({
  selector: 'app-criar-despesa-frota',
  templateUrl: './criar-despesa-frota.component.html',
  styleUrls: ['./criar-despesa-frota.component.css']
})
export class CriarDespesaFrotaComponent implements OnInit {

  despesaFrota: DespesaFrota
  especies


  constructor(
    public despesaFrotaService: DespesaFrotaService,
    private route: ActivatedRoute,
    private motoristaService: MotoristaService,
    private router: Router,
    private dialogService: DialogService,
    public searchService: SearchService,
    private segurancaService: SegurancaService,
    private veiculoService: VeiculoService,
    private viagemService: ViagemService,
    private fb: FormBuilder
  ){}


  formGroup = this.fb.group({
    id:[],
    categoria:[],
    especie:[,Obrigatorio],
    valor:[,Obrigatorio],
    dataDaDespesa:[,Obrigatorio],
    dataDePagamento:[],
    viagem:[],
    veiculo:[],
    motorista:[],
    litros:[],
    descricao:[],
    cliente:[this.segurancaService.pessoaLogada() as Cliente],
  })

  bsConfig = bsDatepickerConfig;


  ngOnInit() {
    this.despesaFrota = this.despesaFrotaService.despesaFrota || new DespesaFrota()
    this.despesaFrota.cliente = this.segurancaService.pessoaLogada() as Cliente
    this.formGroup.setValue(this.despesaFrota)
    this.formGroup.controls.categoria.disable()
    this.formGroup.controls.litros.disable()
    this.formGroup.controls.viagem.disable()

    if (this.despesaFrota.categoria == 'VIAGEM') {
      this.formGroup.controls.motorista.disable()
      this.formGroup.controls.motorista.clearValidators()
      this.formGroup.controls.veiculo.disable()
      this.formGroup.controls.veiculo.clearValidators()
      this.especies = this.despesaFrotaService.especies.filter(
        value => value.value.includes('COMBUSTIVEL') ||
          value.value.includes('ESTIVA') ||
          value.value.includes('DIARIA') ||
          value.value.includes('OUTROS')
      );

    }else  if(this.despesaFrota.categoria == 'VEICULO') {
      this.formGroup.controls.motorista.disable()
      this.formGroup.controls.motorista.clearValidators()
      this.formGroup.controls.veiculo.setValidators(Obrigatorio)
      this.especies = this.despesaFrotaService.especies.filter(
        value => value.value.includes('COMBUSTIVEL') ||
          value.value.includes('IPVA') ||
          value.value.includes('LICENCIAMENTO') ||
          value.value.includes('MANUTENCAO') ||
          value.value.includes('MULTA') ||
          value.value.includes('SEGURO') ||
          value.value.includes('OUTROS')
      );

    }else if(this.despesaFrota.categoria == 'MOTORISTA') {
      this.formGroup.controls.veiculo.disable()
      this.formGroup.controls.veiculo.clearValidators()
      this.formGroup.controls.motorista.setValidators(Obrigatorio)
      this.especies = this.despesaFrotaService.especies.filter(
        value => value.value.includes('TRABALHISTA') ||
          value.value.includes('OUTROS')
      );
    }

    if(this.despesaFrotaService.operacao==Operacao.SHOW) {
      setAllDisabled(this.formGroup, true)
    }
  }



  submitForm() {

    if(this.formGroup.invalid) {
      markControlsAsTouched(this.formGroup)
      this.dialogService.showDialog("Atenção","Seu formulário contém erros")
      return
    }

    setAllDisabled(this.formGroup,false)
    let despesaFrota = this.formGroup.value as DespesaFrota

    this.dialogService.showWaitDialog("...aguarde, Processando")
    this.despesaFrotaService.postDespesaFrota(despesaFrota).subscribe(
      resp=>{
        this.dialogService.closeWaitDialog()
        this.despesaFrotaService.goList()
      },error1 => {
        let {error,message} = error1.error
        this.dialogService.closeWaitDialog()
        this.dialogService.showDialog(error,message)
      })
  }



  listarDespesaFrotas() {
    this.router.navigate(['/listarDespesaFrotas']);
  }

  enableDisableLitros() {
    if(this.formGroup.get("especie").value=='COMBUSTIVEL') {
      this.formGroup.controls.litros.setValidators(Obrigatorio)
      this.formGroup.get("litros").enable()
    }else {
      this.formGroup.controls.litros.clearValidators()
      this.formGroup.get("litros").disable()
    }
  }
}
