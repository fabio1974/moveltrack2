import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {
  Contrato,
  Operacao,
  Page,
  DespesaFrota,
  DespesaFrotaCategoria,
  Motorista,
  Cliente,
  DespesaFrotaEspecie,
  Veiculo, Viagem
} from '../shared/model';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {SegurancaService} from '../seguranca/seguranca.service';
import {MotoristaService} from '../motoristas/motorista.service';
import {VeiculoService} from '../veiculos/veiculo.service';
import {SearchService} from '../shared/search.service';

@Injectable({
  providedIn: 'root'
})
export class DespesaFrotaService {

  despesaFrota: DespesaFrota
  filtro = new DespesaFrota()
  categorias = DespesaFrotaCategoria
  especies = DespesaFrotaEspecie
  motoristas: Motorista[]
  veiculos: Veiculo[]


  operacao

  constructor(private http: HttpClient,
              private router: Router,
              private motoristaService: MotoristaService,
              public searchService: SearchService,
              private veiculoService: VeiculoService,
              private segurancaService: SegurancaService) {
    this.filtro.cliente = this.segurancaService.pessoaLogada() as Cliente
    this.filtro.viagem = new Viagem()

  }



  goCreate(viagem,veiculo,motorista){

    console.log(viagem,veiculo,motorista)

    this.despesaFrota = new DespesaFrota()
    this.despesaFrota.viagem = viagem
    this.despesaFrota.veiculo = veiculo
    this.despesaFrota.motorista = motorista
    if(viagem)
      this.despesaFrota.categoria = 'VIAGEM'
    else if(veiculo)
      this.despesaFrota.categoria = 'VEICULO'
    else if(motorista)
      this.despesaFrota.categoria = 'MOTORISTA'
    this.operacao = Operacao.CREATE

    console.log("this.despesaFrota",this.despesaFrota)

    this.router.navigate(["/criarDespesaFrota"])
  }

  goUpdate(despesaFrota){
    this.despesaFrota = despesaFrota
    this.operacao = Operacao.UPDATE
    this.router.navigate(["/criarDespesaFrota"])
  }

  goShow(despesaFrota){
    this.despesaFrota = despesaFrota
    this.operacao = Operacao.SHOW
    this.router.navigate(["/criarDespesaFrota"])
  }

  getDespesaFrotas(page: Page<DespesaFrota>, filtro: DespesaFrota): Observable<any>  {
    const query = `pageIndex=${page.pageIndex}&pageSize=${page.pageSize}&sortField=${page.sortField}&sortDirection=${page.sortDirection}`;
    return this.http.patch(`${environment.apiUrl}/despesaFrotas?${query}`,filtro);
  }

  deleteDespesaFrota(despesaFrota: DespesaFrota): Observable<any>  {
    return this.http.delete(`${environment.apiUrl}/deleteDespesaFrota/${despesaFrota.id}`);
  }


  postDespesaFrota(despesaFrota) {
    return this.http.post(`${environment.apiUrl}/despesaFrotas`,despesaFrota);
  }

  getPdfFile(despesaFrota: any) : Observable<any>  {
    return this.http.get(`${environment.apiUrl}/getDespesaFrotaPdf/${despesaFrota.id}`, {responseType: 'json' });
  }

  goList(){
    this.router.navigate(["/listarDespesaFrotas"])
  }

}
