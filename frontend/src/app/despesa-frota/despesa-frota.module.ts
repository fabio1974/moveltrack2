import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ListarDespesaFrotaComponent} from './listar-despesa-frota/listar-despesa-frota.component';
import {CriarDespesaFrotaComponent} from './criar-despesa-frota/criar-despesa-frota.component';
import {SharedModule} from '../shared/shared.module';
import {AutoCompleteModule} from 'primeng/primeng';
import {RouterModule} from '@angular/router';
import {MotoristaService} from '../motoristas/motorista.service';
import {VeiculoService} from '../veiculos/veiculo.service';
import {ViagemService} from '../viagem/viagem.service';


@NgModule({
  declarations: [ListarDespesaFrotaComponent, CriarDespesaFrotaComponent],
  imports: [
    CommonModule,
    SharedModule,
    AutoCompleteModule,
    RouterModule,
  ],
  providers:[
    MotoristaService,
    VeiculoService,
    ViagemService
  ]
})
export class DespesaFrotaModule { }
