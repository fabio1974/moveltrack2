import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Cliente, Contrato, Location2, Veiculo} from '../shared/model';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class MapaHttpService {

  clienteSelected: Cliente
  contratoSelected: Contrato

  constructor(private http: HttpClient,private router: Router) { }

  locationsByImeiDates(imei,inicio,fim) {
    return this.http.post(`${environment.apiUrl}/locationsByImeiDates`,this.buildLocationRequest(imei,inicio,fim));
  }

  relatorioPercurso(imei,inicio,fim) {
    return this.http.post(`${environment.apiUrl}/relatorioPercurso`,this.buildLocationRequest(imei,inicio,fim));
  }

  relatorioParadas(imei,inicio,fim) {
    return this.http.post(`${environment.apiUrl}/relatorioParadas`,this.buildLocationRequest(imei,inicio,fim));
  }

/*
  visualisarVeiculos(contrato){
    this.contratoSelected = contrato
    this.router.navigate(["/mapaVeiculo"])
  }
*/

  findVeiculosCliente(cliente: Cliente) {
    return this.http.post(`${environment.apiUrl}/findVeiculosByCliente`,cliente);
  }

  findVeiculosAtivosCliente(cliente: Cliente):  Observable<any> {
    return this.http.post(`${environment.apiUrl}/findVeiculosAtivosByCliente`,cliente);
  }

  mapaFrota(cliente: Cliente):  Observable<any> {
    return this.http.post(`${environment.apiUrl}/mapaFrota`,cliente);
  }

  telimetria(imei):  Observable<any> {
    return this.http.get(`${environment.apiUrl}/telimetria/${imei}`);
  }



  buildLocationRequest(imei,inicio,fim) {
    const locationRequest = new Location2()
    locationRequest.imei = imei
    locationRequest.dateLocationInicio = inicio
    locationRequest.dateLocation = fim
    return locationRequest
  }

}










