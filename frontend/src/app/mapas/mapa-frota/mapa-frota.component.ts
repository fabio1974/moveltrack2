import {Component, OnInit, ViewChild} from '@angular/core';
import {getIcon, getMapaObject, MapaObject} from '../mapaUtils';
import {GoogleMapService} from '../mapa-veiculo/google-map.service';
import {MapaDataService} from '../mapa-veiculo/mapa-data.service';
import {DialogService} from '../../shared/dialog.service';
import {MapaHttpService} from '../mapa-http.service';
import {MessageService} from 'primeng/api';
import {Cliente, LastLocation, Location2, RelatorioFrota} from '../../shared/model';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {formatDate, formatNumber} from '@angular/common';
import {buildCabecalho, buildCabecalhoFrota, fontSize, getDoc} from '../../relatorios/relatoriosUtils';
import * as moment from 'moment';
import {ExcelService} from '../../shared/excel-service';
import {DatepickerConfigService} from '../../shared/bsDateTimePickerConfig';

@Component({
  selector: 'app-mapa-frota',
  templateUrl: './mapa-frota.component.html',
  styleUrls: ['./mapa-frota.component.css']
})
export class MapaFrotaComponent implements OnInit {

  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;
  mapaObject: MapaObject = getMapaObject([]);
  cliente: Cliente
  lastLocations: LastLocation[];
  carMarkers: google.maps.Marker[] = [];

  constructor(
    private mapDataService: MapaDataService,
    private dialogService: DialogService,
    private mapasHttpService: MapaHttpService,
    private messageService: MessageService,
    private segurancaService: SegurancaService,
    private excelService: ExcelService,
    private datepickerConfigService: DatepickerConfigService
  ) {
  }


  loadMap(gmapElement) {
    var mapProp = {
      center: new google.maps.LatLng(this.mapaObject.centro.latitude, this.mapaObject.centro.longitude),
      zoom: 5,
      mapTypeControl: true,
      mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
        position: google.maps.ControlPosition.RIGHT_TOP,
      }
    };
    this.map = new google.maps.Map(gmapElement.nativeElement, mapProp);
  }


  chato:boolean = false

  ngOnInit() {
    this.loadMap(this.gmapElement)
    this.cliente = this.segurancaService.pessoaLogada() as Cliente
    this.chato = this.cliente.nome.indexOf("WELLFIELD")!=-1
    this.dialogService.showWaitDialog("carregando mapa da frota...aguarde")
    this.atualizaMapaDaFrota()
    if(!this.chato)
      this.startTimer()
  }





  time: number = 0;


  mapaTimer;
  private startTimer() {
    this.pauseTimer()
    this.mapaTimer = setInterval(() => {
      this.time++;
       console.log("timer round frota",this.time)
      this.atualizaMapaDaFrota()
    },60000)
    console.log("timer frota reiniciado",this.mapaTimer)
  }

  private pauseTimer() {
    clearInterval(this.mapaTimer);
    console.log("timer frota pausado",this.mapaTimer)
  }



  atualizaMapaDaFrota(){
    this.cliente.dataCadastro = this.cliente.nome.indexOf("WELLFIELD")!=-1? this.dataCorte: null
    if(this.chato)
      this.dialogService.showWaitDialog("...carregando")
    this.mapasHttpService.mapaFrota(this.cliente).subscribe(
      resp=>{
        this.dialogService.closeWaitDialog()
        console.log(this.lastLocations)

        if ((resp as []).length <= 0) {
          this.map.setCenter({lat: this.mapaObject.centro.latitude, lng: this.mapaObject.centro.longitude});
          this.map.setZoom(this.mapaObject.zoom);
          this.messageService.add({severity:'error',closable:false, summary:`Placa ${this.mapDataService.veiculo.placa}`, detail:'Mapa da frota sem veículos!'});
        } else {
          this.lastLocations = resp as LastLocation[];
          this.mapaObject = getMapaObject(this.lastLocations.map(lastLoc => {
              let loc = new Location2()
              loc.latitude = lastLoc.latitude
              loc.longitude = lastLoc.longitude
              return loc
            }
          ));
          this.desenhaMapaDaFrota();
        }
      },
      error1 => {
        this.dialogService.closeWaitDialog()
        console.log("ERROR",error1)
      }
    )
  }



  desenhaMapaDaFrota() {
     this.limpaComponentes()
     this.map.setCenter({lat: this.mapaObject.centro.latitude, lng: this.mapaObject.centro.longitude});
     this.map.setZoom(this.mapaObject.zoom);
     this.lastLocations.forEach(
       lastLoc =>{

         let carMarker = new google.maps.Marker({
           position: new google.maps.LatLng(lastLoc.latitude, lastLoc.longitude),
           map: this.map,
           zIndex: 1000,
           icon: `assets/icon/${getIcon(lastLoc.tipo)}.gif`,
         });

         var contentString = this.createVeiculoInfoWindow(lastLoc);
         carMarker.addListener('click', this.onClickBuildAndOpenInfoWindow.bind(this, carMarker, contentString));
         this.carMarkers.push(carMarker);

       }
     )

    //

    //
  }


  limpaComponentes() {
    this.carMarkers.forEach(carMarker => {
      carMarker.setMap(null);
    });
    this.carMarkers = [];
  }


  createVeiculoInfoWindow(lastLoc) {
    let linhaIgnition = '';
    if (lastLoc.ignition) {
      linhaIgnition = '<tr>'
        + '<td class=\'tg1\'>Ignição</td>'
        + '<td class=\'tg1\'>' + (lastLoc.ignition=='0'?'OFF':'ON') + '</td>'
        + '</tr>';
    }

    let linhaEvento = ''
    if(lastLoc.comando){
      linhaEvento =  '<tr>'
      + '<td class=\'tg2\'>Evento</td>'
      + '<td class=\'tg2\'>' + lastLoc.comando + '</td>'
      + '</tr>'
    }

    let icon = `assets/icon/${getIcon(lastLoc.tipo)}.png`;
    let table = '<table style=\'width: 100%\' class=\'tg\'>'
      + '<tr>'
      + '<th colspan=\'2\' class=\'tg2\'><img width=\'10%\' src=\'' + icon + '\'></img> ' + lastLoc.placa + ' - ' + lastLoc.marcaModelo + ' - ' + lastLoc.cor + '</th>'
      + '</tr>'
      + '<tr>'
      + '<td class=\'tg1\'>Último Sinal</td>'
      + '<td class=\'tg1\'>' + formatDate(lastLoc.dateLocation, 'dd/MM/yy HH:mm\'h\'', 'pt_BR') + '</td>'
      + '</tr>'
      + '<tr>'
      + '<td class=\'tg2\'>Velocidade</td>'
      + '<td class=\'tg2\'>' + formatNumber(lastLoc.velocidade, 'pt-BR', '1.2-2') + 'km/h</td>'
      + '</tr>'
      + linhaIgnition
      + linhaEvento
      + '</table>';
    return this.infoWindowStyle + table;
  }

  infoWindowStyle = '<style type=\'text/css\'>'
    + '.tg  {border-collapse:collapse;border-spacing:0;border:none;border-color:#93a1a1;}'
    + '.tg td{text-align:center;,font-weight:bold;font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#93a1a1;color:#002b36;background-color:#fdf6e3;}'
    + '.tg th{text-align:left;font-weight:bold;font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#93a1a1;color:#fdf6e3;background-color:#657b83;}'
    + '.tg .tg1{background-color:#eee8d5;vertical-align:top}'
    + '.tg .tg2{vertical-align:top}'
    + '</style>';


  onClickBuildAndOpenInfoWindow = function (marker, contentString) {
    var infowindow = new google.maps.InfoWindow({
      content: contentString
    });
    infowindow.open(this.map, marker);
  };
  dataCorte: Date = new Date();

  geraRelatorioUltimosSinais() {
    let doc = getDoc('portrait','mm','a4')
    let list0 = this.lastLocations
    let list1 = list0.map(
      (row,index) => {
        index++

        let diff= '-'
        let now = this.chato? this.dataCorte : new Date()
        let m  = moment(now).diff(moment(row.dateLocation))
        var duration = moment.duration(m);
        diff = `${duration.months()>0?duration.months()+'/':''}${duration.days()} ${moment.utc(m).format("HH:mm:ss")}`

        return [
          row.placa,
          row.marcaModelo,
          // formatNumber(row.latitude,'pt-BR','1.6-6'),
          // formatNumber(row.longitude,'pt-BR','1.6-6'),
          //`www.google.com/maps/search/?api=1&query=${row.latitude},${row.longitude}`,
          formatDate(row.dateLocation,"dd/MM/yy HH:mm:ss",'pt-BR'),
          diff,
          `${formatNumber(row.velocidade as number,'pt-BR','1.2-2')}km/h`,
          row.comando,
          row.imei
        ];
      })
    doc.autoTable({
      startY: buildCabecalhoFrota(this.cliente, doc, 'Últimos Sinais dos Veículos'),
      styles: {halign: 'center', fontSize},
      head: [['Placa','Marca/Modelo','Último Sinal','Atraso','Velocidade','Evento','Serial']],
      body: [...list1]
    });
    doc.save('RelatorioUltimosSinais.pdf');
  }

  geraRelatorioUltimosSinaisExcel() {

    let header = [
      { 'key': "Relatório:", 'value': 'Últimos Sinais dos Veículos'},
      { 'key': "Cliente:", 'value': this.cliente.nome},
      { 'key': "Data:", 'value': formatDate(this.chato? this.dataCorte : new Date(),"dd/MM/yy HH:mm:ss",'pt-BR')},
    ]
    let data : any = [];
    this.lastLocations.forEach(row=>{

      let diff= '-'
      let now = this.chato? this.dataCorte : new Date()
      let m  = moment(now).diff(moment(row.dateLocation))
      var duration = moment.duration(m);
      diff = `${duration.months()>0?duration.months()+'/':''}${duration.days()} ${moment.utc(m).format("HH:mm:ss")}`


      data.push({
        'Placa':row.placa,
        'Marca/Modelo':row.marcaModelo,
        'Último Sinal':formatDate(row.dateLocation,"dd/MM/yy HH:mm:ss",'pt-BR'),
        'Atraso': diff,
        'Velocidade':`${formatNumber(row.velocidade as number,'pt-BR','1.2-2')}km/h`,
        'Evento': row.comando,
        'Serial': row.imei
      })
    })
    this.excelService.exportAsExcelFile(data, 'relatorioDistanciaPercorrida',header);
  }

}


