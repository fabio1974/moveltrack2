import { Injectable } from '@angular/core';
import {Rota, Operacao, Page, Municipio} from '../shared/model';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RotasService {

  rota
  operacao = Operacao.SHOW
  filtro: Rota = new Rota()




  constructor(private http: HttpClient,private router: Router) {
  }

  goCreate(){
    this.filtro = new Rota()
    this.rota = new Rota()
    this.operacao = Operacao.CREATE
    this.router.navigate(["/criarRota"])
  }

  goUpdate(rota){
    this.rota = rota
    this.operacao = Operacao.UPDATE
    //this.router.navigate(["/criarRota"])
  }

  goShow(rota){
    this.rota = rota
    this.operacao = Operacao.SHOW
    this.router.navigate(["/criarRota"])
  }

  goList(){
    this.router.navigate(["/listarRotas"])
  }

  getRotas(page: Page<Rota>, filtro: Rota): Observable<any>  {
    const query = `pageIndex=${page.pageIndex}&pageSize=${page.pageSize}&sortField=${page.sortField}&sortDirection=${page.sortDirection}`;
    return this.http.patch(`${environment.apiUrl}/rotas?${query}`,filtro);
  }




  postRota(rota) {
    return this.http.post(`${environment.apiUrl}/rotas`,rota);
  }

  getPdfFile(rota: any) : Observable<any>  {
    return this.http.get(`${environment.apiUrl}/getRotaPdf/${rota.id}`, {responseType: 'json' });
  }

  putRota(rota) {
    return this.http.put(`${environment.apiUrl}/rotas/${rota.id}`,rota);
  }


  deleteRota(rota: any) {
    return this.http.delete(`${environment.apiUrl}/rotas/${rota.id}`);
  }
}
