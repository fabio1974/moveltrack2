import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapaVeiculoComponent } from './mapa-veiculo/mapa-veiculo.component';
import {SharedModule} from '../shared/shared.module';
import { AgmCoreModule } from '@agm/core';
import {MapaHttpService} from './mapa-http.service';
import {DialogService} from '../shared/dialog.service';
import {AutoCompleteModule, MessageService} from 'primeng/primeng';
import { RotasComponent } from './rotas/rotas.component';
import { DirectionsMapDirective } from './directions-map.directive';
import { RotasListarComponent } from './rotas-listar/rotas-listar.component';
import { MapaFrotaComponent } from './mapa-frota/mapa-frota.component';
import { TelemetriaComponent } from './telemetria/telemetria.component';





@NgModule({
  declarations: [MapaVeiculoComponent,  RotasComponent, DirectionsMapDirective, RotasListarComponent, MapaFrotaComponent, TelemetriaComponent],
  imports: [
    AutoCompleteModule,
    CommonModule,
    SharedModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCg5eE_buXJLsJZbnTZ7z3MnJBOV3_RoYc'
    })
  ],
  providers:[
    MapaHttpService,DialogService, MessageService
  ]
})
export class MapasModule { }
