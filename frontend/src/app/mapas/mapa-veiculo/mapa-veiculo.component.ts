import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MapaHttpService} from '../mapa-http.service';
import {Chart, Cliente, Contrato, Equipamento, Location2, ModeloRastreador, Veiculo, Viagem} from '../../shared/model';
import {getIcon, getMapaObject, MapaObject} from '../mapaUtils';
import {DialogService} from '../../shared/dialog.service';
import {geraRelatorioParadasPdf} from '../../relatorios/relatoriosRastreamento';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {SearchService} from '../../shared/search.service';
import {DatepickerConfigService} from '../../shared/bsDateTimePickerConfig';

import {GoogleMapService} from './google-map.service';
import {MapaDataService} from './mapa-data.service';
import {AutoRastreioService} from './auto-rastreio.service';
import {MessageService} from 'primeng/api';
import {MapaRelatorioService} from './mapa-relatorio.service';
import {CortaCorrenteService} from './corta-corrente.service';
import {formatDate} from '@angular/common';
import {TelemetriaComponent} from '../telemetria/telemetria.component';


// @ts-ignore


@Component({
  selector: 'app-mapa-veiculo',
  templateUrl: './mapa-veiculo.component.html',
  styleUrls: ['./mapa-veiculo.component.css']
})
export class MapaVeiculoComponent implements OnInit {


  @ViewChild('gmap') gmapElement: any;

  @ViewChild(TelemetriaComponent) telemetriaComponent:TelemetriaComponent;

  isDropup = true;

  constructor(private mapasHttpService: MapaHttpService,
              public dialogService: DialogService,
              public segurancaService: SegurancaService,
              public searchService: SearchService,
              public datePickerConfigService: DatepickerConfigService,
              public googleMapService: GoogleMapService,
              public mapDataService: MapaDataService,
              public autoRastreioService: AutoRastreioService,
              private messageService: MessageService,
              public mapaRelatorioService: MapaRelatorioService,
              private cortaCorrenteService: CortaCorrenteService
  ){}


  ngOnInit() {

    this.chart = new Chart()
    this.chart.type='LineChart';
    this.chart.title = 'Velocidade x Tempo';
    this.chart.data = [];
    this.chart.options = {
      pointSize:5,
      hAxis: {
        title: 'Tempo'
      },
      vAxis: {
        title: 'Velocidade (km/h)'
      },
    };

    this.googleMapService.loadMap(this.gmapElement)

    if(this.segurancaService.isCliente()) {

      //Acesso de Cliente

      this.mapDataService.cliente = this.segurancaService.pessoaLogada() as Cliente
      if(this.mapDataService.veiculos.length==0)
        this.loadVeiculos()
      else if(this.mapDataService.viagem)
         this.setViagem()
      else
        this.setVeiculo()

    }else{

      //Acesso Moveltrack

      if(this.mapDataService.cliente)
        this.loadVeiculos()
    }

  }


  private loadVeiculos() {
    this.dialogService.showWaitDialog('...carregando veículos');
    this.mapasHttpService.findVeiculosAtivosCliente(this.mapDataService.cliente).subscribe(
      resp => {

        this.dialogService.closeWaitDialog()
        this.mapDataService.veiculos = resp as Veiculo[]

        if (this.mapDataService.veiculos.length > 0) {
          if(this.mapDataService.viagem)
            this.setViagem()
          else
            this.setVeiculo()
        }

      }, error => {
        this.dialogService.closeWaitDialog();
        this.dialogService.showDialog('Erro', error);
      }
    );
  }



  setVeiculo(){
      if (this.mapDataService.veiculo)
        this.mapDataService.veiculo = this.mapDataService.veiculos.find(it => it.id == this.mapDataService.veiculo.id)
      else
        this.mapDataService.veiculo = this.mapDataService.veiculos[0]
      this.googleMapService.rastrearVeiculo(()=>this.displayTelemetria())

  }



  setViagem() {
    this.mapDataService.inicio = new Date(this.mapDataService.viagem.partida)
    this.mapDataService.fim = new Date(this.mapDataService.viagem.chegadaPrevista)
    this.mapDataService.veiculo = this.mapDataService.veiculos.find(it=> it.id == this.mapDataService.viagem.veiculo.id)
    this.googleMapService.rastrearVeiculo(() => this.displayTelemetria())
  }

  onSelectCliente() {
    if(this.mapDataService.cliente) {
      this.mapDataService.veiculo = null
      this.loadVeiculos()
    }else{
      this.mapDataService.veiculos = []
    }
  }


  operacao
  chart: Chart;

  displayConfirmDialog(operacao: string) {
    this.operacao = operacao
    if(this.operacao=='bloqueio')
      this.dialogService.showConfirmDialog("Bloqueio", "Deseja bloquear o veículo?")
    else if(operacao=='desbloqueio')
      this.dialogService.showConfirmDialog("Desbloqueio", "Deseja desloquear o veículo?")
  }


  doMethod() {
    if(this.operacao=='bloqueio')
      this.cortaCorrenteService.bloquear(this.mapDataService.veiculo)
    else if(this.operacao=='desbloqueio')
      this.cortaCorrenteService.desbloquear(this.mapDataService.veiculo)
  }


  displayTacografo() {

    if (this.mapDataService.locations.length <= 0) {
      this.dialogService.showDialog('Atenção', 'Escolha um veículo que esteja mostrando rastreamento no mapa');
      return;
    }

    this.chart.data = [];
    this.chart.title =  `${this.mapDataService.cliente.nome}
                        Veículo: ${this.mapDataService.veiculo.placa} - ${this.mapDataService.veiculo.marcaModelo}
                        Período: de ${formatDate(this.mapDataService.inicio, "dd/MM/yy HH:mm","pt_BR")} a ${formatDate(this.mapDataService.fim, "dd/MM/yy HH:mm","pt_BR")}`

    this.mapDataService.locations.forEach(loc => {
      if (this.isParada(loc)) {
        this.chart.data.push([loc.dateLocationInicio, 0]);
        this.chart.data.push([loc.dateLocation, 0]);
      } else {
        this.chart.data.push([loc.dateLocation, loc.velocidade]);
      }
      this.dialogService.showChartDialog('Tacógrafo Virtual');
    });

  }

  isParada(loc){
    return loc.dateLocation.getTime() - loc.dateLocationInicio.getTime() > 180000
  }

  sairModoViagem() {
    this.mapDataService.viagem = null
    this.mapDataService.inicio = new Date()
    this.mapDataService.fim = new Date()
    this.mapDataService.inicio.setHours(0,0,0,0)
    this.mapDataService.fim.setHours(23,59,59,999)
  }


  displayViagemDialog() {
    console.log("VIAGEM",this.mapDataService.viagem)
    this.dialogService.showViagemDialog(`Dados da Viagem ${this.mapDataService.viagem.numeroViagem}`  )
  }


  closeTelemetria() {
    this.telemetriaComponent.pauseTimer()
  }

  displayTelemetria(){
    if(this.mapDataService.veiculo && this.mapDataService.veiculo.equipamento &&
      this.mapDataService.veiculo.equipamento.modelo != ModeloRastreador.SPOT_TRACE &&
      this.mapDataService.lastLoc){
          this.telemetriaComponent.startTimer()
          this.dialogService.showTelemetriaDialog()
    }
  }

  botaoRastrearVeiculo() {
    this.googleMapService.rastrearVeiculo(()=>this.displayTelemetria())
  }

  ModeloRastreador = ModeloRastreador
}
