import { Injectable } from '@angular/core';
import {Cliente, Contrato, DespesaFrota, Location2, Operacao, Veiculo, Viagem} from '../../shared/model';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class MapaDataService {

  lastLoc
  locations: Location2[] =[]
  veiculo: Veiculo
  veiculos: Veiculo[] = []
  viagem: Viagem
  paradas: Location2[]
  cliente: Cliente
  //contrato: Contrato


  inicio: Date = new Date()
  fim: Date = new Date()
  rastreioAutomatico: boolean = false;

  constructor(private router: Router) {
    this.inicio.setHours(0,0,0,0)
    this.fim.setHours(23,59,59,999)
  }

  isHistorico(){
    return this.fim.getTime() <  (new Date()).getTime()
  }

  isRastreando() {
      return this.rastreioAutomatico && !this.isHistorico()
  }


  goMapaFromViagem(viagem){
    this.viagem = viagem
    this.cliente = viagem.cliente
    this.router.navigate(["/mapaVeiculo"])
  }

  goMapaFromVeiculo(veiculo: Veiculo){
    this.viagem = null
    this.veiculo = veiculo
    this.cliente = veiculo.contrato.cliente
    this.router.navigate(["/mapaVeiculo"])
  }

  goMapaFromCliente(cliente){
    this.viagem = null
    this.veiculo = null
    this.cliente = cliente
    this.router.navigate(["/mapaVeiculo"])
  }




}
