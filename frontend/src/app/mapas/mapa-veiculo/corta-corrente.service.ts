import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Sms, SmsStatus, Veiculo} from '../../shared/model';
import {environment} from '../../../environments/environment';
import {MapaDataService} from './mapa-data.service';
import {DialogService} from '../../shared/dialog.service';

@Injectable({
  providedIn: 'root'
})
export class CortaCorrenteService {

  constructor(
      private http: HttpClient,
      public mapDataService: MapaDataService,
      private dialogService: DialogService

  ) {
  }

  loadingStatusPopup
  cortaCorrenteTimer;
  sms: Sms
  count: number = 0

  SmsStatus = SmsStatus

  desbloquear(veiculo: Veiculo) {
    return this.http.post<Sms>(`${environment.apiUrl}/desbloquear`, veiculo).subscribe(
        resp=> {this.startCortaCorrenteService(resp)}
        )
  }

  bloquear(veiculo: Veiculo) {
    return this.http.post<Sms>(`${environment.apiUrl}/bloquear`, veiculo).subscribe(
        resp=> {this.startCortaCorrenteService(resp)}
    )
  }

  startCortaCorrenteService(sms: Sms) {
    console.log("Sms retornado",sms)
    this.sms = sms
    this.startTimer()
    this.dialogService.showWaitDialog(this.formatMessage())
   // this.showComandoStatusPopup()
  }


  private startTimer() {
    this.pauseTimer()
    this.cortaCorrenteTimer = setInterval(() => {

      if(this.count == 10){
        this.pauseTimer()
        this.dialogService.closeWaitDialog()
        this.count = 0
      }else
        this.checkLastSms(this.mapDataService.veiculo.equipamento.chip.numero)
    },5000)
  }

  private pauseTimer() {
    console.log("fechando timer")
    clearInterval(this.cortaCorrenteTimer);
  }

  recebido: boolean

  private checkLastSms(celular: string){
      this.count++
      return this.http.get<Sms>(`${environment.apiUrl}/lastSmsRecebido/${celular}`).subscribe(
        sms => {
          this.sms = sms
          if(this.sms.status == SmsStatus.RECEBIDO) {
            this.pauseTimer()
            this.dialogService.closeWaitDialog()
            this.dialogService.showDialog("Resposta do Rastreador", this.formatMessage())
          }else
            this.dialogService.messageTitle = this.formatMessage()
          console.log("lastSms", sms)
        },
        error1 => console.log("errror'", error1)
      );

  }

 /* private async showComandoStatusPopup() {
    this.loadingStatusPopup = await this.loadingController.create({
      message: this.formatMessage(),
      duration: 45*1000,
      mode: 'ios'
    });
    await this.loadingStatusPopup.present();
    const { role, data } = await this.loadingStatusPopup.onDidDismiss();
    this.pauseTimer()
    console.log('Loading dismissed!');
  }*/


  private formatMessage(): string{
    let message = "Status do Comando..."
    if(this.sms && this.sms.status == SmsStatus.ESPERANDO)
      message = `${this.sms.tipo} ESPERANDO`
    else if(this.sms && this.sms.status == SmsStatus.ESPERANDO_SOCKET)
      message = `${this.sms.tipo} ESPERANDO...`

    else if(this.sms.status == SmsStatus.RECEBIDO)
      message = this.sms.mensagem
    else
      message = `${this.sms.tipo} ${this.sms.status}`
    return message
  }

}
