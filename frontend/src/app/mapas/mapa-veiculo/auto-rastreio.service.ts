import { Injectable } from '@angular/core';
import {GoogleMapService} from './google-map.service';
import {MapaDataService} from './mapa-data.service';

@Injectable({
  providedIn: 'root'
})
export class AutoRastreioService {

  constructor(
    private googleMapService: GoogleMapService,
    private mapDataService: MapaDataService
  ) { }

  time: number = 0;


  mapaTimer;
  private startTimer() {
    this.pauseTimer()
    this.mapaTimer = setInterval(() => {
      this.time++;
      console.log("timer round",this.time)
      if(this.mapDataService.veiculo)
        this.googleMapService.rastrearVeiculo()
    },10000)
    console.log("timer reiniciado",this.mapaTimer)
  }

  private pauseTimer() {
    clearInterval(this.mapaTimer);
    console.log("timer pausado",this.mapaTimer)
  }


  controlRastreamento(fim: HTMLInputElement) {
    console.log("Control rasteramento")
    if (this.mapDataService.isRastreando()) {
      this.googleMapService.aproxima()
      this.startTimer();
    } else {
      this.pauseTimer();
    }
    this.googleMapService.rastrearVeiculo()
  }


}
