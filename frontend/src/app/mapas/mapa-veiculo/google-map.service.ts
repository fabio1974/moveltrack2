import {Injectable} from '@angular/core';
// @ts-ignore
import {} from '@types/googlemaps';
import {formatDate, formatNumber} from '@angular/common';
import {getIcon, getMapaObject, MapaObject} from '../mapaUtils';
import * as moment from 'moment';
import {Location2, Veiculo} from '../../shared/model';
import {MapaDataService} from './mapa-data.service';
import {DialogService} from '../../shared/dialog.service';
import {MapaHttpService} from '../mapa-http.service';
import {MessageService} from 'primeng/api';


@Injectable({
  providedIn: 'root'
})
export class GoogleMapService {

  map: google.maps.Map;
  mapaObject: MapaObject = getMapaObject([]);



  constructor(
    private mapDataService: MapaDataService,
    private dialogService: DialogService,
    private mapasHttpService: MapaHttpService,
    private messageService: MessageService
  ) {
  }


  loadMap(gmapElement) {
    var mapProp = {
      center: new google.maps.LatLng(this.mapaObject.centro.latitude, this.mapaObject.centro.longitude),
      zoom: 5,
      mapTypeControl: true,
      mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
        position: google.maps.ControlPosition.RIGHT_TOP,
      }
    };
    this.map = new google.maps.Map(gmapElement.nativeElement, mapProp);
  }


  rastrearVeiculo(showTelemetria:()=>void = null) {



    if (!this.mapDataService.veiculo) {
     // this.dialogService.showDialog('Atenção', 'Escolha um veículo para rastrear!');
      return;
    }


    if (!this.mapDataService.veiculo.equipamento) {
      this.dialogService.showDialog('Atenção', 'Veículo sem rastreador cadastrado!');
      return;
    }

    if(!this.mapDataService.rastreioAutomatico)
      this.dialogService.showWaitDialog('...rastreando veículo. Aguarde!');

    this.mapasHttpService.locationsByImeiDates(this.mapDataService.veiculo.equipamento.imei, this.mapDataService.inicio, this.mapDataService.fim).subscribe(
      resp => {
        this.dialogService.closeWaitDialog();
        this.limpaComponentes();
        this.mapDataService.lastLoc = null
        if ((resp as []).length <= 0) {

          this.map.setCenter({lat: this.mapaObject.centro.latitude, lng: this.mapaObject.centro.longitude});
          this.map.setZoom(this.mapaObject.zoom);
          //this.toastrService.error('Sem rastreamento no período selecionado!','Atenção')
          this.messageService.add({severity:'error',closable:false, summary:`Placa ${this.mapDataService.veiculo.placa}`, detail:'Veículo sem rastreamento no período selecionado!'});
          //this.dialogService.showDialog('Atenção', );
        } else {
          this.mapDataService.locations = resp as Location2[];
          this.mapDataService.lastLoc = this.mapDataService.locations.pop();
          this.mapDataService.locations.push(this.mapDataService.lastLoc)
          this.mapDataService.paradas = this.mapDataService.locations.filter(loc => loc.dateLocation.getTime() != loc.dateLocationInicio.getTime());
          this.mapaObject = getMapaObject(this.mapDataService.locations);
          this.desenhaMapaComponentes();
          showTelemetria()
        }

      },
      error => {
        this.dialogService.closeWaitDialog();
        this.dialogService.showDialog('Erro', error);
      });
  }


  polyline: google.maps.Polyline = null;
  carMarker: google.maps.Marker = null;
  stopMarkers: google.maps.Marker[] = [];
  bulletMarkers: google.maps.Marker[] = [];
  pathSize: number = null;

  desenhaMapaComponentes() {

    let isSpot = this.mapDataService.veiculo.equipamento.modelo == 'SPOT_TRACE'

    if(!this.mapDataService.isRastreando()) {
      this.map.setCenter({lat: this.mapaObject.centro.latitude, lng: this.mapaObject.centro.longitude});
      this.map.setZoom(this.mapaObject.zoom);
    }else{
      this.map.setCenter({lat: this.mapDataService.lastLoc.latitude, lng: this.mapDataService.lastLoc.longitude});
      //this.map.setZoom(18);
    }

    if(isSpot){
      this.mapDataService.locations.forEach(loc=>{
        if(!this.isParada(loc)) {

          var iconImage = {
            url: this.getBullet(loc.velocidade),
            anchor: new google.maps.Point(8,8)
          };

          let bulletMarker = new google.maps.Marker({
            position: new google.maps.LatLng(loc.latitude, loc.longitude),
            map: this.map,
            icon:iconImage
          });
          var contentString = this.createBulletInfoWindow(loc);
          bulletMarker.addListener('click', this.onClickBuildAndOpenInfoWindow.bind(this, bulletMarker, contentString));
          this.bulletMarkers.push(bulletMarker);
        }
      })
    }


    this.polyline = new google.maps.Polyline({
      path: this.mapDataService.locations.map(loc => {
        return {lat: loc.latitude, lng: loc.longitude};
      }),
      strokeColor: '#0000FF',
      strokeOpacity: isSpot? 0 : 0.6,
      icons: [{
          icon: this.seta,
          offset: '0',
          repeat: '200px'
        },
        {
          icon: this.lineSymbol,
          offset: '0',
          repeat: '15px'
        },
      ]
    });
    this.polyline.setMap(this.map);
    this.pathSize = google.maps.geometry.spherical.computeLength(this.polyline.getPath())/1000;


    if(this.mapDataService.viagem && this.mapDataService.viagem.rota){
      var jsonData = {
        "overview_polyline": {
          "points": this.mapDataService.viagem.rota.polyline
        }
      };
      let rota = new google.maps.Polyline({
        path: google.maps.geometry.encoding.decodePath(jsonData.overview_polyline.points),
        strokeColor: '#FF0000',
        strokeOpacity: 0.6,
      });
      rota.setMap(this.map);
    }


    this.mapDataService.paradas.forEach((parada, index) => {
      let zIndex = 0
      index++;
      if(index==this.mapDataService.paradas.length) {
        zIndex = 1001
      }

      let iconImage = {
        url: 'assets/icon/stop.png',
        labelOrigin: new google.maps.Point(11, 11),
        anchor: new google.maps.Point(11,55)
      };

      var stop = new google.maps.Marker({
        position: new google.maps.LatLng(parada.latitude, parada.longitude),
        map: this.map,
        zIndex: zIndex,
        label: {
          text: formatNumber(index, 'pt-BR', '2.0-0'),
          color: '#fff',
          fontWeight: 'bold',
        },
        icon: iconImage
      });
      var contentString = this.createParadaInfoWindow(parada, index);
      stop.addListener('click', this.onClickBuildAndOpenInfoWindow.bind(this, stop, contentString));
      this.stopMarkers.push(stop);
    });



    this.carMarker = new google.maps.Marker({
      position: new google.maps.LatLng(this.mapDataService.lastLoc.latitude, this.mapDataService.lastLoc.longitude),
      map: this.map,
      zIndex: 1002,
      icon: this.getIcon('gif'),
    });
    var contentString = this.createVeiculoInfoWindow();
    this.carMarker.addListener('click', this.onClickBuildAndOpenInfoWindow.bind(this, this.carMarker, contentString));



  }

  lineSymbol = {
    path: 'M 0,-1 0,1',
    strokeOpacity: 0.6,
    strokeWeight: 2,
    scale: 3
  };

  seta = {
    path: google.maps.SymbolPath.FORWARD_OPEN_ARROW,
    strokeOpacity: 0.8,
    strokeWeight: 2,
    scale: 2
  };

  aproxima(){
    this.map.setZoom(18);
  }

  limpaComponentes() {
    if (this.polyline) {
      this.polyline.setMap(null);
    }
    if (this.carMarker) {
      this.carMarker.setMap(null);
    }
    this.stopMarkers.forEach(stop => {
      stop.setMap(null);
    });

    this.bulletMarkers.forEach(stop => {
      stop.setMap(null);
    });

    this.polyline = null;
    this.carMarker = null;
    this.stopMarkers = [];
    this.bulletMarkers = [];
  }


  onClickBuildAndOpenInfoWindow = function (marker, contentString) {
    var infowindow = new google.maps.InfoWindow({
      content: contentString
    });
    infowindow.open(this.map, marker);
  };


  createVeiculoInfoWindow() {
    let linhaIgnition = '';
    if (this.mapDataService.lastLoc.ignition) {
      linhaIgnition = '<tr>'
        + '<td class=\'tg2\'>Ignição</td>'
        + '<td class=\'tg2\'>' + (this.mapDataService.lastLoc.ignition=='0'?'OFF':'ON') + '</td>'
        + '</tr>';
    }

    let linhaEvento = ''
    if(this.mapDataService.lastLoc.comando){
      linhaEvento =  '<tr>'
        + '<td class=\'tg2\'>Evento</td>'
        + '<td class=\'tg2\'>' + this.mapDataService.lastLoc.comando + '</td>'
        + '</tr>'
    }

    let icon = `assets/icon/${getIcon(this.mapDataService.veiculo.tipo)}.png`;
    let table = '<table style=\'width: 100%\' class=\'tg\'>'
      + '<tr>'
      + '<th colspan=\'2\' class=\'tg2\'><img width=\'10%\' src=\'' + icon + '\'></img> ' + this.mapDataService.veiculo.placa + ' - ' + this.mapDataService.veiculo.marcaModelo + ' - ' + this.mapDataService.veiculo.cor + '</th>'
      + '</tr>'
      + '<tr>'
      + '<td class=\'tg1\'>Último Sinal</td>'
      + '<td class=\'tg1\'>' + formatDate(this.mapDataService.lastLoc.dateLocation, 'dd/MM/yy HH:mm\'h\'', 'pt_BR') + '</td>'
      + '</tr>'

      + '<tr>'
      + '<td class=\'tg2\'>Velocidade</td>'
      + '<td class=\'tg2\'>' + formatNumber(this.mapDataService.lastLoc.velocidade, 'pt-BR', '1.2-2') + 'km/h</td>'
      + '</tr>'

      + '<tr>'
      + '<td class=\'tg1\'>Distância Percorrida</td>'
      + '<td class=\'tg1\'>' + formatNumber(this.pathSize, 'pt-BR', '1.3-3') + 'km</td>'
      + '</tr>'


      + linhaIgnition
      + linhaEvento

      + '<tr>'
      + '<td class=\'tg1\'>Endereço</td>'
      + '<td class=\'tg1\'>' + this.mapDataService.lastLoc.endereco + '</td>'
      + '</tr>'

      + '</table>';
    return this.infoWindowStyle + table;
  }


  isParada(loc){
    return loc.dateLocation.getTime() - loc.dateLocationInicio.getTime() > 180000
  }


  createParadaInfoWindow(parada, index) {

    let diff = moment.utc(moment(parada.dateLocation).diff(moment(parada.dateLocationInicio))).format('HH:mm:ss');

    let icon = `assets/icon/${getIcon(this.mapDataService.veiculo.tipo)}.png`;
    let table = '<table style=\'width: 100%\' class=\'tg\'>'
      + '<tr>'
      + '<th colspan=\'2\' class=\'tg2\'><img width=\'10%\' src=\'' + icon + '\'></img> ' + this.mapDataService.veiculo.placa + ' - Parada ' + formatNumber(index, 'pt-BR', '2.0-0') + '</th>'
      + '</tr>'
      + '<tr>'
      + '<td class=\'tg1\'>Chegada</td>'
      + '<td class=\'tg1\'>' + formatDate(parada.dateLocationInicio, 'dd/MM/yy HH:mm\'h\'', 'pt_BR') + '</td>'
      + '</tr>'
      + '<tr>'
      + '<td class=\'tg2\'>Saída</td>'
      + '<td class=\'tg2\'>' + formatDate(parada.dateLocation, 'dd/MM/yy HH:mm\'h\'', 'pt_BR') + '</td>'
      + '</tr>'
      + '<tr>'
      + '<td class=\'tg1\'>Tempo de Permanência</td>'
      + '<td class=\'tg1\'>' + diff + '</td>'
      + '</tr>'
      + '</table>';
    return this.infoWindowStyle + table;
  }


  createBulletInfoWindow(loc) {

    let linhaIgnition = '';
    if (loc.ignition) {
      linhaIgnition = '<tr>'
        + '<td class=\'tg2\'>Ignição</td>'
        + '<td class=\'tg2\'>' + (loc.ignition=='0'?'OFF':'ON') + '</td>'
        + '</tr>';
    }

    let linhaEvento = ''
    if(loc.comando){
      linhaEvento =  '<tr>'
        + '<td class=\'tg2\'>Evento</td>'
        + '<td class=\'tg2\'>' + loc.comando + '</td>'
        + '</tr>'
    }

    let diff = moment.utc(moment(loc.dateLocation).diff(moment(loc.dateLocationInicio))).format('HH:mm:ss');

    let icon = `assets/icon/${getIcon(this.mapDataService.veiculo.tipo)}.png`;
    let table = '<table style=\'width: 100%\' class=\'tg\'>'
      + '<tr>'
      + '<th colspan=\'2\' class=\'tg2\'><img width=\'10%\' src=\'' + icon + '\'></img> ' + this.mapDataService.veiculo.placa  + '</th>'
      + '</tr>'
      + '<tr>'
      + '<td class=\'tg1\'>Data/Hora</td>'
      + '<td class=\'tg1\'>' + formatDate(loc.dateLocation, 'dd/MM/yy HH:mm\'h\'', 'pt_BR') + '</td>'
      + '</tr>'
      + '<tr>'
      + '<td class=\'tg2\'>Velocidade</td>'
      + '<td class=\'tg2\'>' + formatNumber(loc.velocidade, 'pt-BR', '1.2-2') + 'km/h</td>'
      + '</tr>'
      + linhaIgnition
      + linhaEvento
      + '</table>';
    return this.infoWindowStyle + table;
  }



  infoWindowStyle = '<style type=\'text/css\'>'
    + '.tg  {border-collapse:collapse;border-spacing:0;border:none;border-color:#93a1a1;}'
    + '.tg td{text-align:center;,font-weight:bold;font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#93a1a1;color:#002b36;background-color:#fdf6e3;}'
    + '.tg th{text-align:left;font-weight:bold;font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#93a1a1;color:#fdf6e3;background-color:#657b83;}'
    + '.tg .tg1{background-color:#eee8d5;vertical-align:top}'
    + '.tg .tg2{vertical-align:top}'
    + '</style>';


  getIcon(extension) {
    return this.mapDataService.veiculo ? `assets/icon/${getIcon(this.mapDataService.veiculo.tipo)}.${extension}` : null
  }

  getBullet(velocidade: number) {
    if (velocidade > 80)
         return `assets/icon/bullet-red-sm.png`
    else if (velocidade > 60)
      return `assets/icon/bullet-yellow-sm.png`
    else
      return `assets/icon/bullet-green-sm.png`
  }


}
