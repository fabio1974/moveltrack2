import {Injectable} from '@angular/core';
import {buildCabecalho, fontSize, getDoc} from '../../relatorios/relatoriosUtils';
import {MapaDataService} from './mapa-data.service';
import {formatDate, formatNumber} from '@angular/common';
import * as moment from 'moment';
import {Cliente, Location2} from '../../shared/model';
import {MapaHttpService} from '../mapa-http.service';
import {DialogService} from '../../shared/dialog.service';
import {MessageService} from 'primeng/api';
import {SegurancaService} from '../../seguranca/seguranca.service';

@Injectable({
  providedIn: 'root'
})
export class MapaRelatorioService {

  cliente: Cliente

  constructor(private mapaDataService: MapaDataService,
              private mapaHttpService: MapaHttpService,
              private dialogService: DialogService,
              private segurancaService: SegurancaService,
              private messageService: MessageService) {
    this.cliente = this.segurancaService.pessoaLogada() as Cliente
  }


  public geraRelatorioPercurso(){

    if(!this.mapaDataService.viagem && this.mapaDataService.fim.getTime() - this.mapaDataService.inicio.getTime() > 3 * 86400000){
      this.dialogService.showDialog("Atenção","O período máximo para o relatório de percurso é de 72h")
      return;
    }

    if(this.mapaDataService.viagem && this.mapaDataService.fim.getTime() - this.mapaDataService.inicio.getTime() > 30 * 86400000){
      this.dialogService.showDialog("Atenção","O período máximo para o relatório de percurso em viagens é de 30 dias")
      return;
    }


    this.dialogService.showWaitDialog("...processando relatório de percurso")
    this.mapaHttpService.relatorioPercurso(this.mapaDataService.veiculo.equipamento.imei, this.mapaDataService.inicio, this.mapaDataService.fim).subscribe(
      resp => {
        this.dialogService.closeWaitDialog();
        if ((resp as []).length <= 0) {
          this.messageService.add({severity:'error',closable:false, summary:`Placa ${this.mapaDataService.veiculo.placa}`, detail:'Veículo sem rastreamento no período selecionado!'});
        } else {
          this.mapaDataService.locations = resp as Location2[];
          this.relatorioPercursoPdf()
        }
      },
      error => {
        this.dialogService.closeWaitDialog();
        this.dialogService.showDialog('Erro', error);
      });
  }

  public geraRelatorioParadas(){

    if(!this.mapaDataService.viagem && this.mapaDataService.fim.getTime() - this.mapaDataService.inicio.getTime() > 3 * 86400000){
      this.dialogService.showDialog("Atenção","O período máximo para o relatório de percurso é de 72h")
      return;
    }

    if(this.mapaDataService.viagem && this.mapaDataService.fim.getTime() - this.mapaDataService.inicio.getTime() > 30 * 86400000){
      this.dialogService.showDialog("Atenção","O período máximo para o relatório de percurso em viagens é de 30 dias")
      return;
    }

    this.dialogService.showWaitDialog("...processando relatório de paradas")
    this.mapaHttpService.relatorioParadas(this.mapaDataService.veiculo.equipamento.imei, this.mapaDataService.inicio, this.mapaDataService.fim).subscribe(
      resp => {
        this.dialogService.closeWaitDialog();
        if ((resp as []).length <= 0) {
          this.messageService.add({severity:'error',closable:false, summary:`Placa ${this.mapaDataService.veiculo.placa}`, detail:'Veículo sem rastreamento no período selecionado!'});
        } else {
          this.mapaDataService.locations = resp as Location2[];
          this.relatorioParadasPdf()
        }
      },
      error => {
        this.dialogService.closeWaitDialog();
        this.dialogService.showDialog('Erro', error);
      });
  }




  private relatorioPercursoPdf() {
    let doc = getDoc('landscape','mm','a4')
    //let total = rows.pop();
    let list0 = this.mapaDataService.locations
    let indexParadas = 0
    let list1 = list0.map(
      (row,index) => {
        index++
        let diff= '-'
        if(row.dateLocation.getTime()> row.dateLocationInicio.getTime()) {
          indexParadas++
          diff = moment.utc(moment(row.dateLocation).diff(moment(row.dateLocationInicio))).format("HH:mm:ss")
        }
        return [
          diff=='-'?"movimento":`parada ${formatNumber(indexParadas,'pt-BR','2.0-0')}`,
          row.endereco? row.endereco: `${formatNumber(row.latitude,'en','1.6-6')},${formatNumber(row.longitude,'en','1.6-6')}` ,
          //formatNumber(row.latitude,'pt-BR','1.6-6'),
          //formatNumber(row.longitude,'pt-BR','1.6-6'),
          //`www.google.com/maps/search/?api=1&query=${row.latitude},${row.longitude}`
          formatDate(row.dateLocationInicio,"dd/MM/yy HH:mm:ss",'pt-BR'),
          formatDate(row.dateLocation,"dd/MM/yy HH:mm:ss",'pt-BR'),
          diff,
          `${formatNumber(row.velocidade as number,'pt-BR','1.2-2')}km/h`
        ];
      });
    doc.autoTable({
      startY: buildCabecalho(this.cliente,doc, 'Relatório Analítico de Percurso',this.mapaDataService.inicio,this.mapaDataService.fim, `Placa: ${this.mapaDataService.veiculo.placa} -  Marca/Modelo: ${this.mapaDataService.veiculo.marcaModelo}`),
      styles: {halign: 'center', fontSize},
      head: [['Evento','Clique na linha para localizar','Inicio','Fim','Permanência','Velocidade']],
      body: [...list1]
    });
    doc.save('RelatorioDePercurso.pdf');
  }




  private relatorioParadasPdf() {
    let doc = getDoc('landscape','mm','a4')
    let list0 = this.mapaDataService.locations
    let indexParadas = 0

    let list1 = list0.map(
      (row,index) => {
        index++
        let diff= '-'
        if(row.dateLocation.getTime()> row.dateLocationInicio.getTime()) {
          indexParadas++
          diff = moment.utc(moment(row.dateLocation).diff(moment(row.dateLocationInicio))).format("HH:mm:ss")
        }
        return [
          `parada ${formatNumber(indexParadas,'pt-BR','2.0-0')}`,
           row.endereco,
         // formatNumber(row.latitude,'pt-BR','1.6-6'),
         // formatNumber(row.longitude,'pt-BR','1.6-6'),
          //`www.google.com/maps/search/?api=1&query=${row.latitude},${row.longitude}`,
          formatDate(row.dateLocationInicio,"dd/MM/yy HH:mm:ss",'pt-BR'),
          formatDate(row.dateLocation,"dd/MM/yy HH:mm:ss",'pt-BR'),
          diff,
          `${formatNumber(row.velocidade as number,'pt-BR','1.2-2')}km/h`
        ];
      });
    doc.autoTable({
      startY: buildCabecalho(this.cliente, doc, 'Relatório de Paradas',this.mapaDataService.inicio,this.mapaDataService.fim, `Placa: ${this.mapaDataService.veiculo.placa} -  Marca/Modelo: ${this.mapaDataService.veiculo.marcaModelo}`),
      styles: {halign: 'center', fontSize},
      head: [['Evento','Endereço','Inicio','Fim','Permanência','Velocidade']],
      body: [...list1]
    });
    doc.save('RelatorioDeParadas.pdf');
  }


}
