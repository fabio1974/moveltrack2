import {Location2} from '../shared/model';


export class MapaObject{
  lastLocations: Location2[];
  centro:Location2;
  zoom:number;
  pNE:Location2
  pSW:Location2
}

export function getMapaObject(amostra:Location2[]){
  let mapa = new MapaObject()
  let centro = new Location2();
  mapa.centro = centro ;

  if(amostra.length==0){
    mapa.centro.longitude =-51.82
    mapa.centro.latitude =-13.34
    mapa.pNE = mapa.centro
    mapa.pSW = mapa.centro
    mapa.zoom = 4
  }else if(amostra.length==1){
    mapa.centro.latitude = amostra[0].latitude
    mapa.centro.longitude = amostra[0].longitude
    mapa.pNE = mapa.centro
    mapa.pSW = mapa.centro
    mapa.zoom = 21
  }else {

    let maxLon= -181;
    let maxLat= -91;
    let minLon= 181;
    let minLat= 91;

    amostra.forEach(loc=>{
      if(maxLon < loc.longitude)
        maxLon = loc.longitude;
      if(maxLat < loc.latitude)
        maxLat = loc.latitude
      if(minLon > loc.longitude)
        minLon = loc.longitude
      if(minLat > loc.latitude)
        minLat = loc.latitude;
    })

    let ne = new Location2();
    ne.latitude =maxLat;
    ne.longitude =maxLon;
    mapa.pNE =ne;

    let sw = new Location2();
    sw.latitude =minLat;
    sw.longitude =minLon;
    mapa.pSW =sw;

    mapa.centro.latitude = (minLat+maxLat)/2
    mapa.centro.longitude= (minLon+maxLon)/2;

    mapa.zoom = getBestZoom(mapa.pNE, mapa.pSW);
  }
  return mapa;
}


export function getBestZoom(pNE:Location2, pSW:Location2){
  const GLOBE_WIDTH = 256;
  let angleX = pNE.longitude - pSW.longitude;
  let angleY = pNE.latitude - pSW.latitude;
  let delta = 0;
  if(angleY > angleX) {
    angleX = angleY;
    delta = 1;
  }
  if (angleX < 0) {
    angleX += 360;
  }
  let N = Math.log(960 * 360 / angleX / GLOBE_WIDTH);
  let r = Math.floor(N/Math.log(2));
  let zoom = r as number - delta;
  if(zoom>21)
    zoom = 21;
  return  zoom;
}

export function  getIcon(tipo: any) {
  switch (tipo) {
    case 'CAMINHAO' : return 'truck'
      break;
    case 'AUTOMOVEL': return 'carro'
      break;
    case 'TRATOR' : return 'trator'
      break;
    case 'PICKUP' : return 'pickup'
      break;
    case 'MOTOCICLETA' : return 'moto'
      break;
    default: return 'none'
  }
}
