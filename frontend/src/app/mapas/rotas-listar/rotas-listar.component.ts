import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {DialogService} from '../../shared/dialog.service';
import {SearchService} from '../../shared/search.service';
import {Cliente, Municipio, Page, Rota} from '../../shared/model';
import {LazyLoadEvent} from 'primeng/api';
import {RotasService} from '../rotas.service';
import {SegurancaService} from '../../seguranca/seguranca.service';

@Component({
  selector: 'app-rotas-listar',
  templateUrl: './rotas-listar.component.html',
  styleUrls: ['./rotas-listar.component.css']
})
export class RotasListarComponent implements OnInit {

  constructor(
    public rotasService:RotasService,
    private dialogService: DialogService,
    private searchService: SearchService,
    private segurancaService: SegurancaService
  ) {}


  page:Page<Rota> = null;
  inited:boolean = false
  loaded: boolean = false;


  ngOnInit(): void {
    this.page = new Page()
    this.page.pageSize = 5
    this.page.sortField = 'id'
    this.listarPagina();
    this.inited = true
  }

  listarPagina() {
    this.rotasService.filtro.cliente   = this.segurancaService.pessoaLogada() as Cliente
    this.dialogService.showWaitDialog("...processando. Aguarde!")
    console.log("Filtro",this.rotasService.filtro)
    console.log("Municipioresults", this.searchService.municipiosResult)
    this.rotasService.getRotas(this.page,this.rotasService.filtro)
      .subscribe(
        resp => {
          this.dialogService.closeWaitDialog()
          this.loaded = true;
          this.page.list = (resp as any).content;
          this.page.totalRecords = (resp as any).totalElements;
        },
        error =>{
          this.dialogService.closeWaitDialog()
          this.dialogService.showDialog(error.statusText,error.message)
        }
      );
  }


  sortBy(field,direction) {
    this.page.sortField = field
    this.page.sortDirection = direction
    this.page.pageIndex=0
    this.listarPagina()
    this.page.sortDirection = direction
  }



  onLazyLoad(event: LazyLoadEvent) {
    console.log("EVENT",event)
    this.page.pageIndex = event.first / event.rows;
    if(!this.inited)
      this.listarPagina();
    this.inited = false
  }

  filtrar() {

    // @ts-ignore
    if(this.rotasService.filtro.origem =='')
      this.rotasService.filtro.origem = null

    if(this.rotasService.filtro.destino =='')
      this.rotasService.filtro.destino = null

    this.page.pageIndex=0
    this.listarPagina()
  }


  updatePageSize() {
    this.page.pageIndex=0
    this.listarPagina()
  }

  formataItinerario(itinerario: Municipio[]) {
    let s: string = ''
    itinerario.forEach(m=>{
      s+= `, ${m.label}`
    })
    return s.substr(1)
  }

  @Output() emitter = new EventEmitter();
  update(rota) {
    this.emitter.emit(rota)
  }

  clearValue() {
    console.log("ONCLEAR")
    this.searchService.municipiosResult = null
    console.log(this.rotasService.filtro)
  }

  delete(rota) {
    this.dialogService.showWaitDialog("...processando. Aguarde!")
      this.rotasService.deleteRota(rota).subscribe(
        resp=>{
          this.listarPagina()
          this.dialogService.closeWaitDialog()
          this.dialogService.showDialog("Atenção","Rota apagada com sucesso!")

        },
        error=>{
          this.dialogService.closeWaitDialog()
          this.dialogService.showDialog("Não foi possível excluir","Essa rota está associada a uma viagem!")

    })
  }

}
