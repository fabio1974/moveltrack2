import {Component, OnInit, ViewChild} from '@angular/core';
import {MapaHttpService} from '../mapa-http.service';
import {DialogService} from '../../shared/dialog.service';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {SearchService} from '../../shared/search.service';
import {Cliente, Municipio, Rota, Veiculo} from '../../shared/model';
import {getMapaObject, MapaObject} from '../mapaUtils';
import {DirectionsMapDirective} from '../directions-map.directive';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {RotasService} from '../rotas.service';
import {RotasListarComponent} from '../rotas-listar/rotas-listar.component';

@Component({
  selector: 'app-rotas',
  templateUrl: './rotas.component.html',
  styleUrls: ['./rotas.component.css']
})
export class RotasComponent implements OnInit {

  mapaObject: MapaObject = getMapaObject([])
  mapTypeId = 'roadmap'
  rota: Rota
  municipioNovo: Municipio;
  displayDirections = false;
  caminhoSelecionado: Municipio[];


  constructor(private mapasService: MapaHttpService,
              private dialogService: DialogService,
              private segurancaService: SegurancaService,
              public searchService: SearchService,
              private http: HttpClient,
              private rotasService: RotasService
  ) {
    this.rota =  new Rota()

    this.caminhoSelecionado = []
  }





  ngOnInit() {
    console.log("mapaObject",this.mapaObject)
  }


  addMunicipio() {
    if(this.rota.itinerario.length>=8) {
      this.dialogService.showDialog("Erro", "Tamanho máximo do intinerário: 8 municípios")
      return
    }
    this.rota.itinerario.push(this.municipioNovo)
    this.municipioNovo = null
    this.displayDirections = false;
    this.directionsMapDirective.status = null
  }

  deletaMunicipio() {
    console.log("this.caminhoSelecionado",this.caminhoSelecionado)
    this.rota.itinerario = this.rota.itinerario.filter(it=> !this.caminhoSelecionado.includes(it))
    this.caminhoSelecionado = null
    this.displayDirections = false;
    this.directionsMapDirective.status = null
  }

  changeDestino() {
    this.displayDirections = false;
    this.directionsMapDirective.status = null
  }

  changeOrigem() {
    this.displayDirections = false;
    this.directionsMapDirective.status = null
  }

  plotarRota() {

    if(!this.rota.origem){
      this.dialogService.showDialog("Erro", "É necessário o município de orígem da rota")
      return
    }

    if(!this.rota.origem.latitude || !this.rota.origem.longitude){
      this.dialogService.showDialog("Erro", `Município ${this.rota.origem.label} não possui coordenadas cadastradas. Informe ao administrador`)
      return
    }

    if(!this.rota.destino){
      this.dialogService.showDialog("Erro", "É necessário o município de destino da rota")
      return
    }

    if(!this.rota.destino.latitude || !this.rota.destino.longitude){
      this.dialogService.showDialog("Erro", `Município ${this.rota.destino.label} não possui coordenadas cadastradas. Informe ao administrador`)
      return
    }

    this.dialogService.showWaitDialog("calculando rota...")

    this.displayDirections = true;
  }

  @ViewChild(DirectionsMapDirective) directionsMapDirective:DirectionsMapDirective;
  @ViewChild(RotasListarComponent) rotasListarComponent: RotasListarComponent

  salvaRota() {


    if(!this.rota.polyline) {
      this.rota.polyline = this.directionsMapDirective.polyline
    }

    if(!this.rota.polyline){
      this.dialogService.showDialog("Erro","Rota ainda carregando...tente novamente")
      return
    }

    if(!this.displayDirections){
      this.dialogService.showDialog("Aviso","Para salvar é necessário mostrar a rota  no mapa ao lado")
      return

    }

    this.rota.distancia = this.directionsMapDirective.distancia
    this.rota.cliente   = this.segurancaService.pessoaLogada() as Cliente

    this.dialogService.showWaitDialog("Salvando a rota")
    this.http.post(`${environment.apiUrl}/rotas`,this.rota).subscribe(
      resp=>{

        let rotaRetorno = resp as Rota
        if(rotaRetorno){
          this.directionsMapDirective.status = null
          this.dialogService.showDialog("Aviso", `Esta rota já existe no banco de dados. Rota ${rotaRetorno.id.toString().padStart(5,"0")}`)
        }else{
          this.rotasListarComponent.listarPagina()
          this.dialogService.showDialog("Aviso", this.rota.id? `Rota ${this.rota.id.toString().padStart(5,"0")} modificada com sucesso!`:"Nova rota criada!")
        }
        this.dialogService.closeWaitDialog()
        console.log(resp)
      },error=>{
        this.dialogService.showDialog("Erro", error)
        this.dialogService.closeWaitDialog()
      }
    )

    console.log("Rota",this.rota)
  }

  updateRota(rota: Rota) {
    this.displayDirections = false;
    this.directionsMapDirective.status = null
    this.rota = rota
    this.caminhoSelecionado = []
  }

  criarNovaRota() {
    this.displayDirections = false;
    this.directionsMapDirective.status = null
    this.rota = new Rota()
    this.caminhoSelecionado = []
  }
}
