import { Component, OnInit } from '@angular/core';
import {MapaHttpService} from '../mapa-http.service';
import {MapaDataService} from '../mapa-veiculo/mapa-data.service';

@Component({
  selector: 'app-telemetria',
  templateUrl: './telemetria.component.html',
  styleUrls: ['./telemetria.component.css']
})
export class TelemetriaComponent implements OnInit {

  constructor(private mapaHttpService: MapaHttpService,private mapDataService: MapaDataService) { }

  ngOnInit() {
    if (this.mapDataService.veiculo.equipamento) {
      this.mapaHttpService.telimetria(this.mapDataService.veiculo.equipamento.imei).subscribe(
        resp => this.sinal = this.locationToSinal(resp as Location)
      ), error => {
        console.log('Erro na telemetria', error);
      };
    }
  }

  update(){
    if (this.mapDataService.veiculo.equipamento) {
      this.mapaHttpService.telimetria(this.mapDataService.veiculo.equipamento.imei).subscribe(
        resp => this.sinal = this.locationToSinal(resp as Location)
      ), error => {
        console.log("Erro na telemetria", error)
      }
    }
  }

  imei
  sinal = new Sinal()

  time: number = 0;
  timer;

  public startTimer() {
    this.update()
    this.pauseTimer()
    this.timer = setInterval(() => {
      this.time++;
      console.log("timer telemetria round",this.time)

      this.update()

    },10 * 1000)
    console.log("timer telemetria reiniciado",this.timer)
  }

  public pauseTimer() {
    clearInterval(this.timer);
    console.log("timer telemetria pausado",this.timer)
  }



  locationToSinal(location):Sinal{
    let sinal = new Sinal()

    switch (location.battery) {
      case '0': {
        sinal.bateria = 'On'
        sinal.bateriaClass = 'btn-success'
        break;
      }
      case '1': {
        sinal.bateria = 'Off';
        sinal.bateriaClass = 'btn-danger'
        break;
      }
    }

    switch (location.gsm) {
      case '0': {
        sinal.gsm = '0%'
        sinal.gsmClass = 'btn-danger'
        break;
      }
      case '1': {
        sinal.gsm = '~20%';
        sinal.gsmClass = 'btn-danger'
        break;
      }
      case '2': {
        sinal.gsm = '~50%';
        sinal.gsmClass = 'btn-warning'
        break;
      }
      case '3': {
        sinal.gsm = '~75%';
        sinal.gsmClass = 'btn-success'
        break;
      }
      case '4': {
        sinal.gsm = '>90%';
        sinal.gsmClass = 'btn-success'
        break;
      }
    }

    switch (location.gps) {
      case 'On': {
        sinal.gps = 'On'
        sinal.gpsClass = 'btn-success'
        break;
      }
      case 'Off': {
        sinal.gps = 'Off';
        sinal.gpsClass = 'btn-danger'
        break;
      }
    }

    sinal.satelites = location.satelites
    sinal.sateliteClass = sinal.satelites<4?'btn-secondary':'btn-success'

    sinal.bloqueio = location.bloqueio
    sinal.bloqueioClass = sinal.bloqueio=='Sim'?'btn-danger':'btn-success'

    switch (location.ignition) {
      case '0': {
        sinal.ignicao = 'Off';
        sinal.ignicaoClass = 'btn-secondary'
        break;
      }
      case '1': {
        sinal.ignicao = 'On';
        sinal.ignicaoClass = 'btn-success'
        break;
      }
    }

    switch (location.alarmType) {
      case '000': {
        sinal.alarme = 'Off';
        sinal.alarmeClass = 'btn-secondary'
        break;
      }
      case '100': {
        sinal.alarme = 'Pânico!';
        sinal.alarmeClass = 'btn-danger'
        break;
      }
      case '010': {
        sinal.alarme = 'Desconectando Bateria!';
        sinal.alarmeClass = 'btn-danger'
        break;
      }
      case '011': {
        sinal.alarme = 'Bateria Fraca!';
        sinal.alarmeClass = 'btn-danger'
        break;
      }
      case '001': {
        sinal.alarme = 'Shock';
        sinal.alarmeClass = 'btn-danger'
        break;
      }
    }

    sinal.voltagem = location.volt
    switch (location.volt) {
      case '>90%': {
        sinal.voltagemClass = 'btn-success'
        break;
      }
      case '>70%': {
        sinal.voltagemClass = 'btn-success'
        break;
      }
      case '~50%': {
        sinal.voltagemClass = 'btn-warning'
        break;
      }
      case '<30%': {
        sinal.voltagemClass = 'btn-warning'
        break;
      }
      case '<10%': {
        sinal.voltagemClass = 'btn-danger'
        break;
      }
      case '<5%': {
        sinal.voltagemClass = 'btn-danger'
        break;
      }
      case '0%': {
        sinal.voltagemClass = 'btn-danger'
        break;
      }
    }
    sinal.leitura = location.dateLocation
    return sinal
  }



}



export class Sinal{
  bateria
  bateriaClass
  voltagem
  voltagemClass
  gsm
  gsmClass
  gps
  gpsClass
  satelites
  sateliteClass
  bloqueio
  bloqueioClass
  ignicao
  ignicaoClass
  alarme
  alarmeClass
  leitura = new Date()
}
