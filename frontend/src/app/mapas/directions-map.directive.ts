import {Directive, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {GoogleMapsAPIWrapper, LatLng} from '@agm/core';
import {Municipio} from '../shared/model';
import {DialogService} from '../shared/dialog.service';

// You can use any other interface for origem and destino, but it must contain latlng data
export interface ILatLng {
  latitude: number;
  longitude: number;
}

// the will keep typescript from throwing errors w.r.t the google object
declare var google: any;

@Directive({
  selector: '[appDirectionsMap]'
})
export class DirectionsMapDirective implements OnInit, OnChanges {

  @Input() origem: Municipio;
  @Input() itinerario:Municipio[];
  @Input() destino: Municipio;
  @Input() showDirection: boolean;
  polyline:any
  distancia:number

  // We'll keep a single google maps directions renderer instance so we get to reuse it.
  // using a new renderer instance every time will leave the previous one still active and visible on the page
  private directionsRenderer: any;
  status: any;

  // We inject AGM's google maps api wrapper that handles the communication with the Google Maps Javascript
  constructor(private gmapsApi: GoogleMapsAPIWrapper, private dialogService: DialogService) {}

  ngOnInit() {
    this.drawDirectionsRoute();
  }

  drawDirectionsRoute() {
    this.gmapsApi.getNativeMap().then(map => {
      if (!this.directionsRenderer) {
        // if you already have a marker at the coordinate location on the map, use suppressMarkers option
        // suppressMarkers prevents google maps from automatically adding a marker for you
        this.directionsRenderer = new google.maps.DirectionsRenderer({suppressMarkers: true});
      }
      const directionsRenderer = this.directionsRenderer;

      if ( this.showDirection && this.destino ) {
        const directionsService = new google.maps.DirectionsService;
        directionsRenderer.setMap(map);
        directionsService.route({
          origin: {lat: this.origem.latitude, lng: this.origem.longitude},
          destination: {lat: this.destino.latitude, lng: this.destino.longitude},
          waypoints: this.itinerario.map(m => {
            return {location: `${m.descricao},${m.uf}`, stopover: true}
          }),
          optimizeWaypoints: true,
          travelMode: 'DRIVING'
        }, (response, status) => {
          this.status = status
          if (status === 'OK') {
            directionsRenderer.setDirections(response);
            this.polyline = response.routes[0].overview_polyline

            var jsonData = {
              "overview_polyline": {
                "points": response.routes[0].overview_polyline
              }
            };
            var path = google.maps.geometry.encoding.decodePath(jsonData.overview_polyline.points);
            this.distancia = google.maps.geometry.spherical.computeLength(path)/1000;
            this.dialogService.closeWaitDialog()
            //let poly = google.maps.geometry.encoding.decodePath(jsonData.overview_polyline.points);
            // If you'll like to display an info window along the route
            // middleStep is used to estimate the midpoint on the route where the info window will appear
            // const middleStep = (polyline.routes[0].legs[0].steps.length / 2).toFixed();
            // const infowindow2 = new google.maps.InfoWindow();
            // infowindow2.setContent(`${polyline.routes[0].legs[0].distance.text} <br> ${polyline.routes[0].legs[0].duration.text}  `);
            // infowindow2.setPosition(polyline.routes[0].legs[0].steps[middleStep].end_location);
            // infowindow2.open(map);
          } else {
            this.dialogService.closeWaitDialog()
            this.dialogService.showDialog("Erro",status)
            console.log('Directions request failed due to ' + status);
          }
        });
      }

    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.destination || changes.showDirection) {
      // this checks if the show directions input changed, if so the directions are removed
      // else we redraw the directions
      if (changes.showDirection && !changes.showDirection.currentValue) {
        if (this.directionsRenderer !== undefined) { // check this value is not undefined
          this.directionsRenderer.setDirections({routes: []});
          return;
        }
      } else {
        this.drawDirectionsRoute();
      }
    }
  }

}
