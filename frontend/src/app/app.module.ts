import {LOCALE_ID, NgModule} from '@angular/core';
import {PerfectScrollbarConfigInterface} from 'ngx-perfect-scrollbar';
import {AppComponent} from './app.component';
import {HomeComponent} from './core/home/home.component';
import {SegurancaModule} from './seguranca/seguranca.module';
import {CoreModule} from './core/core.module';
import {BoletoModule} from './boleto/boleto.module';
import {CalendarModule} from 'primeng/primeng';
import {CarneModule} from './carne/carne.module';
import {ContratoModule} from './contrato/contrato.module';
import {MapasModule} from './mapas/mapas.module';
import {ViagemModule} from './viagem/viagem.module';
import {DespesaFrotaModule} from './despesa-frota/despesa-frota.module';
import {MotoristasModule} from './motoristas/motoristas.module';
import {VeiculosModule} from './veiculos/veiculos.module';
import {RelatoriosModule} from './relatorios/relatorios.module';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {MyHttpInterceptor} from './seguranca/MyHttpInterceptor';
import {ChipModule} from './chip/chip.module';
import {EquipamentoModule} from './equipamento/equipamento.module';
import {OrdemDeServicoModule} from './ordem-de-servico/ordem-de-servico.module';
import {LancamentoModule} from './lancamento/lancamento.module';
import {defineLocale} from 'ngx-bootstrap/chronos';
import {ptBrLocale} from 'ngx-bootstrap/locale';
import {BsLocaleService} from 'ngx-bootstrap';
import {RelatoriosInternosModule} from './relatorios-internos/relatorios-internos.module';
import {EmpregadoModule} from './empregado/empregado.module';
import {ControleModule} from './controle/controle.module';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

defineLocale('pt-br', ptBrLocale);

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    CoreModule,
    BoletoModule,
    SegurancaModule,
    CalendarModule,
    CarneModule,
    ContratoModule,
    MapasModule,
    ViagemModule,
    EquipamentoModule,
    ChipModule,
    DespesaFrotaModule,
    MotoristasModule,
    VeiculosModule,
    RelatoriosModule,
    OrdemDeServicoModule,
    LancamentoModule,
    RelatoriosInternosModule,
    EmpregadoModule,
    ControleModule
  ],


  providers: [
    { provide: LOCALE_ID, useValue: "pt-BR" },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MyHttpInterceptor,
      multi: true,

    }
  ],

  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private bsLocaleService: BsLocaleService) {
    this.bsLocaleService.use('pt-br');
  }
}
