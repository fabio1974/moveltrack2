import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListarEquipamentoComponent } from './listar-equipamento/listar-equipamento.component';
import { CriarEquipamentoComponent } from './criar-equipamento/criar-equipamento.component';
import {SharedModule} from '../shared/shared.module';
import {AutoCompleteModule} from 'primeng/primeng';

@NgModule({
  declarations: [ListarEquipamentoComponent, CriarEquipamentoComponent],
  imports: [
    CommonModule,
    SharedModule,
    AutoCompleteModule
  ]
})
export class EquipamentoModule { }
