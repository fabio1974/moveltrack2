import { Component, OnInit } from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {EquipamentoService} from '../equipamento.service';
import {Chip, Cliente, Empregado, Equipamento, EquipamentoSituacao, Operacao, Veiculo} from '../../shared/model';
import {markControlsAsTouched, Obrigatorio, setAllDisabled, TamanhoMaximo, TamanhoMinimo} from '../../shared/formUtils';
import {DialogService} from '../../shared/dialog.service';
import {SearchService} from '../../shared/search.service';
import {DatepickerConfigService} from '../../shared/bsDateTimePickerConfig';


@Component({
  selector: 'app-criar-equipamento',
  templateUrl: './criar-equipamento.component.html',
  styleUrls: ['./criar-equipamento.component.css']
})
export class CriarEquipamentoComponent implements OnInit {



  constructor(private fb: FormBuilder,
              public equipamentoService: EquipamentoService,
              private dialogService: DialogService,
              public searchService: SearchService,
              public datepickerConfigService: DatepickerConfigService
              ) {

    this.formGroup.setValue(this.equipamentoService.equipamento)

    setAllDisabled(this.formGroup)

    if(this.equipamentoService.operacao==Operacao.CREATE)
      setAllDisabled(this.formGroup,false)
    else if(this.equipamentoService.operacao==Operacao.SHOW)
      setAllDisabled(this.formGroup,true)
    else if(this.equipamentoService.operacao==Operacao.UPDATE){
      setAllDisabled(this.formGroup,false)
    }
  }

  ngOnInit() {
  }

  formGroup = this.fb.group({
    id: [],
    dataCadastro:  [,Obrigatorio],
    situacao:   [,Obrigatorio],
    proprietario:   [,],
    modelo:   [,Obrigatorio],
    senha:   [],
    imei:   [,[Obrigatorio,TamanhoMinimo(15)]],
    atrasoGmt: [],
    chip: [],
    comando: [],
    observacao: [],
    ultimaAlteracao: [],
    possuidor: [,Obrigatorio],
    label:[],
    lastLocation:[],
    veiculo:[],
    inicioSpot:[],
    vencimentoSpot:[]
  })


  submitForm() {
    if(this.formGroup.invalid) {
      console.log("this.formGroup.controls",this.formGroup.controls)
      markControlsAsTouched(this.formGroup)
      this.dialogService.showDialog("Atenção","Seu formulário contém erros")
      return
    }
    //setAllDisabled(this.formGroup,false)
    let equipamento = this.formGroup.value as Equipamento

    this.dialogService.showWaitDialog("...aguarde, Processando")
    this.equipamentoService.postEquipamento(equipamento).subscribe(
      resp=>{
        this.dialogService.closeWaitDialog()
        this.equipamentoService.goList()
      },error1 => {
        let {error,message} = error1.error
        this.dialogService.closeWaitDialog()
        this.dialogService.showDialog(error,message)
      })
  }

}
