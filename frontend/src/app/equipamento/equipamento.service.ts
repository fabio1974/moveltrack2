import { Injectable } from '@angular/core';
import {Equipamento, Contrato, Operacao, Page, SelectOption, Chip} from '../shared/model';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {DialogService} from '../shared/dialog.service';

@Injectable({
  providedIn: 'root'
})
export class EquipamentoService {

  equipamento: Equipamento
  operacao = Operacao.SHOW
  filtro: Equipamento = new Equipamento()
  modelosRastreador: SelectOption[]
  equipamentosSituacao: SelectOption[]

  page:Page<Equipamento> = new Page();


  constructor(private http: HttpClient,
              private dialogService: DialogService,
              private router: Router) {
    this.filtro.chip = new Chip()
    this.page.pageIndex = 1
    this.page.pageSize = 10
    this.loadModelosRastreador()
    this.loadEquipamentosSituacao()
    this.listarPagina()
  }

  listarPagina() {
    this.dialogService.showWaitDialog("...processando. Aguarde!")
    this.getEquipamentos(this.page,this.filtro)
      .subscribe(
        resp => {
          this.dialogService.closeWaitDialog()
          this.page.list = ((resp as any).content) as Equipamento[];
          this.page.totalRecords = (resp as any).totalElements;
        },
        error =>{
          this.dialogService.closeWaitDialog()
          this.dialogService.showDialog(error.statusText,error.message)
        }
      );
  }

  goCreate(){
    this.operacao = Operacao.CREATE
    this.equipamento = new Equipamento()
    this.router.navigate(["/criarEquipamento"])
  }

  goUpdate(equipamento){
    this.equipamento = equipamento
    this.operacao = Operacao.UPDATE
    this.router.navigate(["/criarEquipamento"])
  }

  goShow(equipamento){
    this.equipamento = equipamento
    this.operacao = Operacao.SHOW
    this.router.navigate(["/criarEquipamento"])
  }

  goList(){
    this.listarPagina()
    this.router.navigate(["/listarEquipamento"])
  }

  getEquipamentos(page: Page<Equipamento>, filtro: Equipamento): Observable<any>  {
    const query = `pageIndex=${page.pageIndex-1}&pageSize=${page.pageSize}&sortField=${page.sortField}&sortDirection=${page.sortDirection}`;
    return this.http.patch(`${environment.apiUrl}/equipamentos?${query}`,filtro);
  }

  loadModelosRastreador(){
    this.http.get(`${environment.apiUrl}/modelosRastreador`).subscribe(
      resp=>{
        console.log(resp)
        this.modelosRastreador = (resp as string[]).map(it=> new SelectOption(it,it))
      },
      error => {
        console.log(error)
      }
    );
  }

  loadEquipamentosSituacao(){
    this.http.get(`${environment.apiUrl}/equipamentosSituacao`).subscribe(
      resp=>{
        console.log(resp)
        this.equipamentosSituacao = resp as SelectOption[]
      },
      error => {
        console.log(error)
      }
    );
  }


  postEquipamento(equipamento) {
    return this.http.post(`${environment.apiUrl}/equipamentos`,equipamento);
  }

  putEquipamento(equipamento) {
    return this.http.put(`${environment.apiUrl}/equipamentos/${equipamento.id}`,equipamento);
  }

}
