import {Component, HostListener, OnInit} from '@angular/core';
import {EquipamentoService} from '../../equipamento/equipamento.service';
import {DialogService} from '../../shared/dialog.service';
import {Router} from '@angular/router';
import {SearchService} from '../../shared/search.service';
import {DespesaFrotaService} from '../../despesa-frota/despesa-frota.service';
import {SegurancaService} from '../../seguranca/seguranca.service';
import {AtivoInativo, Equipamento, Page} from '../../shared/model';
import {LazyLoadEvent} from 'primeng/api';
import {PageChangedEvent} from 'ngx-bootstrap';

@Component({
  selector: 'app-listar-equipamento',
  templateUrl: './listar-equipamento.component.html',
  styleUrls: ['./listar-equipamento.component.css']
})
export class ListarEquipamentoComponent  {

  constructor(
    public equipamentoService: EquipamentoService,
    private dialogService: DialogService,
    private router: Router,
    public searchService: SearchService,
    public segurancaService: SegurancaService
  ) {

  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvents(event: KeyboardEvent) {
    console.log('event.key', event.key)
    switch (event.key) {
      case 'Enter':
        event.preventDefault();
        this.filtrar()
        break;
    }
  }

  filtrar() {
    this.equipamentoService.page.pageIndex=1
    this.equipamentoService.listarPagina()
  }

  changePageSize() {
    this.equipamentoService.page.pageIndex=1
    this.equipamentoService.listarPagina()
  }

  pageChanged($event: PageChangedEvent) {
    if(this.equipamentoService.page.pageIndex != $event.page){
      this.equipamentoService.page.pageIndex = $event.page;
      this.equipamentoService.listarPagina();
    }
  }
}
