#!/bin/bash
REGISTRY=fb040974
IMAGE=moveltrack-frontend
VERSAO=$(grep VERSAO src/environments/versao.ts | cut -d '"' -f2)

echo "docker build . -t $REGISTRY/$IMAGE:$VERSAO"
docker build . -t $REGISTRY/$IMAGE:$VERSAO

echo "docker push $REGISTRY/$IMAGE:$VERSAO"
docker push $REGISTRY/$IMAGE:$VERSAO

echo "kubectl apply -f deployment.yml"
kubectl apply -f deployment.yml

echo "kubectl delete pods -l name=moveltrack-frontend"
kubectl delete pods -l name=moveltrack-frontend

echo "kubectl delete hpa moveltrack-frontend"
kubectl delete hpa moveltrack-frontend

echo "kubectl autoscale -f deployment.yml  --min=1  --max=5 --cpu-percent=80"
kubectl autoscale -f deployment.yml  --min=1  --max=5 --cpu-percent=80


echo "Versão Nova no Ar!!!!"

