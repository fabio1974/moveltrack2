package br.com.moveltrack2.backend.utils

import br.com.moveltrack.persistence.domain.Location


object GeoDistanceCalulator {

    private val acochambratorFactor = 1.06f

   /* fun distVincenty(ponto1: LatLng, ponto2: LatLng): Double {
        return vicentDistance(ponto1.lat, ponto1.lng, ponto2.lat, ponto2.lng)
    }*/

    /**
     * Calculates geodetic distance between two points specified by latitude/longitude using Vincenty inverse formula
     * for ellipsoids
     *
     * @param lat1
     * first point latitude in decimal degrees
     * @param lon1
     * first point longitude in decimal degrees
     * @param lat2
     * second point latitude in decimal degrees
     * @param lon2
     * second point longitude in decimal degrees
     * @returns distance in meters between points with 5.10<sup>-4</sup> precision
     * @see [Originally posted here](http://www.movable-type.co.uk/scripts/latlong-vincenty.html)
     */
    fun vicentDistance(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
        val a = 6378137.0
        val b = 6356752.314245
        val f = 1 / 298.257223563 // WGS-84 ellipsoid params
        val L = Math.toRadians(lon2 - lon1)
        val U1 = Math.atan((1 - f) * Math.tan(Math.toRadians(lat1)))
        val U2 = Math.atan((1 - f) * Math.tan(Math.toRadians(lat2)))
        val sinU1 = Math.sin(U1)
        val cosU1 = Math.cos(U1)
        val sinU2 = Math.sin(U2)
        val cosU2 = Math.cos(U2)

        var sinLambda: Double
        var cosLambda: Double
        var sinSigma: Double
        var cosSigma: Double
        var sigma: Double
        var sinAlpha: Double
        var cosSqAlpha: Double
        var cos2SigmaM: Double
        var lambda = L
        var lambdaP: Double
        var iterLimit = 100.0
        do {
            sinLambda = Math.sin(lambda)
            cosLambda = Math.cos(lambda)
            sinSigma = Math.sqrt(cosU2 * sinLambda * (cosU2 * sinLambda) + (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda) * (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda))
            if (sinSigma == 0.0)
                return 0.0 // co-incident points
            cosSigma = sinU1 * sinU2 + cosU1 * cosU2 * cosLambda
            sigma = Math.atan2(sinSigma, cosSigma)
            sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma
            cosSqAlpha = 1 - sinAlpha * sinAlpha
            cos2SigmaM = cosSigma - 2.0 * sinU1 * sinU2 / cosSqAlpha
            if (java.lang.Double.isNaN(cos2SigmaM))
                cos2SigmaM = 0.0 // equatorial line: cosSqAlpha=0 (§6)
            val C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha))
            lambdaP = lambda
            lambda = L + ((1 - C) * f * sinAlpha
                    * (sigma + C * sinSigma * (cos2SigmaM + C * cosSigma * (-1 + 2.0 * cos2SigmaM * cos2SigmaM))))
        } while (Math.abs(lambda - lambdaP) > 1e-12 && --iterLimit > 0)

        if (iterLimit == 0.0)
            return java.lang.Double.NaN // formula failed to converge

        val uSq = cosSqAlpha * (a * a - b * b) / (b * b)
        val A = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)))
        val B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)))
        val deltaSigma = (B
                * sinSigma
                * (cos2SigmaM + B / 4 * (cosSigma * (-1 + 2.0 * cos2SigmaM * cos2SigmaM) - (B / 6 * cos2SigmaM
                * (-3 + 4.0 * sinSigma * sinSigma) * (-3 + 4.0 * cos2SigmaM * cos2SigmaM)))))
        val dist = b * A * (sigma - deltaSigma)

        return acochambratorFactor * dist
    }

    fun haverSineDistance(obj2: Location, obj1: Location): Double {
       // if (obj2 is Location) {
            val loc1 = obj1 as Location
            val loc2 = obj2 as Location
            return haverSineDistance(loc1.latitude, loc1.longitude, loc2.latitude, loc2.longitude)
        /*} else {
            val loc1 = obj1 as Location
            val loc2 = obj2 as Location
            return haverSineDistance(loc1.latitude, loc1.longitude, loc2.latitude, loc2.longitude)
        }*/
    }


    fun vicentDistance(obj2: Location, obj1: Location): Double {
       // if (obj2 is Location) {
            val loc1 = obj1 as Location
            val loc2 = obj2 as Location
            return vicentDistance(loc1.latitude, loc1.longitude, loc2.latitude, loc2.longitude)

/*
        } else {
            val loc1 = obj1 as Location2
            val loc2 = obj2 as Location2
            return vicentDistance(loc1.latitude, loc1.longitude, loc2.latitude, loc2.longitude)
        }
*/
    }


    fun haverSineDistance(lat1: Double, lng1: Double, lat2: Double, lng2: Double): Double {
        var lat1 = lat1
        var lng1 = lng1
        var lat2 = lat2
        var lng2 = lng2
        // mHager 08-12-2012
        // http://en.wikipedia.org/wiki/Haversine_formula
        // Implementation

        // convert to radians
        lat1 = Math.toRadians(lat1)
        lng1 = Math.toRadians(lng1)
        lat2 = Math.toRadians(lat2)
        lng2 = Math.toRadians(lng2)

        val dlon = lng2 - lng1
        val dlat = lat2 - lat1

        val a = Math.pow(Math.sin(dlat / 2), 2.0) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dlon / 2), 2.0)

        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))

        return acochambratorFactor.toDouble() * 6371.0 * c //in kilometers
    }


    fun getPathLength(locs: List<Location>): Double {
        var distance = 0.0
        for (i in 0 until locs.size - 1) {
            distance = distance + GeoDistanceCalulator.haverSineDistance(locs[i + 1], locs[i])
        }
        return distance
    }


    /*public static double distFrom(double lat1, double lng1, double lat2, double lng2) {
	    double earthRadius = 3958.75; // miles (or 6371.0 kilometers)
	    double dLat = Math.toRadians(lat2-lat1);
	    double dLng = Math.toRadians(lng2-lng1);
	    double sindLat = Math.sin(dLat / 2);
	    double sindLng = Math.sin(dLng / 2);
	    double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
	            * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	    double dist = earthRadius * c;

	    return dist;
	    }*/





}
