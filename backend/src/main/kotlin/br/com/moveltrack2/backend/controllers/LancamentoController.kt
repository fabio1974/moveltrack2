package br.com.moveltrack2.backend.controllers

import br.com.moveltrack.persistence.domain.Lancamento
import br.com.moveltrack.persistence.repositories.*
import br.com.moveltrack2.backend.specifications.LancamentoSpecification
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.rest.webmvc.BasePathAwareController
import org.springframework.web.bind.annotation.*


@RestController
@BasePathAwareController
class LancamentoController(
        val lancamentoRepository: LancamentoRepository
) {

    @ResponseBody
    @PatchMapping("/lancamentos")
    fun patch(@RequestParam(value = "pageIndex") pageIndex:Int,
              @RequestParam(value = "pageSize") pageSize:Int,
              @RequestParam(value = "sortField") sortField:String?=null,
              @RequestParam(value = "sortDirection",defaultValue = "DESC") sortDirection: Sort.Direction ,
              @RequestBody filtro: Lancamento): Page<Lancamento> {

        val especification = LancamentoSpecification.getFiltro(filtro, lancamentoRepository)
        val pageRequest = PageRequest.of(pageIndex,pageSize,sortDirection,sortField)
        val list: Page<Lancamento> = lancamentoRepository.findAll(especification,pageRequest)
        return list
    }



    @ResponseBody
    @PostMapping("/lancamentos")
    fun create(@RequestBody lancamento: Lancamento): Any {
        var aux = lancamentoRepository.save(lancamento);
        return "Viagem gerada com sucesso!"
    }

}
