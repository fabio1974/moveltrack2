package br.com.moveltrack2.backend.controllers

import br.com.moveltrack.persistence.pojos.RelatorioFiltro
import br.com.moveltrack.persistence.pojos.RelatorioSinalFiltro
import br.com.moveltrack.persistence.repositories.ContratoRepository
import br.com.moveltrack.persistence.repositories.MBoletoRepository
import br.com.moveltrack.persistence.util.header
import br.com.moveltrack2.backend.repositories.RelatorioCobranca
import br.com.moveltrack2.backend.repositories.RelatorioInstalador
import br.com.moveltrack2.backend.repositories.RelatorioSinais
import br.com.moveltrack2.backend.utils.*
import br.com.moveltrack2.backend.utils.Utils.moeda
import com.lowagie.text.*
import com.lowagie.text.pdf.PdfPTable
import com.lowagie.text.pdf.PdfWriter
import org.springframework.data.rest.webmvc.BasePathAwareController
import org.springframework.web.bind.annotation.*
import java.io.ByteArrayOutputStream


@RestController
@BasePathAwareController
class RelatoriosInternosController(
        val mBoletoRepository: MBoletoRepository,
        val contratoRepository: ContratoRepository,
        val relatorioSinais: RelatorioSinais,
        val relatorioCobranca: RelatorioCobranca,
        val relatorioInstalador: RelatorioInstalador
) {





    @Throws(DocumentException::class)
    @PostMapping("/getRelatorioCobranca")
    fun  getRelatorioCobranca(@RequestBody filtro: RelatorioFiltro): ByteArray {
        val contratos = mBoletoRepository.findAllBySituacaoAndDataVencimentoBefore(filtro.boletoStatus!!,filtro.fimCobranca!!)?.groupBy{it.contrato}
        val os = ByteArrayOutputStream()
        val document = Document(PageSize.A4,2f,2f,36f,36f)
        PdfWriter.getInstance(document, os)
        document.open()

        var totalDebito = relatorioCobranca.addToDocument(contratos, filtro, document)

        val footer = PdfPTable(1)
        footer.setHorizontalAlignment(PdfPTable.ALIGN_CENTER)
        footer.setWidthPercentage(90f)
        footer.setSpacingBefore(20f)
        footer.addCell(header("Valor Total De Débitos: ${moeda(totalDebito)}", 1,PdfPTable.ALIGN_CENTER))
        document.add(footer)
        document.close()
        return os.toByteArray()
    }




    @Throws(DocumentException::class)
    @PostMapping("/getRelatorioSinais")
    fun  getRelatorioSinais(@RequestBody filtro: RelatorioSinalFiltro): ByteArray {
        val rel = relatorioSinais.getRelatorioSinais(filtro)
        val os = ByteArrayOutputStream()
        val document = Document(PageSize.A3,2f,2f,36f,36f)
        PdfWriter.getInstance(document, os)
        document.open()
        document.add(relatorioSinais.getTable(rel))
        document.close()
        return os.toByteArray()
    }


    @Throws(DocumentException::class)
    @PostMapping("/getRelatorioInstalador")
    fun  getRelatorioInstalador(@RequestBody filtro: RelatorioFiltro): ByteArray {
        val os = ByteArrayOutputStream()
        val document = Document(PageSize.A3,2f,2f,36f,36f)
        PdfWriter.getInstance(document, os)
        document.open()

        document.add(relatorioInstalador.getOsTable(filtro))
        document.add(relatorioInstalador.getRecebimentoTable(filtro))
        document.add(relatorioInstalador.getDevocucaoTable(filtro))
        document.add(relatorioInstalador.getValeTable(filtro))
        document.add(relatorioInstalador.getGastosTable(filtro))
        document.add(relatorioInstalador.getResumoTable(filtro))

        document.close()
        return os.toByteArray()
    }







}
