package br.com.moveltrack2.backend.controllers

import br.com.moveltrack2.backend.Iugu.IuguUtils
import br.com.moveltrack.persistence.domain.*
import br.com.moveltrack.persistence.repositories.*
import br.com.moveltrack2.backend.specifications.ContratoSpecification
import br.com.moveltrack2.backend.seguranca.Criptografia
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.rest.webmvc.BasePathAwareController
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import java.time.ZonedDateTime
import java.util.*

@RestController
@BasePathAwareController
class ContratoController(
        val perfilRepository: PerfilRepository,
        val pessoaRepository: PessoaRepository,
        val usuarioRepository: UsuarioRepository,
        val mBoletoMyRepository: MBoletoRepository,
        val contratoRepository: ContratoRepository,
        val iuguRepository: IuguRepository,
        val clienteRepository: ClienteRepository,
        val iuguUtils: IuguUtils,
        val veiculoRepositoryMy: VeiculoRepository
) {


    @ResponseBody
    @PatchMapping("/contratos")
    fun patch(@RequestParam(value = "pageIndex",defaultValue = "0") pageIndex:Int,
              @RequestParam(value = "pageSize",defaultValue = "21000000") pageSize:Int,
              @RequestParam(value = "sortField",defaultValue = "id") sortField:String?=null,
              @RequestParam(value = "sortDirection",defaultValue = "ASC") sortDirection: Sort.Direction,
              @RequestBody filtro: Contrato): Page<Contrato> {

        val especification = ContratoSpecification.getFiltro(filtro)
        val pageRequest = PageRequest.of(pageIndex, pageSize, sortDirection,sortField)
        val list: Page<Contrato> = contratoRepository.findAll(especification,pageRequest)

        list.map {buildContrato(it)}

        return list
    }

    private fun buildContrato(it: Contrato): Contrato {
        var veiculos: List<Veiculo>  = veiculoRepositoryMy.findAllByContratoAndStatus(it,VeiculoStatus.ATIVO)
        it.veiculos = veiculos.map { v -> buildVeiculo(v) }
        it.dataBase = getDataBase(it)
        return it;
    }

    private fun buildVeiculo(v: Veiculo): Veiculo {
        v.contrato = null
        return v
    }


    fun getDataBase(contrato: Contrato?): ZonedDateTime {
        var ultimoBoleto = mBoletoMyRepository.findTopByTipoAndContratoOrderByDataVencimentoDesc(MBoletoTipo.MENSAL, contrato!!)
        if(ultimoBoleto==null)
            return contrato.inicio!!
        return ultimoBoleto.dataVencimento!!
    }

    @Transactional
    @PostMapping("/contratos")
    fun create(@RequestBody contrato: Contrato): Any {
        try {
            if (contrato.id == null) {
                contrato.cliente?.usuario?.senha = Criptografia().calculaHash("1234")
                usuarioRepository.save(contrato.cliente?.usuario!!)
                clienteRepository.save(contrato.cliente!!)
                contratoRepository.save(contrato)
                contrato.numeroContrato = "01${contrato.id.toString().padStart(5, '0')}"
                contratoRepository.save(contrato)
                return "Contrato inserido com sucesso!"
            } else {
                usuarioRepository.save(contrato.cliente?.usuario!!)
                clienteRepository.save(contrato.cliente!!)
                contratoRepository.save(contrato)
                return "Contrato atualizado com sucesso!"
            }
        }catch (e: Exception){
            val perfilTipo = contrato.cliente?.usuario?.perfil?.tipo
            throw Exception("${if(perfilTipo==PerfilTipo.CLIENTE_PF) "CPF" else "CNPJ" } já existe no banco! ",e.cause)
        }
    }




}
