package br.com.moveltrack2.backend.globalstar.smartone.model

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlType

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Prvmsg", propOrder = ["esn", "provID", "tStart", "tEnd","txRetryMinSec","txRetryMaxSec","txRetries","rfChannel"])
class Prvmsg {
    @XmlElement(required = true)
    var esn: String? = null

    @XmlElement(required = true)
    var provID: String? = null

    @XmlElement(required = true)
    var tStart: String? = null

    @XmlElement(required = true)
    var tEnd: String? = null

    @XmlElement(required = true)
    var txRetryMinSec: String? = null

    @XmlElement(required = true)
    var txRetryMaxSec: String? = null

    @XmlElement(required = true)
    var txRetries: String? = null

    @XmlElement(required = true)
    var rfChannel: String? = null

}
