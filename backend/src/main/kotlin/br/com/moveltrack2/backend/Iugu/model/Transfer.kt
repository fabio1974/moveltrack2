package br.com.moveltrack2.backend.Iugu.model

import com.fasterxml.jackson.annotation.JsonProperty

class Transfer(@field:JsonProperty("receiver_id")
               val receiverId: String, @field:JsonProperty("amount_cents")
               val amountCents: Int)
