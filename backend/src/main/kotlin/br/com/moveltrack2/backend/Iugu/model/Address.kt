package br.com.moveltrack2.backend.Iugu.model

import java.io.Serializable

import com.fasterxml.jackson.annotation.JsonProperty


class Address(
        /**
         * Rua do cliente
         */
        val street: String,
        /**
         * Número da Rua do Cliente
         */
        val number: String,
        /**
         * Cidade do cliente
         */
        val city: String,
        /**
         * Estado do cliente
         */
        val state: String,
        /**
         * País do cliente
         */
        val country: String,
        /**
         * CEP do cliente
         */
        @field:JsonProperty("zip_code")
        val zipCode: String,
        /**
         * Bairro do cliente
         */
        var district: String?) : Serializable {
    companion object {

        private const val serialVersionUID = 3266886175287194L
    }
}
