package br.com.moveltrack2.backend.specifications

import br.com.moveltrack.persistence.domain.Chip
import br.com.moveltrack.persistence.domain.ChipStatus
import br.com.moveltrack.persistence.domain.Operadora
import br.com.moveltrack.persistence.repositories.ChipRepository
import org.springframework.data.jpa.domain.Specification
import org.springframework.util.StringUtils
import java.util.*
import javax.persistence.criteria.Predicate


object ChipSpecification {


    fun getFiltro(filtro: Chip, repo: ChipRepository): Specification<Chip> {
        return Specification { root, query, builder ->
            val predicates = ArrayList<Predicate>()
            if (!StringUtils.isEmpty(filtro.iccid)) {
                val d = "%" + filtro.iccid!!.trim { it <= ' ' } + "%"
                predicates.add(builder.like(root.get("iccid"), d))
            }
            if (!StringUtils.isEmpty(filtro.numero)) {
                val d = "%" + filtro.numero!!.trim { it <= ' ' } + "%"
                predicates.add(builder.like(root.get("numero"), d))
            }
            if (filtro.operadora!=null) {
                predicates.add(builder.equal(root.get<Operadora>("operadora"), filtro.operadora))
            }
            if (filtro.status!=null) {
                predicates.add(builder.equal(root.get<ChipStatus>("status"), filtro.status))
            }
            builder.and(*predicates.toTypedArray())
        }
    }


}
