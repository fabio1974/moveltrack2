package br.com.moveltrack2.backend.seguranca

import br.com.moveltrack.persistence.domain.Perfil
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.*

class User (var id: String?=null,
            var nome: String? = null,
            var nomeUsuario: String? = null,
            var senha: String? = null,
            var cpf: String? = null,
            var perfil: Perfil? = null


):UserDetails {

    private val authorities = HashSet<GrantedAuthority>()

    override fun getAuthorities(): Collection<GrantedAuthority> {
        return authorities
    }

    override fun getPassword(): String? {
        return senha
    }

    override fun getUsername(): String? {
        return nomeUsuario
    }

    override fun isAccountNonExpired(): Boolean {
        return false
    }

    override fun isAccountNonLocked(): Boolean {
        return false
    }

    override fun isCredentialsNonExpired(): Boolean {
        return false
    }

    override fun isEnabled(): Boolean {
        return false
    }
}
