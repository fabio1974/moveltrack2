package br.com.moveltrack2.backend.Iugu.responses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty


@JsonIgnoreProperties(ignoreUnknown = true)
class PlanResponse {

    var id: String? = null

    var name: String? = null

    var identifier: String? = null

    var interval: String? = null

    @JsonProperty("interval_type")
    var intervalType: String? = null

    var prices: List<PriceResponse>? = null

    var features: List<FeatureResponse>? = null

}