package br.com.moveltrack2.backend.globalstar.smartone.model

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlType

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StuMessage", propOrder = ["esn", "unixTime", "gps", "payload"])
class StuMessage {
    @XmlElement(required = true)
    var esn: String? = null

    @XmlElement(required = true)
    var unixTime: String? = null

    @XmlElement(required = true)
    var gps: String? = null

    @XmlElement(required = true)
    var payload: String? = null
}
