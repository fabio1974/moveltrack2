package br.com.moveltrack2.backend.controllers

import br.com.moveltrack.persistence.domain.Chip
import br.com.moveltrack.persistence.domain.Rota
import br.com.moveltrack.persistence.repositories.*
import br.com.moveltrack2.backend.specifications.ChipSpecification
import br.com.moveltrack2.backend.specifications.RotaSpecification
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.rest.webmvc.BasePathAwareController
import org.springframework.web.bind.annotation.*


@RestController
@BasePathAwareController
class RotaController(
        val rotaRepository: RotaRepository
) {

    @ResponseBody
    @PatchMapping("/rotas")
    fun patch(@RequestParam(value = "pageIndex") pageIndex:Int,
              @RequestParam(value = "pageSize") pageSize:Int,
              @RequestParam(value = "sortField") sortField:String?=null,
              @RequestParam(value = "sortDirection",defaultValue = "DESC") sortDirection: Sort.Direction ,
              @RequestBody filtro: Rota): Page<Rota> {

        val especification = RotaSpecification.getFiltro(filtro, rotaRepository)
        val pageRequest = PageRequest.of(pageIndex,pageSize,sortDirection,sortField)
        return  rotaRepository.findAll(especification,pageRequest)
    }


    @ResponseBody
    @PostMapping("/rotas")
    fun create(@RequestBody rota: Rota): Rota? {
        if(rota.id==null){
            val rotas = rotaRepository.findAllByOrigemAndDestino(rota.origem,rota.destino)
            val rotaIgual = rotas?.firstOrNull{isRotaIgual(it,rota)}
            if(rotaIgual!=null)
                return rotaIgual
        }
        var aux = rotaRepository.save(rota);
        return null
    }

    private fun isRotaIgual(r: Rota, rota: Rota) : Boolean {
        return r.itinerario?.size==rota.itinerario?.size && rota.itinerario!!.containsAll(r.itinerario!!)
    }

}
