package br.com.moveltrack2.backend.services

import br.com.moveltrack.persistence.domain.Cerca
import br.com.moveltrack.persistence.domain.Viagem
import br.com.moveltrack.persistence.domain.ViagemStatus
import br.com.moveltrack.persistence.repositories.LocationRepository
import br.com.moveltrack.persistence.repositories.ViagemRepository
import br.com.moveltrack2.backend.TIME_ZONE_STR
import br.com.moveltrack2.backend.utils.GeoDistanceCalulator
import br.com.moveltrack2.backend.utils.MapaUtils
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.time.Duration
import java.time.ZonedDateTime

@Component
@EnableScheduling
class TaskAtualizaViagem(
        val viagemRepository: ViagemRepository,
        val locationRepository: LocationRepository
) {

    @Scheduled(cron = "0 0/30 * * * ?",zone = TIME_ZONE_STR) //a cada meia hora
    fun atualizaViagensEmAndamentos(){
        val viagensEmAndamento = viagemRepository.findAllByStatusNotAndChegadaPrevistaAfter(ViagemStatus.ENCERRADA,ZonedDateTime.now())
        println("${viagensEmAndamento?.size?:0} viagens em andamento...atualizando todas...")
        viagensEmAndamento?.forEach {viagem->
            atualizaStatus(viagem)
            viagem.distanciaPercorrida = getDistanciaDaViagem(viagem, ZonedDateTime.now())
            viagemRepository.save(viagem)
        }
    }

    @Scheduled(cron = "0 0 0 * * ?",zone = TIME_ZONE_STR)  //todos os dias 00:00
    fun atualizaViagensAtrasadasNaoEncerradas(){
        val viagensAtrasadasNaoEnceradas = viagemRepository.findAllByStatusNotAndChegadaPrevistaBefore(ViagemStatus.ENCERRADA,ZonedDateTime.now().minusDays(5))
        println("${viagensAtrasadasNaoEnceradas?.size?:0} viagens atrasadas...atualizando todas...")
        viagensAtrasadasNaoEnceradas?.forEach {viagem->
            viagem.status = ViagemStatus.ENCERRADA
            viagem.distanciaPercorrida = getDistanciaDaViagem(viagem,viagem.chegadaPrevista!!)
            viagem.chegadaReal = viagem.chegadaPrevista
            viagemRepository.save(viagem)
        }
    }

    private fun atualizaStatus(viagem: Viagem) {
        val now = ZonedDateTime.now()
        val lastLoc = locationRepository.findFirstByImeiOrderByDateLocationDesc(viagem.veiculo?.equipamento?.imei)?:return
        val cerca: Cerca = viagem.cliente?.cerca?: return
        val distancia: Double = GeoDistanceCalulator.haverSineDistance(cerca.lat1, cerca.lon1, lastLoc.latitude, lastLoc.longitude)
        println("----- atualizando viagem:  ${viagem.id}------")
        if (viagem.distanciaPercorrida == 0.0 && viagem.saiuDaCerca == null && isInCerca(distancia, cerca)) {
            if (Duration.between(viagem.partida,now).toMinutes() > 15) //15 minutos de tolerancia
                viagem.status = ViagemStatus.PARTIDA_EM_ATRASO
        } else if (viagem.distanciaPercorrida > 0 && viagem.saiuDaCerca == null && isInCerca(distancia, cerca)) {
            viagem.status = ViagemStatus.SAINDO
        } else if (viagem.distanciaPercorrida > 0 && viagem.saiuDaCerca == null && !isInCerca(distancia, cerca)) {
            viagem.status = ViagemStatus.SAIU_DA_CERCA
            viagem.saiuDaCerca = now
        } else if (viagem.distanciaPercorrida > 0 && viagem.saiuDaCerca != null && viagem.entrouNaCerca == null && !isInCerca(distancia, cerca) && viagem.status !== ViagemStatus.NA_ESTRADA) {
            viagem.status = ViagemStatus.NA_ESTRADA
        } else if (viagem.distanciaPercorrida > 0 && viagem.saiuDaCerca != null && viagem.entrouNaCerca == null && isInCerca(distancia, cerca) && viagem.status !== ViagemStatus.ENTROU_NA_CERCA) {
            viagem.status = ViagemStatus.ENTROU_NA_CERCA
            viagem.entrouNaCerca = now
        } else if (viagem.distanciaPercorrida > 0 && viagem.saiuDaCerca != null && viagem.entrouNaCerca != null && viagem.chegadaReal == null && isInCerca(distancia, cerca)) {
            if (1000 * distancia > 500 && viagem.status !== ViagemStatus.SE_APROXIMANDO) {
                viagem.status = ViagemStatus.SE_APROXIMANDO
            } else {
                if (Duration.between(viagem.chegadaPrevista,now).toMinutes() < 15 &&  viagem.status !== ViagemStatus.CHEGADA_EM_TEMPO)
                    viagem.status = ViagemStatus.CHEGADA_EM_TEMPO
                else
                    viagem.status = ViagemStatus.CHEGADA_EM_ATRASO
                viagem.chegadaReal = now
            }
        } else if  (viagem.distanciaPercorrida>0 && viagem.saiuDaCerca!=null && viagem.entrouNaCerca!=null && viagem.chegadaReal!=null && isInCerca(distancia, cerca) && viagem.status!=ViagemStatus.ENCERRADA){
            viagem.status = ViagemStatus.ENCERRADA
        } else if (viagem.distanciaPercorrida>0 && viagem.saiuDaCerca!=null && viagem.entrouNaCerca!=null && !isInCerca(distancia,cerca) && viagem.status!=ViagemStatus.SAIU_DA_CERCA) {
            viagem.status = ViagemStatus.SAIU_DA_CERCA
            viagem.saiuDaCerca =now
            viagem.entrouNaCerca = null
            viagem.chegadaReal = null
        }
    }

    private fun isInCerca(distancia: Double, cerca: Cerca): Boolean = distancia <= cerca.raio

    private fun getDistanciaDaViagem(viagem: Viagem,fimDaViagem:ZonedDateTime): Double {
        val imei = viagem.veiculo?.equipamento?.imei
        if(imei==null) return 0.0
        val pontosCrus = locationRepository.findAllByImeiAndDateLocationGreaterThanEqualAndDateLocationLessThanEqualOrderByDateLocationInicio(imei, viagem.partida!!,fimDaViagem)
        val otimizado = MapaUtils.otimizaPontosDoBanco(pontosCrus, viagem.partida!!, fimDaViagem, viagem.veiculo?.equipamento, locationRepository)
        return  GeoDistanceCalulator.getPathLength(otimizado)
    }



}
