package br.com.moveltrack2.backend.controllers

import br.com.moveltrack.persistence.domain.DespesaFrota
import br.com.moveltrack.persistence.repositories.*
import br.com.moveltrack2.backend.specifications.DespesaFrotaSpecification
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.rest.webmvc.BasePathAwareController
import org.springframework.web.bind.annotation.*


@RestController
@BasePathAwareController
class DespesaFrotaController(
        val contratoRepository: ContratoRepository,
        val despesaFrotaRepositoryMy: DespesaFrotaRepositoryMy
) {


    @ResponseBody
    @PatchMapping("/despesaFrotas")
    fun patch(@RequestParam(value = "pageIndex") pageIndex:Int,
              @RequestParam(value = "pageSize") pageSize:Int,
              @RequestParam(value = "sortField") sortField:String?=null,
              @RequestParam(value = "sortDirection",defaultValue = "DESC") sortDirection: Sort.Direction ,
              @RequestBody filtro: DespesaFrota): Page<DespesaFrota> {

        val especification = DespesaFrotaSpecification.getFiltro(filtro, contratoRepository)
        val pageRequest = PageRequest.of(pageIndex,pageSize,sortDirection,sortField)
        val list: Page<DespesaFrota> = despesaFrotaRepositoryMy.findAll(especification,pageRequest)
        return list
    }




    @PostMapping("/despesaFrotas")
    fun create(@RequestBody despesaFrota: DespesaFrota): Any {
        despesaFrotaRepositoryMy.save(despesaFrota);
        return "DespesaFrota gerada com sucesso!"
    }

    @DeleteMapping("/deleteDespesaFrota/{id}")
    fun delete(@PathVariable id: Int): Any {
        despesaFrotaRepositoryMy.deleteById(id);
        return "DespesaFrota gerada com sucesso!"
    }


}
