package br.com.moveltrack2.backend.repositories

import br.com.moveltrack.persistence.domain.LancamentoTipo
import br.com.moveltrack.persistence.pojos.RelatorioFiltro
import br.com.moveltrack.persistence.repositories.*
import br.com.moveltrack.persistence.util.cell
import br.com.moveltrack.persistence.util.header
import br.com.moveltrack2.backend.utils.*
import br.com.moveltrack2.backend.utils.Utils.moeda
import com.lowagie.text.pdf.PdfPTable
import org.springframework.stereotype.Component
import java.time.format.DateTimeFormatter
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Component
class RelatorioInstalador constructor(
        @PersistenceContext var em: EntityManager,
        val veiculoRepository: VeiculoRepository,
        val mBoletoRepository: MBoletoRepository,
        val equipamentoRepository: EquipamentoRepository,
        val ordemDeServicoRepository: OrdemDeServicoRepository,
        val lancamentoRepository: LancamentoRepository,
        val locationRepository: LocationRepository,
        val contratoRepository: ContratoRepository,
        val googleGeo: GoogleGeo
) {

    fun String.max(size:Int): String? = if(this.length<=size) this  else this.substring(0,size)

    val format = DateTimeFormatter.ofPattern("dd/MM/yy HH:mm");

    var totalOs=0.0
    var totalRecebimento=0.0
    var totalDevolucao=0.0
    var totalVale=0.0
    var totalGasto=0.0

    fun getOsTable(filtro: RelatorioFiltro): PdfPTable? {

        val table = PdfPTable(29)
        table.setHorizontalAlignment(PdfPTable.ALIGN_CENTER)
        table.setWidthPercentage(90f)
        table.setSpacingBefore(20f)

        table.addCell(header("OS",2, PdfPTable.ALIGN_CENTER))
        table.addCell(header("Data do Serviço",3, PdfPTable.ALIGN_CENTER))
        table.addCell(header("Serviço",3, PdfPTable.ALIGN_CENTER))
        table.addCell(header("Cliente",9, PdfPTable.ALIGN_CENTER))
        table.addCell(header("Observação",9, PdfPTable.ALIGN_CENTER))
        table.addCell(header("Valor",3, PdfPTable.ALIGN_CENTER))

        val oss = ordemDeServicoRepository.findAllByOperadorAndDataDoServicoBetweenOrderByDataDoServico(filtro.instalador,filtro.inicio,filtro.fim)

        totalOs=0.0
        oss?.forEach {
            table.addCell(cell(PdfPTable.ALIGN_CENTER,it.numero?:"",2))
            table.addCell(cell(PdfPTable.ALIGN_CENTER, it.dataDoServico?.format(format)?:"",3))
            table.addCell(cell(PdfPTable.ALIGN_CENTER,it.servico?.name?:"",3))
            table.addCell(cell(PdfPTable.ALIGN_CENTER,it.cliente?.nome?.max(30)?:"",9))
            table.addCell(cell(PdfPTable.ALIGN_CENTER,it.observacao?.max(30)?:"",9))
            table.addCell(cell(PdfPTable.ALIGN_RIGHT, Utils.moeda(it.valorDoServico),3))
            totalOs += it.valorDoServico
        }

        table.addCell(header("Total Custo dos Serviços".toUpperCase(),26, PdfPTable.ALIGN_RIGHT))
        table.addCell(header(Utils.moeda(totalOs),3, PdfPTable.ALIGN_RIGHT))
        return table
    }

    fun getRecebimentoTable(filtro: RelatorioFiltro): PdfPTable? {
        val table = PdfPTable(18)
        table.setHorizontalAlignment(PdfPTable.ALIGN_CENTER)
        table.setWidthPercentage(90f)
        table.setSpacingBefore(20f)

        table.addCell(header("Data Recebimento",2, PdfPTable.ALIGN_CENTER))
        table.addCell(header("Forma",2, PdfPTable.ALIGN_CENTER))
        table.addCell(header("Cliente",4, PdfPTable.ALIGN_CENTER))
        table.addCell(header("Registrado por",4, PdfPTable.ALIGN_CENTER))
        table.addCell(header("Observação",4, PdfPTable.ALIGN_CENTER))
        table.addCell(header("Valor",2, PdfPTable.ALIGN_CENTER))

        val recebimentos = lancamentoRepository.findAllByOperacaoAndSolicitanteAndDataBetweenOrderByData(
                LancamentoTipo.RECEBIMENTO_DE_CLIENTE,filtro.instalador,filtro.inicio,filtro.fim)

        totalRecebimento=0.0
        recebimentos?.forEach {
            table.addCell(cell(PdfPTable.ALIGN_CENTER, it.data?.format(format)?:"",2))
            table.addCell(cell(PdfPTable.ALIGN_CENTER,it.formaPagamento?.name?:"",2))
            table.addCell(cell(PdfPTable.ALIGN_CENTER,it.ordemDeServico?.cliente?.nome?:"",4))
            table.addCell(cell(PdfPTable.ALIGN_CENTER,it.operador?.nome?:"",4))
            table.addCell(cell(PdfPTable.ALIGN_CENTER,it.observacao?:"",4))
            table.addCell(cell(PdfPTable.ALIGN_RIGHT, Utils.moeda(it.valor),2))
            totalRecebimento += it.valor
        }

        table.addCell(header("Total Recebido".toUpperCase(),16, PdfPTable.ALIGN_RIGHT))
        table.addCell(header(Utils.moeda(totalRecebimento),2, PdfPTable.ALIGN_RIGHT))
        return table
    }





    fun getDevocucaoTable(filtro: RelatorioFiltro): PdfPTable? {
        val table = PdfPTable(11)
        table.setHorizontalAlignment(PdfPTable.ALIGN_CENTER)
        table.setWidthPercentage(90f)
        table.setSpacingBefore(20f)

        table.addCell(header("Data Devolução",2, PdfPTable.ALIGN_CENTER))
        table.addCell(header("Registrado por",4, PdfPTable.ALIGN_CENTER))
        table.addCell(header("Observação",4, PdfPTable.ALIGN_CENTER))
        table.addCell(header("Valor",1, PdfPTable.ALIGN_CENTER))

        val recebimentos = lancamentoRepository.findAllByOperacaoAndSolicitanteAndDataBetweenOrderByData(
                LancamentoTipo.DEVOLUCAO_DE_DINHEIRO,filtro.instalador,filtro.inicio,filtro.fim)

        totalDevolucao=0.0
        recebimentos?.forEach {
            table.addCell(cell(PdfPTable.ALIGN_CENTER, it.data?.format(format)?:"",2))
            table.addCell(cell(PdfPTable.ALIGN_CENTER,it.operador?.nome?:"",4))
            table.addCell(cell(PdfPTable.ALIGN_CENTER,it.observacao?:"",4))
            table.addCell(cell(PdfPTable.ALIGN_RIGHT, Utils.moeda(it.valor),1))
            totalDevolucao += it.valor
        }

        table.addCell(header("Total Devolvido".toUpperCase(),10, PdfPTable.ALIGN_RIGHT))
        table.addCell(header(Utils.moeda(totalDevolucao),1, PdfPTable.ALIGN_RIGHT))
        return table
    }

    fun getValeTable(filtro: RelatorioFiltro): PdfPTable? {
        val table = PdfPTable(11)
        table.setHorizontalAlignment(PdfPTable.ALIGN_CENTER)
        table.setWidthPercentage(90f)
        table.setSpacingBefore(20f)

        table.addCell(header("Data do Vale",2, PdfPTable.ALIGN_CENTER))
        table.addCell(header("Registrado por",4, PdfPTable.ALIGN_CENTER))
        table.addCell(header("Observação",4, PdfPTable.ALIGN_CENTER))
        table.addCell(header("Valor",1, PdfPTable.ALIGN_CENTER))

        val recebimentos = lancamentoRepository.findAllByOperacaoAndSolicitanteAndDataBetweenOrderByData(
                LancamentoTipo.VALE,filtro.instalador,filtro.inicio,filtro.fim)

        totalVale=0.0
        recebimentos?.forEach {
            table.addCell(cell(PdfPTable.ALIGN_CENTER, it.data?.format(format)?:"",2))
            table.addCell(cell(PdfPTable.ALIGN_CENTER,it.operador?.nome?:"",4))
            table.addCell(cell(PdfPTable.ALIGN_CENTER,it.observacao?:"",4))
            table.addCell(cell(PdfPTable.ALIGN_RIGHT, Utils.moeda(it.valor),1))
            totalVale += it.valor
        }

        table.addCell(header("Total de Vales".toUpperCase(),10, PdfPTable.ALIGN_RIGHT))
        table.addCell(header(Utils.moeda(totalVale),1, PdfPTable.ALIGN_RIGHT))
        return table
    }

    fun getGastosTable(filtro: RelatorioFiltro): PdfPTable? {
        val table = PdfPTable(11)
        table.setHorizontalAlignment(PdfPTable.ALIGN_CENTER)
        table.setWidthPercentage(90f)
        table.setSpacingBefore(20f)

        table.addCell(header("Data do Gasto",2, PdfPTable.ALIGN_CENTER))
        table.addCell(header("Registrado por",4, PdfPTable.ALIGN_CENTER))
        table.addCell(header("Observação",4, PdfPTable.ALIGN_CENTER))
        table.addCell(header("Valor",1, PdfPTable.ALIGN_CENTER))

        val list = lancamentoRepository.findAllByOperacaoAndSolicitanteAndDataBetweenOrderByData(
                LancamentoTipo.GASTO_DE_MATERIAL,filtro.instalador,filtro.inicio,filtro.fim)

        totalGasto=0.0
        list?.forEach {
            table.addCell(cell(PdfPTable.ALIGN_CENTER, it.data?.format(format)?:"",2))
            table.addCell(cell(PdfPTable.ALIGN_CENTER,it.operador?.nome?:"",4))
            table.addCell(cell(PdfPTable.ALIGN_CENTER,it.observacao?:"",4))
            table.addCell(cell(PdfPTable.ALIGN_RIGHT, moeda(it.valor),1))
            totalGasto += it.valor
        }

        table.addCell(header("Total de Gastos".toUpperCase(),10, PdfPTable.ALIGN_RIGHT))
        table.addCell(header(moeda(totalVale),1, PdfPTable.ALIGN_RIGHT))
        return table
    }

    fun getResumoTable(filtro: RelatorioFiltro): PdfPTable? {
        val table = PdfPTable(11)
        table.setHorizontalAlignment(PdfPTable.ALIGN_CENTER)
        table.setWidthPercentage(90f)
        table.setSpacingBefore(20f)

        table.addCell(header("DINHEIRO COM OPERADOR",10, PdfPTable.ALIGN_CENTER))
        table.addCell(header("Valor",1, PdfPTable.ALIGN_CENTER))

        table.addCell(cell(PdfPTable.ALIGN_RIGHT, "RECEBIMENTOS",10))
        table.addCell(cell(PdfPTable.ALIGN_RIGHT, moeda(totalRecebimento),1))
        table.addCell(cell(PdfPTable.ALIGN_RIGHT, "DEVOLUÇÃO",10))
        table.addCell(cell(PdfPTable.ALIGN_RIGHT,"-${moeda(totalDevolucao)}",1))
        table.addCell(cell(PdfPTable.ALIGN_RIGHT, "VALES",10))
        table.addCell(cell(PdfPTable.ALIGN_RIGHT, "-${moeda(totalVale)}",1))
        table.addCell(cell(PdfPTable.ALIGN_RIGHT, "GASTOS",10))
        table.addCell(cell(PdfPTable.ALIGN_RIGHT, "-${moeda(totalGasto)}",1))


        var total = totalRecebimento-totalDevolucao-totalGasto-totalVale

        table.addCell(header("TOTAL DA MÃO DO OPERADOR",10, PdfPTable.ALIGN_RIGHT))
        table.addCell(header(moeda(total),1, PdfPTable.ALIGN_RIGHT))
        return table
    }


}
