package br.com.moveltrack2.backend.Iugu.model


import br.com.moveltrack2.backend.Iugu.enums.ItemType
import com.fasterxml.jackson.annotation.JsonProperty


class PaymentMethod {

    var description: String? = null

    var data: Data? = null

    @JsonProperty("item_type")
    var itemType: ItemType? = null

    var token: String? = null

    @JsonProperty("set_as_default")
    var default: Boolean? = null

    constructor(description: String, token: String, isDefault: Boolean?) {
        this.description = description
        this.token = token
        this.default = isDefault
    }

    constructor(description: String, data: Data, isDefault: Boolean?) {
        this.description = description
        this.data = data
        this.default = isDefault
    }

    override fun toString(): String {
        return "PaymentMethod{" +
                "description='" + description + '\''.toString() +
                ", data=" + data +
                ", itemType=" + itemType +
                ", token='" + token + '\''.toString() +
                ", isDefault=" + default +
                '}'.toString()
    }
}
