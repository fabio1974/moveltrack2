package br.com.moveltrack2.backend.specifications

import br.com.moveltrack.persistence.domain.Cliente
import java.util.ArrayList

import javax.persistence.criteria.Predicate

import org.springframework.data.jpa.domain.Specification
import org.springframework.util.StringUtils


object ClienteSpecification {


    fun getFiltro(filtro: Cliente): Specification<Cliente> {
        return Specification { root, query, builder ->
            val predicates = ArrayList<Predicate>()

/*
            if (filtro.contrato?.numeroContrato != null) {
                val contrato = repo.findByNomeContaining(filtro.nome)
                predicates.add(builder.equal(root.get<ContratoMy>("contrato"), contrato))
            }
*/

            if (!StringUtils.isEmpty(filtro.nome)) {
                val d = "%" + filtro.nome!!.trim { it <= ' ' } + "%"
                predicates.add(builder.like(root.get("nome"), d))
            }
            builder.and(*predicates.toTypedArray())
        }
    }


}
