package br.com.moveltrack2.backend.Iugu.model

import java.util.HashMap


class Trigger {
    var event: String? = null
    var data = HashMap<String, String>()
}
