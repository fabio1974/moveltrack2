package br.com.moveltrack2.backend.controllers

import br.com.moveltrack.persistence.domain.*
import br.com.moveltrack.persistence.pojos.LastLocation
import br.com.moveltrack.persistence.repositories.*
import br.com.moveltrack2.backend.repositories.NativeRepositoryImpl
import org.springframework.data.rest.webmvc.BasePathAwareController
import org.springframework.web.bind.annotation.*
import java.sql.Timestamp
import java.time.ZonedDateTime
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext


@RestController
@BasePathAwareController
class SmsController(
        val smsRepository: SmsRepository,
        val nativeRepositoryImpl: NativeRepositoryImpl,
        @PersistenceContext var em: EntityManager
) {


    @ResponseBody
    @PostMapping("/bloquear")
    fun bloqueia(@RequestBody veiculo: Veiculo): Any {
        val numero = veiculo.equipamento?.chip?.numero?.replace("[^0-9]".toRegex(),"")
        var sms  = Sms(
                celular = "+55$numero",
                mensagem ="relay,1#",
                imei=veiculo.equipamento?.imei,
                status= SmsStatus.ESPERANDO_SOCKET,//if(veiculo.equipamento?.atrasoGmt==-1) SmsStatus.ESPERANDO_SOCKET2 else SmsStatus.ESPERANDO_SOCKET,
                tipo=SmsTipo.BLOQUEIO,
                dataUltimaAtualizacao = ZonedDateTime.now()
        )
        var aux = smsRepository.save(sms);
        return sms
    }

    @ResponseBody
    @PostMapping("/desbloquear")
    fun desbloqueia(@RequestBody veiculo: Veiculo): Any {
        val numero = veiculo.equipamento?.chip?.numero?.replace("[^0-9]".toRegex(),"")
        var sms  = Sms(
                celular = "+55$numero",
                mensagem ="relay,0#",
                imei=veiculo.equipamento?.imei,
                status=SmsStatus.ESPERANDO_SOCKET,//if(veiculo.equipamento?.atrasoGmt==-1) SmsStatus.ESPERANDO_SOCKET2 else SmsStatus.ESPERANDO_SOCKET,
                tipo=SmsTipo.DESBLOQUEIO,
                dataUltimaAtualizacao = ZonedDateTime.now()
        )
        var aux = smsRepository.save(sms);
        return sms
    }

    @ResponseBody
    @GetMapping("/lastSmsRecebido/{celular}")
    fun lastSms(@PathVariable("celular") celular:String): Sms?{
        val numero = celular.filter { it.isDigit() }
        return smsRepository.findFirstByCelularOrderByIdDesc("+55$numero")
    }

    @ResponseBody
    @GetMapping("/teste/conexao")
    fun testeConexao1(){
    }

}
