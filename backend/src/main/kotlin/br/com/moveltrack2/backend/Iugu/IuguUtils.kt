package br.com.moveltrack2.backend.Iugu

import br.com.moveltrack.persistence.domain.Iugu
import br.com.moveltrack.persistence.domain.MBoleto
import java.io.ByteArrayOutputStream
import java.net.URL


import br.com.moveltrack2.backend.Iugu.model.Address
import br.com.moveltrack2.backend.Iugu.model.Invoice
import br.com.moveltrack2.backend.Iugu.model.Item
import br.com.moveltrack2.backend.Iugu.model.Payer
import br.com.moveltrack2.backend.Iugu.responses.InvoiceResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import org.springframework.util.ObjectUtils.isEmpty
import java.util.*

@Component
class IuguUtils {

    @Autowired
    internal var invoiceService: InvoiceService? = null

    @Throws(IuguException::class)
    fun createIuguInvoice(mBoleto: MBoleto): Iugu?  {
        var response: InvoiceResponse = invoiceService!!.createInvoice(buildInvoiceObject(mBoleto))
        val iugu = Iugu()
        iugu.invoiceId = response.id
        iugu.codigoBarras = response.bankSlip!!.digitableLine
        iugu.codigoBarrasImagem = getBarCodeInBytes(response.bankSlip!!.barcode)
        return iugu
    }


    fun buildInvoiceObject(mBoleto: MBoleto): Invoice{
        val cliente = mBoleto.contrato!!.cliente
        val valor = (mBoleto.valor * 100).toInt()
        val invoice = Invoice("suporte@moveltrack.com.br", Date.from(mBoleto.dataVencimento!!.toInstant()), Item(if (isEmpty(mBoleto.mensagem34)) "Valor a ser pago." else mBoleto.mensagem34, 1, valor))
        var address: Address? = Address(cliente!!.endereco!!, cliente.numero!!, cliente.municipio!!.descricao!!, cliente.municipio!!.uf!!.sigla!!, "Brasil", cliente.cep!!, cliente.bairro)
        val ddd = if (cliente.celular1 != null) cliente.celular1!!.substring(1, 3) else ""
        val celular = if (cliente.celular1 != null) cliente.celular1!!.substring(4) else ""
        invoice.payer = Payer(cliente.nome!!, "suporte@moveltrack.com.br", if (cliente.cpf != null) cliente.cpf!! else cliente.cnpj!!, ddd, celular, address)
        return invoice
    }




    fun cancelIuguInvoice(mBoleto: MBoleto) {
        if (mBoleto.iugu != null)
            invoiceService!!.cancelInvoice(mBoleto.iugu!!.invoiceId!!)
    }

    private fun getBarCodeInBytes(url: String?): ByteArray? {
        val outputStream = ByteArrayOutputStream()
        try {
            val u = URL(url!!)
            val chunk = ByteArray(4096)
            var bytesRead: Int=0
            val stream = u.openStream()

            while ({bytesRead = stream.read(chunk);bytesRead}() > 0) {
                outputStream.write(chunk, 0, bytesRead)
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }

        return outputStream.toByteArray()
    }


    fun findById(id: String): InvoiceResponse {
        return invoiceService!!.findInvoice(id)
    }



}
