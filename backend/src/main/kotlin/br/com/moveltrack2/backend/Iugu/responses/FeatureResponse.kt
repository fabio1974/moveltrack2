package br.com.moveltrack2.backend.Iugu.responses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
class FeatureResponse {

    var id: String? = null

    var identifier: String? = null

    var important: Boolean? = null

    var name: String? = null

    @JsonProperty("plan_id")
    var planId: String? = null

    var position: Int? = null

    var value: Int? = null

}