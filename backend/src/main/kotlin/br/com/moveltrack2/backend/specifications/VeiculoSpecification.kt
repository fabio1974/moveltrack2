package br.com.moveltrack2.backend.specifications

import br.com.moveltrack.persistence.domain.Cliente
import br.com.moveltrack.persistence.domain.Equipamento
import br.com.moveltrack.persistence.domain.ModeloRastreador
import br.com.moveltrack.persistence.domain.Veiculo
import org.springframework.data.jpa.domain.Specification
import org.springframework.util.StringUtils
import java.util.*
import javax.persistence.criteria.Predicate


object VeiculoSpecification {


    fun getFiltro(filtro: Veiculo): Specification<Veiculo> {
        return Specification { root, query, builder ->
            val predicates = ArrayList<Predicate>()


            if (filtro.contrato?.cliente?.id != null) {
                predicates.add(builder.equal(root.get<Cliente>("contrato").get<Cliente>("cliente").get<Int>("id"),filtro.contrato?.cliente?.id))
            }


            if (!StringUtils.isEmpty(filtro.placa)) {
                val d = "%" + filtro.placa!!.trim { it <= ' ' } + "%"
                predicates.add(builder.like(root.get("placa"), d))
            }

            if (!StringUtils.isEmpty(filtro.marcaModelo)) {
                val d = "%" + filtro.marcaModelo!!.trim { it <= ' ' } + "%"
                predicates.add(builder.like(root.get("marcaModelo"), d))
            }

            if (filtro.tipo!=null) {
                predicates.add(builder.equal(root.get<String>("tipo"), filtro.tipo))
            }

            if (filtro.status!=null) {
                predicates.add(builder.equal(root.get<String>("status"), filtro.status))
            }

            if (filtro.equipamento?.imei!=null) {
                predicates.add(builder.equal(root.get<Equipamento>("equipamento").get<String>("imei"), filtro.equipamento?.imei))
            }

            if (filtro.equipamento?.modelo!=null) {
                predicates.add(builder.equal(root.get<Equipamento>("equipamento").get<ModeloRastreador>("modelo"), filtro.equipamento?.modelo))
            }

            if (filtro.equipamento?.atrasoGmt!=100) {
                if(filtro.equipamento?.atrasoGmt==-1)
                    predicates.add(builder.equal(root.get<Equipamento>("equipamento").get<Int>("atrasoGmt"), -1))
                else
                    predicates.add(builder.notEqual(root.get<Equipamento>("equipamento").get<Int>("atrasoGmt"), -1))
            }




            builder.and(*predicates.toTypedArray())
        }
    }


}
