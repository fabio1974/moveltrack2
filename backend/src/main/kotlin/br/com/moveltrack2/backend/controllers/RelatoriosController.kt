package br.com.moveltrack2.backend.controllers

import br.com.moveltrack.persistence.domain.*
import br.com.moveltrack.persistence.pojos.*
import br.com.moveltrack.persistence.repositories.*
import br.com.moveltrack2.backend.repositories.NativeRepositoryImpl
import br.com.moveltrack2.backend.repositories.RelatoriosRepositoryImpl
import org.springframework.data.rest.webmvc.BasePathAwareController
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController


@RestController
@BasePathAwareController
class RelatoriosController(
        val relatorioRepository: RelatoriosRepositoryImpl,
        val nativeRepo: NativeRepositoryImpl,
        val contratoRepository: ContratoRepository,
        val viagemRepository: ViagemRepository,
        val relatorioUsoIndevidoParamRepository: RelatorioUsoIndevidoParamRepository,
        val motoristaRepository: MotoristaRepository
        ) {


    @ResponseBody
    @PostMapping("/relatorioProdutividadeVeiculo")
    fun rel1(@RequestBody filtro: RelatorioFiltro): List<Any?> {
        var list = relatorioRepository.getRelatorioProdutividadeVeiculo(filtro.cliente,filtro.inicio,filtro.fim)
        var total = RelatorioFrota()
        for (obj in list) {
            total.qtdViagens += obj.qtdViagens
            total.qtdCidades += obj.qtdCidades
            total.qtdClientes += obj.qtdClientes
            total.valorDaCarga += obj.valorDaCarga
            total.pesoDaCarga += obj.pesoDaCarga
        }
        list.add(total)
        return list
    }


    @ResponseBody
    @PostMapping("/relatorioConsumoVeiculo")
    fun rel2(@RequestBody filtro: RelatorioFiltro): MutableList<ConsumoPorVeiculo> {
        var list = relatorioRepository.getRelatorioConsumoVeiculo(filtro.cliente,filtro.inicio,filtro.fim)
        var total = ConsumoPorVeiculo()
        for (obj in list) {
            val lv = relatorioRepository.getDespesasLitroPorVeiculo(obj.placa!!, filtro.inicio,filtro.fim)
            obj.litros = lv.litros
            if (obj.litros > 0) {
                obj.kml = obj.distanciaPercorrida / obj.litros
                obj.kml2 = obj.distanciaHodometro / obj.litros
            }
            if (obj.distanciaHodometro > 0)
                obj.erro =  (obj.distanciaHodometro - obj.distanciaPercorrida) / obj.distanciaHodometro

            total.kml += obj.kml
            total.distanciaHodometro += obj.distanciaHodometro
            total.distanciaPercorrida += obj.distanciaPercorrida
            total.erro += obj.erro
            total.kml2 += obj.kml2
            total.litros += obj.litros
            total.qtdViagens += obj.qtdViagens
        }
        list = list.sortedByDescending{it.kml2}.toMutableList()
        total.kml = total.kml/list.size
        total.kml2 = total.kml2/list.size
        total.erro = total.erro/list.size

        list.add(total)
        return list
    }





    @ResponseBody
    @PostMapping("/relatorioExpiracaoCnh")
    fun relatorioExpiracaoCnh(@RequestBody cliente: Cliente): MutableList<Motorista>? {
        val r = motoristaRepository.findAllByPatraoEqualsAndValidadeCnhLessThanEqualAndStatusEqualsOrderByValidadeCnh(cliente,cliente.dataNascimento,PessoaStatus.ATIVO)
        return r
    }

    @ResponseBody
    @PostMapping("/relatorioDistanciaDiaria")
    fun relatorioDistanciaDiaria(@RequestBody filtro: RelatorioFiltro): MutableList<RelatorioFrota>? {
        val r = relatorioRepository.getRelatorioDistanciaDiaria(filtro.cliente,filtro.veiculo,filtro.inicio,filtro.fim,filtro.orderBy)
        return r
    }




    @ResponseBody
    @PostMapping("/relatorioMotorista")
    fun rel3(@RequestBody filtro: RelatorioFiltro): MutableList<RelatorioFrota> {
        var list = relatorioRepository.getRelatorioMotorista(filtro.cliente,filtro.inicio,filtro.fim)
        var total = RelatorioFrota()
        for (obj in list) {

            val lv = relatorioRepository.getConsumoCombustivel(obj.id, filtro.inicio,filtro.fim)
            obj.litros =lv.litros
            obj.despesaCombustivel = lv.valor
            obj.despesaEstiva = relatorioRepository.getDespesaEstivaMotorista(obj.id, filtro.inicio,filtro.fim)  //id do motorista
            obj.despesaDiaria = relatorioRepository.getDespesaDiariaMotorista(obj.id, filtro.inicio,filtro.fim)  //id do motorista
            obj.despesaOutras = relatorioRepository.getDespesaOutrasMotorista(obj.id, filtro.inicio,filtro.fim)  //id do motorista

            if (obj.litros > 0)
                obj.kml =  obj.distanciaPercorrida / obj.litros

            total.litros += obj.litros
            total.despesaCombustivel += obj.despesaCombustivel
            total.despesaEstiva += obj.despesaEstiva
            total.despesaDiaria += obj.despesaDiaria
            total.despesaOutras += obj.despesaOutras
            total.diasViagens += obj.diasViagens
            total.distanciaPercorrida += obj.distanciaPercorrida

        }
        list = list.sortedByDescending{it.kml}.toMutableList()
        total.kml = total.distanciaPercorrida/total.litros

        list.add(total)
        return list
    }





    @ResponseBody
    @PostMapping("/relatorioEspectroVelocidades")
    fun relatorioEspectroVelocidades(@RequestBody filtro: RelatorioFiltro): List<LastLocation>? {

        return nativeRepo.relatorioEspectroVelocidades(filtro)
    }

    @ResponseBody
    @PostMapping("/getRelatorioUsoIndevido")
    fun getRelatorioUsoIndevido(@RequestBody relatorioUsoIndevidoParam: RelatorioUsoIndevidoParam): List<LastLocation>? {

        return nativeRepo.relatorioUsoIndevido(relatorioUsoIndevidoParam)
    }



    @ResponseBody
    @PostMapping("/relatorioViagensPorEstado")
    fun relatorioViagensPorEstado(@RequestBody filtro: RelatorioFiltro): MutableList<RelatorioMotoristaPorViagem?> {

        var list : MutableList<RelatorioMotoristaPorViagem?> = ArrayList<RelatorioMotoristaPorViagem?>()

        var total = RelatorioMotoristaPorViagem()

        var viagens = relatorioRepository.getRelatorioViegensPorEstado(filtro.cliente,filtro.uf!!,filtro.inicio,filtro.fim)
        if (viagens != null) {
            for (viagem in viagens) {

                var despesaCombustivel = relatorioRepository.getDespesasLitroPorVeiculo(viagem?.veiculo?.placa!!,viagem.partida!!,viagem.chegadaReal!!)
                var despesaDiarias = relatorioRepository.getDespesaViagemPorEspecie(viagem.id, DespesaFrotaEspecie.DIARIA.name)
                var despesaEstivas = relatorioRepository.getDespesaViagemPorEspecie(viagem.id, DespesaFrotaEspecie.ESTIVA.name)
                var despesaDiversas = relatorioRepository.getDespesaViagemPorEspecie(viagem.id, DespesaFrotaEspecie.OUTROS.name)

                var item = RelatorioMotoristaPorViagem()
                item.viagem = viagem
                item.despesaCombustivel = despesaCombustivel
                item.despesaEstivas = despesaEstivas
                item.despesaDiarias = despesaDiarias
                item.despesaDiversas = despesaDiversas
                item.pesoDaCarga = viagem.pesoDaCarga
                item.diasViagens = ((viagem.chegadaReal?.toEpochSecond()!! - viagem.partida?.toEpochSecond()!!).toDouble()/86400)

                list.add(item)

                total.despesaCombustivel.litros += item.despesaCombustivel.litros
                total.despesaCombustivel.valor += item.despesaCombustivel.valor
                total.despesaDiarias += item.despesaDiarias
                total.despesaEstivas += item.despesaEstivas
                total.despesaDiversas += item.despesaDiversas
                total.diasViagens += item.diasViagens
                total.viagem.valorDaCarga += item.viagem.valorDaCarga

                total.viagem.distanciaHodometro += viagem.distanciaHodometro
                total.viagem.distanciaPercorrida += viagem.distanciaPercorrida

            }
        }

        list.add(total)
        return list
    }


    @ResponseBody
    @PostMapping("/relatorioViagensDoMotorista")
    fun relatorioViagensDoMotorista(@RequestBody filtro: RelatorioFiltro): MutableList<RelatorioMotoristaPorViagem?> {

        val list : MutableList<RelatorioMotoristaPorViagem?> = ArrayList<RelatorioMotoristaPorViagem?>()
        val total = RelatorioMotoristaPorViagem()
        val viagens = relatorioRepository.getRelatorioViegensDoMotorista(filtro.cliente,filtro.motorista!!,filtro.inicio,filtro.fim)
        if (viagens != null) {
            for (viagem in viagens) {

                val despesaCombustivel = relatorioRepository.getDespesasLitroPorVeiculo(viagem?.veiculo?.placa!!,viagem.partida!!,viagem.chegadaReal!!)
                val despesaDiarias = relatorioRepository.getDespesaDiariaMotorista(filtro.motorista?.id,viagem.partida!!,viagem.chegadaReal!!)
                val despesaEstivas = relatorioRepository.getDespesaEstivaMotorista(filtro.motorista?.id,viagem.partida!!,viagem.chegadaReal!!)

                val item = RelatorioMotoristaPorViagem()
                item.viagem = viagem
                item.despesaCombustivel = despesaCombustivel
                item.despesaEstivas = despesaEstivas
                item.despesaDiarias = despesaDiarias
                item.pesoDaCarga = viagem.pesoDaCarga
                item.diasViagens = ((viagem.chegadaReal?.toEpochSecond()!! - viagem.partida?.toEpochSecond()!!).toDouble()/86400)

                list.add(item)

                total.despesaCombustivel.litros += item.despesaCombustivel.litros
                total.despesaCombustivel.valor += item.despesaCombustivel.valor
                total.despesaDiarias += item.despesaDiarias
                total.despesaEstivas += item.despesaEstivas
                total.diasViagens += item.diasViagens
                total.pesoDaCarga += item.pesoDaCarga

                total.viagem.distanciaHodometro += viagem.distanciaHodometro
                total.viagem.distanciaPercorrida += viagem.distanciaPercorrida

            }
        }
        list.add(total)
        return list
    }


    @ResponseBody
    @PostMapping("/relatorioViagensDoVeiculo")
    fun relatorioViagensDoVeiculo(@RequestBody filtro: RelatorioFiltro): MutableList<RelatorioMotoristaPorViagem?> {

        val list : MutableList<RelatorioMotoristaPorViagem?> = ArrayList<RelatorioMotoristaPorViagem?>()
        val total = RelatorioMotoristaPorViagem()
        val viagens =  viagemRepository.findAllByVeiculoAndPartidaBetweenAndChegadaRealNotNullOrderByPartida(filtro.veiculo!!,filtro.inicio,filtro.fim)
        if (viagens != null) {
            for (viagem in viagens) {

                val despesaCombustivel = relatorioRepository.getDespesasLitroPorVeiculo(viagem?.veiculo?.placa!!,viagem.partida!!,viagem.chegadaReal!!)
                val despesaDiarias = relatorioRepository.getDespesaDiariaMotorista(filtro.motorista?.id,viagem.partida!!,viagem.chegadaReal!!)
                val despesaEstivas = relatorioRepository.getDespesaEstivaMotorista(filtro.motorista?.id,viagem.partida!!,viagem.chegadaReal!!)

                val item = RelatorioMotoristaPorViagem()
                item.viagem = viagem
                item.despesaCombustivel = despesaCombustivel
                item.despesaEstivas = despesaEstivas
                item.despesaDiarias = despesaDiarias
                item.pesoDaCarga = viagem.pesoDaCarga
                item.diasViagens = ((viagem.chegadaReal?.toEpochSecond()!! - viagem.partida?.toEpochSecond()!!).toDouble()/86400)

                list.add(item)

                total.despesaCombustivel.litros += item.despesaCombustivel.litros
                total.despesaCombustivel.valor += item.despesaCombustivel.valor
                total.despesaDiarias += item.despesaDiarias
                total.despesaEstivas += item.despesaEstivas
                total.diasViagens += item.diasViagens
                total.pesoDaCarga += item.pesoDaCarga

                total.viagem.distanciaHodometro += viagem.distanciaHodometro
                total.viagem.distanciaPercorrida += viagem.distanciaPercorrida

            }
        }
        list.add(total)
        return list
    }



    @ResponseBody
    @PostMapping("/relatorioDestino")
    fun rel4(@RequestBody filtro: RelatorioFiltro): MutableList<RelatorioFrota> {
        var list = relatorioRepository.getRelatorioDestino(filtro.cliente,filtro.inicio,filtro.fim)
        var total = RelatorioFrota()
        for (obj in list) {
            val lv = relatorioRepository.getDespesasLitroPorDestino(obj.id, filtro.inicio,filtro.fim)
            obj.litros =lv.litros
            obj.despesaCombustivel = lv.valor

            //obj.despesaEstiva = relatorioRepository.getDespesaEstiva(obj.id, filtro.inicio,filtro.fim)  //id do motorista
            //obj.despesaDiaria = relatorioRepository.getDespesaDiaria(obj.id, filtro.inicio,filtro.fim)  //id do motorista

            total.litros += obj.litros
            total.despesaCombustivel += obj.despesaCombustivel
            //total.despesaEstiva += obj.despesaEstiva
            //total.despesaDiaria += obj.despesaDiaria
            total.diasViagens += obj.diasViagens
            total.distanciaPercorrida += obj.distanciaPercorrida
            total.qtdCidades += obj.qtdCidades
            total.qtdClientes += obj.qtdClientes
            total.qtdViagens += obj.qtdViagens
            total.valorDaCarga += obj.valorDaCarga
            total.pesoDaCarga += obj.pesoDaCarga
            total.valorDevolucao += obj.valorDevolucao
        }
        list.add(total)
        return list
    }



    @ResponseBody
    @PostMapping("/relatorioEstado")
    fun rel5(@RequestBody filtro: RelatorioFiltro): MutableList<RelatorioFrota> {
        var list = relatorioRepository.getRelatorioEstado(filtro.cliente,filtro.inicio,filtro.fim)
        var total = RelatorioFrota()
        for (obj in list) {
            val lv = relatorioRepository.getDespesasLitroPorEstado(obj.id, filtro.inicio,filtro.fim)
            obj.litros =lv.litros
            obj.despesaCombustivel = lv.valor

            //obj.despesaEstiva = relatorioRepository.getDespesaEstiva(obj.id, filtro.inicio,filtro.fim)  //id do motorista
            //obj.despesaDiaria = relatorioRepository.getDespesaDiaria(obj.id, filtro.inicio,filtro.fim)  //id do motorista

            total.litros += obj.litros
            total.despesaCombustivel += obj.despesaCombustivel
            //total.despesaEstiva += obj.despesaEstiva
            //total.despesaDiaria += obj.despesaDiaria
            total.diasViagens += obj.diasViagens
            total.distanciaPercorrida += obj.distanciaPercorrida
            total.qtdCidades += obj.qtdCidades
            total.qtdClientes += obj.qtdClientes
            total.qtdViagens += obj.qtdViagens
            total.valorDaCarga += obj.valorDaCarga
            total.pesoDaCarga += obj.pesoDaCarga
            total.valorDevolucao += obj.valorDevolucao
        }
        list.add(total)
        return list
    }


    @ResponseBody
    @PostMapping("/relatorioProdutividadeMensal")
    fun rel6(@RequestBody filtro: RelatorioFiltro): MutableList<RelatorioFrota> {
        var list = relatorioRepository.getProdutividadeMensal(filtro.cliente,filtro.inicio,filtro.fim)
        var total = RelatorioFrota()
        for (obj in list) {

            //val lv = relatorioRepository.getDespesasLitroPorEstado(obj.id, filtro.inicio,filtro.fim)
            //obj.litros =lv.litros
            //obj.despesaCombustivel = lv.valor

            //obj.despesaEstiva = relatorioRepository.getDespesaEstiva(obj.id, filtro.inicio,filtro.fim)  //id do motorista
            //obj.despesaDiaria = relatorioRepository.getDespesaDiaria(obj.id, filtro.inicio,filtro.fim)  //id do motorista

            total.litros += obj.litros
            total.despesaCombustivel += obj.despesaCombustivel
            //total.despesaEstiva += obj.despesaEstiva
            //total.despesaDiaria += obj.despesaDiaria
            total.diasViagens += obj.diasViagens
            total.distanciaPercorrida += obj.distanciaPercorrida
            total.qtdCidades += obj.qtdCidades
            total.qtdClientes += obj.qtdClientes
            total.qtdViagens += obj.qtdViagens
            total.valorDaCarga += obj.valorDaCarga
            total.pesoDaCarga += obj.pesoDaCarga
            total.valorDevolucao += obj.valorDevolucao
            total.despesaTotal += obj.despesaTotal
            total.kml += obj.kml
        }
        total.kml = total.kml/list.size
        list.add(total)
        return list
    }



    @ResponseBody
    @PostMapping("/relatorioDespesaMensal")
    fun rel7(@RequestBody filtro: RelatorioFiltro): MutableList<RelatorioMensalDespesa> {
        var list = relatorioRepository.getRelatorioDespesaMensal(filtro.cliente,filtro.inicio,filtro.fim, filtro.numeroViagem)
        var total = RelatorioMensalDespesa()
        var percentagens = RelatorioMensalDespesa()
        for (obj in list) {

            total.carga += obj.carga
            total.combustivel += obj.combustivel
            total.manutencao += obj.manutencao
            total.estivas += obj.estivas
            total.diarias += obj.diarias
            total.transito += obj.transito
            total.ipva += obj.ipva
            total.outras += obj.outras

            obj.despesa = obj.combustivel + obj.manutencao + obj.estivas + obj.diarias + obj.ipva + obj.outras + obj.transito

        }

        total.despesa =  total.combustivel + total.manutencao + total.estivas + total.diarias + total.ipva + total.outras + total.transito

        percentagens.carga = 100.toDouble()
        percentagens.combustivel = 100 * total.combustivel/total.carga
        percentagens.manutencao = 100 * total.manutencao/total.carga
        percentagens.estivas = 100 * total.estivas/total.carga
        percentagens.diarias = 100 * total.diarias/total.carga
        percentagens.ipva = 100 * total.ipva/total.carga
        percentagens.outras = 100 * total.outras/total.carga

        percentagens.despesa = percentagens.combustivel + percentagens.manutencao + percentagens.estivas + percentagens.diarias + percentagens.ipva + percentagens.outras + percentagens.transito

        list.add(total)
        list.add(percentagens)

        return list
    }




    @ResponseBody
    @PostMapping("/getRelatorioUsoIndevidoParam")
    fun getRelatorioUsoIndevidoParam(@RequestBody filtro: RelatorioFiltro): MutableList<RelatorioUsoIndevidoParam>{
        return relatorioUsoIndevidoParamRepository.findAllByCliente(filtro.cliente)
    }


    @ResponseBody
    @PostMapping("/salvaRelatorioUsoIndevidoParam")
    fun salvaRelatorioUsoIndevidoParam(@RequestBody relatorioUsoIndevidoParam: RelatorioUsoIndevidoParam): Unit{
        val list = relatorioUsoIndevidoParamRepository.findAllByCliente(relatorioUsoIndevidoParam.cliente)
        if(!list.isEmpty()){
            val doBanco = list.get(0)
            relatorioUsoIndevidoParam.id = doBanco.id
        }
        relatorioUsoIndevidoParamRepository.save(relatorioUsoIndevidoParam)
    }








}
