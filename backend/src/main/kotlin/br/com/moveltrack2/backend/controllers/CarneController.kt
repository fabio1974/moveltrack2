package br.com.moveltrack2.backend.controllers

import br.com.moveltrack2.backend.Iugu.IuguException
import br.com.moveltrack2.backend.Iugu.IuguUtils
import br.com.moveltrack.persistence.domain.*
import br.com.moveltrack.persistence.repositories.*
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.rest.webmvc.BasePathAwareController
import org.springframework.util.StringUtils.isEmpty
import org.springframework.web.bind.annotation.*

import br.com.moveltrack.persistence.domain.MBoletoTipo
import br.com.moveltrack.persistence.domain.MBoletoStatus
import br.com.moveltrack.persistence.domain.TipoDeCobranca
import java.text.SimpleDateFormat
import br.com.moveltrack2.backend.specifications.CarneSpecification
import br.com.moveltrack.persistence.util.BoletoUtils
import br.com.moveltrack.persistence.util.Utils.DDMMYY_FORMAT
import br.com.moveltrack2.backend.utils.Utils.moeda
import java.io.ByteArrayOutputStream
import java.math.RoundingMode
import java.net.URL
import java.time.Duration
import java.time.ZonedDateTime
import javax.servlet.http.HttpServletResponse


@RestController
@BasePathAwareController
class CarneController(
        val empregadoRepository: EmpregadoRepository,
        val mBoletoRepository: MBoletoRepository,
        val contratoRepository: ContratoRepository,
        val carneRepository: CarneRepository,
        val iuguRepository: IuguRepository,
        val iuguUtils: IuguUtils,
        val contratoController: ContratoController
) {




    @ResponseBody
    @PatchMapping("/carnes")
    fun patch(@RequestParam(value = "pageIndex") pageIndex:Int,
              @RequestParam(value = "pageSize") pageSize:Int,
              @RequestParam(value = "sortField") sortField:String?=null,
              @RequestParam(value = "sortDirection",defaultValue = "DESC") sortDirection: Sort.Direction ,
              @RequestBody filtro: Carne): Page<Carne> {

        val especification = CarneSpecification.getFiltro(filtro, contratoRepository)
        val pageRequest = PageRequest.of(pageIndex,pageSize,sortDirection,sortField)
        val list: Page<Carne> = carneRepository.findAll(especification,pageRequest)

        list.map{adicionaValorPrimeiroBoleto(it)}

        return list
    }

    private fun adicionaValorPrimeiroBoleto(it: Carne?): Carne {
        var list: List<MBoleto> = mBoletoRepository.findAllByCarne(it)
        it!!.valorPrimeiroBoleto =  list[0].valor
        return it
    }

    @PostMapping("/atualizarCarne")
    fun atualizar(@RequestBody carne: Carne){
        carneRepository.save(carne)
    }


    @ResponseBody
    @PostMapping("/carnes/{quantidadeBoleto}")
    @Throws(IuguException::class)
    fun create(@RequestBody contrato: Contrato, @PathVariable("quantidadeBoleto") quantidadeBoleto: Int): Any {

        var carne = Carne()
        carne.contrato = contrato
        carne.tipoDeCobranca = TipoDeCobranca.COM_REGISTRO
        carne.quantidadeBoleto = quantidadeBoleto
        carne.dataVencimentoInicio = getPrimeiroVencimento(contrato);
        var emissor = empregadoRepository.findById(3).get()
        val qtdeVeiculosAtivos = contrato.veiculos!!.size
        carne.dataVencimentoFim = geraBoletos(carne,emissor,qtdeVeiculosAtivos)

        var contratoAux = contratoRepository.findById(contrato.id!!).get()
        contratoAux.mensalidade = contrato.mensalidade
        contratoAux.contratoGeraCarne = ContratoGeraCarne.GERADO;
        contratoRepository.save(contratoAux)

        carneRepository.save(carne);

        return "Carnê gerado com sucesso!"
    }


    @GetMapping("/getCarnePdf/{id}")
    fun  getCarnePdf(response : HttpServletResponse, @PathVariable("id")id:Int): ByteArray {
        val carne = carneRepository.findById(id).get()
        val mBoletos:List<MBoleto> = mBoletoRepository.findAllByCarne(carne)
        return BoletoUtils.getCarneIuguInBytes(mBoletos)
    }


    fun geraBoletos(carne: Carne, emissor: Empregado, qtdeVeiculosAtivos: Int): ZonedDateTime {

        var vencimento: ZonedDateTime = carne.dataVencimentoInicio!!
        for (i in 0 until carne.quantidadeBoleto) {

            var boleto = criaMBoleto(i, vencimento, carne, emissor, qtdeVeiculosAtivos)

            var iugu: Iugu? = iuguUtils.createIuguInvoice(boleto)
            if(!isEmpty(iugu!!.invoiceId))
                boleto.iugu = iuguRepository.save(iugu!!)

            if(carne.id==null)
                carneRepository.save(carne)

            mBoletoRepository.save(boleto)
            vencimento = vencimento.plusMonths(1)
        }
        return vencimento
    }



    private fun criaMBoleto(i: Int, vencimento: ZonedDateTime, carne: Carne, emissor: Empregado, qtdeVeiculosAtivos: Int): MBoleto {
        val mBoleto = MBoleto()
        mBoleto.contrato =carne.contrato
        mBoleto.dataEmissao = ZonedDateTime.now()
        mBoleto.tipoDeCobranca =carne.tipoDeCobranca
        mBoleto.dataVencimento = vencimento
        mBoleto.emissor =emissor
        mBoleto.carne =carne
        mBoleto.mensagem34 = "referente ao rastreamento mensal de $qtdeVeiculosAtivos veículo${if(qtdeVeiculosAtivos>1) "s" else ""}"

        var valor = carne.contrato?.mensalidade!! * qtdeVeiculosAtivos
        if (i == 0) { //no primeiro boleto, calculo do proporcional
            val dataBase = carne.contrato!!.dataBase
            if (dataBase?.plusMonths(1)?.dayOfYear != vencimento.dayOfYear) {
                val days = Duration.between(dataBase!!,vencimento).toDays()
                valor = valor * (days.toDouble() / 31)
                valor = valor.toBigDecimal().setScale(2, RoundingMode.UP).toDouble()
                mBoleto.mensagem34 = "Proporcional de ${moeda(valor)}: ${days.toInt()} dias de rastreamento (${dataBase.format(DDMMYY_FORMAT)} a ${vencimento.format(DDMMYY_FORMAT)})"
            } else
                mBoleto.mensagem34 =  "Referente ao rastreamento mensal de $qtdeVeiculosAtivos  veículo" + if (qtdeVeiculosAtivos > 1) "s" else ""
        }

        mBoleto.valor = valor
        mBoleto.multa=5.toDouble()
        mBoleto.juros=1.toDouble()
        mBoleto.nossoNumero= this.mBoletoRepository.getProximoNossoNumero(TipoDeCobranca.COM_REGISTRO) // mBoletoDao.getProximoNossoNumero(mBoleto.getTipoDeCobranca()))
        mBoleto.situacao=MBoletoStatus.EMITIDO
        mBoleto.tipo =MBoletoTipo.MENSAL
        mBoleto.tipoDeCobranca = TipoDeCobranca.COM_REGISTRO

        return mBoleto
    }


    fun getPrimeiroVencimento(contrato: Contrato?): ZonedDateTime {
        val dataBase: ZonedDateTime = contratoController.getDataBase(contrato)
        var primeiroVencimento: ZonedDateTime = dataBase.withDayOfMonth(contrato?.diaVencimento!!)
        val daquiHaQuinzeDias = ZonedDateTime.now().plusDays(15)

        //primeiro vencimento tem que ser maior que a data base e maior que o dia de hoje + 15 dias
        while (!primeiroVencimento.isAfter(daquiHaQuinzeDias) || !primeiroVencimento.isAfter(dataBase)) {
            primeiroVencimento = primeiroVencimento.plusMonths(1)
        }
        return primeiroVencimento
    }

    private fun getBarCodeInBytes(url: String?): ByteArray? {
        val outputStream = ByteArrayOutputStream()
        URL(url).openStream().copyTo(outputStream,4096)
        return outputStream.toByteArray()
    }
}
