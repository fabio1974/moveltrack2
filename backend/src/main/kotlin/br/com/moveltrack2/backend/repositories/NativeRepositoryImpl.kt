package br.com.moveltrack2.backend.repositories

import br.com.moveltrack.persistence.domain.*
import br.com.moveltrack.persistence.pojos.LastLocation
import br.com.moveltrack.persistence.pojos.RelatorioFiltro
import br.com.moveltrack.persistence.repositories.ContratoRepository
import br.com.moveltrack.persistence.repositories.EquipamentoRepository
import br.com.moveltrack.persistence.repositories.LocationRepository
import br.com.moveltrack.persistence.repositories.VeiculoRepository
import br.com.moveltrack2.backend.utils.GoogleGeo
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.sql.Timestamp
import java.util.*
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Component
class NativeRepositoryImpl constructor(
        @PersistenceContext var em: EntityManager,
        val veiculoRepository: VeiculoRepository,
        val equipamentoRepository: EquipamentoRepository,
        val locationRepository: LocationRepository,
        val contratoRepository: ContratoRepository,
        val googleGeo: GoogleGeo
) {


    fun searchViagemByNumeroViagemContainig(numeroViagem:Int): List<Viagem> {
        val sql = ("select * from viagem  WHERE CAST(numeroViagem as CHAR) LIKE '%${numeroViagem}%' ")
        val query = em.createNativeQuery(sql, Viagem::class.java)
        val r =  query.resultList.map{it as Viagem}
        em.close()
        return r
    }


    fun getAllContratosByClienteNomeContaining(nome:String): List<Contrato> {
            val sql = ("select c.* from contrato c join pessoa p on c.cliente_id = p.id where p.nome like '%${nome}%' order by p.nome ")
            val query = em.createNativeQuery(sql, Contrato::class.java)
            val r =  query.resultList.map{it as Contrato}
        em.close()
        return r
    }

    fun getVeiculoByImei(imei:String): Veiculo?{
        val sql = (" select v.* from veiculo v" +
                " inner join equipamento e on e.id = v.equipamento_id" +
                " where imei = ${imei} ")
        val query = em.createNativeQuery(sql,Veiculo::class.java)
        val r = query.singleResult as Veiculo
        em.close()
        return r
    }


    fun getMapaFrotaByContratoId(contratoId:Int,dataCorte: Date?=null): List<LastLocation>?{
        val sql = (" select " +
                " v.placa,"+
                " v.marcaModelo,"+
                " v.cor,"+
                " v.tipo,"+
                " v.status,"+
                " l.battery," +
                " l.comando," +
                " l.dateLocation," +
                " l.gps," +
                " l.gsm," +
                " l.ignition," +
                " l.latitude," +
                " l.longitude," +
                " l.imei,"+
                " l.velocidade from (" +
                " select max(l2.dateLocation) as dateLocation, l2.imei from location l2" +
                " inner join equipamento e2 on e2.imei = l2.imei  " +
                " inner join veiculo v2 on v2.equipamento_id = e2.id" +
                " where v2.contrato_id = ${contratoId}" +
                (if(dataCorte!=null)  (" and l2.dateLocation < :dataCorte ") else "") +
                " and v2.status = 'ATIVO'" +
                " group by l2.imei" +
                " ) as md " +
                " inner join location l on (l.imei = md.imei and l.dateLocation = md.dateLocation)" +
                " inner join equipamento e on e.imei = md.imei  " +
                " inner join veiculo v on v.equipamento_id = e.id" +
                " group by v.placa, v.marcaModelo, v.cor, v.tipo, v.status, l.battery, l.comando, l.dateLocation, l.gps, l.gsm, l.ignition, l.latitude, l.longitude, l.imei, l.velocidade" +
                " order by l.dateLocation desc"
                )
        val query = em.createNativeQuery(sql)
        if(dataCorte!=null)
            query.setParameter("dataCorte",dataCorte)
        val r = query.resultList.toList().map { it ->
            var a = it as Array<Any?>
            LastLocation(
                    placa = a[0] as String?,
                    marcaModelo = a[1] as String?,
                    cor = findCorByName(a[2] as String) ,
                    tipo =  findVeiculoTipoByName(a[3] as String),
                    status = findVeiculoStatusByName(a[4] as String),
                    battery = a[5] as String?,
                    comando = a[6] as String?,
                    dateLocation = a[7] as Timestamp,
                    gps = a[8] as String?,
                    gsm = a[9] as String?,
                    ignition = a[10] as String?,
                    latitude = a[11] as Double,
                    longitude = a[12] as Double,
                    imei = a[13] as String,
                    velocidade = a[14] as Double
            )
        }
        em.close()
        return r
    }













    fun relatorioUsoIndevido(relatorioUsoIndevidoParam: RelatorioUsoIndevidoParam): List<LastLocation>?{

        var contrato = contratoRepository.findByCliente(relatorioUsoIndevidoParam.cliente)

        var orderBy = if(relatorioUsoIndevidoParam.orderBy!=null && relatorioUsoIndevidoParam.orderBy == "veiculo") "v.placa," else ""
        var filtroVeiculo = " "
        if(relatorioUsoIndevidoParam.veiculo!=null)
            filtroVeiculo = " and v.id = "+ relatorioUsoIndevidoParam.veiculo?.id + " "


        val sql = (" select  v.placa, v.marcaModelo, v.cor, v.tipo,  l.dateLocation,  l.latitude, l.longitude, l.velocidade  " +
                " from location l " +
                " inner join equipamento e on e.imei = l.imei    " +
                " inner join veiculo v on v.equipamento_id = e.id " +
                " where v.contrato_id = :contratoId  " +
                " and v.status = 'ATIVO' " + filtroVeiculo +
                " and l.velocidade >= 10  " +
                " and l.dateLocation between :inicio and :fim" +
                " and " +
                " " +
                "(" +
                " (dayofweek(l.dateLocation) = 1 and time(l.dateLocation) not between time(:segundaInicio) and time(:segundaFim)) or" +
                " (dayofweek(l.dateLocation) = 2 and time(l.dateLocation) not between time(:tercaInicio) and time(:tercaFim)) or" +
                " (dayofweek(l.dateLocation) = 3 and time(l.dateLocation) not between time(:quartaInicio) and time(:quartaFim)) or" +
                " (dayofweek(l.dateLocation) = 4 and time(l.dateLocation) not between time(:quintaInicio) and time(:quintaFim)) or" +
                " (dayofweek(l.dateLocation) = 5 and time(l.dateLocation) not between time(:sextaInicio) and time(:sextaFim)) or" +
                " (dayofweek(l.dateLocation) = 6 and time(l.dateLocation) not between time(:sabadoInicio) and time(:sabadoFim)) or" +
                " (dayofweek(l.dateLocation) = 7 and time(l.dateLocation) not between time(:domingoInicio) and time(:domingoFim)) " +
                ")" +
                " order by "+orderBy+" l.dateLocation")
        val query = em.createNativeQuery(sql)
        query.setParameter("contratoId",contrato.id)
        query.setParameter("inicio",relatorioUsoIndevidoParam.inicio)
        query.setParameter("fim",relatorioUsoIndevidoParam.fim)
        query.setParameter("segundaInicio",relatorioUsoIndevidoParam.segundaInicio)
        query.setParameter("segundaFim",relatorioUsoIndevidoParam.segundaFim)
        query.setParameter("tercaInicio",relatorioUsoIndevidoParam.tercaInicio)
        query.setParameter("tercaFim",relatorioUsoIndevidoParam.tercaFim)
        query.setParameter("quartaInicio",relatorioUsoIndevidoParam.quartaInicio)
        query.setParameter("quartaFim",relatorioUsoIndevidoParam.quartaFim)
        query.setParameter("quintaInicio",relatorioUsoIndevidoParam.quintaInicio)
        query.setParameter("quintaFim",relatorioUsoIndevidoParam.quintaFim)
        query.setParameter("sextaInicio",relatorioUsoIndevidoParam.sextaInicio)
        query.setParameter("sextaFim",relatorioUsoIndevidoParam.sextaFim)
        query.setParameter("sabadoInicio",relatorioUsoIndevidoParam.sabadoInicio)
        query.setParameter("sabadoFim",relatorioUsoIndevidoParam.sabadoFim)
        query.setParameter("domingoInicio",relatorioUsoIndevidoParam.domingoInicio)
        query.setParameter("domingoFim",relatorioUsoIndevidoParam.domingoFim)

        val r = query.resultList.toList().map { it ->
            var a = it as Array<Any?>
            LastLocation(
                    placa = a[0] as String?,
                    marcaModelo = a[1] as String?,
                    cor = findCorByName(a[2] as String) ,
                    tipo =  findVeiculoTipoByName(a[3] as String),
                    dateLocation = a[4] as Timestamp,
                    latitude = a[5] as Double,
                    longitude = a[6] as Double,
                    velocidade = a[7] as Double
            )
        }
        em.close()
        return r
    }


    @Transactional(timeout = 300)
    public fun relatorioEspectroVelocidades(filtro: RelatorioFiltro): List<LastLocation>?{

        var contrato = contratoRepository.findByCliente(filtro.cliente)

        val sql = (" select  v.placa, v.marcaModelo, v.cor, v.tipo,  l.dateLocation,  l.latitude, l.longitude, l.velocidade " +
                " from location l" +
                " inner join equipamento e on e.imei = l.imei   " +
                " inner join veiculo v on v.equipamento_id = e.id" +
                " where v.contrato_id = :contratoId " +
                " and v.status = 'ATIVO' " +
                " and l.velocidade >= :inferior " +   (if(filtro.superior!=null) " and l.velocidade <= :superior" else " ") +
                " and l.dateLocation between :inicio and :fim")
        val query = em.createNativeQuery(sql)
        query.setParameter("contratoId",contrato.id)
        query.setParameter("inferior",filtro.inferior)
        if(filtro.superior!=null)
            query.setParameter("superior",filtro.superior)
        query.setParameter("inicio",filtro.inicio)
        query.setParameter("fim",filtro.fim)

        val r = query.resultList.toList().map { it ->
            var a = it as Array<Any?>
            LastLocation(
                    placa = a[0] as String?,
                    marcaModelo = a[1] as String?,
                    cor = findCorByName(a[2] as String) ,
                    tipo =  findVeiculoTipoByName(a[3] as String),
                    dateLocation = a[4] as Timestamp,
                    latitude = a[5] as Double,
                    longitude = a[6] as Double,
                    velocidade = a[7] as Double,
                    endereco = this.googleGeo.getAddressFromLocation(Location(latitude = a[5] as Double,longitude = a[6] as Double),true)
            )
        }
        em.close()
        return r
        //runBlocking {paradas.pmap{it.endereco = googleGeo.getAddressFromLocation(it, true)}}
    }




}
