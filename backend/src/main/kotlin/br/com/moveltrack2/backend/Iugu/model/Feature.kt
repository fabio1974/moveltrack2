package br.com.moveltrack2.backend.Iugu.model

class Feature(val name: String, val identifier: String, val value: Int)
