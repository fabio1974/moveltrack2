package br.com.moveltrack2.backend.Iugu

import br.com.moveltrack.persistence.util.OSValidator
import br.com.moveltrack2.backend.Iugu.model.Invoice
import br.com.moveltrack2.backend.Iugu.responses.InvoiceResponse
import com.fasterxml.jackson.databind.ObjectMapper
import com.sun.jersey.api.client.Client
import com.sun.jersey.api.client.ClientResponse
import org.springframework.stereotype.Component
import java.util.*

//import sun.misc.BASE64Encoder

@Component
class InvoiceService{

    val DOMAIN = "https://api.iugu.com/v1"

    fun getIuguAuth(): String{
        var user=""
        if(OSValidator.isUnix) {
            //println("Cuidado!!!...modificando sistema iugu PRODUÇÂO")
            user = "929467175fcce7aa3378e74d4c5304c7";  //produção

        }else {
            //println("...modificando sistema iugu HOMOLOGAÇÂO")
            //user = "929467175fcce7aa3378e74d4c5304c7";  //produção
            user = "1ff25a762d28d51bd34863406cbb8c2b";  //homologacao
        }
        val password = ""
        val authString = "$user:$password"
        return Base64.getEncoder().encodeToString(authString.toByteArray())
        //BASE64Encoder().encode(authString.toByteArray())
    }

    fun cancelInvoice(id: String): InvoiceResponse {
        val url = "$DOMAIN/invoices/$id/cancel"
        val webResource = Client.create().resource(url)
        val resp = webResource
                  .accept("application/json")
                  .header("Authorization", "Basic ${getIuguAuth()}")
                  .put(ClientResponse::class.java)

        if (resp.status != 200) {
            System.err.println("Unable to connect to the server")
        }
        return ObjectMapper().readValue(resp.getEntity(String::class.java),InvoiceResponse::class.java)
    }

    fun findInvoice(id: String): InvoiceResponse {
        val url = "$DOMAIN/invoices/$id"
        val webResource = Client.create().resource(url)
        val resp = webResource
                .accept("application/json")
                .header("Authorization", "Basic ${getIuguAuth()}")
                .get(ClientResponse::class.java)
        if (resp.status != 200) {
            System.err.println("Unable to connect to the server")
        }
        val obj =  ObjectMapper().readValue(resp.getEntity(String::class.java),InvoiceResponse::class.java)
        return obj
    }


    fun createInvoice(invoice: Invoice) : InvoiceResponse{
        val url = "$DOMAIN/invoices"
        val webResource = Client.create().resource(url)
        var mapper = ObjectMapper()
        val resp = webResource
                   .accept("application/json")
                   .header("Authorization", "Basic ${getIuguAuth()}")
                   .type("application/json")
                   .post(ClientResponse::class.java,mapper.writeValueAsString(invoice))
        if (resp.status != 200) {
            System.err.println("Problema na criação do Boleto na Iugu...Erro${resp.status}")
            throw IuguException("Erro crianco invoice",resp.status,resp.toString())
        }
        return mapper.readValue(resp.getEntity(String::class.java),InvoiceResponse::class.java)
    }


}
