package br.com.moveltrack2.backend.controllers

import br.com.moveltrack2.backend.Iugu.IuguUtils
import br.com.moveltrack.persistence.domain.*
import br.com.moveltrack.persistence.repositories.*
import br.com.moveltrack2.backend.seguranca.Criptografia
import br.com.moveltrack2.backend.specifications.MotoristaSpecification
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.rest.webmvc.BasePathAwareController
import org.springframework.web.bind.annotation.*

@RestController
@BasePathAwareController
//@RequestMapping("api")
class MotoristaController(
        val pessoaRepository: PessoaRepository,
        val usuarioRepository: UsuarioRepository,
        val mBoletoMyRepository: MBoletoRepository,
        val contratoRepository: ContratoRepository,
        val iuguRepository: IuguRepository,
        val motoristaRepository: MotoristaRepository,
        val perfilRepository: PerfilRepository,
        val iuguUtils: IuguUtils,
        val veiculoRepositoryMy: VeiculoRepository
) {


    @ResponseBody
    @PatchMapping("/motoristas")
    fun patch(@RequestParam(value = "pageIndex") pageIndex:Int,
              @RequestParam(value = "pageSize") pageSize:Int,
              @RequestParam(value = "sortField") sortField:String?=null,
              @RequestParam(value = "sortDirection",defaultValue = "1") sortDirection: Sort.Direction ,
              @RequestBody filtro: Motorista): Page<Motorista> {

        val especification = MotoristaSpecification.getFiltro(filtro)
        val pageRequest = PageRequest.of(pageIndex, pageSize, sortDirection,sortField)
        val list: Page<Motorista> = motoristaRepository.findAll(especification,pageRequest)
        //list.map {buildContrato(it)}
        return list
    }


    @PostMapping("/motoristas")
    fun create(@RequestBody motorista: Motorista): Any {
        motorista.nome = motorista.nome?.toUpperCase()
        motorista.usuario = null
/*
        if(motorista.usuario==null)
            motorista.usuario = Usuario()
        motorista.usuario?.nomeUsuario = motorista.cpf?.replace(".","")?.replace("-","")
        motorista.usuario?.senha = Criptografia().calculaHash(motorista.usuario?.nomeUsuario?.substring(7)!!)
        motorista.usuario?.perfil = perfilRepository.findByTipo(PerfilTipo.VENDEDOR)
        usuarioRepository.save(motorista.usuario!!)
*/
        motoristaRepository.save(motorista);
        return "Motorista criado com sucesso!"
    }




}
