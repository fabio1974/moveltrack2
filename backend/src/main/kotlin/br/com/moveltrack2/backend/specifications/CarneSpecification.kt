package br.com.moveltrack2.backend.specifications

import br.com.moveltrack.persistence.domain.Carne
import java.util.ArrayList

import javax.persistence.criteria.Predicate

import br.com.moveltrack.persistence.domain.Contrato
import br.com.moveltrack.persistence.repositories.ContratoRepository
import org.springframework.data.jpa.domain.Specification


object CarneSpecification {


    fun getFiltro(filtro: Carne, repo: ContratoRepository): Specification<Carne> {
        return Specification { root, query, builder ->
            val predicates = ArrayList<Predicate>()

            if (filtro.contrato?.cliente?.id != null) {
                val contrato = repo.findByCliente(filtro.contrato!!.cliente!!)
                predicates.add(builder.equal(root.get<Contrato>("contrato"), contrato))
            }

            builder.and(*predicates.toTypedArray())
        }
    }


}
