package br.com.moveltrack2.backend.Iugu.model

import com.fasterxml.jackson.annotation.JsonProperty

class Item(var description: String?, var quantity: Int?, priceCents: Int?) {

    @JsonProperty("price_cents")
    var priceCents: Int? = null
        private set

    init {
        this.priceCents = priceCents
    }

    fun setPrice_cents(priceCents: Int?) {
        this.priceCents = priceCents
    }

}
