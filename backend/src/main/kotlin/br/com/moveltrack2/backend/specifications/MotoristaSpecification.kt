package br.com.moveltrack2.backend.specifications

import br.com.moveltrack.persistence.domain.Cliente
import br.com.moveltrack.persistence.domain.Motorista
import java.util.ArrayList

import javax.persistence.criteria.Predicate

import org.springframework.data.jpa.domain.Specification
import org.springframework.util.StringUtils


object MotoristaSpecification {


    fun getFiltro(filtro: Motorista): Specification<Motorista> {
        return Specification { root, query, builder ->
            val predicates = ArrayList<Predicate>()


            if (filtro.patrao != null) {
                predicates.add(builder.equal(root.get<Cliente>("patrao").get<Int>("id"),filtro.patrao?.id))
            }


            if (!StringUtils.isEmpty(filtro.nome)) {
                val d = "%" + filtro.nome!!.trim { it <= ' ' } + "%"
                predicates.add(builder.like(root.get("nome"), d))
            }

            if (!StringUtils.isEmpty(filtro.status)) {
                predicates.add(builder.equal(root.get<String>("status"),filtro.status))
            }


            builder.and(*predicates.toTypedArray())
        }
    }


}
