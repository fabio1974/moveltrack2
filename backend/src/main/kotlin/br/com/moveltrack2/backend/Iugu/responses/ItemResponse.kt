package br.com.moveltrack2.backend.Iugu.responses

import java.util.Date

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty


@JsonIgnoreProperties(ignoreUnknown = true)
class ItemResponse {

    var id: String? = null

    var description: String? = null

    var quantity: Int? = null

    @JsonProperty("price_cents")
    var priceCents: Int? = null

    @JsonProperty("created_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    var createdAt: Date? = null

    @JsonProperty("updated_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    var updatedAt: Date? = null

    var price: String? = null


}
