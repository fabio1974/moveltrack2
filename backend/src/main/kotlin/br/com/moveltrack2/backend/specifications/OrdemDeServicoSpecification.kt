package br.com.moveltrack2.backend.specifications

import br.com.moveltrack.persistence.domain.Cliente
import br.com.moveltrack.persistence.domain.OrdemDeServico
import br.com.moveltrack.persistence.domain.OrdemDeServicoStatus
import br.com.moveltrack.persistence.repositories.OrdemDeServicoRepository
import org.springframework.data.jpa.domain.Specification
import org.springframework.util.StringUtils
import java.util.*
import javax.persistence.criteria.Predicate


object OrdemDeServicoSpecification {


    fun getFiltro(filtro: OrdemDeServico, repo: OrdemDeServicoRepository): Specification<OrdemDeServico> {
        return Specification { root, query, builder ->
            val predicates = ArrayList<Predicate>()

            if (!StringUtils.isEmpty(filtro.numero)) {
                val d = "%" + filtro.numero!!.trim { it <= ' ' } + "%"
                predicates.add(builder.like(root.get("numero"), d))
            }

            if (filtro.cliente!=null) {
                predicates.add(builder.equal(root.get<Cliente>("cliente"), filtro.cliente))
            }
            if (filtro.status!=null) {
                predicates.add(builder.equal(root.get<OrdemDeServicoStatus>("status"), filtro.status))
            }
            builder.and(*predicates.toTypedArray())
        }
    }


}
