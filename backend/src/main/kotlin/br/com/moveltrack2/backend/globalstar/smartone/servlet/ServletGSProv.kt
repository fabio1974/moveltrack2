package br.com.moveltrack2.backend.globalstar.smartone.servlet

//import globalstar.GSServlet
import br.com.moveltrack.persistence.domain.Location
import br.com.moveltrack.persistence.domain.Location2
import br.com.moveltrack.persistence.repositories.Location2Repository
import br.com.moveltrack.persistence.repositories.LocationRepository
import br.com.moveltrack2.backend.globalstar.smartone.model.PrvResponseMsg
import br.com.moveltrack2.backend.globalstar.smartone.model.Prvmsgs
import br.com.moveltrack2.backend.globalstar.smartone.model.StuMessages
import br.com.moveltrack2.backend.globalstar.smartone.model.StuResponseMsg
import br.com.moveltrack2.backend.utils.GeoDistanceCalulator
import globalstar.Message
import globalstar.MessageList
import java.io.BufferedReader
import javax.servlet.ServletException
import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import java.io.IOException
import java.io.StringReader
import java.time.temporal.ChronoUnit
import java.util.*
import javax.xml.bind.JAXB

@WebServlet(name = "ServletGSProv", value = ["/gsprov"])
class ServletGSProv(
        val locationRepository: LocationRepository,
        val location2Repository: Location2Repository
) : HttpServlet() {
    @Throws(ServletException::class, IOException::class)
    override fun doPost(request: HttpServletRequest, response: HttpServletResponse) {

        val reader = BufferedReader(request.reader)
        val buffer = StringBuffer()

        reader.forEachLine {
            buffer.append(it)
        }

        val messages = JAXB.unmarshal(StringReader(buffer.toString()), Prvmsgs::class.java)
        messages.prvmsg?.forEach {
            println(it.esn)
        }

        val uuid = UUID.randomUUID().toString()
        val prvResponseMsg = PrvResponseMsg(correlationID = uuid, messageID = uuid )

        response.setContentType("text/xml");
        JAXB.marshal(prvResponseMsg, response.getOutputStream());
    }


    fun saveMessage(message: Message) {

        val location = Location()
        location.imei = message.esn.replace("-".toRegex(),"").padStart(15,'0')
        location.latitude = message.latitude.toDouble()
        location.longitude = message.longitude.toDouble()
        location.dateLocationInicio = message.timestamp.toGregorianCalendar().toZonedDateTime()
        location.dateLocation = location.dateLocationInicio
        location.comando = message.messageType
        location.battery = message.batteryState
        location.velocidade = getGSSpeed(location)

        if (location.velocidade >= 0  &&  location.velocidade < 200 && location.latitude > -999) {

            locationRepository.save(location)

            val location2 = Location2()
            location2.imei = location.imei
            location2.latitude = location.latitude
            location2.longitude = location.longitude
            location2.dateLocationInicio = location.dateLocationInicio
            location2.dateLocation = location.dateLocation
            location2.comando = location.comando
            location2.battery = location.battery
            location2.velocidade = location.velocidade
            location2Repository.save(location2)
        }
    }


    fun getGSSpeed(current: Location): Double {

        val next = locationRepository.findFirstByImeiAndDateLocationAfterOrderByDateLocation(current.imei!!,current.dateLocation)
        if(next!=null){
            val dt = ChronoUnit.SECONDS.between(current.dateLocation,next.dateLocation);
            if(dt < 120) {
                //println("${Date()} -- imei: ${current.imei} --  current: ${current.dateLocation} -- next:${next.dateLocation} -- dt:$dt")
                locationRepository.delete(next)
                try {
                    location2Repository.deleteByImeiAndDateLocation(next.imei, next.dateLocation)
                }catch(e: Exception){
                    println(e.message)
                }
            }
        }

        val previous = locationRepository.findFirstByImeiAndDateLocationLessThanEqualOrderByDateLocationDesc(current.imei!!,current.dateLocation)
        if(previous!=null) {
            val dt = ChronoUnit.SECONDS.between(previous.dateLocation,current.dateLocation);
            if(dt < 120) {
                //println("${Date()} -- imei: ${current.imei} --  current: ${current.dateLocation} -- previous:${previous.dateLocation} -- dt:$dt")
                return -1.toDouble()
            }
            val ds = GeoDistanceCalulator.vicentDistance(previous, current)
            val speed = (ds / 1000) / (dt.toDouble() / 3600)
            return speed
        }else
            return 0.0
    }


}



