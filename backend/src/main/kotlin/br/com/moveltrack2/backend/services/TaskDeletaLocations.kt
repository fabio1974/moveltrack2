package br.com.moveltrack2.backend.services

import br.com.moveltrack.persistence.repositories.*
import br.com.moveltrack2.backend.TIME_ZONE_STR
import br.com.moveltrack2.backend.controllers.TIME_ZONE
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit
import java.util.*

@Component
@EnableScheduling
class TaskDeletaLocations(
        val locationRepository: LocationRepository,
        val location2Repository: Location2Repository
) {

    @Transactional(timeout = 600)
    @Scheduled(cron = "0 0 3 * * ?",zone = TIME_ZONE_STR) //todos os dias às 03:00 da manha
    fun deletaLocations(){
        val d = Date()
        val limite = ZonedDateTime.now().minusYears(1)
        locationRepository.deleteAllByDateLocationBefore(limite)
        println("Deletando registro de Locations em ${Date().time - d.time} milliseconds...")
    }

    @Transactional(timeout = 600)
    @Scheduled(cron = "0 30 2 * * ?",zone = TIME_ZONE_STR) //todos os dias às 02:30 da manha
    fun deletaLocations2(){
        val limite = ZonedDateTime.now(TIME_ZONE).minusDays(1).truncatedTo(ChronoUnit.DAYS)
        val count = location2Repository.deleteAllByDateLocationBefore(limite)
        println("Deletando $count registro de Locations2")
    }



}




