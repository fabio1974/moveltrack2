package br.com.moveltrack2.backend.specifications

import br.com.moveltrack.persistence.domain.Cliente
import br.com.moveltrack.persistence.domain.Contrato
import org.springframework.data.jpa.domain.Specification
import org.springframework.util.ObjectUtils.isEmpty
import java.util.*
import javax.persistence.criteria.Predicate


object ContratoSpecification {


    fun getFiltro(filtro: Contrato): Specification<Contrato> {
        return Specification { root, query, builder ->
            val predicates = ArrayList<Predicate>()

            if (!isEmpty(filtro.contratoGeraCarne)) {
                predicates.add(builder.equal(root.get<String>("contratoGeraCarne"),filtro.contratoGeraCarne))
            }

            if (!isEmpty(filtro?.cliente?.id)) {
                predicates.add(builder.equal(root.get<Cliente>("cliente").get<Int>("id"),filtro.cliente!!.id))
            }


            if (!isEmpty(filtro.status)) {
                predicates.add(builder.equal(root.get<String>("status"),filtro.status))
            }


            builder.and(*predicates.toTypedArray())
        }
    }


}
