package br.com.moveltrack2.backend.seguranca

import java.io.IOException

import javax.servlet.Filter
import javax.servlet.FilterChain
import javax.servlet.FilterConfig
import javax.servlet.ServletException
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
class CorsFilter : Filter {

    //private val originPermitida = "http://localhost:4200" // TODO: desenvolvimento
    //private String originPermitida = "http://138.197.22.183"; //\producao

    @Throws(IOException::class, ServletException::class)
    override fun doFilter(req: ServletRequest, resp: ServletResponse, chain: FilterChain) {

        val request = req as HttpServletRequest
        val response = resp as HttpServletResponse


        response.setHeader("Access-Control-Allow-Origin", "*")
        response.setHeader("Access-Control-Allow-Credentials", "true")

        if ("OPTIONS" == request.method /*&& originPermitida.equals(request.getHeader("Origin"))*/) {
            response.setHeader("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, OPTIONS, PATCH")
            response.setHeader("Access-Control-Allow-Headers", "Authorization, Content-Type, Accept")
            response.setHeader("Access-Control-Max-Age", "3600")
            response.status = HttpServletResponse.SC_OK
        } else {
            chain.doFilter(req, resp)
        }

    }

    override fun destroy() {}

    @Throws(ServletException::class)
    override fun init(arg0: FilterConfig?) {
    }

}
