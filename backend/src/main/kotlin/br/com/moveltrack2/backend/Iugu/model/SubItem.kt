package br.com.moveltrack2.backend.Iugu.model

import com.fasterxml.jackson.annotation.JsonProperty

class SubItem(val description: String, val quantity: Int?, @field:JsonProperty("price_cents")
val priceCents: Int?) {

    var isRecurrent: Boolean = false

    override fun toString(): String {
        return "SubItem{" +
                "description='" + description + '\''.toString() +
                ", quantity=" + quantity +
                ", priceCents=" + priceCents +
                ", recurrent=" + isRecurrent +
                '}'.toString()
    }
}
