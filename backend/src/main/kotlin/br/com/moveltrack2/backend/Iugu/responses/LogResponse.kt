package br.com.moveltrack2.backend.Iugu.responses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
class LogResponse {

    var id: String? = null

    var description: String? = null

    var notes: String? = null

    @JsonProperty("created_at")
    var createdAt: String? = null

    override fun toString(): String {
        return "LogResponse{" +
                "id='" + id + '\''.toString() +
                ", description='" + description + '\''.toString() +
                ", notes='" + notes + '\''.toString() +
                ", createdAt='" + createdAt + '\''.toString() +
                '}'.toString()
    }
}