package br.com.moveltrack2.backend.Iugu.responses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
class PriceResponse {

    var id: String? = null

    var currency: String? = null

    @JsonProperty("plan_id")
    var planId: String? = null

    @JsonProperty("value_cents")
    var valueCents: Int? = null

}