package br.com.moveltrack2.backend.controllers

import br.com.moveltrack2.backend.Iugu.IuguUtils
import br.com.moveltrack.persistence.domain.*
import br.com.moveltrack.persistence.repositories.*
import br.com.moveltrack2.backend.repositories.NativeRepositoryImpl
import br.com.moveltrack2.backend.services.TaskDistanciaDiaria
import org.springframework.data.rest.webmvc.BasePathAwareController
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController

@RestController
@BasePathAwareController
class SearchController(
        val equipamentoRepositoryMy: EquipamentoRepository,
        val chipRepository: ChipRepository,
        val empregadoRepository: EmpregadoRepository,
        val veiculoRepository: VeiculoRepository,
        val contratoRepository: ContratoRepository,
        val iuguRepository: IuguRepository,
        val clienteRepository: ClienteRepository,
        val municipioRepositoryMy: MunicipioRepositoryMy,
        val viagemRepository: ViagemRepository,
        val motoristaRepository: MotoristaRepository,
        val iuguUtils: IuguUtils,
        val nativeRepo:  NativeRepositoryImpl,
        val perfilRepository: PerfilRepository,
        val taskDistanciaDiaria: TaskDistanciaDiaria

) {


    @ResponseBody
    @GetMapping("/searchClientes")
    fun searchClientes(@RequestParam(value = "nome",required=false) nome:String?=null, required:Boolean=false): MutableList<Cliente> {
        return this.clienteRepository.findByNomeContaining(nome)
    }


    @ResponseBody
    @GetMapping("/searchEquipamentos")
    fun searchRastreadores(@RequestParam(value = "imei",required=false) imei:String?=null, required:Boolean=false): List<Equipamento> {
        return this.equipamentoRepositoryMy.findAllByImeiContaining(imei).map{
            it.label = "${it.imei}-${it.modelo?.name}"
            it
        }
    }


    @ResponseBody
    @GetMapping("/searchChips")
    fun searchChips(@RequestParam(value = "iccid",required=false) iccid:String?=null, required:Boolean=false): List<Chip> {
        return this.chipRepository.findByIccidContaining(iccid).map {
            it.label = "${it.iccid}-${it.operadora}"
            it
        }
    }


    @ResponseBody
    @GetMapping("/searchMunicipios")
    fun searchMunicipios(@RequestParam(value = "nome",required=false) nome:String?=null, required:Boolean=false): MutableList<Municipio> {
        return this.municipioRepositoryMy.findByDescricaoContaining(nome)
    }

    @ResponseBody
    @GetMapping("/searchContratos")
    fun searchContratos(@RequestParam(value = "nome",required=false) nome:String, required:Boolean=false): List<Contrato> {
        return nativeRepo.getAllContratosByClienteNomeContaining(nome)
    }

    @ResponseBody
    @GetMapping("/searchEmpregados")
    fun searchEmpregados(@RequestParam(value = "nome",required=false) nome:String, required:Boolean=false): List<Empregado> {
        return empregadoRepository.findAllByNomeContaining(nome)
    }

    @ResponseBody
    @GetMapping("/searchVeiculos")
    fun searchVeiculos(@RequestParam(value = "placa",required=false) placa:String, required:Boolean=false): List<Veiculo> {
        return veiculoRepository.findAllByPlacaContaining(placa)
    }

    @ResponseBody
    @GetMapping("/searchMotoristas")
    fun searchMotoristas(@RequestParam(value = "nome",required=false) nome:String, required:Boolean=false): List<Motorista> {
        return motoristaRepository.findAllByNomeContaining(nome)
    }


    @ResponseBody
    @GetMapping("/searchViagens")
    fun searchViagens(@RequestParam(value = "numeroViagem",required=false) numeroViagem:Int, required:Boolean=false): List<Viagem> {
        return nativeRepo.searchViagemByNumeroViagemContainig(numeroViagem)
    }


    @ResponseBody
    @GetMapping("/perfis")
    fun searchPerfis(): List<Perfil> {
        return perfilRepository.findAll()
    }


    @ResponseBody
    @GetMapping("/teste")
    fun teste(){
        taskDistanciaDiaria.corrigeDistanciaDiaria()
    }



}
