package br.com.moveltrack2.backend.Iugu.responses

import java.util.Date

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty


@JsonIgnoreProperties(ignoreUnknown = true)
class SubscriptionResponse {

    var id: String? = null

    var suspended: Boolean? = null

    @JsonProperty("plan_identifier")
    var planIdentifier: String? = null

    @JsonProperty("price_cents")
    var priceCents: Int? = null

    var currency: String? = null

    //TODO Features
    @JsonProperty("expires_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
    var expiresAt: Date? = null

    @JsonProperty("customer_name")
    var customerName: String? = null

    @JsonProperty("customer_email")
    var customerEmail: String? = null

    @JsonProperty("cycled_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
    var cycledAt: Date? = null

    @JsonProperty("credits_min")
    var creditsMin: Int? = null

    //TODO Credits Cycle

    @JsonProperty("customer_id")
    var customerId: String? = null

    @JsonProperty("plan_name")
    var planName: String? = null

    @JsonProperty("customer_ref")
    var customerRef: String? = null

    @JsonProperty("plan_ref")
    var planRef: String? = null

    var active: Boolean? = null

    @JsonProperty("in_trial")
    var inTrial: Boolean? = null

    var credits: Int? = null

    @JsonProperty("credits_based")
    var creditsBased: Boolean? = null

    //TODO Recent Invoices

    var subitems: List<SubItemResponse>? = null

    var logs: List<LogResponse>? = null

    @JsonProperty("custom_variables")
    var customVariables: List<CustomVariableResponse>? = null

    override fun toString(): String {
        return "SubscriptionResponse{" +
                "id='" + id + '\''.toString() +
                ", suspended=" + suspended +
                ", planIdentifier='" + planIdentifier + '\''.toString() +
                ", priceCents=" + priceCents +
                ", currency='" + currency + '\''.toString() +
                ", expiresAt=" + expiresAt +
                ", customerName='" + customerName + '\''.toString() +
                ", customerEmail='" + customerEmail + '\''.toString() +
                ", cycledAt=" + cycledAt +
                ", creditsMin=" + creditsMin +
                ", customerId='" + customerId + '\''.toString() +
                ", planName='" + planName + '\''.toString() +
                ", customerRef='" + customerRef + '\''.toString() +
                ", planRef='" + planRef + '\''.toString() +
                ", active=" + active +
                ", inTrial=" + inTrial +
                ", credits=" + credits +
                ", creditsBased=" + creditsBased +
                ", subitems=" + subitems +
                ", logs=" + logs +
                ", customVariables=" + customVariables +
                '}'.toString()
    }
}