package br.com.moveltrack2.backend.seguranca

import org.springframework.context.annotation.Bean
import org.springframework.security.crypto.password.PasswordEncoder

import java.math.BigInteger
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class Md5 {

    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return object : PasswordEncoder {
            override fun encode(charSequence: CharSequence): String? {
                return getMd5(charSequence.toString())
            }

            override fun matches(charSequence: CharSequence, s: String): Boolean {
                return getMd5(charSequence.toString()) == s
            }
        }
    }

    companion object {

        fun encode(charSequence: CharSequence): String? {
            return getMd5(charSequence.toString())
        }

        fun matches(charSequence: CharSequence, s: String): Boolean {
            return getMd5(charSequence.toString()) == s
        }

        fun getMd5(input: String): String? {
            try {
                // Static getInstance method is called with hashing SHA
                val md = MessageDigest.getInstance("MD5")

                // digest() method called
                // to calculate message digest of an input
                // and return array of byte
                val messageDigest = md.digest(input.toByteArray())

                // Convert byte array into signum representation
                val no = BigInteger(1, messageDigest)

                // Convert message digest into hex value
                var hashtext = no.toString(16)

                while (hashtext.length < 32) {
                    hashtext = "0$hashtext"
                }

                return hashtext

            } catch (e: NoSuchAlgorithmException) {
                println("Exception thrown"
                        + " for incorrect algorithm: " + e)
                return null
            }
            // For specifying wrong message digest algorithms
        }
    }


}
