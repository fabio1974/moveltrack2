package br.com.moveltrack2.backend.globalstar.smartone.servlet


import br.com.moveltrack.persistence.repositories.Location2Repository
import br.com.moveltrack.persistence.repositories.LocationRepository
import br.com.moveltrack2.backend.globalstar.smartone.model.StuMessages
import br.com.moveltrack2.backend.globalstar.smartone.model.StuResponseMsg
import java.io.BufferedReader
import java.io.IOException
import java.io.StringReader
import java.util.*
import javax.servlet.ServletException
import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.xml.bind.JAXB

@WebServlet(name = "ServletGSSOne", value = ["/gssone"])
class ServletGSSOne(
        val locationRepository: LocationRepository,
        val location2Repository: Location2Repository
) : HttpServlet() {
    @Throws(ServletException::class, IOException::class)
    override fun doPost(request: HttpServletRequest, response: HttpServletResponse) {

        val reader = BufferedReader(request.reader)
        val buffer = StringBuffer()

        reader.forEachLine {
            buffer.append(it)
        }

        val messages = JAXB.unmarshal(StringReader(buffer.toString()), StuMessages::class.java)
        messages.stuMessage?.forEach {
            println(it.payload)
        }

        val uuid = UUID.randomUUID().toString()
        val stuResponseMsg = StuResponseMsg(correlationID = uuid, messageID = uuid )

        response.setContentType("text/xml");
        JAXB.marshal(stuResponseMsg, response.getOutputStream());
    }





}



