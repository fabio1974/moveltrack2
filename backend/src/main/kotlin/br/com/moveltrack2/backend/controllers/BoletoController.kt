package br.com.moveltrack2.backend.controllers

import br.com.moveltrack2.backend.Iugu.IuguUtils
import br.com.moveltrack2.backend.Iugu.model.Event
import br.com.moveltrack2.backend.Iugu.model.Trigger
import br.com.moveltrack.persistence.domain.*
import br.com.moveltrack.persistence.repositories.*
import br.com.moveltrack.persistence.util.BoletoUtils
import br.com.moveltrack2.backend.specifications.BoletoSpecification
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.rest.webmvc.BasePathAwareController
import org.springframework.http.MediaType
import org.springframework.util.StringUtils.isEmpty
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletResponse
import org.springframework.web.bind.annotation.ResponseBody
import java.time.ZoneId
import java.time.ZonedDateTime


@RestController
@BasePathAwareController
class BoletoController(
        val pessoaRepository: PessoaRepository,
        val mBoletoRepository: MBoletoRepository,
        val contratoRepository: ContratoRepository,
        val iuguRepository: IuguRepository,
        val iuguUtils: IuguUtils
) {

    @ResponseBody
    @PostMapping("/boletos")
    fun create(@RequestBody boleto: MBoleto):Any{

        val nossoNumero = this.mBoletoRepository.getProximoNossoNumero(TipoDeCobranca.COM_REGISTRO)
        boleto.nossoNumero = nossoNumero
        boleto.multa = 5.toDouble()
        boleto.juros = 1.toDouble()

        var iugu: Iugu? = iuguUtils.createIuguInvoice(boleto)
        if(!isEmpty(iugu!!.invoiceId))
            boleto.iugu = this.iuguRepository.save(iugu!!)
        mBoletoRepository.save(boleto)
        return boleto;
    }


    @ResponseBody
    @PatchMapping("/boletos")
    fun patch(@RequestParam(value = "pageIndex") pageIndex:Int,
              @RequestParam(value = "pageSize") pageSize:Int,
              @RequestParam(value = "sortField", defaultValue = "id") sortField:String?=null,
              @RequestParam(value = "sortDirection",defaultValue = "DESC") sortDirection: Sort.Direction ,
              @RequestBody filtro: MBoleto): Page<MBoleto> {

        val especification = BoletoSpecification.getFiltro(filtro, contratoRepository)
        val pageRequest = PageRequest.of(pageIndex,pageSize,sortDirection,sortField)
        val list: Page<MBoleto> = mBoletoRepository.findAll(especification,pageRequest)

        return list
    }

    @ResponseBody
    @PutMapping("/boletos/{id}")
    fun update(@PathVariable("id")id:Int,@RequestBody boleto: MBoleto):Any?{
        var boletoAux = this.mBoletoRepository.findById(id).get()!!
        boletoAux.situacao = boleto.situacao
        boletoAux.dataRegistroPagamento = boleto.dataRegistroPagamento
        this.mBoletoRepository.save(boletoAux)
        if(boleto.situacao == MBoletoStatus.CANCELADO)
            iuguUtils.cancelIuguInvoice(boleto)
        return "ok";
    }


    @GetMapping("/getBoletoPdf/{id}")
    fun  getOpenedEventsInPdf(response : HttpServletResponse, @PathVariable("id")id:Int): ByteArray {
        println("Id do boleto:  $id ")
        return  BoletoUtils.getBoletoIuguInBytes(mBoletoRepository.findById(id).get())
    }




    @PostMapping(value= ["/iugu/baixa"],consumes = [MediaType.APPLICATION_FORM_URLENCODED_VALUE])
    fun baixaFatura(trigger: Trigger?=null):Any{

        println("Entrando na baixa de boletos")
        val invoiceId = trigger?.data?.get("id")
        println("InvoiceId=$invoiceId IuguStatus: ${trigger?.data?.get("status")} for invoiceId = $invoiceId")

        if (trigger?.event == Event.invoice_status_changed.value && (trigger?.data?.get("status") == "paid" || trigger?.data?.get("status")=="partially_paid")) {

            val iugu = iuguRepository.findByInvoiceId(invoiceId)
            val mBoleto: MBoleto? =  mBoletoRepository.findByIugu(iugu)
            println("InvoiceId=$invoiceId com sttaus anterior ${mBoleto?.situacao}")
            mBoleto?.situacao = MBoletoStatus.PAGAMENTO_EFETUADO
            mBoleto?.dataRegistroPagamento = ZonedDateTime.now()

            try{
                val ir = iuguUtils.findById(invoiceId!!)
                mBoleto?.dataPagamento = ZonedDateTime.ofInstant(ir.paidAt?.toInstant(),TIME_ZONE)
            }catch(e:Exception){
                println("ERRO BUSCANDO INVOICE RESPONSE")
            }

            mBoletoRepository.save(mBoleto!!)
            println("InvoiceId=$invoiceId paga em ${mBoleto.dataPagamento}")
            println("InvoiceId=$invoiceId registrada em ${mBoleto.dataRegistroPagamento}")
            println("---")
        }
        return "Ok"
    }






}

val TIME_ZONE = ZoneId.of("America/Fortaleza");
