package br.com.moveltrack2.backend.services

import br.com.moveltrack.persistence.repositories.ConfigRepository
import br.com.moveltrack2.backend.TIME_ZONE_STR
import org.springframework.data.repository.findByIdOrNull
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.stereotype.Component
import org.springframework.scheduling.annotation.Scheduled


@Component
@EnableScheduling
class TaskResetGeocodeCount (val configRepository: ConfigRepository) {


    @Scheduled(cron = "1 0 0 1 * ?",zone = TIME_ZONE_STR)
    fun resetGeocodeCount() {
        var config = configRepository.findByIdOrNull(1)
        //println("GEOCODE COUNT RESET FROM ${config?.geoCodeCount} to  ZERO at ${Date()}")
        config?.geoCodeCount = 0
        configRepository.save(config!!)
    }



}


