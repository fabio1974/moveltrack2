package br.com.moveltrack2.backend.Iugu.responses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class VariableResponse {

    var id: String? = null

    var variable: String? = null

    var value: String? = null

}