package br.com.moveltrack2.backend.services

import br.com.moveltrack.persistence.domain.MBoletoStatus
import br.com.moveltrack.persistence.repositories.IuguRepository
import br.com.moveltrack.persistence.repositories.MBoletoRepository
import br.com.moveltrack2.backend.Iugu.IuguUtils
import br.com.moveltrack2.backend.TIME_ZONE_STR
import br.com.moveltrack2.backend.utils.logs
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.time.ZonedDateTime

@Component
@EnableScheduling
class TaskBoletos(
        val mBoletoRepository: MBoletoRepository,
        val iuguRepository: IuguRepository,
        val iuguUtils: IuguUtils
) {

    @Scheduled(cron = "0 40 0 1/5 * ?",zone = TIME_ZONE_STR) //de 5 em 5 dias às 00:40
    fun cancelaFaturasPagasManualmente(){
        logs(true,"Cancelando Faturas Pagas MAnualmente")
        val inicio = ZonedDateTime.now().minusDays(10)
        val fim = ZonedDateTime.now().minusDays(5)
        val list = mBoletoRepository.findAllBySituacaoAndDataRegistroPagamentoBetweenAndDataPagamentoIsNull(MBoletoStatus.PAGAMENTO_EFETUADO,inicio,fim)
        list?.forEach{
            if(it.iugu?.invoiceId!=null) {
                val ir = iuguUtils.findById(it.iugu?.invoiceId!!)
                if(ir.status!="paid" && ir.status!="partially_paid"){
                    iuguUtils.cancelIuguInvoice(it)
                }
            }
        }
    }

    @Scheduled(cron = "0 10 0 * * ?",zone = TIME_ZONE_STR) //todos os dias 00:10
    fun venceBoletosDoDiaAnterior(){
        logs(true,"Colocando status de boletos vencidos")
        val limite = ZonedDateTime.now().withHour(0).withMinute(0).withMinute(0).withSecond(0).withNano(0)
        mBoletoRepository.findAllBySituacaoAndDataVencimentoBefore(MBoletoStatus.EMITIDO,limite)?.forEach {
            it.situacao = MBoletoStatus.VENCIDO
            mBoletoRepository.save(it)
        }
    }


}
