package br.com.moveltrack2.backend.services

import br.com.moveltrack.persistence.domain.Contrato
import br.com.moveltrack.persistence.domain.ContratoGeraCarne
import br.com.moveltrack.persistence.domain.ContratoStatus
import br.com.moveltrack.persistence.domain.MBoletoTipo
import br.com.moveltrack.persistence.repositories.ContratoRepository
import br.com.moveltrack.persistence.repositories.MBoletoRepository
import br.com.moveltrack2.backend.TIME_ZONE_STR
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.time.ZonedDateTime

@Component
@EnableScheduling
class TaskPendenciasCarnes(
        val contratoRepository: ContratoRepository,
        val mBoletoRepository: MBoletoRepository
) {

    @Scheduled(cron = "0 20 0 * * ?",zone = TIME_ZONE_STR) //todos os dias as 00:20
    fun checaPendencias(){
        val daquiHaUmMes = ZonedDateTime.now().plusMonths(1)
        contratoRepository.findAllByStatusAndContratoGeraCarneNot(ContratoStatus.ATIVO, ContratoGeraCarne.PENDENTE)?.forEach {contrato->
            if (!contrato.pagamentoAnual && contrato.mensalidade > 0) {
                val dataBase = getDataBase(contrato)
                if (dataBase!!.isBefore(daquiHaUmMes)) {
                    contrato.contratoGeraCarne = ContratoGeraCarne.PENDENTE
                    contratoRepository.save(contrato)
                }
            }
        }
    }

    fun getDataBase(contrato: Contrato): ZonedDateTime? {
        val boleto = mBoletoRepository.findTopByTipoAndContratoOrderByDataVencimentoDesc(MBoletoTipo.MENSAL,contrato)
        return if(boleto==null) contrato.inicio else boleto.dataVencimento
    }

}
