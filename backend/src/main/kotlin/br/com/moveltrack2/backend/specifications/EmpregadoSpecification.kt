package br.com.moveltrack2.backend.specifications

import br.com.moveltrack.persistence.domain.Empregado
import br.com.moveltrack.persistence.domain.PessoaStatus
import br.com.moveltrack.persistence.repositories.EmpregadoRepository
import org.springframework.data.jpa.domain.Specification
import org.springframework.util.StringUtils
import java.util.*
import javax.persistence.criteria.Predicate


object EmpregadoSpecification {


    fun getFiltro(filtro: Empregado, repo: EmpregadoRepository): Specification<Empregado> {
        return Specification { root, query, builder ->
            val predicates = ArrayList<Predicate>()
            if (!StringUtils.isEmpty(filtro.nome)) {
                val d = "%" + filtro.nome!!.trim { it <= ' ' } + "%"
                predicates.add(builder.like(root.get("nome"), d))
            }
            if (!StringUtils.isEmpty(filtro.cpf)) {
                val d = "%" + filtro.cpf!!.trim { it <= ' ' } + "%"
                predicates.add(builder.like(root.get("cpf"), d))
            }
            if (filtro.status!=null) {
                predicates.add(builder.equal(root.get<PessoaStatus>("status"), filtro.status))
            }
            builder.and(*predicates.toTypedArray())
        }
    }


}
