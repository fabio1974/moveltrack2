package br.com.moveltrack2.backend.Iugu.responses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
class SubItemResponse {

    var id: String? = null

    var description: String? = null

    var quantity: Int? = null

    @JsonProperty("price_cents")
    var priceCents: Int? = null

    var price: String? = null

    var total: String? = null

}