package br.com.moveltrack2.backend.Iugu.responses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class CustomVariableResponse {

    var id: String? = null

    var name: String? = null

    var value: String? = null

}