package br.com.moveltrack2.backend.Iugu.model


import br.com.moveltrack2.backend.Iugu.enums.Currency
import com.fasterxml.jackson.annotation.JsonProperty


class Price(val currency: Currency, @field:JsonProperty("value_cents")
val valueCents: Int)
