package br.com.moveltrack2.backend.specifications


import br.com.moveltrack.persistence.domain.*
import java.util.ArrayList

import javax.persistence.criteria.Predicate

import br.com.moveltrack.persistence.repositories.ContratoRepository
import org.springframework.data.jpa.domain.Specification


object ViagemSpecification {


    fun getFiltro(filtro: Viagem, repo: ContratoRepository): Specification<Viagem> {
        return Specification { root, query, builder ->
            val predicates = ArrayList<Predicate>()

            if (filtro.cliente?.id != null) {
                predicates.add(builder.equal(root.get<Cliente>("cliente"), filtro.cliente))
            }

            if (filtro.veiculo!=null) {
                predicates.add(builder.equal(root.get<Veiculo>("veiculo"), filtro.veiculo))
            }

            if (filtro.cidadeDestino!=null) {
                predicates.add(builder.equal(root.get<Municipio>("cidadeDestino"), filtro.cidadeDestino))
            }

            if (filtro.numeroViagem!=null) {
                predicates.add(builder.equal(root.get<Int>("numeroViagem"), filtro.numeroViagem))
            }

            if (filtro.motorista!=null) {
                predicates.add(builder.equal(root.get<Motorista>("motorista"), filtro.motorista))
            }

            if (filtro.status!=null) {
                predicates.add(builder.equal(root.get<Motorista>("status"), filtro.status))
            }

            builder.and(*predicates.toTypedArray())
        }
    }


}
