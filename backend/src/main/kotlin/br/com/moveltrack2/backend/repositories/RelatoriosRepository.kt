package br.com.moveltrack2.backend.repositories


import br.com.moveltrack.persistence.domain.*
import org.springframework.stereotype.Component
import java.math.BigDecimal
import java.math.BigInteger
import java.time.ZonedDateTime
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import java.util.ArrayList
import java.util.Calendar
import br.com.moveltrack.persistence.domain.Viagem
import br.com.moveltrack.persistence.pojos.ConsumoPorVeiculo
import br.com.moveltrack.persistence.pojos.LitrosValor
import br.com.moveltrack.persistence.pojos.RelatorioFrota
import br.com.moveltrack.persistence.pojos.RelatorioMensalDespesa
import br.com.moveltrack.persistence.repositories.ViagemRepository


@Component
class RelatoriosRepositoryImpl
constructor(
        @PersistenceContext var em: EntityManager,
        val viagemRepository: ViagemRepository

) {

    fun getRelatorioProdutividadeVeiculo(cliente: Cliente?, inicio: ZonedDateTime?, fim: ZonedDateTime?): MutableList<RelatorioFrota> {
        var list: List<RelatorioFrota> = listOf<RelatorioFrota>()
        val sql = ("select v2.id as id,v2.placa as placa,"
                + " count(*) as qtdViagens,"
                + " sum(v.qtdCidades) as qtdCidades,"
                + " sum(v.qtdClientes) as qtdClientes,"
                + " sum(v.valorDaCarga) as valorDaCarga,"
                + " sum(v.pesoDaCarga) as pesoDaCarga,"
                + " sum(v.distanciaPercorrida) as distanciaPercorrida,"
                + " 0 as litros,"
                + " 0 as kml"
                + " from viagem v "
                + " join veiculo v2 on v.veiculo_id = v2.id"
                + " where"
                + " v.cliente_id=:clienteId and"
                + " v.partida >=:inicio and"
                + " v.partida <=:fim "
                + " group by placa,id  order by qtdViagens desc ")

        val query = em.createNativeQuery(sql)
        query.setParameter("clienteId", cliente!!.id)
        query.setParameter("inicio", inicio)
        query.setParameter("fim", fim)
        list = query.resultList.toList().map { it ->
            var a = it as Array<Any>
            RelatorioFrota(
                    id = a[0] as Int,
                    placa = a[1] as String,
                    qtdViagens = (a[2] as BigInteger).toInt(),
                    qtdCidades = (a[3] as BigDecimal).toInt(),
                    qtdClientes = (a[4] as BigDecimal).toInt(),
                    valorDaCarga = (a[5] as Double),
                    pesoDaCarga = (a[6] as BigDecimal).toInt(),
                    distanciaPercorrida = (a[7] as Double)
            )
        }
        em.close()
        return list.toMutableList<RelatorioFrota>()
    }

    fun getRelatorioConsumoVeiculo(cliente: Cliente?, inicio: ZonedDateTime?, fim: ZonedDateTime?): MutableList<ConsumoPorVeiculo> {
        var list: List<ConsumoPorVeiculo> = listOf<ConsumoPorVeiculo>()
        val sql = ("select v2.id as id,v2.placa as placa,v2.marcaModelo as marcaModelo,"
                + " count(*) as qtdViagens,"
                + " sum(v.distanciaPercorrida) as distanciaPercorrida,"
                + " sum(v.distanciaHodometro) as distanciaHodometro,"
                + " 0 as litros,"
                + " 0 as erro,"
                + " 0 as kml,"
                + " 0 as kml2"
                + " from viagem v "
                + " join veiculo v2 on v.veiculo_id = v2.id"
                + " where"
                + (if (cliente != null) " v.cliente_id=:clienteId and" else " ")
                + " v.partida >=:inicio and"
                + " v.partida <=:fim "
                + " group by placa,id  order by qtdViagens desc ")

        val query = em.createNativeQuery(sql)
        if (cliente != null)
            query.setParameter("clienteId", cliente.id)
        query.setParameter("inicio", inicio)
        query.setParameter("fim", fim)
        list = query.resultList.toList().map { it ->
            var a = it as Array<Any>
            ConsumoPorVeiculo(
                    id = a[0] as Int,
                    placa = a[1] as String,
                    marcaModelo = a[2] as String,
                    qtdViagens = (a[3] as BigInteger).toInt(),
                    distanciaPercorrida = a[4] as Double,
                    distanciaHodometro = a[5] as Double,
                    litros = (a[6] as BigInteger).toDouble(),
                    erro = (a[7] as BigInteger).toDouble(),
                    kml = (a[8] as BigInteger).toDouble(),
                    kml2 = (a[9] as BigInteger).toDouble())
        }
        em.close()
        return list.toMutableList()
    }

    fun getDespesasLitroPorVeiculo(placa: String, inicio: ZonedDateTime?, fim: ZonedDateTime?): LitrosValor {
        val lvResult = LitrosValor()
        var list: List<Any?>
        val sql = (" (select sum(d.litros), sum(d.valor) , v.placa as placa"
                + " from veiculo v join despesafrota as d on d.veiculo_id = v.id"
                + " where placa =:placa"
                + " and d.especie = 'COMBUSTIVEL'"
                + " and d.dataDaDespesa >=:inicio"
                + " and d.dataDaDespesa <=:fim)"
                + " union"
                + " (select sum(d.litros), sum(d.valor), ve.placa as placa"
                + " from viagem vi join despesafrota as d on d.viagem_id = vi.id"
                + " join veiculo as ve on ve.id = vi.veiculo_id"
                + " where placa =:placa"
                + " and d.especie = 'COMBUSTIVEL'"
                + " and vi.partida >=:inicio"
                + " and vi.partida <=:fim)"

                + "order by placa")

        val d: DespesaFrota

        val query = em.createNativeQuery(sql)
        query.setParameter("placa", placa)
        query.setParameter("inicio", inicio)
        query.setParameter("fim", fim)
        list = query.resultList.toList()
        for (obj in list) {
            try {
                lvResult.litros += (obj as Array<Any?>)[0] as Double
            } catch (e: Exception) {
            }
            try {
                lvResult.valor += (obj as Array<Any?>)[1] as Double
            } catch (e: Exception) {
            }
        }
        em.close()
        return lvResult
    }

    fun getRelatorioViegensPorEstado(cliente: Cliente?, uf: String, inicio: ZonedDateTime?, fim: ZonedDateTime?): MutableList<Viagem?>? {
        val sql = ("select *  " +
                " from viagem v join municipio m on v.cidadeDestino_codigo = m.codigo join uf uf on m.uf_id = uf.id" +

                " where uf.sigla  = '${uf}'  " +
                " and v.cliente_id=${cliente!!.id} " +
                " and v.chegadaReal is not null " +
                " and v.partida between :inicio and :fim"
                + " order by v.partida")
        val query = em.createNativeQuery(sql, Viagem::class.java)
        query.setParameter("inicio", inicio)
        query.setParameter("fim", fim)
        val r = query.resultList as MutableList<Viagem?>?
        em.close()
        return r
    }


    fun getRelatorioViegensDoMotorista(cliente: Cliente?, motorista: Motorista?, inicio: ZonedDateTime?, fim: ZonedDateTime?): MutableList<Viagem?>? {

        return viagemRepository.findAllByClienteAndMotoristaAndPartidaBetweenAndChegadaRealNotNullOrderByPartida(cliente, motorista, inicio, fim)

    }

    fun getRelatorioMotorista(cliente: Cliente?, inicio: ZonedDateTime?, fim: ZonedDateTime?): MutableList<RelatorioFrota> {
        var list: List<RelatorioFrota> = listOf<RelatorioFrota>()
        val sql = ("select"

                + " 0 as ano,"
                + " 0 as mes,"
                + "  CAST(null as char) as estado,"
                + "  CAST(null as char) as data,"
                + " p.id as id,"
                + " p.nome as motorista,"
                + " ' ' as destino,"
                + " count(*) as qtdViagens,"
                + " sum((time_to_sec(timediff(v.chegadaReal,v.partida))/(60*60*24))) as diasViagens,"
                + " sum(v.qtdCidades) as qtdCidades,"
                + " sum(v.qtdClientes) as qtdClientes,"
                + " sum(v.valorDaCarga) as valorDaCarga,"
                + " sum(v.pesoDaCarga) as pesoDaCarga,"
                + " sum(v.distanciaHodometro) as distanciaPercorrida,"
                + " 0 as despesaEstiva,"
                + " 0 as despesaTotal,"
                + " 0 as despesaDiaria,"
                + " 0 as despesaCombustivel,"
                + " 0 as litros,"
                + " 0 as kml,"
                + " sum(v.valorDevolucao) as valorDevolucao"

                + " from viagem v join pessoa p on v.motorista_id = p.id"
                + " where"
                + (if (cliente != null) " v.cliente_id=:clienteId and" else " ")
                + " v.partida between :inicio and :fim "
                + " group by p.id order by qtdViagens desc")

        val query = em.createNativeQuery(sql)
        query.setParameter("clienteId", cliente!!.id)
        query.setParameter("inicio", inicio)
        query.setParameter("fim", fim)
        list = query.resultList.toList().map { it ->
            var a = it as Array<Any>
            RelatorioFrota(
                    ano = (a[0] as BigInteger).toInt(),
                    mes = (a[1] as BigInteger).toInt(),
                    //estado = a[2] as String,
                    //data=a[3] as ZonedDateTime,
                    id = a[4] as Int,
                    motorista = a[5] as String,
                    destino = a[6] as String,
                    qtdViagens = (a[7] as BigInteger).toInt(),
                    diasViagens = (a[8] as BigDecimal).toDouble(),
                    qtdCidades = (a[9] as BigDecimal).toInt(),
                    qtdClientes = (a[10] as BigDecimal).toInt(),
                    valorDaCarga = (a[11] as Double),
                    pesoDaCarga = (a[12] as BigDecimal).toInt(),
                    distanciaPercorrida = (a[13] as Double),
                    despesaEstiva = (a[14] as BigInteger).toDouble(),
                    despesaTotal = (a[15] as BigInteger).toDouble(),
                    despesaCombustivel = (a[16] as BigInteger).toDouble(),
                    litros = (a[17] as BigInteger).toDouble(),
                    kml = (a[18] as BigInteger).toDouble(),
                    valorDevolucao = (a[19] as BigInteger).toDouble()
            )
        }
        em.close()
        return list.toMutableList<RelatorioFrota>()
    }


    fun getRelatorioVeiculoParado(cliente: Cliente?, inicio: ZonedDateTime, fim: ZonedDateTime): List<Any?> {
        var list: List<Any?> = ArrayList<Any?>()
        val sql = " select v3.placa, v3.marcaModelo" +
                " from veiculo v3 " +
                " join contrato c on v3.contrato_id = c.id" +
                " where  " +
                (if (cliente != null) " c.cliente_id=:clienteId and" else " ") +
                " v3.status = 'ATIVO' and " +
                " v3.id not in (" +
                "	select v2.id as id" +
                "	from viagem v join veiculo v2 on v.veiculo_id = v2.id" +
                "	where " +
                (if (cliente != null) " v.cliente_id=:clienteId and" else " ") +
                " v.partida between :inicio and :fim"
        " group by id ) order by v3.placa"

        val query = em.createNativeQuery(sql)
        if (cliente != null)
            query.setParameter("clienteId", cliente.id)
        query.setParameter("inicio", inicio)
        query.setParameter("fim", fim)
        val r = query.resultList.toList()
        em.close()
        return r
/*
try {
 list = query.resultList.toList()
 for (obj in list!!) {
     val v = Veiculo()
     v.placa = obj[0] as String
     v.marcaModelo = obj[1] as String
     r.add(v)
 }
} catch (e: Exception) {
 e.printStackTrace()
 return null
}
return r
*/
    }

    fun getRelatorioFrequencia(cliente: Cliente?, motorista: Motorista, inicio: ZonedDateTime, fim: ZonedDateTime): List<Any> {

        val sql = ("select v from Viagem v where v.motorista.id =" + motorista.id + " and "
                + (if (cliente != null) " v.cliente.id=:clienteId and" else " ")
                + " v.partida between :inicio and :fim"
                + " order by v.partida")

        val query = em.createQuery(sql, Viagem::class.java)
        if (cliente != null)
            query.setParameter("clienteId", cliente.id)
        query.setParameter("inicio", inicio)
        query.setParameter("fim", fim)
        val r = query.resultList
        em.close()
        return r
    }


    fun getRelatorioDestino(cliente: Cliente?, inicio: ZonedDateTime?, fim: ZonedDateTime?): MutableList<RelatorioFrota> {
        var list: List<RelatorioFrota> = mutableListOf<RelatorioFrota>()
        val sql = ("select"
                + " m.codigo as id,"
                + " 0 as ano,"
                + " 0 as mes,"
                + " CAST(null as char) as data,"
                + " m.descricao as destino,"
                + " uf.sigla as estado,"
                + " count(*) as qtdViagens,"
                + " sum((time_to_sec(timediff(v.chegadaReal,v.partida))/(60*60*24))) as diasViagens,"
                + " sum(v.qtdCidades) as qtdCidades,"
                + " sum(v.qtdClientes) as qtdClientes,"
                + " sum(v.valorDaCarga) as valorDaCarga,"
                + " sum(v.pesoDaCarga) as pesoDaCarga,"
                + " sum(v.distanciaHodometro) as distanciaPercorrida,"
                + " 0 as despesaEstiva,"
                + " 0 as despesaDiaria,"
                + " 0 as despesaTotal,"
                + " 0 as despesaCombustivel,"
                + " '' as motorista, "
                + " 0 as litros,"
                + " 0 as kml,"
                + " sum(v.valorDevolucao) as valorDevolucao"

                + " from viagem v join municipio m on v.cidadeDestino_codigo = m.codigo join uf uf on m.uf_id = uf.id"
                + " where"
                + (if (cliente != null) " v.cliente_id=:clienteId and" else " ")
                + " v.partida between :inicio and :fim"
                + " group by m.codigo order by estado,qtdViagens desc")

        val query = em.createNativeQuery(sql)
        query.setParameter("clienteId", cliente!!.id)
        query.setParameter("inicio", inicio)
        query.setParameter("fim", fim)
        list = query.resultList.map { it ->
            var a = it as Array<Any>
            RelatorioFrota(
                    id = a[0] as Int,
                    //ano=(a[1] as BigInteger).toInt(),
                    //mes=(a[2] as BigInteger).toInt(),
                    //data=a[3] as ZonedDateTime,
                    destino = a[4] as String,
                    estado = a[5] as String,
                    qtdViagens = (a[6] as BigInteger).toInt(),
                    diasViagens = (a[7] as BigDecimal).toDouble(),
                    qtdCidades = (a[8] as BigDecimal).toInt(),
                    qtdClientes = (a[9] as BigDecimal).toInt(),
                    valorDaCarga = (a[10] as Double),
                    pesoDaCarga = (a[11] as BigDecimal).toInt(),
                    distanciaPercorrida = (a[12] as Double),
                    //despesaEstiva = (a[13] as BigInteger).toDouble(),
                    //despesaEstiva = (a[14] as BigInteger).toDouble(),
                    //despesaTotal = (a[15] as BigInteger).toDouble(),
                    //despesaCombustivel = (a[16] as BigInteger).toDouble(),
                    //motorista = a[17] as String,
                    //litros = (a[18] as BigInteger).toDouble(),
                    //kml = (a[19] as BigInteger).toDouble(),
                    valorDevolucao = a[20] as Double
            )
        }
        em.close()
        return list.toMutableList<RelatorioFrota>()
    }


    fun getRelatorioEstado(cliente: Cliente?, inicio: ZonedDateTime?, fim: ZonedDateTime?): MutableList<RelatorioFrota> {
        var list: List<RelatorioFrota> = mutableListOf<RelatorioFrota>()
        val sql = ("select"
                + " uf.id as id,"
                + " 0 as ano,"
                + " 0 as mes,"
                + " CAST(null as char) as data,"
                + " '' as destino,"
                + " uf.sigla as estado,"
                + " count(*) as qtdViagens,"
                + " sum((time_to_sec(timediff(v.chegadaReal,v.partida))/(60*60*24))) as diasViagens,"
                + " sum(v.qtdCidades) as qtdCidades,"
                + " sum(v.qtdClientes) as qtdClientes,"
                + " sum(v.valorDaCarga) as valorDaCarga,"
                + " sum(v.pesoDaCarga) as pesoDaCarga,"
                + " sum(v.distanciaHodometro) as distanciaPercorrida,"
                + " 0 as despesaEstiva,"
                + " 0 as despesaDiaria,"
                + " 0 as despesaTotal,"
                + " 0 as despesaCombustivel,"
                + " '' as motorista, "
                + " 0 as litros,"
                + " 0 as kml,"
                + " sum(v.valorDevolucao) as valorDevolucao"

                + " from viagem v join municipio m on v.cidadeDestino_codigo = m.codigo join uf uf on m.uf_id = uf.id"
                + " where"
                + (if (cliente != null) " v.cliente_id=:clienteId and" else " ")
                + " v.partida >=:inicio and"
                + " v.partida <=:fim "
                + " group by id order by qtdViagens desc")

        val query = em.createNativeQuery(sql)
        query.setParameter("clienteId", cliente!!.id)
        query.setParameter("inicio", inicio)
        query.setParameter("fim", fim)
        list = query.resultList.map { it ->
            var a = it as Array<Any>
            RelatorioFrota(
                    id = a[0] as Int,
                    //ano=(a[1] as BigInteger).toInt(),
                    //mes=(a[2] as BigInteger).toInt(),
                    //data=a[3] as ZonedDateTime,
                    //destino = a[4] as String,
                    estado = a[5] as String,
                    qtdViagens = (a[6] as BigInteger).toInt(),
                    diasViagens = (a[7] as BigDecimal).toDouble(),
                    qtdCidades = (a[8] as BigDecimal).toInt(),
                    qtdClientes = (a[9] as BigDecimal).toInt(),
                    valorDaCarga = (a[10] as Double),
                    pesoDaCarga = (a[11] as BigDecimal).toInt(),
                    distanciaPercorrida = (a[12] as Double),
                    //despesaEstiva = (a[13] as BigInteger).toDouble(),
                    //despesaDiaria = (a[14] as BigInteger).toDouble(),
                    //despesaTotal = (a[15] as BigInteger).toDouble(),
                    //despesaCombustivel = (a[16] as BigInteger).toDouble(),
                    //motorista = a[17] as String,
                    //litros = (a[18] as BigInteger).toDouble(),
                    //kml = (a[19] as BigInteger).toDouble(),
                    valorDevolucao = a[20] as Double
            )
        }
        em.close()
        return list.toMutableList<RelatorioFrota>()
    }


    fun getProdutividadeMensal(cliente: Cliente?, inicio: ZonedDateTime?, fim: ZonedDateTime?): MutableList<RelatorioFrota> {
        val relatoriosFrota = ArrayList<RelatorioFrota>()

        var inicioAux: ZonedDateTime?
        inicioAux = inicio

        while (inicioAux!!.isBefore(fim)) {
            val rf = RelatorioFrota()
            rf.ano = inicioAux.year
            rf.mes = inicioAux.monthValue

            setRelatorioFrota(rf, cliente, rf.ano, rf.mes)

            val c = Calendar.getInstance()
            c.set(Calendar.YEAR, rf.ano)
            c.set(Calendar.MONTH, rf.mes - 1)
            rf.data = c.time

            setDespesaCombustivelELitrosDoMes(rf, cliente, rf.ano, rf.mes)
            setDespesaTotalDoMes(rf, cliente, rf.ano, rf.mes)


            if (rf.distanciaPercorrida > 0 && rf.litros > 0)
                rf.kml = rf.distanciaPercorrida / rf.litros
            else
                rf.kml = 0.toDouble()

            inicioAux = inicioAux.plusMonths(1)
            relatoriosFrota.add(rf)
        }
        return relatoriosFrota
    }


    private fun setDespesaCombustivelELitrosDoMes(rf: RelatorioFrota, cliente: Cliente?, ano: Int, mes: Int) {
        val sql = ("select sum(d.valor),sum(d.litros) from despesafrota d"
                + " join viagem v on d.viagem_id = v.id "
                + " where"
                + (if (cliente != null) " v.cliente_id=:clienteId and" else " ")
                + " month(v.partida)=:mes and year(v.partida)=:ano and "
                + " d.especie=:especie")

        val query = em.createNativeQuery(sql)
        if (cliente != null)
            query.setParameter("clienteId", cliente.id)
        query.setParameter("mes", mes)
        query.setParameter("ano", ano)
        query.setParameter("especie", DespesaFrotaEspecie.COMBUSTIVEL.toString())

        var obj: Array<Any?>? = null
        try {
            obj = query.resultList.toList().get(0) as Array<Any?>
            em.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        try {
            rf.despesaCombustivel = obj!![0] as Double
        } catch (e: Exception) {
        }

        try {
            rf.litros = obj!![1] as Double
        } catch (e: Exception) {
        }

    }


    private fun setDespesaTotalDoMes(rf: RelatorioFrota, cliente: Cliente?, ano: Int?, mes: Int?) {
        val sql = ("select sum(d.valor) from despesafrota d"
                + " join viagem v on d.viagem_id = v.id "
                + " where"
                +  " v.cliente_id=:clienteId and"
                + " month(v.partida)=:mes and year(v.partida)=:ano ")

        val query = em.createNativeQuery(sql)
        query.setParameter("clienteId", cliente?.id)
        query.setParameter("mes", mes)
        query.setParameter("ano", ano)
        try {
            val obj = query.getSingleResult() as Any
            em.close()
            if (obj != null)
                rf.despesaTotal = obj as Double
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    fun setRelatorioFrota(rf: RelatorioFrota, cliente: Cliente?, ano: Int, mes: Int): Unit {
        val sql = ("select"
                + " :data as id,"
                + " 0 as despesaCombustivel,"
                + " '' as estado,"
                + " 0 as despesaEstiva,"
                + " 0 as despesaDiaria,"
                + " 0 as despesaTotal,"
                + " '' as destino,"
                + " '' as motorista,"
                + " '' as data,"
                + " count(*) as qtdViagens,"
                + " sum((time_to_sec(timediff(v.chegadaReal,v.partida))/(60*60*24))) as diasViagens,"
                + " sum(v.qtdCidades) as qtdCidades,"
                + " sum(v.qtdClientes) as qtdClientes,"
                + " sum(v.valorDaCarga) as valorDaCarga,"
                + " sum(v.pesoDaCarga) as pesoDaCarga,"
                + " sum(v.distanciaHodometro) as distanciaPercorrida,"
                + " 0 as litros,"
                + " 0 as ano,"
                + " 0 as mes,"
                + " 0 as kml,"
                + " sum(v.valorDevolucao) as valorDevolucao"

                + " from viagem v "
                + " where"
                + " v.partida is not null and "
                + " v.cliente_id=:clienteId and"
                + " month(v.partida)=:mes and year(v.partida)=:ano"
                + " order by qtdViagens desc")

        val query = em.createNativeQuery(sql)

        query.setParameter("data", Integer.valueOf(ano.toString() + "" + mes))
        query.setParameter("clienteId", cliente!!.id)
        query.setParameter("mes", mes)
        query.setParameter("ano", ano)

        var a = query.singleResult as Array<Any>

        em.close()

        var rfAux = RelatorioFrota(
                // = a[0] as String,
                //ano=(a[1] as BigInteger).toInt(),
                //mes=(a[2] as BigInteger).toInt(),
                //data=a[3] as ZonedDateTime,
                //destino = a[4] as String,
                //estado = a[5] as String,
                qtdViagens = if(a[9]!=null) (a[9] as BigInteger).toInt() else 0,
                diasViagens =if(a[10]!=null) (a[10] as BigDecimal).toDouble() else 0.0,
                qtdCidades = if(a[11]!=null) (a[11] as BigDecimal).toInt() else 0,
                qtdClientes = if(a[12]!=null) (a[12] as BigDecimal).toInt() else 0,
                valorDaCarga =if(a[13]!=null) (a[13] as Double) else 0.0,
                pesoDaCarga = if(a[14]!=null) (a[14] as BigDecimal).toInt() else 0,
                distanciaPercorrida = if(a[15]!=null) (a[15] as Double) else 0.0,
                //despesaEstiva = (a[13] as BigInteger).toDouble(),
                //despesaDiaria = (a[14] as BigInteger).toDouble(),
                //despesaTotal = (a[15] as BigInteger).toDouble(),
                //despesaCombustivel = (a[16] as BigInteger).toDouble(),
                //motorista = a[17] as String,
                //litros = (a[18] as BigInteger).toDouble(),
                //kml = (a[19] as BigInteger).toDouble(),
                valorDevolucao = if(a[20]!=null) (a[20] as Double) else 0.0
        )

        rf.qtdViagens = rfAux.qtdViagens
        rf.diasViagens = rfAux.diasViagens
        rf.qtdCidades = rfAux.qtdCidades
        rf.qtdClientes = rfAux.qtdClientes
        rf.valorDaCarga = rfAux.valorDaCarga
        rf.pesoDaCarga = rfAux.pesoDaCarga
        rf.distanciaPercorrida = rfAux.distanciaPercorrida
        rf.valorDevolucao = rfAux.valorDevolucao
    }


/*    select v.placa, v.marcaModelo, sum(distanciaPercorrida)
    from distanciadiaria d
    inner join veiculo v on d.veiculo_id = v.id
    where d.dataComputada between '2020-01-01' and '2020-01-02'
    and v.placa = 'PSB-1860'
    group by d.veiculo_id*/



    fun getRelatorioDespesaMensal(cliente: Cliente?, inicio: ZonedDateTime?, fim: ZonedDateTime?, numeroViagem: Int?): MutableList<RelatorioMensalDespesa> {

        val relatorioDespesas = mutableListOf<RelatorioMensalDespesa>()

        var inicioAux: ZonedDateTime?
        inicioAux = inicio

        while (inicioAux!!.isBefore(fim)) {
            val rmd = RelatorioMensalDespesa()
            rmd.ano = inicioAux.year//cInicio.get(Calendar.YEAR)
            rmd.mes = inicioAux.monthValue//cInicio.get(Calendar.MONTH) + 1
            setDespesasDoMes(rmd, cliente, rmd.ano, rmd.mes, numeroViagem)
            setCargasDoMes(rmd, cliente, rmd.ano, rmd.mes, numeroViagem)
            inicioAux = inicioAux.plusMonths(1)//.add(Calendar.MONTH, 1)
            relatorioDespesas.add(rmd)
        }
        return relatorioDespesas
    }


    private fun setCargasDoMes(rmd: RelatorioMensalDespesa, cliente: Cliente?, ano: Int, mes: Int, numeroViagem: Int?) {
        val sql = ("select sum(valorDaCarga) from viagem "
                + " where"
                + (if (cliente != null) " cliente_id=:clienteId and" else " ")
                + (if (numeroViagem != null && numeroViagem > 0) " numeroViagem=$numeroViagem and " else " ")
                + " month(partida)=:mes and year(partida)=:ano")

        val query = em.createNativeQuery(sql)
        if (cliente != null)
            query.setParameter("clienteId", cliente.id)
        query.setParameter("mes", mes)
        query.setParameter("ano", ano)
        try {
            var valor = 0.0
            val obj = query.getSingleResult() as Any?
            em.close()
            if (obj != null)
                valor = (obj as Double).toDouble()
            rmd.carga = valor
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    private fun setDespesasDoMes(rmd: RelatorioMensalDespesa, cliente: Cliente?, ano: Int, mes: Int, numeroViagem: Int?) {

        val sql = ("select d.especie,sum(d.valor) from despesafrota d"
                + " join viagem v on d.viagem_id = v.id "
                + " where"
                + (if (cliente != null) " v.cliente_id=:clienteId and" else " ")
                + (if (numeroViagem != null && numeroViagem >0 ) " v.numeroViagem=$numeroViagem and " else " ")
                + " month(v.partida)=:mes and year(v.partida)=:ano"
                + " group by d.especie")

        val query = em.createNativeQuery(sql)
        if (cliente != null)
            query.setParameter("clienteId", cliente.id)
        query.setParameter("mes", mes)
        query.setParameter("ano", ano)
        try {
            val objs = query.resultList as List<Array<Any>>
            em.close()
            if (objs != null)
                for (obj in objs) {
                    val especie = DespesaFrotaEspecie.valueOf(obj[0] as String)
                    val valor = obj[1] as Double
                    when (especie) {
                        DespesaFrotaEspecie.COMBUSTIVEL -> rmd.combustivel = valor
                        DespesaFrotaEspecie.DIARIA -> rmd.diarias = valor
                        DespesaFrotaEspecie.ESTIVA -> rmd.estivas = valor
                        DespesaFrotaEspecie.IPVA -> rmd.ipva = valor
                        DespesaFrotaEspecie.LICENCIAMENTO -> rmd.transito = rmd.transito + valor
                        DespesaFrotaEspecie.MANUTENCAO -> rmd.diarias = valor
                        DespesaFrotaEspecie.MULTA_DE_TRANSITO -> rmd.transito = rmd.transito + valor
                        DespesaFrotaEspecie.OUTROS -> rmd.outras = valor
                        DespesaFrotaEspecie.SEGURO_OBRIGATORIO -> rmd.transito = rmd.transito + valor
                        DespesaFrotaEspecie.TRABALHISTAS -> rmd.trabalhistas = valor
                        else -> {
                        }
                    }
                }

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    fun getConsumoCombustivel(motoristaId: Int?, inicio: ZonedDateTime?, fim: ZonedDateTime?): LitrosValor {
        var obj: Array<Any?>? = null
        val sql = ("select sum(d.litros), sum(d.valor)"
                + " from viagem v join despesafrota as d on d.viagem_id = v.id"
                + " where"
                + " v.motorista_id =:motoristaId and"
                + " d.categoria =:categoria and"
                + " d.especie =:especie and"
                + " v.partida >=:inicio and"
                + " v.partida <=:fim ")

        if (motoristaId == 12)
            println("peguei")

        val query = em.createNativeQuery(sql)
        query.setParameter("categoria", DespesaFrotaCategoria.VIAGEM.name)
        query.setParameter("especie", DespesaFrotaEspecie.COMBUSTIVEL.name)
        query.setParameter("motoristaId", motoristaId)
        query.setParameter("inicio", inicio)
        query.setParameter("fim", fim)
        try {
            obj = query.resultList.toList().get(0) as Array<Any?>
            em.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        val lv = LitrosValor()

        try {
            lv.litros = (obj!![0] as Double).toDouble()
        } catch (e: Exception) {
        }

        try {
            lv.valor = (obj!![1] as Double).toDouble()
        } catch (e: Exception) {
        }

        return lv
    }


    fun getDespesasLitroPorDestino(cidadeDestinoCodigo: Int?, inicio: ZonedDateTime?, fim: ZonedDateTime?): LitrosValor {
        var obj: Array<Any?>? = null
        val sql = ("select sum(d.litros), sum(d.valor)"
                + " from viagem v join despesafrota as d on d.viagem_id = v.id"
                + " where"
                + " v.cidadeDestino_codigo =:cidadeDestinoCodigo and"
                + " d.categoria =:categoria and"
                + " v.partida >=:inicio and"
                + " v.partida <=:fim ")


        val query = em.createNativeQuery(sql)
        query.setParameter("categoria", DespesaFrotaCategoria.VIAGEM.name)
        query.setParameter("cidadeDestinoCodigo", cidadeDestinoCodigo)
        query.setParameter("inicio", inicio)
        query.setParameter("fim", fim)
        try {
            obj = query.resultList.toList().get(0) as Array<Any?>
            em.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        val lv = LitrosValor()

        try {
            lv.litros = (obj!![0] as Double).toDouble()
        } catch (e: Exception) {
        }

        try {
            lv.valor = (obj!![1] as Double).toDouble()
        } catch (e: Exception) {
        }

        return lv
    }


    fun getDespesasLitroPorEstado(ufId: Int?, inicio: ZonedDateTime?, fim: ZonedDateTime?): LitrosValor {
        var obj: Array<Any>? = null
        val sql = ("select sum(d.litros), sum(d.valor)"
                + " from viagem v "
                + " join despesafrota as d on d.viagem_id = v.id"
                + " join municipio m on v.cidadeDestino_codigo = m.codigo join uf uf on m.uf_id = uf.id "
                + " where"
                + " uf.id =:ufId and"
                + " d.categoria =:categoria and"
                + " v.partida >=:inicio and"
                + " v.partida <=:fim ")


        val query = em.createNativeQuery(sql)
        query.setParameter("categoria", DespesaFrotaCategoria.VIAGEM.name)
        query.setParameter("ufId", ufId)
        query.setParameter("inicio", inicio)
        query.setParameter("fim", fim)
        try {
            obj = query.resultList.get(0) as Array<Any>
            em.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        val lv = LitrosValor()

        try {
            lv.litros = (obj!![0] as Double).toDouble()
        } catch (e: Exception) {
        }

        try {
            lv.valor = (obj!![1] as Double).toDouble()
        } catch (e: Exception) {
        }

        return lv
    }


    private fun converToLitrosValor(obj: Array<Any>): LitrosValor {

        return LitrosValor()
    }


    fun getDespesaEstivaMotorista(motoristaId: Int?, inicio: ZonedDateTime?, fim: ZonedDateTime?): Double {
        var valor = 0.0
        val sql = ("select sum(d.valor)"
                + " from viagem v join despesafrota as d on d.viagem_id = v.id"
                + " where"
                + " v.motorista_id =:motoristaId and"
                + " d.categoria =:categoria and"
                + " d.especie =:especie and"
                + " v.partida >=:inicio and"
                + " v.partida <=:fim ")

        val query = em.createNativeQuery(sql)
        query.setParameter("categoria", DespesaFrotaCategoria.VIAGEM.name)
        query.setParameter("especie", DespesaFrotaEspecie.ESTIVA.name)
        query.setParameter("motoristaId", motoristaId)
        query.setParameter("inicio", inicio)
        query.setParameter("fim", fim)
        try {
            valor = (query.getSingleResult() as Double).toDouble()
            em.close()
        } catch (e: Exception) {
           // System.err.println("NÃ£o trouxe estivas :" + e.message)
        }
        return valor
    }


   /* fun getDespesaEstivaViagem(viagemId: Int?): Double {
        var valor = 0.0
        val sql = ("select sum(d.valor)"
                + " from viagem v join despesafrota as d on d.viagem_id = v.id"
                + " where"
                + " d.categoria =:categoria and"
                + " d.especie =:especie and"
                + " v.id =:viagemId")

        val query = em.createNativeQuery(sql)
        query.setParameter("categoria", DespesaFrotaCategoria.VIAGEM.name)
        query.setParameter("especie", DespesaFrotaEspecie.ESTIVA.name)
        query.setParameter("viagemId", viagemId)
        try {
            valor = (query.getSingleResult() as Double).toDouble()
        } catch (e: Exception) {
            println("Nao trouxe estivas para viagem ${viagemId}")
        }
        return valor
    }*/



    fun getDespesaDiariaMotorista(motoristaId: Int?, inicio: ZonedDateTime?, fim: ZonedDateTime?): Double {
        var valor = 0.0
        val sql = ("select sum(d.valor)"
                + " from viagem v join despesafrota as d on d.viagem_id = v.id"
                + " where"
                + " v.motorista_id =:motoristaId and"
                + " d.categoria =:categoria and"
                + " d.especie =:especie and"
                + " v.partida >=:inicio and"
                + " v.partida <=:fim ")

        val query = em.createNativeQuery(sql)
        query.setParameter("categoria", DespesaFrotaCategoria.VIAGEM.name)
        query.setParameter("especie", DespesaFrotaEspecie.DIARIA.name)
        query.setParameter("motoristaId", motoristaId)
        query.setParameter("inicio", inicio)
        query.setParameter("fim", fim)
        try {
            valor = (query.getSingleResult() as Double).toDouble()
            em.close()
        } catch (e: Exception) {
            //System.err.println("NÃ£o trouxe diarias :" + e.message)
        }

        return valor
    }

    fun getDespesaOutrasMotorista(motoristaId: Int?, inicio: ZonedDateTime?, fim: ZonedDateTime?): Double {
        var valor = 0.0
        val sql = ("select sum(d.valor)"
                + " from viagem v join despesafrota as d on d.viagem_id = v.id"
                + " where"
                + " v.motorista_id =:motoristaId and"
                + " d.categoria =:categoria and"
                + " d.especie =:especie and"
                + " v.partida >=:inicio and"
                + " v.partida <=:fim ")

        val query = em.createNativeQuery(sql)
        query.setParameter("categoria", DespesaFrotaCategoria.VIAGEM.name)
        query.setParameter("especie", DespesaFrotaEspecie.OUTROS.name)
        query.setParameter("motoristaId", motoristaId)
        query.setParameter("inicio", inicio)
        query.setParameter("fim", fim)
        try {
            valor = (query.getSingleResult() as Double).toDouble()
            em.close()
        } catch (e: Exception) {
            //System.err.println("NÃ£o trouxe diarias :" + e.message)
        }
        return valor
    }




    /*   fun getDespesaDiariaViagem(viagemId: Int?): Double {
           var valor = 0.0
           val sql = ("select sum(d.valor)"
                   + " from viagem v join despesafrota as d on d.viagem_id = v.id"
                   + " where"
                   + " d.categoria =:categoria and"
                   + " d.especie =:especie and"
                   + " v.id =:viagemId"
                   )

           val query = em.createNativeQuery(sql)
           query.setParameter("categoria", DespesaFrotaCategoria.VIAGEM.name)
           query.setParameter("especie", DespesaFrotaEspecie.DIARIA.name)
           query.setParameter("viagemId", viagemId)
           try {
               valor = (query.getSingleResult() as Double).toDouble()
           } catch (e: Exception) {
              println("Nao trouxe diarias para viagem ${viagemId}")
           }
           return valor
       }*/

    fun getDespesaViagemPorEspecie(viagemId: Int?,despesaFrotaEspecie: String ): Double {
        var valor = 0.0
        val sql = ("select sum(d.valor)"
                + " from viagem v join despesafrota as d on d.viagem_id = v.id"
                + " where"
                + " d.categoria =:categoria and"
                + " d.especie =:especie and"
                + " v.id =:viagemId"
                )

        val query = em.createNativeQuery(sql)
        query.setParameter("categoria", DespesaFrotaCategoria.VIAGEM.name)
        query.setParameter("especie", despesaFrotaEspecie)
        query.setParameter("viagemId", viagemId)
        try {
            valor = (query.getSingleResult() as Double).toDouble()
            em.close()
        } catch (e: Exception) {
           // println("Nao trouxe diarias para viagem ${viagemId}")
        }
        return valor
    }




    fun getRelatorioDistanciaDiaria(cliente: Cliente?, veiculo: Veiculo?, inicio: ZonedDateTime?, fim: ZonedDateTime?, orderBy: String?): MutableList<RelatorioFrota> {
        var list: List<RelatorioFrota> = mutableListOf<RelatorioFrota>()
        val sql = ("select v.placa, v.marcaModelo, sum(distanciaPercorrida) " +
                " from distanciadiaria d" +
                " inner join veiculo v on d.veiculo_id = v.id" +
                " inner join contrato c on v.contrato_id = c.id" +
                " where d.dataComputada between :inicio and :fim" +
                " and v.status = 'ATIVO'" +
                (if(veiculo!=null) " and v.id=:veiculoId " else " ") +
                " and c.cliente_id =:clienteId" +
                " group by d.veiculo_id" +
                (if(orderBy!=null && orderBy=="placa") " order by v.placa " else " order by sum(distanciaPercorrida)  ")
                )

        val query = em.createNativeQuery(sql)
        query.setParameter("clienteId", cliente?.id)
        if(veiculo!=null)
            query.setParameter("veiculoId", veiculo.id)
        query.setParameter("inicio", inicio)
        query.setParameter("fim", fim)


        list = query.resultList.map { it ->
            var a = it as Array<Any>
            RelatorioFrota(
                    placa = a[0] as String,
                    marcaModelo = a[1] as String,
                    distanciaPercorrida = a[2] as Double
            )
        }
        em.close()
        return list.toMutableList<RelatorioFrota>()
    }




}








