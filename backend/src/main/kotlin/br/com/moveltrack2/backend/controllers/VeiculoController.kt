package br.com.moveltrack2.backend.controllers

import br.com.moveltrack.persistence.domain.*
import br.com.moveltrack.persistence.pojos.LastLocation
import br.com.moveltrack.persistence.repositories.*
import br.com.moveltrack2.backend.repositories.NativeRepositoryImpl
import br.com.moveltrack2.backend.specifications.VeiculoSpecification
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.rest.webmvc.BasePathAwareController
import org.springframework.web.bind.annotation.*

@RestController
@BasePathAwareController
class VeiculoController(
        val contratoRepository: ContratoRepository,
        val nativeRepo: NativeRepositoryImpl,
        val veiculoRepository: VeiculoRepository,
        val veiculoRepositoryMy: VeiculoRepository
) {


    @ResponseBody
    @PatchMapping("/veiculos")
    fun patch(@RequestParam(value = "pageIndex") pageIndex:Int,
              @RequestParam(value = "pageSize") pageSize:Int,
              @RequestParam(value = "sortField") sortField:String?=null,
              @RequestParam(value = "sortDirection",defaultValue = "1") sortDirection: Sort.Direction ,
              @RequestBody filtro: Veiculo): Page<Veiculo> {

        val especification = VeiculoSpecification.getFiltro(filtro)
        val pageRequest = PageRequest.of(pageIndex, pageSize, sortDirection,sortField)
        val list: Page<Veiculo> = veiculoRepository.findAll(especification,pageRequest).map{
                    it.equipamento?.label = "${it.equipamento?.imei}-${it.equipamento?.modelo?.name}"
                    it
                }

        return list
    }


    @ResponseBody
    @PostMapping("/veiculos")
    fun create(@RequestBody veiculo: Veiculo): Any {
        try{
            veiculoRepositoryMy.save(veiculo);
            return "Veiculo gerado com sucesso!"
        }catch (e: Exception){
            if(e.message!!.contains("veiculo_UN_placa"))
                throw Exception("Placa já cadastrada!")
            else {
                val v = veiculoRepository.findByEquipamento(veiculo.equipamento!!)
                throw Exception("Veículo de placa ${v?.placa} já está cadastrado com esse rastreador")
            }
        }
    }


    @ResponseBody
    @PostMapping("/findVeiculosByCliente")
    fun findVeiculosByCliente(@RequestBody cliente: Cliente): List<Veiculo>? {
        var contrato = contratoRepository.findByCliente(cliente)
        val veiculos =  veiculoRepositoryMy.findAllByContratoOrderByPlaca(contrato)
        return veiculos
    }

    @ResponseBody
    @PostMapping("/findVeiculosAtivosByCliente")
    fun findVeiculosAtivosByCliente(@RequestBody cliente: Cliente): List<Veiculo>? {
        var contrato = contratoRepository.findByCliente(cliente)
        return veiculoRepositoryMy.findAllByContratoAndStatusOrderByPlaca(contrato,VeiculoStatus.ATIVO)
    }

    @ResponseBody
    @PostMapping("/mapaFrota")
    fun mapaFrota(@RequestBody cliente: Cliente): List<LastLocation>? {
        var contrato = contratoRepository.findByCliente(cliente)
        val r = nativeRepo.getMapaFrotaByContratoId(contrato.id!!,cliente.dataCadastro)
        return r
    }



}
