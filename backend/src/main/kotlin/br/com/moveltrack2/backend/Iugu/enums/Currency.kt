package br.com.moveltrack2.backend.Iugu.enums

import com.fasterxml.jackson.annotation.JsonValue

enum class Currency private constructor(@get:JsonValue
                                        var value: String?) {

    BRL("BRL")

}
