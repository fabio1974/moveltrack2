package br.com.moveltrack2.backend.utils


import java.text.NumberFormat
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.*

fun logs(debug: Boolean, content: String?) {
    if (debug) println("${ZonedDateTime.now(LOCAL_ZONE).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)}: $content")
}

var LOCAL_ZONE = ZoneId.ofOffset("UTC", ZoneOffset.ofHours(-3))

var FORMAT_DATE_GLOBALSTAR = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss 'GMT'")

object Utils {
    fun booleanToString(Value: Boolean): String {
        return java.lang.Boolean.toString(Value)
    }

   /* fun stringToTrigger(gatilho: String): Trigger {
        println(gatilho)
        val trigger = Trigger()
        val s = gatilho.split("&".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        for (linha in s) {
            if (linha.startsWith("event")) {
                var e = linha.substring(linha.indexOf("event=") + 6)
                e = e.replace(".", "_")
                trigger.event = Event.valueOf(e)
                println(trigger.event!!.value)
            }
            if (linha.startsWith("data")) {
                val key = linha.substring(linha.indexOf("data%5B") + 7, linha.indexOf("%5D"))
                val value = linha.substring(linha.indexOf("=") + 1)
                //trigger.data[key] = value
                println("data=$key=>$value;")
            }
        }
        return trigger
    }*/

    fun moeda(valor: Double):String{
        var moeda = NumberFormat.getCurrencyInstance(Locale("pt", "BR"));
        return moeda.format(valor)
    }

    @Throws(java.text.ParseException::class)
    fun formatString(string: String, mask: String): String {
        val mf = javax.swing.text.MaskFormatter(mask)
        mf.valueContainsLiteralCharacters = false
        return mf.valueToString(string)
    }

    fun getTimeDiffInDoubleDays(date1: Date, date2: Date): Double{
        return (date2.time - date1.time) / (1000 * 60 * 60 * 24).toDouble();
    }
}
