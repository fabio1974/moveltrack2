package br.com.moveltrack2.backend.globalstar.smartone.model

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.XmlType


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = ["prvmsg"])
@XmlRootElement(name = "prvmsgs")
class Prvmsgs {

    var prvmsg: List<Prvmsg>? = null

}
