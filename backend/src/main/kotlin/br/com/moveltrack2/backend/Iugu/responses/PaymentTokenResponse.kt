package br.com.moveltrack2.backend.Iugu.responses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
class PaymentTokenResponse {

    @JsonProperty("id")
    var id: String? = null

    @JsonProperty("method")
    var method: String? = null

    @JsonProperty("test")
    var test: Boolean? = null

    override fun toString(): String {
        return "PaymentTokenResponse{" +
                "id='" + id + '\''.toString() +
                ", method='" + method + '\''.toString() +
                ", isTest=" + test +
                '}'.toString()
    }
}
