package br.com.moveltrack2.backend.Iugu.model

import java.io.Serializable

import com.fasterxml.jackson.annotation.JsonProperty


class Payer(
        /**
         * Nome do cliente
         */
        val name: String,
        /**
         * E-Mail do cliente
         */
        val email: String,
        /**
         * cnpj/cpf do cliente
         */
        @field:JsonProperty("cpf_cnpj")
        val cpfCnpj: String,
        /**
         * Prefixo do Telefone do Cliente
         */
        @field:JsonProperty("phone_prefix")
        val phonePrefix: String,
        /**
         * Telefone do cliente
         */
        val phone: String,
        /**
         * Endereço do cliente
         */
        val address: Address?) : Serializable {
    companion object {

        private const val serialVersionUID = 3266886175287194L
    }
}
