package br.com.moveltrack2.backend.Iugu.model

import br.com.moveltrack2.backend.Iugu.enums.Currency
import br.com.moveltrack2.backend.Iugu.enums.IntervalType
import br.com.moveltrack2.backend.Iugu.enums.PayableWith
import com.fasterxml.jackson.annotation.JsonProperty


class Plan(val name: String, val identifier: String, val interval: String, @field:JsonProperty("interval_type")
val intervalType: IntervalType, val currency: Currency,
           @field:JsonProperty("value_cents")
           val valueCents: Int) {

    @JsonProperty("payable_with")
    var payableWith: PayableWith? = null

    var prices: List<Price>? = null

    var features: List<Feature>? = null

}
