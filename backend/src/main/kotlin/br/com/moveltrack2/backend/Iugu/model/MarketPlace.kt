package br.com.moveltrack2.backend.Iugu.model

import com.fasterxml.jackson.annotation.JsonProperty

class MarketPlace {

    var name: String? = null

    @JsonProperty("comission_percent")
    var commissionPercent: Double = 0.toDouble()

}
