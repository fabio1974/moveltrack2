package br.com.moveltrack2.backend.seguranca

import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import java.util.*
import kotlin.collections.ArrayList
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority


object TokenAuthenticationService {

    private val EXPIRATIONTIME: Long = 864000000
    private val SECRET = "MySecreteApp"
    private val TOKEN_PREFIX = "Bearer"
    private val HEADER_STRING = "Authorization"

    fun addAuthentication(response: HttpServletResponse, auth: Authentication) {

        val JWT = Jwts.builder()
                .setSubject((auth.principal as User).nomeUsuario)
                .claim("id",(auth.principal as User).id)
                .claim("nome",(auth.principal as User).nome)
                .claim("cpf",(auth.principal as User).cpf)
                .claim("perfil",(auth.principal as User).perfil?.tipo?.name)
                .claim("roles",auth.authorities.map{it.authority})
                .setExpiration(Date(System.currentTimeMillis() + EXPIRATIONTIME))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact()


        val out = response.getWriter()
        response.setContentType("application/json")
        response.setCharacterEncoding("UTF-8")

        val mapper = ObjectMapper()
        val jsonInString = mapper.writeValueAsString(JWT)

        out.print("{\"token\":${jsonInString}}")
        out.flush()
    }


    fun getAuthentication(request: HttpServletRequest): Authentication? {
        val token = request.getHeader(HEADER_STRING)
        //println("TOKEN ====> $token")
        return if (token != null) {
            getByToken(token)
        } else null
    }


    private fun getByToken(token: String): Authentication? {
        try {

            val jwt = Jwts.parser()
                    .setSigningKey(SECRET)
                    .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                    .body

            var authorities = HashSet<GrantedAuthority>()
            (jwt["roles"] as ArrayList<String>).forEach {
                authorities.add(SimpleGrantedAuthority(it))
            }

            var user =  User(jwt["id"].toString(),jwt["nome"].toString(),jwt.subject,null,jwt["cpf"].toString())
            return if (jwt.subject != null) UsernamePasswordAuthenticationToken(user,null,authorities) else null

        }catch(e: Exception){
            return null;
        }

    }
}
