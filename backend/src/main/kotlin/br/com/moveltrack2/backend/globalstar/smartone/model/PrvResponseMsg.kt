package br.com.moveltrack2.backend.globalstar.smartone.model

import br.com.moveltrack2.backend.utils.FORMAT_DATE_GLOBALSTAR
import java.sql.Timestamp
import java.time.Instant
import java.time.LocalDateTime
import java.util.*
import javax.xml.bind.annotation.*

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StuResponseMsg", propOrder = ["state", "stateMessage"])
class PrvResponseMsg(

        @XmlAttribute(name="xmlns:xsi")
        var xmlnsxsi: String? = "http://www.w3.org/2001/XMLSchema-instance",

        @XmlAttribute(name="xsi:noNamespaceSchemaLocation")
        var xsinoNamespaceSchemaLocation: String? = "http://cody.glpconnect.com/XSD/ProvisionResponse_Rev1_0.xsd",

        @XmlAttribute
        var deliveryTimeStamp: String? =  LocalDateTime.now().format(FORMAT_DATE_GLOBALSTAR),

        @XmlAttribute
        var correlationID: String? = null,

        @XmlAttribute
        var messageID: String? = null,

        @XmlElement(required = true)
        var state: String? = "PASS",

        @XmlElement(required = true)
        var stateMessage: String? = "Store OK"
)
