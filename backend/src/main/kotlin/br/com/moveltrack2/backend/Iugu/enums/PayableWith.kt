package br.com.moveltrack2.backend.Iugu.enums

import com.fasterxml.jackson.annotation.JsonValue

enum class PayableWith private constructor(@get:JsonValue
                                           var value: String?) {

    CREDIT_CARD("credit_card"), ALL("all"), BANK_SLIP("bank_slip")

}
