package br.com.moveltrack2.backend.Iugu.model

import java.io.Serializable
import java.util.ArrayList
import java.util.Arrays
import java.util.Date

import br.com.moveltrack2.backend.Iugu.enums.PayableWith
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty


@JsonIgnoreProperties(ignoreUnknown = true)
class Invoice {
    /**
     * E-Mail do cliente
     */
    var email: String? = null
        private set

    /**
     * Data de Expiração (DD/MM/AAAA)
     */
    @JsonProperty("due_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    var dueDate: Date? = null
        private set

    /**
     * Itens da Fatura
     */
    private val items = ArrayList<Item>()

    /**
     * Payer
     */
    var payer: Payer? = null

    /**
     * Emails de cópia
     */
    @JsonProperty("cc_emails")
    var ccEmails: String? = null
        private set

    /**
     * Cliente é redirecionado para essa URL após efetuar o pagamento da Fatura
     * pela página de Fatura da Iugu
     */
    @JsonProperty("return_url")
    var returnUrl: String? = null
        private set

    /**
     * Cliente é redirecionado para essa URL se a Fatura que estiver acessando
     * estiver expirada
     */
    @JsonProperty("expired_url")
    var expiredUrl: String? = null
        private set

    /**
     * ID do cliente
     */
    @JsonProperty("customer_id")
    var customerId: String? = null
        private set

    /**
     * URL chamada para todas as notificações de Fatura, assim como os webhooks
     * (Gatilhos) são chamados
     */
    @JsonProperty("notification_url")
    var notificationUrl: String? = null
        private set

    /**
     * Valor dos Impostos em centavos
     */
    @JsonProperty("tax_cents")
    var taxCents: Int? = null
        private set

    /**
     * Booleano para Habilitar ou Desabilitar multa por atraso de pagamento
     */
    var fines: Boolean? = null
        private set

    /**
     * Determine a multa a ser cobrada para pagamentos efetuados após a data de
     * vencimento
     */
    @JsonProperty("late_payment_fine")
    var latePaymentFine: Double? = null

    /**
     * Booleano que determina se cobra ou não juros por dia de atraso. 1% ao mês
     * pro rata.
     */
    @JsonProperty("per_day_interest")
    var perDayInterest: Double? = null

    /**
     * Valor dos Descontos em centavos
     */
    @JsonProperty("discount_cents")
    var discountCents: Int? = null

    @JsonProperty("payable_with")
    var payableWith: PayableWith? = null

    constructor() {

    }

    constructor(email: String, dueDate: Date, vararg items: Item) {
        this.email = email
        this.dueDate = dueDate
        this.items.addAll(Arrays.asList(*items)) // FIXME Tratar null pointer
    }

    constructor(email: String, dueDate: Date, Payer: Payer, vararg items: Item) {
        this.email = email
        this.dueDate = dueDate
        this.payer = Payer
        this.items.addAll(Arrays.asList(*items)) // FIXME Tratar null pointer
    }

    fun getItems(): List<Item> {
        return items
    }

    fun withReturnUrl(returnUrl: String): Invoice {
        this.returnUrl = returnUrl
        return this
    }

    fun withExpiredUrl(expiredUrl: String): Invoice {
        this.expiredUrl = expiredUrl
        return this
    }

    fun withCustomerId(customerId: String): Invoice {
        this.customerId = customerId
        return this
    }

    fun withNotificationUrl(notificationUrl: String): Invoice {
        this.notificationUrl = notificationUrl
        return this
    }

    fun withTaxCents(taxCents: Int?): Invoice {
        this.taxCents = taxCents
        return this
    }

    fun withFines(fines: Boolean?): Invoice {
        this.fines = fines
        return this
    }

    fun withCcEmails(ccEmails: String): Invoice {
        this.ccEmails = ccEmails
        return this
    }

}