package br.com.moveltrack2.backend.Iugu.model

import com.fasterxml.jackson.annotation.JsonProperty

class CustomVariable(val name: String, val value: String) {

    @JsonProperty("_destroy")
    var destroy: Boolean? = null

}
