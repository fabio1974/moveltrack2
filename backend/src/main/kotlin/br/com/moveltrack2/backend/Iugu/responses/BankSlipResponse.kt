package br.com.moveltrack2.backend.Iugu.responses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
class BankSlipResponse {

    @JsonProperty("digitable_line")
    var digitableLine: String? = null

    @JsonProperty("barcode_data")
    var barcodeData: String? = null

    var barcode: String? = null

}