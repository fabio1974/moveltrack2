package br.com.moveltrack2.backend.specifications

import br.com.moveltrack.persistence.domain.Empregado
import br.com.moveltrack.persistence.domain.Lancamento
import br.com.moveltrack.persistence.domain.LancamentoStatus
import br.com.moveltrack.persistence.domain.LancamentoTipo
import br.com.moveltrack.persistence.repositories.LancamentoRepository
import org.springframework.data.jpa.domain.Specification
import org.springframework.util.StringUtils
import java.util.*
import javax.persistence.criteria.Predicate


object LancamentoSpecification {


    fun getFiltro(filtro: Lancamento, repo: LancamentoRepository): Specification<Lancamento> {
        return Specification { root, query, builder ->
            val predicates = ArrayList<Predicate>()
            if (!StringUtils.isEmpty(filtro.id)) {
                predicates.add(builder.equal(root.get<Int>("id"), filtro.id))
            }
            if (filtro.status!=null) {
                predicates.add(builder.equal(root.get<LancamentoStatus>("status"), filtro.status))
            }

            if (filtro.operador!=null) {
                predicates.add(builder.equal(root.get<Empregado>("operador"), filtro.operador))
            }

            if (filtro.operacao!=null) {
                predicates.add(builder.equal(root.get<LancamentoTipo>("operacao"), filtro.operacao))
            }



            builder.and(*predicates.toTypedArray())
        }
    }


}
