package br.com.moveltrack2.backend.controllers

import br.com.moveltrack2.backend.Iugu.IuguUtils
import br.com.moveltrack.persistence.domain.*
import br.com.moveltrack.persistence.repositories.*
import br.com.moveltrack2.backend.specifications.ClienteSpecification
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.rest.webmvc.BasePathAwareController
import org.springframework.web.bind.annotation.*

@RestController
@BasePathAwareController
class ClienteController(
        val pessoaRepository: PessoaRepository,
        val usuarioRepository: UsuarioRepository,
        val mBoletoMyRepository: MBoletoRepository,
        val contratoRepository: ContratoRepository,
        val iuguRepository: IuguRepository,
        val clienteRepository: ClienteRepository,
        val iuguUtils: IuguUtils,
        val veiculoRepositoryMy: VeiculoRepository
) {


    @ResponseBody
    @PatchMapping("/clientes")
    fun patch(@RequestParam(value = "pageIndex") pageIndex:Int,
              @RequestParam(value = "pageSize") pageSize:Int,
              @RequestParam(value = "sortField") sortField:String?=null,
              @RequestParam(value = "sortDirection",defaultValue = "1") sortDirection: Sort.Direction ,
              @RequestBody filtro: Cliente): Page<Cliente> {

        val especification = ClienteSpecification.getFiltro(filtro)
        val pageRequest = PageRequest.of(pageIndex, pageSize, sortDirection,sortField)
        val list: Page<Cliente> = clienteRepository.findAll(especification,pageRequest)
        //list.map {buildContrato(it)}
        return list
    }


   /* private fun buildContrato(it: Cliente): Any {
        var contrato = contratoRepositoryMy.findByCliente(it)
        var veiculos: List<Veiculo>  = veiculoRepositoryMy.findAllByContrato(contrato)
        contrato.veiculos = veiculos.map { v -> buildVeiculo(v) }
        contrato.cliente = null
        //it.contrato = contrato
        return it;
    }*/

    private fun buildVeiculo(v: Veiculo): Veiculo {
        v.contrato = null
        return v
    }







}
