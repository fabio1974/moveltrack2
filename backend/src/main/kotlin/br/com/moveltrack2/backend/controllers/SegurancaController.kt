package br.com.moveltrack2.backend.controllers

import br.com.moveltrack.persistence.domain.Chip
import br.com.moveltrack.persistence.domain.Perfil
import br.com.moveltrack.persistence.domain.Permissao
import br.com.moveltrack.persistence.domain.Usuario
import br.com.moveltrack.persistence.repositories.*
import br.com.moveltrack2.backend.seguranca.Criptografia
import br.com.moveltrack2.backend.specifications.ChipSpecification
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.rest.webmvc.BasePathAwareController
import org.springframework.web.bind.annotation.*


@RestController
@BasePathAwareController
class SegurancaController(
        val permissaoRepository: PermissaoRepository,
        val perfilRepository: PerfilRepository,
        val usuarioRepository: UsuarioRepository,
        val pessoaRepository: PessoaRepository
) {




    @ResponseBody
    @PostMapping("/perfil")
    fun create(@RequestBody perfil: Perfil): Any {
        perfilRepository.save(perfil)
        return "Viagem gerada com sucesso!"
    }

    @GetMapping("/permissoes")
    fun getPermissoes(): MutableList<Permissao> {
        return permissaoRepository.findAll()
    }


    @PostMapping("/trocaSenha")
    fun trocaSenha(@RequestBody usuario: Usuario): Any {
        val aux = usuarioRepository.findByNomeUsuario(usuario.nomeUsuario!!)
        aux?.senha = Criptografia().calculaHash(usuario.senha!!)
        usuarioRepository.save(aux!!)
        return "Senha trocada com sucesso"
    }

    @PostMapping("/permissoesUsuario")
    fun permissoesUsuario(@RequestBody usuario: Usuario): Any {
        val aux = usuarioRepository.findByNomeUsuario(usuario.nomeUsuario!!)
        aux?.permissoes = usuario.permissoes
        usuarioRepository.save(aux!!)
        return "Permissoes atualizadas com sucesso"
    }






}
