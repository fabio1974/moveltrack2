package br.com.moveltrack2.backend.controllers

import br.com.moveltrack2.backend.Iugu.IuguException
import br.com.moveltrack.persistence.domain.Viagem
import br.com.moveltrack.persistence.domain.ViagemStatus
import br.com.moveltrack.persistence.repositories.*
import br.com.moveltrack2.backend.specifications.ViagemSpecification
import br.com.moveltrack2.backend.utils.GeoDistanceCalulator
import br.com.moveltrack2.backend.utils.MapaUtils.otimizaPontosDoBanco
import org.hibernate.exception.ConstraintViolationException
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.rest.webmvc.BasePathAwareController
import org.springframework.web.bind.annotation.*
import java.time.ZonedDateTime


@RestController
@BasePathAwareController
class ViagemController(
        val contratoRepository: ContratoRepository,
        val viagemRepository: ViagemRepository,
        val locationRepository: LocationRepository
) {


    @ResponseBody
    @PatchMapping("/viagems")
    fun patch(@RequestParam(value = "pageIndex") pageIndex:Int,
              @RequestParam(value = "pageSize") pageSize:Int,
              @RequestParam(value = "sortField") sortField:String?=null,
              @RequestParam(value = "sortDirection",defaultValue = "DESC") sortDirection: Sort.Direction ,
              @RequestBody filtro: Viagem): Page<Viagem> {

        val especification = ViagemSpecification.getFiltro(filtro, contratoRepository)
        val pageRequest = PageRequest.of(pageIndex,pageSize,sortDirection,sortField)
        val list: Page<Viagem> = viagemRepository.findAll(especification,pageRequest)
        return list.map { rebuildViagem(it) }
    }

    fun rebuildViagem(it:Viagem): Viagem{
        it.cliente?.usuario = null
        it.veiculo?.instalador = null
        it.veiculo?.contrato = null
        it.motorista?.patrao = null
        return it
    }




    @ResponseBody
    @PostMapping("/viagems")
    @Throws(IuguException::class)
    fun create(@RequestBody viagem: Viagem): Any {
        if(viagem.id==null){
            if(viagem.chegadaReal!=null && viagem.chegadaReal!!.isBefore(ZonedDateTime.now()))
                viagem.distanciaPercorrida = getDistanciaDaViagemEncerrada(viagem)
            var aux = viagemRepository.save(viagem);
            viagem.numeroViagem = aux.id
            viagemRepository.save(viagem);
        }else{
            if(viagem.status == ViagemStatus.ENCERRADA)
                viagem.distanciaPercorrida = getDistanciaDaViagemEncerrada(viagem)
            viagemRepository.save(viagem);
        }
        return "Viagem gerada com sucesso!"
    }

    fun getDistanciaDaViagemEncerrada(viagem: Viagem): Double {
        val imei = viagem.veiculo?.equipamento?.imei
        val pontosCrus = locationRepository.findAllByImeiAndDateLocationGreaterThanEqualAndDateLocationLessThanEqualOrderByDateLocationInicio(imei!!, viagem.partida!!, viagem.chegadaReal!!)
        val otimizado = otimizaPontosDoBanco(pontosCrus, viagem.partida!!, viagem.chegadaReal!!, viagem.veiculo?.equipamento, locationRepository)
        return  GeoDistanceCalulator.getPathLength(otimizado)
    }


    @DeleteMapping("/viagems/{id}")
    fun deleteViagem(@PathVariable(value = "id") id:Int): Any{
        try {
            viagemRepository.deleteById(id)
            return "ok"
        }catch (e: ConstraintViolationException){
            throw Exception("Esta viagem não pode ser deletada. Existem outros cadastros atrelados a ela!")
        }catch (e: DataIntegrityViolationException){
            throw Exception("Esta viagem não pode ser deletada. Existem outros cadastros atrelados a ela!")
        }
    }


}
