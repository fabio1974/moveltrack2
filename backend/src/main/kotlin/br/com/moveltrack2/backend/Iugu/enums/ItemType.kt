package br.com.moveltrack2.backend.Iugu.enums

import com.fasterxml.jackson.annotation.JsonValue

enum class ItemType private constructor(@get:JsonValue
                                        var value: String?) {

    CREDIT_CARD("credit_card")

}
