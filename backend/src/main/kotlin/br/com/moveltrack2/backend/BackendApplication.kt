package br.com.moveltrack2.backend


import br.com.moveltrack2.backend.services.TaskDistanciaDiaria
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.boot.web.servlet.ServletComponentScan

@ServletComponentScan
@SpringBootApplication
class BackendApplication(

)

const val TIME_ZONE_STR = "America/Fortaleza"

fun main(args: Array<String>) {
    runApplication<BackendApplication>(*args)
    //val nanai = Testa()



}

