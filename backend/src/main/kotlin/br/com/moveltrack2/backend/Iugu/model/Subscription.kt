package br.com.moveltrack2.backend.Iugu.model

import java.util.Date

import br.com.moveltrack2.backend.Iugu.enums.PayableWith
import com.fasterxml.jackson.annotation.JsonProperty


class Subscription(@field:JsonProperty("customer_id")
                   val customerId: String) {

    @JsonProperty("plan_identifier")
    var planIdentifier: String?=null

    var expiresAt: Date?=null

    @JsonProperty("only_on_charge_sucess")
    var onlyOnChargeSucess: String?=null

    @JsonProperty("payable_with")
    var payableWith: PayableWith?=null

    @JsonProperty("credits_based")
    var isCreditsBased: Boolean = false

    @JsonProperty("price_cents")
    var priceCents: Int = 0

    @JsonProperty("credits_cycle")
    var creditsCycle: Int = 0

    @JsonProperty("credits_min")
    var creditsMin: Int = 0

    @JsonProperty("custom_variables")
    var customVariables: List<CustomVariable>?=null

    @JsonProperty("subitems")
    var subItems: List<SubItem>?=null
}
