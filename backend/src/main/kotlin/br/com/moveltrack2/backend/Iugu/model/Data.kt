package br.com.moveltrack2.backend.Iugu.model

import com.fasterxml.jackson.annotation.JsonProperty

class Data(number: String, verificationValue: String, firstName: String, lastName: String, month: String, year: String) {

    var number: String
        internal set

    @JsonProperty("verification_value")
    var verificationValue: String
        internal set

    @JsonProperty("first_name")
    var firstName: String
        internal set

    @JsonProperty("last_name")
    var lastName: String
        internal set

    var month: String
        internal set

    var year: String
        internal set

    init {
        this.number = number
        this.verificationValue = verificationValue
        this.firstName = firstName
        this.lastName = lastName
        this.month = month
        this.year = year
    }

}
