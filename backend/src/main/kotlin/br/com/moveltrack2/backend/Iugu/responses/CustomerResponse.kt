package br.com.moveltrack2.backend.Iugu.responses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty


@JsonIgnoreProperties(ignoreUnknown = true)
class CustomerResponse {

    var id: String? = null

    var email: String? = null

    var name: String? = null

    var notes: String? = null

    @JsonProperty("custom_variables")
    var customVariables: List<CustomVariableResponse>? = null

}