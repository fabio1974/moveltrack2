package br.com.moveltrack2.backend.controllers

import br.com.moveltrack.persistence.domain.Chip
import br.com.moveltrack.persistence.repositories.*
import br.com.moveltrack2.backend.specifications.ChipSpecification
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.rest.webmvc.BasePathAwareController
import org.springframework.web.bind.annotation.*


@RestController
@BasePathAwareController
class ChipController(
        val chipRepository: ChipRepository
) {

    @ResponseBody
    @PatchMapping("/chips")
    fun patch(@RequestParam(value = "pageIndex") pageIndex:Int,
              @RequestParam(value = "pageSize") pageSize:Int,
              @RequestParam(value = "sortField") sortField:String?=null,
              @RequestParam(value = "sortDirection",defaultValue = "DESC") sortDirection: Sort.Direction ,
              @RequestBody filtro: Chip): Page<Chip> {

        val especification = ChipSpecification.getFiltro(filtro, chipRepository)
        val pageRequest = PageRequest.of(pageIndex,pageSize,sortDirection,sortField)
        val list: Page<Chip> = chipRepository.findAll(especification,pageRequest)
        return list
    }


    @ResponseBody
    @PostMapping("/chips")
    fun create(@RequestBody chip: Chip): Any {
        if (chip.numero?.indexOf("-")!! < 0){
            val numero = chip.numero
            chip.numero = java.lang.String.format("(%s) %s-%s", numero?.substring(0, 2), numero?.substring(2, 7), numero?.substring(7, 11))
        }
        var aux = chipRepository.save(chip);
        return "Viagem gerada com sucesso!"
    }

}
