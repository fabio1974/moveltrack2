package br.com.moveltrack2.backend.utils

import br.com.moveltrack.persistence.domain.GeoEndereco
import br.com.moveltrack.persistence.domain.Location
import br.com.moveltrack.persistence.repositories.ConfigRepository
import br.com.moveltrack.persistence.repositories.GeoEnderecoRepository
import com.google.maps.GeoApiContext
import com.google.maps.GeocodingApi
import com.google.maps.model.LatLng
import org.springframework.data.repository.findByIdOrNull
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.stereotype.Component
import kotlin.concurrent.thread
import java.util.*
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext


@Component
@EnableScheduling
class GoogleGeo (@PersistenceContext var em: EntityManager,
                 val geoEnderecoRepository: GeoEnderecoRepository,
                 val configRepository: ConfigRepository) {

    val GOOGLE_MAPS_API_KEY = "AIzaSyCg5eE_buXJLsJZbnTZ7z3MnJBOV3_RoYc"


    fun getAddressFromLocation(location: Location, gc: Boolean): String {
        val loc: GeoEndereco? = getGeoEnderecoMaisProximo(location)
        if (loc != null && !loc.endereco.isNullOrBlank())
            return loc.endereco!!

        if (gc) {
            if (!estorouLimite()) {
                try{
                    thread() {
                        val address = getAddressFromGoogleLocation(location.latitude, location.longitude)
                        if (address.length > 0) {
                            val g = GeoEndereco(
                                    latitude = location.latitude.round(4),
                                    longitude = location.longitude.round(4),
                                    endereco = address,
                                    confiavel = true
                            )
                            geoEnderecoRepository.save(g)
                        }
                    }
                }catch (e:OutOfMemoryError){
                    e.printStackTrace()
                }
                catch (e:java.lang.Exception){
                    e.printStackTrace()
                }
                catch (e:Exception){
                    e.printStackTrace()
                }


            }

        }
        return ""
    }



    var context = GeoApiContext.Builder().apiKey(GOOGLE_MAPS_API_KEY).build()

    private fun getAddressFromGoogleLocation(latitude: Double, longitude: Double): String {
        if(context==null)
            context = GeoApiContext.Builder().apiKey(GOOGLE_MAPS_API_KEY).build()
        val latlng = LatLng(latitude, longitude)
        val req = GeocodingApi.reverseGeocode(context, latlng).language("pt-BR")
        try {
            val results = req.await()
            return results[0].formattedAddress
        } catch (e: Exception) {
            e.printStackTrace()
            return ""
        }
    }



    private fun getGeoEnderecoMaisProximo(location: Location): GeoEndereco? {
        val date = Date()
        val sql = ("select g.* from geoendereco g  where g.latitude=${location.latitude.round(4)} and g.longitude=${location.longitude.round(4)} limit 1")
        val query = em.createNativeQuery(sql,GeoEndereco::class.java)
        try {
            val r =  query.singleResult as GeoEndereco
            return r
        }catch(e: Exception){
            return null
        }
    }



    private fun estorouLimite() : Boolean{
        var config = configRepository.findByIdOrNull(1)
        if(config?.geoCodeCount!! < 25000) {
            config.geoCodeCount++
            //println("GEOCODE COUNT ====> ${config?.geoCodeCount}")
            configRepository.save(config)
            return false
        }else {
            return true
        }
    }

    fun getLocationFromAddress(bairro: String, cidade: String, estadoSigla: String): LatLng? {
        val context = GeoApiContext.Builder().apiKey(GOOGLE_MAPS_API_KEY).build()
        val req = GeocodingApi.geocode(context,"$cidade,$estadoSigla")
        try {
            val results = req.await()
            return results[0]?.geometry?.location
        } catch (e: Exception) {
            return null
        }
    }


}

/*fun main(args: Array<String>){
    val d:Double = -44.98292838793
    val d1 = d.round(5)
    println(d1)
}*/

fun Double.round(decimals: Int? = 4): Double = "%.${decimals}f".format(Locale.ENGLISH,this).toDouble()

