package br.com.moveltrack2.backend.Iugu.model

import java.io.Serializable

import com.fasterxml.jackson.annotation.JsonProperty


class Customer(
        /**
         * Nome do cliente
         */
        val name: String) : Serializable {

    /**
     * E-Mail do cliente
     */
    var email: String? = null
        private set

    /**
     * cnpj/cpf do cliente
     */
    @JsonProperty("cpf_cnpj")
    var cpfCnpj: String? = null
        private set

    /**
     * lista de emails que receberão cópias, em formato string separadas por
     * vírgula
     */
    @JsonProperty("cc_emails")
    var ccEmails: String? = null
        private set

    /**
     * Anotações gerais do cliente
     */
    var notes: String? = null
        private set

    /**
     * Variáveis personalizadas do cliente
     */
    @JsonProperty("custom_variables")
    var customVariables: List<CustomVariable>? = null
        private set

    fun withEmail(email: String): Customer {
        this.email = email
        return this
    }

    fun withCpfCnpj(cpfCnpj: String): Customer {
        this.cpfCnpj = cpfCnpj
        return this
    }

    fun withCcEmails(ccEmails: String): Customer {
        this.ccEmails = ccEmails
        return this
    }

    fun withNotes(notes: String): Customer {
        this.notes = notes
        return this
    }

    fun withCustomVariables(customVariables: List<CustomVariable>): Customer {
        this.customVariables = customVariables

        return this
    }

    companion object {

        private const val serialVersionUID = 3266886175287194L
    }

}
