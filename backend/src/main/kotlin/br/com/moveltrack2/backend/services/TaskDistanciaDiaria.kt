package br.com.moveltrack2.backend.services

import br.com.moveltrack.persistence.domain.*
import br.com.moveltrack.persistence.repositories.*
import br.com.moveltrack2.backend.TIME_ZONE_STR
import br.com.moveltrack2.backend.controllers.TIME_ZONE
import br.com.moveltrack2.backend.utils.GeoDistanceCalulator
import br.com.moveltrack2.backend.utils.MapaUtils
import br.com.moveltrack2.backend.utils.logs
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit

@Component
@EnableScheduling
class TaskDistanciaDiaria(
        val veiculoRepository: VeiculoRepository,
        val locationRepository: LocationRepository,
        val contratoRepository: ContratoRepository,
        val distanciaDiariaRepository: DistanciaDiariaRepository,
        val clienteRepository: ClienteRepository
) {

    @Scheduled(cron = "0 0 2 * * ?",zone = TIME_ZONE_STR) //às 2h da manha, todos os dias
    public fun computaDistanciaDiaria(){
        logs(true,"Entrando na computação de distâncias diárias...")
        val ontemInicio = ZonedDateTime.now().minusDays(1).truncatedTo(ChronoUnit.DAYS)
        val ontemFim = ZonedDateTime.now().truncatedTo(ChronoUnit.DAYS).minusSeconds(1)
        var count = 0

        contratoRepository.findAllByStatus(ContratoStatus.ATIVO)?.forEach {contrato->
                if(contrato.cliente?.usuario?.permissoes?.find {it -> it.descricao == "RELATORIO_DISTANCIA_PERCORRIDA"} != null ||
                   contrato.cliente?.usuario?.perfil?.permissoes?.find {it -> it.descricao == "RELATORIO_DISTANCIA_PERCORRIDA"} !=null){
                   veiculoRepository.findAllByContratoAndStatus(contrato,VeiculoStatus.ATIVO).forEach {veiculo->
                        val dd = distanciaDiariaRepository.findTopByVeiculoAndDataComputadaOrderByDataComputada(veiculo,ontemInicio)?:DistanciaDiaria()
                        dd.dataComputada = ontemInicio
                        dd.veiculo= veiculo
                        dd.distanciaPercorrida = getDistanciaDiaria(veiculo,ontemInicio,ontemFim)
                        distanciaDiariaRepository.save(dd)
                       count++
                    }
                }
        }
        logs(true,"Computado distancia diaria de ${count} veiculos !!!")
    }


    public fun corrigeDistanciaDiaria() {

        var inicio = ZonedDateTime.of(2020, 12, 31, 0, 0, 0, 0, TIME_ZONE)
        val fim = ZonedDateTime.of(2021, 2, 3, 23, 59, 59, 999, TIME_ZONE)
        val cliente = clienteRepository.findByNomeContaining("Irineu")[0]

        while (inicio.isBefore(fim)) {

            logs(true, "Entrando na computação de distâncias diárias...")
            val ontemInicio = inicio.minusDays(1).truncatedTo(ChronoUnit.DAYS)
            val ontemFim = inicio.truncatedTo(ChronoUnit.DAYS).minusSeconds(1)
            var count = 0
            val contrato = contratoRepository.findByCliente(cliente)

            if (contrato.cliente?.usuario?.permissoes?.find { it -> it.descricao == "RELATORIO_DISTANCIA_PERCORRIDA" } != null ||
                contrato.cliente?.usuario?.perfil?.permissoes?.find { it -> it.descricao == "RELATORIO_DISTANCIA_PERCORRIDA" } != null) {
                veiculoRepository.findAllByContratoAndStatus(contrato, VeiculoStatus.ATIVO).forEach { veiculo ->
                    val dd = distanciaDiariaRepository.findTopByVeiculoAndDataComputadaOrderByDataComputada(
                        veiculo,
                        ontemInicio
                    ) ?: DistanciaDiaria()
                    dd.dataComputada = ontemInicio
                    dd.veiculo = veiculo
                    dd.distanciaPercorrida = getDistanciaDiaria(veiculo, ontemInicio, ontemFim)
                    distanciaDiariaRepository.save(dd)
                    count++
                }
            }
            inicio = inicio.plusDays(1)
        }
    }







    private fun getDistanciaDiaria(veiculo: Veiculo, inicioDoDia: ZonedDateTime,fimDoDia:ZonedDateTime): Double {
        val imei = veiculo.equipamento?.imei
        if(imei==null) return 0.0
        val pontosCrus = locationRepository.findAllByImeiAndDateLocationGreaterThanEqualAndDateLocationLessThanEqualOrderByDateLocationInicio(imei,inicioDoDia,fimDoDia)
        val otimizado = MapaUtils.otimizaPontosDoBanco(pontosCrus,inicioDoDia,fimDoDia,veiculo.equipamento,locationRepository)
        return  GeoDistanceCalulator.getPathLength(otimizado)/1.06f
    }




}
