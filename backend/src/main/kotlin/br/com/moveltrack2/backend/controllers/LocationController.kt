package br.com.moveltrack2.backend.controllers

import br.com.moveltrack.persistence.domain.Equipamento
import br.com.moveltrack.persistence.domain.Location
import br.com.moveltrack.persistence.repositories.EquipamentoRepository
import br.com.moveltrack.persistence.repositories.LocationRepository
import br.com.moveltrack.persistence.repositories.VeiculoRepository
import br.com.moveltrack2.backend.utils.GoogleGeo
import br.com.moveltrack2.backend.utils.MapaUtils.otimizaPontosDoBanco
import br.com.moveltrack2.backend.utils.logs
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.runBlocking
import org.springframework.data.rest.webmvc.BasePathAwareController
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@BasePathAwareController
class LocationController(
        val locationRepository: LocationRepository,
        val equipamentoRepository: EquipamentoRepository,
        val veiculoRepository: VeiculoRepository,
        val googleGeo: GoogleGeo
        ) {

    @ResponseBody
    @GetMapping("/telimetria/{imei}")
    fun create(@PathVariable("imei") imei: String):Location?{
        return locationRepository.findFirstByImeiAndGsmIsNotNullOrderByDateLocationDesc(imei)
    }




    @PostMapping("/locationsByImeiDates")
    fun locationsByImeiDates(@RequestBody locationRequest: Location):List<Location>{

        //logs(true,"locationsByImeiDates ${locationRequest.imei}")

        val equipamento: Equipamento = equipamentoRepository.findByImei(locationRequest.imei)
        val pontosCrus = locationRepository.findAllByImeiAndDateLocationGreaterThanEqualAndDateLocationLessThanEqualOrderByDateLocationInicio(locationRequest.imei!!,locationRequest.dateLocationInicio!!,locationRequest.dateLocation!!)
        val otimizado = otimizaPontosDoBanco(pontosCrus, locationRequest.dateLocationInicio!!, locationRequest.dateLocation!!, equipamento,locationRepository)
        if(!otimizado.isEmpty())
            otimizado.last().endereco = googleGeo.getAddressFromLocation(otimizado.last(), true)
        return otimizado
    }


    @PostMapping("/relatorioPercurso")
    fun relatorioPercurso(@RequestBody locationRequest: Location): List<Location> {
        val equipamento: Equipamento = equipamentoRepository.findByImei(locationRequest.imei)
        val pontosCrus = locationRepository.findAllByImeiAndDateLocationGreaterThanEqualAndDateLocationLessThanEqualOrderByDateLocationInicio(locationRequest.imei!!, locationRequest.dateLocationInicio!!, locationRequest.dateLocation!!)
        val otimizado = otimizaPontosDoBanco(pontosCrus, locationRequest.dateLocationInicio!!, locationRequest.dateLocation!!, equipamento,locationRepository)

        var t = Date()

        if (!otimizado.isEmpty()) {
            val last = otimizado.last()
            val first = otimizado.first()
            var count = 0
            runBlocking {
                otimizado.pmap {
                    count++
                    if (it.id == first.id || it.dateLocation!!.isAfter(it.dateLocationInicio) || it.id == last.id)
                        it.endereco = googleGeo.getAddressFromLocation(it, true)
                }
            }
        }

        println("tempo total  ${Date().time - t.time}")

        return otimizado
    }


    @PostMapping("/relatorioParadas")
    fun relatorioParadas(@RequestBody locationRequest: Location):List<Location>{
        val equipamento: Equipamento = equipamentoRepository.findByImei(locationRequest.imei)
        val pontosCrus = locationRepository.findAllByImeiAndDateLocationGreaterThanEqualAndDateLocationLessThanEqualOrderByDateLocationInicio(locationRequest.imei!!, locationRequest.dateLocationInicio!!, locationRequest.dateLocation!!)
        val otimizado = otimizaPontosDoBanco(pontosCrus, locationRequest.dateLocationInicio!!, locationRequest.dateLocation!!, equipamento, locationRepository)
        val paradas = otimizado.filter { it -> it.dateLocation!!.isAfter(it.dateLocationInicio) }
        runBlocking {paradas.pmap{it.endereco = googleGeo.getAddressFromLocation(it, true)}}
        return paradas
    }




    suspend fun <A, B> Iterable<A>.pmap(f: suspend (A) -> B): List<B> = coroutineScope {
        map { async { f(it) } }.awaitAll()
    }






}
