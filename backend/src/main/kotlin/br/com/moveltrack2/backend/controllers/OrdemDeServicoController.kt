package br.com.moveltrack2.backend.controllers

import br.com.moveltrack.persistence.domain.OrdemDeServico
import br.com.moveltrack.persistence.repositories.*
import br.com.moveltrack2.backend.specifications.OrdemDeServicoSpecification
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.rest.webmvc.BasePathAwareController
import org.springframework.web.bind.annotation.*


@RestController
@BasePathAwareController
class OrdemDeServicoController(
        val ordemDeServicoRepository: OrdemDeServicoRepository
) {

    @ResponseBody
    @PatchMapping("/ordemDeServicos")
    fun patch(@RequestParam(value = "pageIndex") pageIndex:Int,
              @RequestParam(value = "pageSize") pageSize:Int,
              @RequestParam(value = "sortField") sortField:String?=null,
              @RequestParam(value = "sortDirection",defaultValue = "DESC") sortDirection: Sort.Direction ,
              @RequestBody filtro: OrdemDeServico): Page<OrdemDeServico> {

        val especification = OrdemDeServicoSpecification.getFiltro(filtro, ordemDeServicoRepository)
        val pageRequest = PageRequest.of(pageIndex,pageSize,sortDirection,sortField)
        val list: Page<OrdemDeServico> = ordemDeServicoRepository.findAll(especification,pageRequest)
        return list
    }


    @ResponseBody
    @PostMapping("/ordemDeServicos")
    fun create(@RequestBody ordemDeServico: OrdemDeServico): Any {
        var aux = ordemDeServicoRepository.save(ordemDeServico);
        if(aux.numero==null) {
            ordemDeServico.numero = aux.id.toString().padStart(7, '0')
            ordemDeServicoRepository.save(ordemDeServico);
        }
        return "OS gerada com sucesso!"
    }

}
