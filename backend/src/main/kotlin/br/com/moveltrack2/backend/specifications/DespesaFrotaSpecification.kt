package br.com.moveltrack2.backend.specifications

import br.com.moveltrack.persistence.domain.*
import br.com.moveltrack.persistence.repositories.ContratoRepository
import org.springframework.data.jpa.domain.Specification
import java.util.*
import javax.persistence.criteria.Predicate


object DespesaFrotaSpecification {


    fun getFiltro(filtro: DespesaFrota, repo: ContratoRepository): Specification<DespesaFrota> {
        return Specification { root, query, builder ->
            val predicates = ArrayList<Predicate>()


            if (filtro.cliente?.id != null) {
                predicates.add(builder.equal(root.get<Cliente>("cliente"), filtro.cliente))
            }

            if(filtro.categoria!=null){
                predicates.add(builder.equal(root.get<DespesaFrotaCategoria>("categoria"), filtro.categoria))
            }

            if(filtro.especie!=null){
                predicates.add(builder.equal(root.get<DespesaFrotaEspecie>("especie"), filtro.especie))
            }

            if(filtro.veiculo?.id!=null){
                predicates.add(builder.equal(root.get<Veiculo>("veiculo"), filtro.veiculo))
            }

            if(filtro.motorista?.id!=null){
                predicates.add(builder.equal(root.get<Motorista>("motorista"), filtro.motorista))
            }

            if(filtro.viagem?.numeroViagem!=null){
                predicates.add(builder.equal(root.get<Viagem>("viagem").get<String>("numeroViagem"), filtro.viagem?.numeroViagem))
            }





            builder.and(*predicates.toTypedArray())
        }
    }


}
