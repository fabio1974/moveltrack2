package br.com.moveltrack2.backend.Iugu

class IuguException(Message: String, ResponseCode: Int, ResponseText: String) :
         Exception("$Message - StatusCode: [$ResponseCode] / ResponseText: [$ResponseText]")
