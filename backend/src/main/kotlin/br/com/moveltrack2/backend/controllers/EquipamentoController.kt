package br.com.moveltrack2.backend.controllers

import br.com.moveltrack.persistence.domain.*
import br.com.moveltrack.persistence.repositories.*
import br.com.moveltrack2.backend.specifications.EquipamentoSpecification
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.rest.webmvc.BasePathAwareController
import org.springframework.web.bind.annotation.*


@RestController
@BasePathAwareController
class EquipamentoController(
        val equipamentoRepository: EquipamentoRepository,
        val locationRepository: LocationRepository,
        val veiculoRepository: VeiculoRepository
) {

    @ResponseBody
    @PatchMapping("/equipamentos")
    fun patch(@RequestParam(value = "pageIndex") pageIndex:Int,
              @RequestParam(value = "pageSize") pageSize:Int,
              @RequestParam(value = "sortField") sortField:String?=null,
              @RequestParam(value = "sortDirection",defaultValue = "DESC") sortDirection: Sort.Direction ,
              @RequestBody filtro: Equipamento): Page<Equipamento> {

        val especification = EquipamentoSpecification.getFiltro(filtro, equipamentoRepository)
        val pageRequest = PageRequest.of(pageIndex,pageSize,sortDirection,sortField)
        val list: Page<Equipamento> = equipamentoRepository.findAll(especification,pageRequest)

        return list.map {
            it.chip?.label = "${it.chip?.iccid}-${it.chip?.operadora}"
           // try{
            val veiculo = veiculoRepository.findByEquipamento(it)
            if(veiculo!=null) {
                it.veiculo = Veiculo()
                it.veiculo?.placa = veiculo.placa
                it.veiculo?.contrato = Contrato()
                it.veiculo?.contrato?.cliente = Cliente()
                it.veiculo?.contrato?.cliente?.nome = veiculo.contrato?.cliente?.nome
            }

/*
            }catch(e:Exception){
                e.printStackTrace()
            }
*/


            it
        }
    }


    @ResponseBody
    @PostMapping("/equipamentos")
    fun create(@RequestBody equipamento: Equipamento): Any {
        try {
            var aux = equipamentoRepository.save(equipamento);
            return "Viagem gerada com sucesso!"
        }catch (e: Exception){
            throw Exception("Equipamento com imei já cadastrado no banco")
        }
    }

    @ResponseBody
    @GetMapping("/modelosRastreador")
    fun getModeloRastreador(): List<ModeloRastreador>{
        return ModeloRastreador.values().toList()
    }


    data class EquipamentoSituacaoPojo(val valor:EquipamentoSituacao?=null,val descricao:String?=null)
    @ResponseBody
    @GetMapping("/equipamentosSituacao")
    fun get(): List<EquipamentoSituacaoPojo>{
        return EquipamentoSituacao.values().map {
            EquipamentoSituacaoPojo(it,it.descricao)
        }
    }

    @GetMapping("/inicioSpot")
    fun getInicioSpot(){
        val list = equipamentoRepository.findAllByModeloAndInicioSpotIsNull(ModeloRastreador.SPOT_TRACE)
        list.forEach {
            val dataInicio = locationRepository.findFirstByImeiOrderByDateLocation(it.imei)?.dateLocationInicio
            if(dataInicio!=null){
                it.inicioSpot = dataInicio
                equipamentoRepository.save(it)
            }
        }
    }




}

