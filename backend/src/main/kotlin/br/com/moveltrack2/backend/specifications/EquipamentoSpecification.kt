package br.com.moveltrack2.backend.specifications


import br.com.moveltrack.persistence.domain.*
import br.com.moveltrack.persistence.repositories.EquipamentoRepository
import org.springframework.data.jpa.domain.Specification
import org.springframework.util.StringUtils
import java.util.*
import javax.persistence.criteria.Predicate


object EquipamentoSpecification {

    fun getFiltro(filtro: Equipamento, repo: EquipamentoRepository): Specification<Equipamento> {
        return Specification { root, query, builder ->
            val predicates = ArrayList<Predicate>()

            if (!StringUtils.isEmpty(filtro.imei)) {
                val d = "%" + filtro.imei!!.trim { it <= ' ' } + "%"
                predicates.add(builder.like(root.get("imei"), d))
            }

            if (filtro.modelo!=null) {
                predicates.add(builder.equal(root.get<ModeloRastreador>("modelo"), filtro.modelo))
            }

            if (filtro.possuidor!=null) {
                predicates.add(builder.equal(root.get<Empregado>("possuidor"), filtro.possuidor))
            }

            if (filtro.chip!=null && filtro.chip?.iccid!=null) {
                predicates.add(builder.equal(root.get<Chip>("chip").get<String>("iccid"), filtro.chip?.iccid))
            }

            if (filtro.chip!=null && filtro.chip?.numero!=null) {
                val d = "%" + filtro.chip?.numero!!.trim { it <= ' ' } + "%"
                predicates.add(builder.like(root.get<Chip>("chip").get<String>("numero"), d))
            }

            if (filtro.situacao!=null) {
                predicates.add(builder.equal(root.get<EquipamentoSituacao>("situacao"), filtro.situacao))
            }
            builder.and(*predicates.toTypedArray())
        }
    }

}
