package br.com.moveltrack2.backend.Iugu.model


import br.com.moveltrack2.backend.Iugu.enums.PayableWith
import com.fasterxml.jackson.annotation.JsonProperty


class PaymentToken {

    @JsonProperty("account_id")
    var accountId: String? = null

    @JsonProperty("method")
    var payableWith: PayableWith? = null

    @JsonProperty("test")
    var test: Boolean? = null

    @JsonProperty("data")
    var data: Data? = null

    override fun toString(): String {
        return "PaymentToken{" +
                "accountId='" + accountId + '\''.toString() +
                ", payableWith=" + payableWith +
                ", isTest=" + test +
                ", data=" + data +
                '}'.toString()
    }
}
