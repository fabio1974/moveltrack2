package br.com.moveltrack2.backend.seguranca


import br.com.moveltrack.persistence.domain.Pessoa
import br.com.moveltrack.persistence.domain.Usuario
import br.com.moveltrack.persistence.repositories.PessoaRepository
import br.com.moveltrack.persistence.repositories.UsuarioRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Component
import java.util.HashSet

@Component
class MyAuthenticationProvider : AuthenticationProvider {

    @Autowired
    lateinit var usuarioRepository: UsuarioRepository

    @Autowired
    lateinit var pessoaRepository: PessoaRepository

    @Throws(AuthenticationException::class,BadCredentialsException::class)
    override fun authenticate(authentication: Authentication): Authentication {
        val username = authentication.name//.replace("[^0-9]".toRegex(), "")
        val password = authentication.credentials as String
        val passwordCodificado = Criptografia().calculaHash(password)
        var usuario:Usuario? = usuarioRepository.findByNomeUsuario(username)
        if (usuario == null)
            throw BadCredentialsException("Problemas de autenticação!")
        else if (passwordCodificado != usuario.senha)
            throw BadCredentialsException("Wrong password.")

        var pessoa: Pessoa = pessoaRepository.findByUsuario(usuario)

        var authorities = HashSet<GrantedAuthority>()
        usuario.permissoes.forEach {authorities.add(SimpleGrantedAuthority(it.descricao))}
        usuario.perfil?.permissoes?.forEach {authorities.add(SimpleGrantedAuthority(it.descricao))}


        var user = User(pessoa.id?.toString(),pessoa.nome,usuario.nomeUsuario,null,pessoa.cpf, usuario.perfil)

        return UsernamePasswordAuthenticationToken(user,null,authorities)
    }

    override fun supports(authentication: Class<*>): Boolean {
        return true
    }
}
