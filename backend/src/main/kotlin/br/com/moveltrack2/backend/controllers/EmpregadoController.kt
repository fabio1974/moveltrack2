package br.com.moveltrack2.backend.controllers

import br.com.moveltrack.persistence.domain.Empregado
import br.com.moveltrack.persistence.domain.PerfilTipo
import br.com.moveltrack.persistence.repositories.*
import br.com.moveltrack2.backend.seguranca.Criptografia
import br.com.moveltrack2.backend.specifications.EmpregadoSpecification
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.rest.webmvc.BasePathAwareController
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*


@RestController
@BasePathAwareController
class EmpregadoController(
        val empregadoRepository: EmpregadoRepository,
        val usuarioRepository: UsuarioRepository,
        val perfilRepository: PerfilRepository

) {

    @ResponseBody
    @PatchMapping("/empregados")
    fun patch(@RequestParam(value = "pageIndex") pageIndex:Int,
              @RequestParam(value = "pageSize") pageSize:Int,
              @RequestParam(value = "sortField") sortField:String?=null,
              @RequestParam(value = "sortDirection",defaultValue = "DESC") sortDirection: Sort.Direction ,
              @RequestBody filtro: Empregado): Page<Empregado> {

        val especification = EmpregadoSpecification.getFiltro(filtro, empregadoRepository)
        val pageRequest = PageRequest.of(pageIndex,pageSize,sortDirection,sortField)
        val list: Page<Empregado> = empregadoRepository.findAll(especification,pageRequest)
        return list
    }


    @Transactional
    @ResponseBody
    @PostMapping("/empregados")
    fun create(@RequestBody empregado: Empregado): Any {
        if(empregado.id==null)
            empregado.usuario?.senha = Criptografia().calculaHash("1234")

        usuarioRepository.save(empregado.usuario!!)
        empregadoRepository.save(empregado)

        return "Empregado salvo com sucesso!"
    }

}
