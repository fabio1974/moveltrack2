package br.com.moveltrack2.backend.Iugu.enums

import com.fasterxml.jackson.annotation.JsonValue

enum class IntervalType private constructor(@get:JsonValue
                                            var value: String?) {

    WEEKS("weeks"), MONTHS("months")

}
