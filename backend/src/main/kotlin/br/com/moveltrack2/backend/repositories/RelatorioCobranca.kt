package br.com.moveltrack2.backend.repositories

import br.com.moveltrack.persistence.domain.Contrato
import br.com.moveltrack.persistence.domain.MBoleto
import br.com.moveltrack.persistence.pojos.RelatorioFiltro
import br.com.moveltrack.persistence.repositories.*
import br.com.moveltrack.persistence.util.Utils.DDMMYY_FORMAT
import br.com.moveltrack.persistence.util.cell
import br.com.moveltrack.persistence.util.cellWithHeader
import br.com.moveltrack.persistence.util.header
import br.com.moveltrack2.backend.utils.*
import com.lowagie.text.Document
import com.lowagie.text.pdf.PdfPTable
import org.springframework.stereotype.Component
import java.text.SimpleDateFormat
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Component
class RelatorioCobranca constructor(
        @PersistenceContext var em: EntityManager,
        val veiculoRepository: VeiculoRepository,
        val mBoletoRepository: MBoletoRepository,
        val equipamentoRepository: EquipamentoRepository,
        val locationRepository: LocationRepository,
        val contratoRepository: ContratoRepository,
        val googleGeo: GoogleGeo
) {

    //var sdf = SimpleDateFormat("dd/MM/yyyy")

    var totalDebito = 0.0

    fun addToDocument(contratos: Map<Contrato?, List<MBoleto>>?, filtro: RelatorioFiltro, document: Document): Double {
        totalDebito = 0.0
        contratos?.keys?.sortedBy { it?.cliente?.nome }?.forEach { it ->
            val boletos = it?.let { it1 -> mBoletoRepository.findAllByContratoAndSituacao(it1, filtro.boletoStatus!!) }
            document.add(boletos?.let { it1 -> getTable(it, it1) })
        }
        return totalDebito
    }

    fun getTable(contrato: Contrato?, boletos: List<MBoleto>): PdfPTable? {

        val table = PdfPTable(24)
        table.setHorizontalAlignment(PdfPTable.ALIGN_CENTER)
        table.setWidthPercentage(90f)
        table.setSpacingBefore(20f)

        val cliente = contrato?.cliente
        val contato = "${cliente?.celular1?:""} / ${cliente?.celular2?:""} / ${cliente?.telefoneFixo ?:""}"

        table.addCell(cellWithHeader("Cliente", cliente?.nome ?: "", 12))
        table.addCell(cellWithHeader("Contato", contato, 12))
        table.addCell(cellWithHeader("Endereço", "${cliente?.endereco?:""},${cliente?.numero?:""},${cliente?.complemento?:""}",12))
        table.addCell(cellWithHeader("Bairro/Cidade", "${cliente?.bairro ?: ""} / ${cliente?.municipio?.descricao ?:""}",12))

        table.addCell(header("Data Vencimento",5, PdfPTable.ALIGN_CENTER))
        table.addCell(header("Mensagem do Boleto",10, PdfPTable.ALIGN_LEFT))
        table.addCell(header("Situação",5, PdfPTable.ALIGN_CENTER))
        table.addCell(header("Valor",4, PdfPTable.ALIGN_CENTER))

        var total=0.0
        boletos.sortedBy { it.dataVencimento }.forEach {
            table.addCell(cell(PdfPTable.ALIGN_CENTER,it.dataVencimento?.format(DDMMYY_FORMAT)?:"",5))
            table.addCell(cell(PdfPTable.ALIGN_CENTER,it.mensagem34?:"",10))
            table.addCell(cell(PdfPTable.ALIGN_CENTER,it.situacao?.name?:"",5))
            table.addCell(cell(PdfPTable.ALIGN_CENTER, Utils.moeda(it.valor),4))
            total += it.valor
        }
        totalDebito +=total
        table.addCell(header("Total",20, PdfPTable.ALIGN_RIGHT))
        table.addCell(header(Utils.moeda(total),4, PdfPTable.ALIGN_CENTER))
        return table
    }


}
