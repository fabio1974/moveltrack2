package br.com.moveltrack2.backend.utils

import br.com.moveltrack.persistence.domain.Equipamento
import br.com.moveltrack.persistence.domain.Location
import br.com.moveltrack.persistence.domain.ModeloRastreador
import br.com.moveltrack.persistence.pojos.ParadaSpot
import br.com.moveltrack.persistence.repositories.LocationRepository
import java.time.ZonedDateTime
import java.util.ArrayList

object MapaUtils {
    val VELOCIDADE_LIMITE_PARADA = 3

    fun otimizaPontosDoBanco(pontosCrus: List<Location>?, inicio: ZonedDateTime, fim: ZonedDateTime, equipamento: Equipamento?, locationRepository: LocationRepository?): List<Location> {
        val pontosOtimizados = ArrayList<Location>()
        if (pontosCrus == null || pontosCrus.size == 0)
            return pontosOtimizados

        if (pontosCrus.size == 1) {
            pontosOtimizados.add(pontosCrus[0])
        } else {
            if (equipamento != null && equipamento.modelo === ModeloRastreador.SPOT_TRACE) {
                val previous = locationRepository?.findFirstByImeiAndDateLocationBeforeOrderByDateLocationDesc(equipamento.imei!!,inicio)
                calculaParadasSpot(pontosCrus, inicio, fim, pontosOtimizados, previous)
            }else
                calculaParadas(pontosCrus, inicio, fim, pontosOtimizados)
        }
        return pontosOtimizados
    }


    private fun calculaParadasSpot(pontosCrus: List<Location>, inicio: ZonedDateTime, fim: ZonedDateTime, pontosOtimizados: MutableList<Location>, previous: Location?) {

        val paradaSpot = ParadaSpot()
        var lastLoc: Location? = null

        if (previous != null && (previous.comando == "STOP"   ||  (previous.comando == "STATUS" && previous.velocidade < VELOCIDADE_LIMITE_PARADA))) {
            previous.dateLocation = inicio
            previous.dateLocationInicio = inicio
            paradaSpot.abreParada(previous)
            lastLoc = previous
        }

        for (i in pontosCrus.indices) {
            val loc = pontosCrus[i]
            if (lastLoc != null && lastLoc.dateLocation == loc.dateLocation && lastLoc.dateLocationInicio == loc.dateLocationInicio) {
                lastLoc = loc
                continue
            }
            if (loc.comando == "STOP" || (loc.comando == "STATUS" && loc.velocidade < VELOCIDADE_LIMITE_PARADA)) {

                if (paradaSpot.isAberta) {
                    val isFar = isFarOfLastLocation(loc, lastLoc)
                    if (isFar)
                        paradaSpot.fechaEAbreNova(pontosOtimizados, loc)
                    else
                        paradaSpot.mantemParada(loc)
                } else {
                    paradaSpot.abreParada(loc)
                    if (i == pontosCrus.size - 1) {
                        paradaSpot.fechaParada(pontosOtimizados, loc)
                    }
                }
            } else if (loc.comando == "NEWMOVEMENT") {
                if (paradaSpot.isAberta)
                    paradaSpot.fechaParada(pontosOtimizados, loc)
                pontosOtimizados.add(loc)
            } else {
                if (paradaSpot.isAberta)
                    paradaSpot.fechaParada(pontosOtimizados, loc)
                pontosOtimizados.add(loc)
            }
            lastLoc = loc
        }
    }


    private fun calculaParadas(pontosDoBanco: List<Location>, inicio: ZonedDateTime, fim: ZonedDateTime, pontosMapa: MutableList<Location>) {
        val pontosDeParada = ArrayList<Location>()
        val size = pontosDoBanco.size
        var lastLoc: Location? = null
        for (i in 0 until size) {
            val loc = pontosDoBanco[i]
            if (lastLoc != null && lastLoc.dateLocation == loc.dateLocation && lastLoc.dateLocationInicio == loc.dateLocationInicio) {
                lastLoc = loc
                continue
            }

            if (i == 0 && loc.dateLocationInicio!!.isBefore(inicio))
                loc.dateLocationInicio = inicio
            if (i == size - 1 && loc.dateLocation!!.isAfter(fim))
                loc.dateLocation = fim

            val isFar = isFarOfLastLocation(loc, lastLoc)
            if (loc.velocidade > VELOCIDADE_LIMITE_PARADA || isFar) {
                if (pontosDeParada.size > 0) {
                    val parada = getAverageLocation(pontosDeParada)
                    pontosDeParada.clear()
                    if (isFar)
                        pontosDeParada.add(loc)
                    if(isParada(parada))
                        pontosMapa.add(parada)
                }
                pontosMapa.add(loc)
            } else {
                pontosDeParada.add(loc)
            }
            lastLoc = loc
        }

        //coloca a última parada
        if (pontosDeParada.size > 0) {
            /* for (location in pontosDeParada) {
                 pontosMapa.add(location)
             }*/
            val parada = getLastStop(pontosDeParada)
            //paradas.add(parada);
            pontosMapa.add(parada)
        }
    }


    fun isParada(loc: Location): Boolean {
        return if (loc.velocidade > VELOCIDADE_LIMITE_PARADA) false else loc.dateLocation!!.toEpochSecond() - loc.dateLocationInicio!!.toEpochSecond() > 180
    }

    private fun isFarOfLastLocation(loc: Location, lastLoc: Location?): Boolean {
        if (lastLoc == null || lastLoc.velocidade > VELOCIDADE_LIMITE_PARADA || loc.velocidade > VELOCIDADE_LIMITE_PARADA)
            return false
        else
            return GeoDistanceCalulator.vicentDistance(loc.latitude, loc.longitude, lastLoc.latitude, lastLoc.longitude) > 100
    }

    fun getLastStop(parada: List<Location>): Location {

        /*if (parada.size <= 0)
            return null
*/
        val firstLoc = parada[0]
        val lastLoc = parada[parada.size - 1]
        val loc = Location()

        loc.ignition=lastLoc.ignition
        loc.latitude=lastLoc.latitude
        loc.longitude=lastLoc.longitude
        loc.velocidade=0.toDouble()
        loc.dateLocationInicio=firstLoc.dateLocationInicio
        loc.dateLocation=lastLoc.dateLocation
        loc.alarm=lastLoc.alarm
        loc.alarmType=lastLoc.alarmType
        loc.battery=lastLoc.battery
        loc.bloqueio=lastLoc.bloqueio
        loc.comando=lastLoc.comando
        loc.gps=lastLoc.gps
        loc.gsm=lastLoc.gsm
        loc.id=lastLoc.id
        loc.imei=lastLoc.imei
        loc.mcc=lastLoc.mcc
        loc.satelites=lastLoc.satelites
        loc.sos=lastLoc.sos
        //loc.Version=lastLoc.Version;
        loc.volt=lastLoc.volt

        return loc

    }

    fun getAverageLocation(parada: List<Location>): Location {
        //----------------------------------------------------------------------------------------------------------
        //O código comentado abaixo faz a média dos pontos. Esse algoritmo porém não é recomendável em casos de roubo ou furto.
        //----------------------------------------------------------------------------------------------------------
/*
        if (parada.size <= 0)
            return null
*/

        var count = 0
        var latMed = 0.0
        var lonMed = 0.0
        for (location in parada) {
            latMed += location.latitude
            lonMed += location.longitude

            val ignition = location.ignition
            if (ignition != null && ignition == "1")
                count++
        }
        latMed = latMed / parada.size
        lonMed = lonMed / parada.size

        val firstLoc = parada[0]
        val lastLoc = parada[parada.size - 1]
        val loc = Location()

        loc.ignition =  if (count > parada.size / 2) "1" else "0"
        loc.latitude = latMed
        loc.longitude=lonMed
        loc.velocidade=0.toDouble()
        loc.dateLocationInicio=parada[0].dateLocationInicio
        loc.dateLocation=parada[parada.size - 1].dateLocation
        loc.alarm=lastLoc.alarm
        loc.alarmType=lastLoc.alarmType
        loc.battery=lastLoc.battery
        loc.bloqueio=lastLoc.bloqueio
        loc.comando=lastLoc.comando
        loc.gps=lastLoc.gps
        loc.gsm=lastLoc.gsm
        loc.id=lastLoc.id
        loc.imei=lastLoc.imei
        loc.mcc=lastLoc.mcc
        loc.satelites=lastLoc.satelites
        loc.sos=lastLoc.sos
        //loc.Version=lastLoc.Version;
        loc.volt=lastLoc.volt
        return loc
    }
}
