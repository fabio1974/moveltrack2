package br.com.moveltrack2.backend.Iugu.model

import com.fasterxml.jackson.annotation.JsonValue

enum class Event private constructor(@get:JsonValue
                                     var value: String?) {

    invoice_created("invoice.created"),
    invoice_status_changed("invoice.status_changed")


}
