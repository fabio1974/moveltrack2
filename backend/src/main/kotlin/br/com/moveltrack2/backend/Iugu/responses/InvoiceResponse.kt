package br.com.moveltrack2.backend.Iugu.responses

import java.io.Serializable
import java.util.Date


import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty


@JsonIgnoreProperties(ignoreUnknown = true)
class InvoiceResponse {

    //Response response;

    var id: String? = null

    @JsonProperty("due_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    var dueDate: String? = null

    var currency: String? = null

    @JsonProperty("discount_cents")
    var discountCents: Int? = null

    var email: String? = null

    @JsonProperty("items_total_cents")
    var itemsTotalCents: Int? = null

    @JsonProperty("notification_url")
    var notificationUrl: String? = null

    @JsonProperty("return_url")
    var returnUrl: String? = null

    var status: String? = null

    @JsonProperty("tax_cents")
    var taxCents: Int? = null

    @JsonProperty("updated_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    var updatedAt: Date? = null

    @JsonProperty("total_cents")
    var totalCents: Int? = null

    @JsonProperty("paid_at")
    var paidAt: Date? = null

    @JsonProperty("secure_id")
    var secureId: String? = null

    @JsonProperty("secure_url")
    var secureUrl: String? = null

    @JsonProperty("customer_id")
    var customerId: String? = null

    @JsonProperty("user_id")
    var userId: Long? = null

    @JsonProperty("total")
    var total: String? = null

    @JsonProperty("total_paid")
    var totalPaid: String? = null

    @JsonProperty("total_on_occurrence_day")
    var totalOnOccurrenceDay: String? = null

    @JsonProperty("taxes_paid")
    var taxesPaid: String? = null

    var interest: String? = null

    var discount: String? = null

    var refundable: Boolean? = null

    var installments: Boolean? = null

    @JsonProperty("bank_slip")
    var bankSlip: BankSlipResponse? = null

    var items: List<ItemResponse>? = null

    var variables: List<VariableResponse>? = null

    /*
	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}
*/

    var logs: List<LogResponse>? = null

}