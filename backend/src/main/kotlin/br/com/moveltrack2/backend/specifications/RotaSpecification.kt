package br.com.moveltrack2.backend.specifications

import br.com.moveltrack.persistence.domain.*
import br.com.moveltrack.persistence.repositories.ChipRepository
import br.com.moveltrack.persistence.repositories.RotaRepository
import org.springframework.data.jpa.domain.Specification
import org.springframework.util.StringUtils
import java.util.*
import javax.persistence.criteria.Predicate


object RotaSpecification {


    fun getFiltro(filtro: Rota, repo: RotaRepository): Specification<Rota> {
        return Specification { root, query, builder ->
            val predicates = ArrayList<Predicate>()

            if(filtro.destino!=null){
                predicates.add(builder.equal(root.get<Municipio>("destino"), filtro.destino))
            }

            if(filtro.origem!=null){
                predicates.add(builder.equal(root.get<Municipio>("origem"), filtro.origem))
            }

            if(filtro.cliente!=null){
                predicates.add(builder.equal(root.get<Cliente>("cliente"), filtro.cliente))
            }



/*
            if (!StringUtils.isEmpty(filtro.iccid)) {
                val d = "%" + filtro.iccid!!.trim { it <= ' ' } + "%"
                predicates.add(builder.like(root.get("iccid"), d))
            }
            if (!StringUtils.isEmpty(filtro.numero)) {
                val d = "%" + filtro.numero!!.trim { it <= ' ' } + "%"
                predicates.add(builder.like(root.get("numero"), d))
            }
            if (filtro.operadora!=null) {
                predicates.add(builder.equal(root.get<Operadora>("operadora"), filtro.operadora))
            }
            if (filtro.status!=null) {
                predicates.add(builder.equal(root.get<ChipStatus>("status"), filtro.status))
            }
*/

            builder.and(*predicates.toTypedArray())
        }
    }


}
