package br.com.moveltrack2.backend.repositories

import br.com.moveltrack.persistence.domain.*
import br.com.moveltrack.persistence.pojos.AtrasoUnidade
import br.com.moveltrack.persistence.pojos.RelatorioSinalFiltro
import br.com.moveltrack.persistence.repositories.*
import br.com.moveltrack.persistence.util.cell
import br.com.moveltrack.persistence.util.header
import br.com.moveltrack2.backend.utils.GoogleGeo
import br.com.moveltrack2.backend.utils.LOCAL_ZONE
import com.lowagie.text.pdf.PdfPTable
import com.lowagie.text.pdf.PdfTable
import org.apache.commons.lang3.time.DurationFormatUtils
import org.springframework.stereotype.Component
import java.sql.Timestamp
import java.time.Duration
import java.time.ZonedDateTime
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Component
class RelatorioSinais constructor(
        @PersistenceContext var em: EntityManager,
        val veiculoRepository: VeiculoRepository,
        val mBoletoRepository: MBoletoRepository,
        val equipamentoRepository: EquipamentoRepository,
        val locationRepository: LocationRepository,
        val contratoRepository: ContratoRepository,
        val googleGeo: GoogleGeo
) {


    fun getRelatorioSinais( filtro: RelatorioSinalFiltro): List<RelatorioSinaisData>?{

        val minMinutos = when(filtro.atrasoMinimoUnidade){
            AtrasoUnidade.MINUTOS -> filtro.atrasoMinimo
            AtrasoUnidade.HORAS -> 60 * filtro.atrasoMinimo
            AtrasoUnidade.DIAS -> 1440 * filtro.atrasoMinimo
            else -> 0
        }

        val maxMinutos = when(filtro.atrasoMaximoUnidade){
            AtrasoUnidade.MINUTOS -> filtro.atrasoMaximo
            AtrasoUnidade.HORAS -> 60 * filtro.atrasoMaximo
            AtrasoUnidade.DIAS -> 1440 * filtro.atrasoMaximo
            else -> 0
        }

        val inicio = if(maxMinutos==0) ZonedDateTime.now().minusYears(10) else ZonedDateTime.now().minusMinutes(maxMinutos.toLong())
        val fim = ZonedDateTime.now().minusMinutes(minMinutos.toLong())

        val sql = ("select e.imei,e.senha, e.modelo, ch.numero, ch.operadora, e.situacao,sinal.dateLocation, c.status, v.placa, v.marcaModelo, v.tipo, p.nome, c.id" +
                "                from (select max(l2.dateLocation) as dateLocation, l2.imei from location l2 group by l2.imei) as sinal" +
                "                inner join equipamento e on e.imei = sinal.imei" +
                "                left join chip ch on e.chip_id = ch.id" +
                "                inner join veiculo v on v.equipamento_id = e.id" +
                "                inner join contrato c on v.contrato_id = c.id" +
                "                inner join pessoa p on c.cliente_id = p.id" +
                " where 1 = 1 " +
                (if(filtro.equipamento!=null) " and e.imei =:imei " else "" )+
                (if(filtro.chip!=null) " and ch.numero =:numero" else "") +
                (if(filtro.operadora!=null) " and ch.operadora =:operadora" else "") +
                (if(filtro.placa!=null) " and v.placa like :placa" else "") +
                (if(filtro.veiculoTipo!=null) " and v.tipo =:veiculoTipo" else "") +
                (if(filtro.cliente!=null)" and p.id =:pessoaId" else "") +
                (if(filtro.modeloRastreador!=null)" and e.modelo =:eModelo" else "") +
                (if(filtro.equipamentoSituacao!=null)" and e.situacao =:eSituacao" else "") +
                (if(filtro.veiculoStatus!=null)" and v.status =:veiculoStatus" else "") +
                (if(filtro.contratoStatus!=null)" and c.status =:contratoStatus" else "") +
                (if(filtro.atrasoMaximo>0)" and sinal.dateLocation >:inicio" else "") +
                (if(filtro.atrasoMinimo>0)" and sinal.dateLocation <:fim" else "") +
                " order by sinal.dateLocation desc")

        val query = em.createNativeQuery(sql)
        if(filtro.equipamento!=null) query.setParameter("imei",filtro.equipamento?.imei)
        if(filtro.chip!=null) query.setParameter("numero",filtro.chip?.numero)
        if(filtro.operadora!=null) query.setParameter("operadora",filtro.operadora?.name)
        if(filtro.placa!=null) query.setParameter("placa","%${filtro.placa}%")
        if(filtro.veiculoTipo!=null) query.setParameter("veiculoTipo",filtro.veiculoTipo?.name)
        if(filtro.cliente!=null) query.setParameter("pessoaId",filtro.cliente?.id)
        if(filtro.modeloRastreador!=null) query.setParameter("eModelo",filtro.modeloRastreador?.name)
        if(filtro.equipamentoSituacao!=null) query.setParameter("eSituacao",filtro.equipamentoSituacao?.name)
        if(filtro.veiculoStatus!=null) query.setParameter("veiculoStatus",filtro.veiculoStatus?.name)
        if(filtro.contratoStatus!=null) query.setParameter("contratoStatus",filtro.contratoStatus?.name)
        if(filtro.atrasoMaximo>0) query.setParameter("inicio",inicio)
        if(filtro.atrasoMinimo>0) query.setParameter("fim",fim)

        val r = query.resultList.toList().map { it ->
            var a = it as Array<Any?>
            RelatorioSinaisData(
                    imei = a[0] as String,
                    senha= a[1] as String?,
                    eModelo = ModeloRastreador.valueOf(a[2] as String),
                    chipNumero =  a[3] as String?,
                    operadora = if(a[4] as String?!=null) Operadora.valueOf(a[4] as String) else null,
                    eSituacao = EquipamentoSituacao.valueOf(a[5] as String),
                    dateLocation = a[6] as Timestamp,
                    contratoStatus = ContratoStatus.valueOf(a[7] as String),
                    placa = a[8] as String?,
                    marcaModelo =  a[9] as String?,
                    veiculoTipo = VeiculoTipo.valueOf(a[10] as String),
                    nome = a[11] as String?,
                    contratoId = a[12] as Int?
            )
        }
        em.close()
        return r
    }


    fun getTable(rel: List<RelatorioSinaisData>?): PdfPTable? {

        val table = PdfPTable(67)
        table.setHorizontalAlignment(PdfPTable.ALIGN_CENTER)
        table.setWidthPercentage(100f)
        table.setSpacingBefore(20f)

        table.addCell(header("Imei", 7, PdfTable.ALIGN_CENTER))
        table.addCell(header("Senha", 3, PdfTable.ALIGN_CENTER))
        table.addCell(header("Modelo", 3, PdfTable.ALIGN_CENTER))
        table.addCell(header("Número", 6, PdfTable.ALIGN_CENTER))
        table.addCell(header("OP", 2, PdfTable.ALIGN_CENTER))
        table.addCell(header("Eq. Status", 6, PdfTable.ALIGN_CENTER))
        table.addCell(header("GPS", 5, PdfTable.ALIGN_CENTER))
        table.addCell(header("Contr. Status", 4, PdfTable.ALIGN_CENTER))
        table.addCell(header("Placa", 4, PdfTable.ALIGN_CENTER))
        table.addCell(header("Modelo", 8, PdfTable.ALIGN_CENTER))
        table.addCell(header("Tipo", 7, PdfTable.ALIGN_CENTER))
        table.addCell(header("Nome", 12, PdfTable.ALIGN_CENTER))
        var count=0
        rel?.forEach {
            count++
            val devendo = mBoletoRepository.countAllByContrato_IdAndSituacao(it.contratoId!!,MBoletoStatus.VENCIDO)
            val bgDark = devendo!! > 0

            val zdt = ZonedDateTime.ofInstant(it.dateLocation?.toInstant(), LOCAL_ZONE)
            val diff = Duration.between( zdt , ZonedDateTime.now())
            val diffStr = DurationFormatUtils.formatDuration(diff.toMillis(), "dd:HH:mm:ss");

            val op = when(it.operadora){
                Operadora.CLARO-> "C"
                Operadora.LINK_CLARO->"LC"
                Operadora.LINK_TIM->"LT"
                Operadora.LINK_VIVO->"LV"
                Operadora.OI->"OI"
                Operadora.TIM->"T"
                Operadora.TRANSMEET_VIVO->"TV"
                Operadora.VIVO->"V"
                Operadora.VODAFONE->"VO"
                else -> ""
            }

            table.addCell(cell(PdfTable.ALIGN_CENTER, it.imei?:"",7,bgDark))
            table.addCell(cell(PdfTable.ALIGN_CENTER, it.senha?:"",3,bgDark))
            table.addCell(cell(PdfTable.ALIGN_CENTER, it.eModelo.toString(),3,bgDark))
            table.addCell(cell(PdfTable.ALIGN_CENTER, it.chipNumero?:"",6,bgDark))
            table.addCell(cell(PdfTable.ALIGN_CENTER, op,2,bgDark))
            table.addCell(cell(PdfTable.ALIGN_CENTER, it.eSituacao.toString(),6,bgDark))
            table.addCell(cell(PdfTable.ALIGN_CENTER, diffStr,5,bgDark))
            table.addCell(cell(PdfTable.ALIGN_CENTER, it.contratoStatus.toString(),4,bgDark))
            table.addCell(cell(PdfTable.ALIGN_CENTER, it.placa?:"",4,bgDark))
            table.addCell(cell(PdfTable.ALIGN_CENTER, it.marcaModelo?:"",8,bgDark))
            table.addCell(cell(PdfTable.ALIGN_CENTER, it.veiculoTipo.toString(),7,bgDark))
            table.addCell(cell(PdfTable.ALIGN_CENTER, it.nome?:"",12,bgDark))
        }
        table.addCell(header("Quantidade de Registros: ${count}",67,PdfTable.ALIGN_CENTER))
        return table
    }

}

data class RelatorioSinaisData(
        val imei: String?=null,
        val senha: String?=null,
        val eModelo: ModeloRastreador?=null,
        val chipNumero: String?=null,
        val operadora: Operadora?=null,
        val eSituacao: EquipamentoSituacao?=null,
        val dateLocation: Timestamp?=null,
        val contratoStatus: ContratoStatus?=null,
        val placa: String?=null,
        val marcaModelo: String?=null,
        val veiculoTipo: VeiculoTipo?=null,
        val nome: String?=null,
        val contratoId: Int?=null
)
