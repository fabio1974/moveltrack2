package br.com.moveltrack2.backend.specifications

import java.util.ArrayList

import javax.persistence.criteria.Predicate

import br.com.moveltrack.persistence.domain.Contrato
import br.com.moveltrack.persistence.domain.Empregado
import br.com.moveltrack.persistence.domain.MBoleto
import br.com.moveltrack.persistence.domain.MBoletoStatus
import br.com.moveltrack.persistence.repositories.ContratoRepository
import org.springframework.data.jpa.domain.Specification
import org.springframework.util.StringUtils


object BoletoSpecification {


    fun getFiltro(filtro: MBoleto, repo: ContratoRepository): Specification<MBoleto> {
        return Specification { root, query, builder ->
            val predicates = ArrayList<Predicate>()

            if (filtro.contrato?.cliente?.id != null) {
                val contrato = repo.findByCliente(filtro.contrato!!.cliente!!)
                predicates.add(builder.equal(root.get<Contrato>("contrato"), contrato))
            }

            if (!StringUtils.isEmpty(filtro.nossoNumero)) {
                val d = "%" + filtro.nossoNumero!!.trim { it <= ' ' } + "%"
                predicates.add(builder.like(root.get("nossoNumero"), d))
            }

            if(filtro.situacao!=null){
                predicates.add(builder.equal(root.get<MBoletoStatus>("situacao"), filtro.situacao))
            }

            if(filtro.emissor!=null){
                predicates.add(builder.equal(root.get<Empregado>("emissor"), filtro.emissor))
            }

            builder.and(*predicates.toTypedArray())
        }
    }


}
