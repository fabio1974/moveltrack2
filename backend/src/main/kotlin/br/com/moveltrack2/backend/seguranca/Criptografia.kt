package br.com.moveltrack2.backend.seguranca

import org.bouncycastle.util.encoders.Base64Encoder

import java.io.IOException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.Base64


class Criptografia {

    private var md: MessageDigest? = null
    //private Base64 encoder = new Base64Encoder();

    fun calculaHash(texto: String): String? {
        var r: String? = null
        try {
            md = MessageDigest.getInstance("MD5")
            val bytes = texto.toByteArray()
            val hash = md!!.digest(bytes)
            r = Base64.getEncoder().encodeToString(hash)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return r
    }

}// TODO Auto-generated constructor stub