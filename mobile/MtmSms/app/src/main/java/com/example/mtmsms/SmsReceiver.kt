package com.example.mtmsms

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.provider.Telephony
import android.telephony.SmsMessage

/*
 * Step 1: Create a broadcastReceiver class called SMSReceiver
 * */
class SmsReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.extras != null) {

            /*
             * Step 2: We need to fetch the incoming sms that is broadcast.
             * For this we check the intent of the receiver.
             * */
            val pdus =  intent.extras!!["pdus"] as Array<Any>?
            for (i in pdus!!.indices) {
                val smsMessage =    if (Build.VERSION.SDK_INT >= 19) Telephony.Sms.Intents.getMessagesFromIntent(intent)[i]
                                    else SmsMessage.createFromPdu(pdus[i] as ByteArray)

                /*
                 * Step 3: We can get the sender & body of the incoming sms.
                 * The actual parsing of otp is not done here since that is not
                 * the purpose of this implementation
                 *  */
                val sender = smsMessage.originatingAddress
                val body = smsMessage.messageBody.toString()
                val otpCode = "123456"

                /*
                 * Step 4: We have parsed the otp. Now we can create an intent
                 * and pass the otp data via that intent.
                 * We have to specify an action for this intent. Now this can be anything String.
                 * This action is important because this action identifies the broadcast event
                 *  */
                val broadcastIntent = Intent()
                broadcastIntent.action = MainActivity.mBroadcastStringAction
                broadcastIntent.putExtra("Data", "$sender-$body")
                context.sendBroadcast(broadcastIntent)
            }
        }
    }


}