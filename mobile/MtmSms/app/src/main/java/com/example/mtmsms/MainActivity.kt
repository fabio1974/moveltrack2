package com.example.mtmsms


import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.Telephony
import android.telephony.SmsMessage
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.mtmsms.domain.Sms
import com.example.mtmsms.domain.SmsStatus
import com.example.mtmsms.domain.SmsTipo
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity(){

    private val MY_PERMISSIONS_REQUEST_SEND_SMS = 0
    private val opcoes = ArrayList<String>()
    private var adaptador: ArrayAdapter<String>? = null
    private val jacksonMapper = ObjectMapper()
    private var lvOpcoes: ListView? = null
    private val mIntentFilter: IntentFilter = IntentFilter()

    companion object {
        const val mBroadcastStringAction: String = "mBroadcastStringAction"
        const val URL = "http://moveltrack-gps.net/api/smsEsperandoDosUltimosMinutos/60"
        const val URL2 = "http://moveltrack-gps.net/api/smsRecebido"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        jacksonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))
        checkAndRequestPermission()

        setAdapter()

        mIntentFilter.addAction(mBroadcastStringAction)
        val serviceIntent = Intent(this, BroadcastService::class.java)
        startService(serviceIntent)

        registerReceiver(serviceReceiver, mIntentFilter)

        val filter = IntentFilter(Telephony.Sms.Intents.SMS_RECEIVED_ACTION)
        registerReceiver(smsReceiver,filter)

        addMessage("iniciando...")

    }


   /* override fun onResume() {
        super.onResume()
        registerReceiver(mReceiver, mIntentFilter)
    }*/

    private val serviceReceiver : BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action == mBroadcastStringAction)
                addMessage(intent.getStringExtra("Data"))
        }
    }

    private val smsReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.extras != null) {
                val pdus =  intent.extras!!["pdus"] as Array<*>?

                for (i in pdus!!.indices) {
                    var smsMessage: SmsMessage? =  null

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        val format = intent.extras!!["format"] as String
                        smsMessage  =  SmsMessage.createFromPdu(pdus[i] as ByteArray,format)
                    } else {
                        smsMessage =   SmsMessage.createFromPdu(pdus[i] as ByteArray)
                    }

                    val sender = smsMessage?.originatingAddress
                    val body = smsMessage?.messageBody.toString()
                    addMessage("$sender-$body")

                    val celular = "+55${sender?.substring(sender.length-11)}"
                    val sms = Sms(celular = celular,mensagem = body,tipo = SmsTipo.CHECAR,status = SmsStatus.RECEBIDO)
                    sendReceivedSmsToServer(sms)
                }
            }
        }
    }

    fun sendReceivedSmsToServer(sms: Sms){
        val requestQueue = Volley.newRequestQueue(this.applicationContext)
        val str = jacksonMapper.writeValueAsString(sms)
        val jsonObject = JSONObject(str)
        val request: JsonObjectRequest =
            JsonObjectRequest(
                Request.Method.POST, URL2, jsonObject,
                Response.Listener<JSONObject?> { response ->
                    addMessage("FOi")
                },
                Response.ErrorListener { error ->
                    addMessage("Erro")
                }
            )
        requestQueue.add(request)
    }



   /* override fun onPause() {
        unregisterReceiver(mReceiver)
        super.onPause()
    }*/

    fun addMessage(message: String){
        if(adaptador?.count!!>=8){
            opcoes.removeAt(0)
            setAdapter()
        }
        val sdf = SimpleDateFormat("HH:mm:ss")
        val time = sdf.format(Date())
        adaptador?.add("$time - ${message}")
    }

    fun setAdapter(){
        adaptador = ArrayAdapter<String>(this@MainActivity,android.R.layout.simple_list_item_1,opcoes)
        lvOpcoes = findViewById(R.id.lvopcoes)
        lvOpcoes?.setAdapter(adaptador)
    }

    protected fun checkAndRequestPermission() {
        if (ContextCompat.checkSelfPermission(this,Manifest.permission.SEND_SMS)!= PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(this,Manifest.permission.RECEIVE_SMS)!= PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.SEND_SMS) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.RECEIVE_SMS)){
            } else {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.SEND_SMS,Manifest.permission.RECEIVE_SMS),MY_PERMISSIONS_REQUEST_SEND_SMS)
            }
        }
    }

}