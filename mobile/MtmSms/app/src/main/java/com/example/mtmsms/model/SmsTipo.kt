package com.example.mtmsms.domain

enum class SmsTipo {
    BLOQUEIO, DESBLOQUEIO, REINICIAR, CHECAR, AVISO, DNS
}
