package com.example.mtmsms.domain

enum class SmsStatus {
    ESPERANDO, ENVIADO, RECEBIDO, DESCARTADO, ESPERANDO_SOCKET
}
