package com.example.mtmsms.domain

import java.time.ZonedDateTime
import java.util.*


class Sms (
    var id: Int? = null,
    val celular: String? = null,
    val mensagem: String? = null,
    val imei: String? = null,
    val tipo: SmsTipo? = null,
    var status: SmsStatus? = null,
    val dataUltimaAtualizacao: Date? = null
)
