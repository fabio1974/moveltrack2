package com.example.mtmsms

import android.annotation.TargetApi
import android.app.Activity
import android.app.PendingIntent
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.IBinder
import android.telephony.SmsManager
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.mtmsms.domain.Sms
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import org.json.JSONException
import java.util.*

class BroadcastService : Service() {
    private var LOG_TAG: String? = null
    private var mList: ArrayList<String>? = null

    val smsManager: SmsManager = SmsManager.getDefault()
    val jacksonMapper = ObjectMapper()

    override fun onCreate() {
        super.onCreate()
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Thread(Runnable {
            getList()
        }).start()
        return START_REDELIVER_INTENT
    }

    fun sendBroadcastMessage(m: String) {
        val broadcastIntent = Intent()
        broadcastIntent.action = MainActivity.mBroadcastStringAction
        broadcastIntent.putExtra("Data", m)
        sendBroadcast(broadcastIntent)
    }

    fun getList(){
        val requestQueue = Volley.newRequestQueue(this.applicationContext)
        val jsonArrayRequest = JsonObjectRequest(
            Request.Method.GET,
            MainActivity.URL,
            null,
            Response.Listener { response ->
                try {
                    val sms = jacksonMapper.readValue(response.toString(), object : TypeReference<Sms>(){})
                    if(sms!=null) {
                        sendSMS(sms)
                        sendBroadcastMessage("disparando para ${sms.celular}, tipo=${sms.tipo} ")
                    } else
                        sendBroadcastMessage("Não trouxe Sms!")
                } catch (e: JSONException) {
                    sendBroadcastMessage(e.message?:"null")
                    e.printStackTrace()
                }
                getList()
            },
            Response.ErrorListener { // Do something when error occurred
                val message = if(it.message !=null && it.message!!.contains("End of input at character 0"))  "sem Sms..." else it.message
                sendBroadcastMessage(message?:"erro sem mensagem")
                //it.printStackTrace()
                getList()
            }
        )
        Thread.sleep(10000)
        requestQueue.add(jsonArrayRequest)
    }


    private fun sendSMS(sms: Sms) {
        try {

            val mensagem ="${sms.celular}, imei ${sms.mensagem}, tipo ${sms.tipo}"

            val SENT = "SMS_SENT_${sms.id}"
            val DELIVERED = "SMS_DELIVERED_${sms.id}"

            val intentSent = Intent(SENT)
            intentSent.putExtra("id",mensagem)

            val intentDelivered = Intent(SENT)
            intentDelivered.putExtra("id",mensagem)

            val sentPI = PendingIntent.getBroadcast(this, 0,intentSent, 0)
            val deliveredPI = PendingIntent.getBroadcast(this, 0,intentDelivered, 0)

            //---when the SMS has been sent---
            registerReceiver(object : BroadcastReceiver() {
                override fun onReceive(
                    arg0: Context,
                    arg1: Intent
                ) {
                    val msg = arg1.getStringExtra("id")
                    when (resultCode) {
                        Activity.RESULT_OK -> sendBroadcastMessage("SMS ENVIADO=>$msg")
                        SmsManager.RESULT_ERROR_GENERIC_FAILURE ->sendBroadcastMessage("GENERIC_FAILURE=>$msg")
                        SmsManager.RESULT_ERROR_NO_SERVICE ->sendBroadcastMessage("NO_SERVICE=>$msg")
                        SmsManager.RESULT_ERROR_NULL_PDU ->sendBroadcastMessage("NULL_PDU=>$msg")
                        SmsManager.RESULT_ERROR_RADIO_OFF ->sendBroadcastMessage("RADIO_OFF=>$msg")
                    }
                }
            }, IntentFilter(SENT))

            //---when the SMS has been delivered---
            registerReceiver(object : BroadcastReceiver() {
                override fun onReceive(
                    arg0: Context,
                    arg1: Intent
                ) {
                    val msg = arg1.getStringExtra("id")
                    when (resultCode) {
                        Activity.RESULT_OK ->sendBroadcastMessage("SMS RECEBIDO NO RASTREADOR=>$msg")
                        Activity.RESULT_CANCELED ->sendBroadcastMessage("RECEBIMENTO SEM RESPOSTA=>$msg")
                    }
                }
            }, IntentFilter(DELIVERED))
            smsManager.sendTextMessage(sms.celular, null, sms.mensagem, sentPI, deliveredPI)
        }catch (e:Exception){
            e.printStackTrace()
        }
    }



    override fun onBind(intent: Intent): IBinder? {
        // Wont be called as service is not bound
        Log.i(LOG_TAG, "In onBind")
        return null
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    override fun onTaskRemoved(rootIntent: Intent) {
        super.onTaskRemoved(rootIntent)
        Log.i(LOG_TAG, "In onTaskRemoved")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(LOG_TAG, "In onDestroy")
    }
}