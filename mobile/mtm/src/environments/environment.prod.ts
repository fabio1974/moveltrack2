export const environment = {
  production: true,
  //apiUrl:'http://138.197.235.221:8080/api',
  apiUrl:'http://159.89.254.203:8080/api',
  //apiUrl:'/api',
  name: 'em construção...',
  //whitelistedDomains: ["138.197.235.221:8080"],
  whitelistedDomains: ['159.89.254.203:8080'],
  //whitelistedDomains: [new RegExp('/api')],
  blacklistedRoutes: [new RegExp('/login')],
};
