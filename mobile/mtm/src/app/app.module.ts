import {LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {GoogleMaps} from '@ionic-native/google-maps';
import {SharedModule} from './shared/shared.module';


import {JWT_OPTIONS, JwtModule} from '@auth0/angular-jwt';
import {IonicStorageModule, Storage} from '@ionic/storage';
import {HttpClientModule} from '@angular/common/http';
import {environment} from '../environments/environment';

import localePt from '@angular/common/locales/pt';
import {registerLocaleData} from '@angular/common';



registerLocaleData(localePt);


export function jwtOptionsFactory(storage) {
  return {
    tokenGetter: () => {
      return storage.get('token');
    },
    whitelistedDomains: environment.whitelistedDomains
  }
}

@NgModule({
  declarations: [AppComponent  ],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    SharedModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    JwtModule.forRoot({
      jwtOptionsProvider: {
        provide: JWT_OPTIONS,
        useFactory: jwtOptionsFactory,
        deps: [Storage],
      }
    })],
  providers: [
    StatusBar,
    SplashScreen,
    GoogleMaps,
    {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
    { provide: LOCALE_ID, useValue: 'pt-BR' }
  ],
  exports: [

  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
