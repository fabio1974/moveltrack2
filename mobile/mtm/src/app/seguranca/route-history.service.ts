import { Injectable } from '@angular/core';
import {Storage} from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class RouteHistoryService {

  constructor(private storage: Storage) { }

  savePriviousRoute(state) {
    try {

      this.storage.get('currentUrl').then(c=> {
        this.storage.set('previousUrl',c)
        this.storage.set("currentUrl",state.url)
        this.storage.get('currentUrl').then(
            current => {
              this.storage.get('previousUrl').then(
                  previous => {
                    if (current == previous)
                      this.storage.set('previousUrl', '/home');
                  })
            })
      })

    } catch (e) {
      console.log("COM ERRO")
      this.storage.set('previousUrl', '/home');
    }

  }



}
