import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {SegurancaService} from './seguranca.service';
import {RouteHistoryService} from './route-history.service';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Storage} from '@ionic/storage';
import {SideMenuService} from '../shared/services/side-menu.service';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private segurancaService:SegurancaService,
              private routeHistoryService:RouteHistoryService,
              private sideMenuService: SideMenuService,
              private storage: Storage,
              private router: Router){
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)   :Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if(this.router.navigated){
      this.routeHistoryService.savePriviousRoute(state)
    }


    return  this.storage.get('token').then((token) => {
        let decodedToken = new JwtHelperService().decodeToken(token) || this.segurancaService.decodedToken;
        if(decodedToken && decodedToken.sub) {
            return true
        }else{
          console.log("Não logado! redirecionando para login")
          this.router.navigate(['/login']);
          return false
        }
    });


  }

}
