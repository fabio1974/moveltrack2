import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SegurancaService} from './seguranca.service';
import {RouteHistoryService} from './route-history.service';

import {JWT_OPTIONS, JwtModule} from '@auth0/angular-jwt';
import {Storage} from '@ionic/storage';


export function jwtOptionsFactory(storage) {
    return {
        tokenGetter: () => {

            let token = storage.get('token');
            console.log("VAI SE FUDER", token)
            return token

        }
    }
}

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        JwtModule.forRoot({
            jwtOptionsProvider: {
                provide: JWT_OPTIONS,
                useFactory: jwtOptionsFactory,
                deps: [Storage]
            }
        })

    ],
    providers: [
        SegurancaService,
        RouteHistoryService,
    ]
})
export class SegurancaModule {}
