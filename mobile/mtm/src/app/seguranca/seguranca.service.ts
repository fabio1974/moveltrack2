import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Storage} from '@ionic/storage';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Cliente, Perfil, PerfilTipo, Pessoa, User, Usuario, Veiculo} from '../shared/model';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {Router} from '@angular/router';

import {json} from '@angular-devkit/core';
import {appPages, SideMenuService} from '../shared/services/side-menu.service';
import {DataService} from '../shared/services/data.service';
import {SearchService} from '../shared/services/search.service';
import {MapaService} from '../home/mapa.service';
import {WaitPopupService} from '../shared/services/wait-popup.service';
import {AlertController} from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class SegurancaService {

    decodedToken;

    constructor(
        private http: HttpClient,
        private router: Router,
        private sideMenuService: SideMenuService,
        private mapaService: MapaService,
        private dataService: DataService,
        private searchService : SearchService,
        private waitPopupService: WaitPopupService,
        private alertController: AlertController
    ) {

        this.dataService.getToken().then((token) => {
            if (token) {
                this.decodedToken = new JwtHelperService().decodeToken(token);
                console.log('decodedToken', this.decodedToken);
            }
        });


    }

    // login2(user:User):Promise<HTTPResponse>{
    //this.http.setDataSerializer("json")
    //return this.http.post(`${environment.apiUrl}/login`,user,{'Content-Type': 'application/json'})
    //return this.http2.post(`${environment.apiUrl}/login`,user,{'Content-Type': 'application/json'})
    //}

    login(user: User): Observable<any> {
        return this.http.post<User>(`${environment.apiUrl}/login`, user);
    }


    isTokenExpired() {
        this.dataService.getToken().then((token) => {
            if (token) {
                return new JwtHelperService().isTokenExpired(token);
            }
            return true;
        });


    }

    logout() {
        this.dataService.clear()
        this.dataService.veiculo = null
        this.dataService.veiculos = null
        this.dataService.cliente = null
        this.dataService.clientes = null
        this.sideMenuService.resetMenu()
        this.decodedToken = null;
        //window.location.replace('/login')
        this.router.navigate(['/login']);
    }




    pessoaLogada() {
        let pessoa = new Pessoa();
        let usuario = new Usuario();
        usuario.nomeUsuario = this.decodedToken.sub;
        usuario.perfil = new Perfil();
        usuario.perfil.tipo =  this.decodedToken.perfil as PerfilTipo;
        pessoa.nome = this.decodedToken.nome;
        pessoa.cpf = this.decodedToken.cpf;
        pessoa.usuario = usuario;
        pessoa.id = this.decodedToken.id;
        return pessoa;
    }

    isLogado() {
        this.dataService.getToken().then((token) => {
            if (token) {
                this.decodedToken = new JwtHelperService().decodeToken(token);
                return this.decodedToken && this.decodedToken.sub;
            }
            return false;
        });
    }






    setMenuFromToken(token: any) {


        this.decodedToken = new JwtHelperService().decodeToken(token);
        this.dataService.pessoaLogada = this.pessoaLogada();

        console.log('this.pessoaLogada()', this.pessoaLogada());

        if (this.pessoaLogada().isCliente()) {
            this.dataService.cliente = this.pessoaLogada() as Cliente;
            this.sideMenuService.removeMenu('/clientes');
            this.waitPopupService.present('...carregando veíuclos');
            this.searchService.findVeiculosAtivosByCliente(this.pessoaLogada() as Cliente).subscribe(
                resp => {
                    this.waitPopupService.dismiss()
                    let veiculos = resp as Veiculo[];
                    this.dataService.veiculos = veiculos;
                    if (veiculos.length >= 1) {
                        this.dataService.veiculo = veiculos[0];
                        console.log(this.dataService.veiculo);
                        this.mapaService.startRastreamento();
                    }

                    if (veiculos.length == 1) {
                        this.sideMenuService.removeMenu('/veiculos');
                    }
                },
                error1 => {
                    console.log(error1);
                    this.waitPopupService.dismiss();
                }
            )
        }
    }
}
