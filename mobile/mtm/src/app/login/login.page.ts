import {Component} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';
import {ToastService} from '../shared/services/toast.service';
import {User} from '../shared/model';
import {JwtHelperService} from '@auth0/angular-jwt';
import {SegurancaService} from '../seguranca/seguranca.service';
import {DataService} from '../shared/services/data.service';
import {WaitPopupService} from '../shared/services/wait-popup.service';
import {AlertController} from '@ionic/angular';




@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})

export class LoginPage {

    user = new User();
    erro

    constructor(
        private segurancaService: SegurancaService,
        private toastService: ToastService,
        private router: Router,
        private waitPopupService: WaitPopupService,
        private dataService: DataService,
        private alertController: AlertController
    ){}

    ionViewWillEnter() {
        console.log("Entrou no login")
        this.segurancaService.logout();
    }





    submitForm(form: NgForm) {
        if (!form.valid) {
            this.toastService.presentToast('Digite Usuário e Senha!', 'primary');
            return;
        }

        if (!navigator.onLine) {
            this.alertaInternet()
            return
        }

        this.waitPopupService.present("...aguarde login")

        this.segurancaService.login(this.user).subscribe(
            resp => {

                this.waitPopupService.dismiss()
                let token = resp.token// JSON.parse(resp.data).token
                this.segurancaService.decodedToken = new JwtHelperService().decodeToken(token);
                console.log("decodedToken",this.segurancaService.decodedToken)

                this.dataService.setToken(token).then(
                    ()=> {
                        this.segurancaService.setMenuFromToken(token)
                        this.router.navigate(['/home']);
                    }
                );

            },
            error => {
                this.waitPopupService.dismiss()
                this.erro = JSON.stringify(error)
                console.log('erro login:', this.erro);
                this.toastService.presentToast("Usuário ou Senha Inválida", 'danger')
            }
        )


    }


    async alertaInternet() {
        const alert = await this.alertController.create({
            mode: 'ios',
            header: 'Sem Internet!',
            message: "O aplicativo não funciona sem internet. Verifique o wi-fi ou o plano de sua operadora",
            buttons: [{text: 'Ok',role: 'cancel'}]
        });
        await alert.present();
    }

}
