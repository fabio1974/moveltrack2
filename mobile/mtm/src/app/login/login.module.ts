import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {LoginPage} from './login.page';
import {SharedModule} from '../shared/shared.module';
import {HttpClientModule} from '@angular/common/http';




const routes: Routes = [
    {
        path: '',
        component: LoginPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        SharedModule,
        HttpClientModule

    ],
    declarations: [LoginPage],

    providers:[

    ]




})
export class LoginPageModule {
}
