import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './seguranca/auth.guard';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  { path: 'login',
    loadChildren: './login/login.module#LoginPageModule',
    //canActivate: [AuthGuard]
   },

  { path: 'home',
    loadChildren: './home/home.module#HomePageModule',
    canActivate: [AuthGuard]
   },

  { path: 'clientes',
    loadChildren: './clientes/clientes.module#ClientesPageModule',
    canActivate: [AuthGuard]
   },

  { path: 'veiculos',
    loadChildren: './veiculos/veiculos.module#VeiculosPageModule',
    canActivate: [AuthGuard]
  },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
