import {Component, OnInit} from '@angular/core';

import {AlertController, MenuController, Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {SideMenuService} from './shared/services/side-menu.service';
import {NavigationEnd, Router, RouterEvent} from '@angular/router';
import {SegurancaService} from './seguranca/seguranca.service';
import {DataService} from './shared/services/data.service';
import {ToastService} from './shared/services/toast.service';


@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {


    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private toastService: ToastService,
        public sideMenuService: SideMenuService,
        private menuCtrl: MenuController,
        private router: Router,
        private segurancaService: SegurancaService,
        public dataService: DataService,
        public alertController: AlertController
    ) {
            window.addEventListener('offline', () => {
                this.segurancaService.logout()
                this.alertaInternet()
            });
            this.initializeApp()
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            this.dataService.getToken().then(token => {if(token) this.segurancaService.setMenuFromToken(token)})
        });
    }

    ngOnInit(): void {

        this.router.events.subscribe((event: RouterEvent) => {
            if (event instanceof NavigationEnd) {
                if (event.url === '/login') {
                    this.menuCtrl.enable(false);
                } else {
                    this.menuCtrl.enable(true);
                }
            }
        });


    }


    async alertaInternet() {
        const alert = await this.alertController.create({
            mode: 'ios',
            header: 'Sem Internet',
            message: "O aplicativo não funciona sem internet. Verifique seu wi-fi ou o plano de sua operadora",
            buttons: [{text: 'Ok',role: 'cancel'},
            ]
        });
        await alert.present();
    }


}
