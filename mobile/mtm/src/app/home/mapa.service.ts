import {Injectable} from '@angular/core';
import {DataService} from '../shared/services/data.service';
import {Location2} from '../shared/model';
import {getIcon, getMapaObject, MapaObject} from './mapaUtils';
import {GoogleMap, GoogleMapsEvent, HtmlInfoWindow, LatLng, Marker} from '@ionic-native/google-maps/ngx';
import {SearchService} from '../shared/services/search.service';
import {ToastService} from '../shared/services/toast.service';
import {formatDate, formatNumber} from '@angular/common';
import * as moment from 'moment';
import {PopoverController} from '@ionic/angular';
import {Storage} from '@ionic/storage';
import {SegurancaService} from '../seguranca/seguranca.service';

declare var google: any;

@Injectable({
  providedIn: 'root'
})
export class MapaService {

  showProgressBar: boolean = false;
  map: GoogleMap;
  locations: Location2[];
  mapaObject: MapaObject;
  ultimoSinal: any;

  constructor(
      private dataService: DataService,
      private toastService: ToastService,
      private storage: Storage,
      private searchService: SearchService) {

  }


  time: number = 0;

  mapaTimer;
  private startTimer() {
    this.pauseTimer()
    this.atualizaMapa()
    this.mapaTimer = setInterval(() => {
      this.time++;
      console.log("timer round",this.time)
      if(this.dataService.veiculo)
        this.atualizaMapa()
    },10000)
    console.log("timer reiniciado",this.mapaTimer)
  }

  private pauseTimer() {
    clearInterval(this.mapaTimer);
    console.log("timer pausado",this.mapaTimer)
  }


  private limparMapa(){
    this.map.clear()
  }



  public async startRastreamento() {

      if (this.dataService.veiculo) {
        if (this.dataService.rastreioAutomatico && !this.isHistorico()) {
          console.log('2')
          this.startTimer()
        } else {
          this.pauseTimer()
          this.atualizaMapa();
        }
      }
  }

  public stopRastreamento(){
    this.pauseTimer()
  }



  private  atualizaMapa() {

    this.showProgressBar = false

    let data = moment(this.dataService.data,"DD/MM/YYYY").toDate()
    let inicio = new Date(data.setHours(0,0,0,0))
    let fim = new Date(data.setHours(23,59,59,999))

      this.searchService.locationsByImeiDates(this.dataService.veiculo.equipamento.imei, inicio, fim).subscribe(
          resp => {
            console.log('resp');
            this.showProgressBar = false;
            this.map.clear().then(
                obj => {

                  this.locations = resp as Location2[];
                  if (this.locations.length <= 0) {
                    this.toastService.presentToast('Veículo sem rastreamento no período selecionado!', 'danger');
                  } else {

                    console.log('desenhando o mapa!!!');


                    let lastLoc = this.locations.pop();
                    this.ultimoSinal = lastLoc.dateLocation;
                    this.locations.push(lastLoc);

                    if(this.isHistorico()){
                      this.mapaObject = getMapaObject(this.locations);
                      this.map.setCameraTarget(new LatLng(this.mapaObject.centro.latitude, this.mapaObject.centro.longitude));
                      this.map.setCameraZoom(this.mapaObject.zoom);
                    }else {
                      this.map.setCameraTarget(new LatLng(lastLoc.latitude, lastLoc.longitude))
                      if(this.map.getCameraZoom()==4)
                        this.map.setCameraZoom(16)
                    }

                    this.addVeiculo(lastLoc);
                    this.addParadas();
                    this.addPolyline();
                    console.log('after polyline');
                  }
                },
                error => {
                  console.log(error);
                }
            );
          },
          error => {
            //this.toastService.presentToast("Erro buscando dados!",'danger');
            console.log(error)
          }
      );
    //}
  }




  private addVeiculo(lastLoc){
    let marker: Marker = this.map.addMarkerSync({
      //title: `${this.dataService.veiculo.placa}  - ${formatDate(lastLoc.dateLocation, "dd/MM/yy HH:mm", "pt_BR")} - ${formatNumber(lastLoc.velocidade,'pt-BR','1.2-2')}km/h`,
      icon: `assets/icon/${getIcon(this.dataService.veiculo.tipo)}.png`,
      // animation: 'DROP',
      position: {
        lat: lastLoc.latitude || 43.0741904,
        lng: lastLoc.longitude || -89.3809802
      }
    });
    marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
      let infoWindow = this.createVeiculoInfoWindow(lastLoc)
      infoWindow.open(marker);
    });
  }






  private addParadas(){
    let count = 0
    this.locations.forEach(loc=>{
      if(loc.dateLocation != loc.dateLocationInicio) {
        count++
        let marker: Marker = this.map.addMarkerSync({
          title: `P-${formatNumber(count,"pt-BR",'2.0-0')}  
                - ${formatDate(loc.dateLocationInicio, "dd/MM/yy HH:mm", "pt_BR")}
                a ${formatDate(loc.dateLocation, "dd/MM/yy HH:mm", "pt_BR")}`,
          //icon: `assets/icon/${getIcon(this.veiculo.tipo)}.png`,
          // animation: 'DROP',
          position: {
            lat: loc.latitude,
            lng: loc.longitude
          }
        });
        let infoWindow = this.createParadaInfoWindow(loc, formatNumber(count,"pt-BR",'2.0-0'))
        marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
          infoWindow.open(marker);
        });
      }
    })
  }


  private addPolyline(){
    let points: LatLng[]=[]
    this.locations.forEach(loc=>{
      points.push(new LatLng(loc.latitude,loc.longitude))
    })
    this.map.addPolyline({
      'points': points,
      'color' : "#00F",
      'width': 3
    }).then(polyline=>{
      polyline.set
    })
  }


  /*addInfoWindowToMarker(marker: Marker) {
    var infoWindowContent = '<div id="content"><h1 id="firstHeading" class="firstHeading">' + marker.getTitle() + '</h1></div>';

    google.maps.event.addListener(marker, 'click', function(){
      let infowindow = new google.maps.infowindow({
        content : this.place
      });
      infowindow.open(this.map,marker);
    });


  }*/


  isHistorico() {
    return this.dataService.data != formatDate(new Date(), "dd/MM/yy", "pt_BR")
  }



  createVeiculoInfoWindow(lastLoc){
    let linhaIgnition = ''
    if(lastLoc.ignition)
      linhaIgnition =         "<tr>"
          +"<td class='tg1'>Ignição</td>"
          +   "<td class='tg1'>"+ lastLoc.ignition+ "</td>"
          +"</tr>"

    let icon = `assets/icon/${getIcon(this.dataService.veiculo.tipo)}.png`
    let table =  "<table style='width: 100%' class='tg'>"
        + "<tr>"
        +    "<th colspan='2' class='tg2'><img width='10%' src='"+icon+"'></img> " +this.dataService.veiculo.placa +" - "+ this.dataService.veiculo.marcaModelo +" - "+ this.dataService.veiculo.cor + "</th>"
        +"</tr>"
        +"<tr>"
           +"<td class='tg1'>Último Sinal</td>"
        +   "<td class='tg1'>"+ formatDate(lastLoc.dateLocation, "dd/MM/yy HH:mm\'h'", 'pt_BR')+ "</td>"
        +"</tr>"
         +"<tr>"
        +   "<td class='tg2'>Velocidade</td>"
        +   "<td class='tg2'>"+formatNumber(lastLoc.velocidade,'pt-BR','1.2-2') +"km/h</td>"
        + "</tr>"
        + linhaIgnition
        + "</table>"
    return this.createInfoWindow(lastLoc,table)
  }


  createParadaInfoWindow(parada, index){

    let diff= moment.utc(moment(parada.dateLocation).diff(moment(parada.dateLocationInicio))).format("HH:mm:ss")

    let icon = `assets/icon/${getIcon(this.dataService.veiculo.tipo)}.png`
    let table =  "<table style='width: 100%' class='tg'>"
        + "<tr>"
        +    "<th colspan='2' class='tg2'><img width='10%' src='"+icon+"'></img> " +this.dataService.veiculo.placa +" - Parada "+ index + "</th>"
        +"</tr>"
        +"<tr>"
        +"<td class='tg1'>Chegada</td>"
        +   "<td class='tg1'>"+ formatDate(parada.dateLocationInicio, "dd/MM/yy HH:mm\'h'", 'pt_BR')+ "</td>"
        +"</tr>"
        +"<tr>"
        +   "<td class='tg2'>Saída</td>"
        +   "<td class='tg2'>"+ formatDate(parada.dateLocation, "dd/MM/yy HH:mm\'h'", 'pt_BR')+ "</td>"
        + "</tr>"
        +"<tr>"
        +   "<td class='tg1'>Tempo de Permanência</td>"
        +   "<td class='tg1'>"+ diff + "</td>"
        + "</tr>"


        + "</table>"
    return this.createInfoWindow(parada,table)
  }





  createInfoWindow(lastLoc,table){

    let infoWindowStyle = "<style type='text/css'>"
        +".tg  {border-collapse:collapse;border-spacing:0;border:none;border-color:#93a1a1;}"
        +".tg td{text-align:center;,font-weight:bold;font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#93a1a1;color:#002b36;background-color:#fdf6e3;}"
        +".tg th{text-align:left;font-weight:bold;font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#93a1a1;color:#fdf6e3;background-color:#657b83;}"
        +".tg .tg1{background-color:#eee8d5;vertical-align:top}"
        +".tg .tg2{vertical-align:top}"
        + "</style>"

    let htmlInfoWindow = new HtmlInfoWindow();
    let frame: HTMLElement = document.createElement('div');
    frame.innerHTML = infoWindowStyle + table
    htmlInfoWindow.setContent(frame, {
      width: "280px",
      //height: "200px"
    });
    return htmlInfoWindow
  }

}
