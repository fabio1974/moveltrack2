import {Component, OnInit} from '@angular/core';
import {Environment, GoogleMapOptions, GoogleMaps, GoogleMapsMapTypeId} from '@ionic-native/google-maps/ngx';
import {ActionSheetController, AlertController, LoadingController, ModalController, Platform, PopoverController} from '@ionic/angular';
import {getIcon} from './mapaUtils';
import {DataService} from '../shared/services/data.service';
import {ConfigsPopoverComponent} from './configs-popover/configs-popover.component';
import {MapaService} from './mapa.service';
import {CortaCorrenteService} from './corta-corrente.service';
import {SegurancaService} from '../seguranca/seguranca.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {


  constructor(private platform:Platform,
              public dataService: DataService,
              public popoverController: PopoverController,
              private alertController: AlertController,
              public actionSheetController: ActionSheetController,
              public modalController: ModalController,
              public cortaCorrenteService: CortaCorrenteService,
              public loadingController: LoadingController,
              private segurancaService: SegurancaService,
              public mapaService: MapaService)
  {
    platform.ready().then(() => {
      this.platform.pause.subscribe(() => {
        this.mapaService.stopRastreamento()
      });

      this.platform.resume.subscribe(() => {
          this.mapaService.startRastreamento()
      });
  })
  }


  ionViewWillEnter() {
    this.mapaService.startRastreamento()
  }

  ionViewWillLeave(){
    this.mapaService.stopRastreamento()
  }


  async ngOnInit() {
    await this.platform.ready()
    await this.loadMap()
  }


  loadMap() {
    Environment.setEnv({ // This code is necessary for browser
      'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyCg5eE_buXJLsJZbnTZ7z3MnJBOV3_RoYc',
      'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyCg5eE_buXJLsJZbnTZ7z3MnJBOV3_RoYc'
    });
    let mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: -14,
          lng: -54
        },
        zoom: 4,
        tilt: 0
      }
    };
    this.mapaService.map = GoogleMaps.create('map_canvas', mapOptions);
    this.mapaService.map.setMapTypeId(this.dataService.satelite?GoogleMapsMapTypeId.HYBRID:GoogleMapsMapTypeId.NORMAL)

  }


  async presentConfigsPopover(ev: any) {
    let popover = await this.popoverController.create({
      component: ConfigsPopoverComponent,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }




  async presentCortaCorrenteActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      mode: 'ios' ,
      header: 'Corta Corrente',
      buttons: [{
        text: 'Bloquear Veículo',
        role: 'destructive',
        icon: 'cut',
        handler: () => {
          this.presentAlertConfirm(CortaCorrente.BLOQUEIO)
          console.log('Delete clicked');
        }
      }, {
        text: 'Desbloquear Veículo',
        icon: 'key',
        handler: () => {
          this.presentAlertConfirm(CortaCorrente.DESBLOQUEIO)
          console.log('Share clicked');
        }
      }, {
        text: 'Fechar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }




  async presentAlertConfirm(cortaCorrente: CortaCorrente) {
    let message = cortaCorrente == CortaCorrente.BLOQUEIO ?'<strong>parar</strong>':'<strong>restabelecer</strong>'
    const alert = await this.alertController.create({
      mode: 'ios' ,
      header: cortaCorrente == CortaCorrente.BLOQUEIO ? 'Bloqueio' : 'Desblqueio',
      message: `Isso vai ${message} o motor do seu veículo! Deseja prosseguir?`,
      buttons: [
        {
          text: 'Não',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');

          }
        }, {
          text: 'Sim',
          handler: () => {
            if(cortaCorrente == CortaCorrente.BLOQUEIO)
              this.cortaCorrenteService.bloquear(this.dataService.veiculo)
            else
              this.cortaCorrenteService.desbloquear(this.dataService.veiculo)
          }
        }
      ]
    });
    await alert.present();
  }









  datePickerObj: any = {
    fromDate: new Date(2019, 6, 1), // default null
    toDate: new Date(), // default null
    showTodayButton: false, // default true
    closeOnSelect: true, // default false
    mondayFirst: true, // default false
    closeLabel: 'Fechar', // default 'Close'
    titleLabel: 'Selecione a Data de Interesse', // default null
    monthsList: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
    weeksList: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
    dateFormat: 'DD/MM/YY', // default DD MMM YYYY
    clearButton: false, // default true
    momentLocale: 'pt_BR', // Default 'en-US'
    yearInAscending: true, // Default false
    btnCloseSetInReverse: true, // Default false
    btnProperties: {
      expand: 'block', // Default 'block'
      fill: '', // Default 'solid'
      size: '', // Default 'default'
      disabled: '', // Default false
      strong: '', // Default false
      color: '' // Default ''
    },
    isSundayHighlighted: {fontColor: '#ee88bf'}
  };

  getIcon() {
    let icon = null
    if(this.dataService.veiculo) {
      icon = `${getIcon(this.dataService.veiculo.tipo)}.png`
    }
    return icon
  }





}

export enum CortaCorrente{
  BLOQUEIO, DESBLOQUEIO
}
