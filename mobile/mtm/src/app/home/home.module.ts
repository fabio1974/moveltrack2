import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HomePage } from './home.page';
import {SharedModule} from '../shared/shared.module';
import {ConfigsPopoverComponent} from './configs-popover/configs-popover.component';


const routes: Routes = [
  {
    path: '',
    component: HomePage
  }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        SharedModule,

    ],
  declarations: [HomePage,ConfigsPopoverComponent],
    entryComponents:[ConfigsPopoverComponent]
})
export class HomePageModule {}
