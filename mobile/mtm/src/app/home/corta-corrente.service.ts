import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Sms, SmsStatus, Veiculo} from '../shared/model';
import {environment} from '../../environments/environment';
import {DataService} from '../shared/services/data.service';
import {LoadingController} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class CortaCorrenteService {

  constructor(
      private http: HttpClient,
      private dataService: DataService,
      private loadingController: LoadingController

  ) {
  }

  loadingStatusPopup
  cortaCorrenteTimer;
  sms: Sms


  desbloquear(veiculo: Veiculo) {
    return this.http.post<Sms>(`${environment.apiUrl}/desbloquear`, veiculo).subscribe(
        resp=> {this.startCortaCorrenteService(resp)}
        )
  }

  bloquear(veiculo: Veiculo) {
    return this.http.post<Sms>(`${environment.apiUrl}/bloquear`, veiculo).subscribe(
        resp=> {this.startCortaCorrenteService(resp)}
    )
  }

  startCortaCorrenteService(sms: Sms) {
    console.log("Sms retornado",sms)
    this.sms = sms
    this.startTimer()
    this.showComandoStatusPopup()
  }


  private checkLastSms(celular: string){
    return this.http.get<Sms>(`${environment.apiUrl}/lastSmsRecebido/${celular}`).subscribe(
        sms=>{
          this.sms = sms
          this.loadingStatusPopup.message = this.formatMessage()
          console.log("lastSms",sms)
        },
        error1 => console.log("errror'",error1)
    );
  }


  private startTimer() {
    this.pauseTimer()
    this.cortaCorrenteTimer = setInterval(() => {
      this.checkLastSms(this.dataService.veiculo.equipamento.chip.numero)
    },5000)
  }

  private pauseTimer() {
    clearInterval(this.cortaCorrenteTimer);
  }


  private async showComandoStatusPopup() {
    this.loadingStatusPopup = await this.loadingController.create({
      message: this.formatMessage(),
      duration: 45*1000,
      mode: 'ios'
    });
    await this.loadingStatusPopup.present();
    const { role, data } = await this.loadingStatusPopup.onDidDismiss();
    this.pauseTimer()
    console.log('Loading dismissed!');
  }


  private formatMessage(): string{
    let message = "Status do Comando..."
    if(this.sms && this.sms.status.toString().startsWith("ESPERANDO"))
      message = `${this.sms.tipo} ESPERANDO`
    else if(this.sms.status == SmsStatus.RECEBIDO)
      message = this.sms.mensagem
    else
      message = `${this.sms.tipo} ${this.sms.status}`
    return message
  }

}
