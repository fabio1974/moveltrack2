import {Component, OnInit} from '@angular/core';
import {DataService} from '../../shared/services/data.service';
import {getIcon} from '../mapaUtils';
import {MapaService} from '../mapa.service';
import {formatDate} from '@angular/common';
import {PopoverController} from '@ionic/angular';
import {Storage} from '@ionic/storage';
import {DataSet} from '../../shared/model';
import {GoogleMapsMapTypeId} from '@ionic-native/google-maps/ngx';

@Component({
    selector: 'app-controles',
    templateUrl: './configs-popover.component.html',
    styleUrls: ['./configs-popover.component.scss'],
})
export class ConfigsPopoverComponent implements OnInit {


    constructor(

        public mapaService: MapaService,
        public popoverController: PopoverController,
        public dataService: DataService,
    ) {
    }

    ngOnInit() {
    }


    async setConfigs() {
        if(this.dataService.satelite)
            this.mapaService.map.setMapTypeId(GoogleMapsMapTypeId.HYBRID)
        else
            this.mapaService.map.setMapTypeId(GoogleMapsMapTypeId.NORMAL)
        this.mapaService.startRastreamento()
    }

    sair(){
        this.popoverController.dismiss()
    }




}
