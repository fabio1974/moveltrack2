import { Component, OnInit } from '@angular/core';
import {DataService} from '../shared/services/data.service';
import {Cliente, Veiculo} from '../shared/model';
import {SearchService} from '../shared/services/search.service';
import {Router} from '@angular/router';
import {getIcon} from '../home/mapaUtils';
import {SideMenuService} from '../shared/services/side-menu.service';
import {MapaService} from '../home/mapa.service';

@Component({
  selector: 'app-veiculos',
  templateUrl: './veiculos.page.html',
  styleUrls: ['./veiculos.page.scss'],
})
export class VeiculosPage implements OnInit {
  showProgressBar = false;

  //cliente: Cliente
  //veiculos: Veiculo[]

  constructor(public dataService: DataService,
              private mapaService: MapaService,
              public searchService: SearchService,
              private sideMenuService: SideMenuService,
              private router: Router,
              ) { }


  ngOnInit() {}

  ionViewWillEnter() {


  }




  goMapa(veiculo: Veiculo) {
    this.dataService.veiculo = veiculo
    console.log("VEICULO",this.dataService.veiculo)
    this.mapaService.ultimoSinal = null
    this.router.navigate(['/home'])
  }

    getIcon(tipo: any) {
        return getIcon(tipo)
    }
}
