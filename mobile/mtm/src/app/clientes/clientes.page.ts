import {Component, OnInit} from '@angular/core';
import {SearchService} from '../shared/services/search.service';
import {Cliente, Veiculo} from '../shared/model';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {DataService} from '../shared/services/data.service';
import {SideMenuService} from '../shared/services/side-menu.service';


@Component({
    selector: 'app-clientes',
    templateUrl: './clientes.page.html',
    styleUrls: ['./clientes.page.scss'],
})
export class ClientesPage implements OnInit {
    nome: string;
    showProgressBar= false;
    //clientes: Cliente[];

    constructor(private searchService: SearchService,
                public dataService: DataService,
                private sideMenuService: SideMenuService,
                private router: Router) {
    }

    ngOnInit() {
    }


    searchClientes() {
        this.showProgressBar = true
        this.dataService.clientes = []
        this.searchService.searchClientes(this.nome).subscribe(
            resp => {
                this.dataService.clientes = resp as Cliente[];
                this.showProgressBar = false
            },
            error1 => {
                console.log('error', error1);
            }
        );
    }

    goVeiculos(cliente: Cliente) {

        this.dataService.cliente = cliente

        this.showProgressBar = true

        console.log("333333")

        this.searchService.findVeiculosAtivosByCliente(this.dataService.cliente).subscribe(
            resp => {
                console.log("444444")
                this.showProgressBar = false
                this.dataService.veiculos = resp as Veiculo[]
                this.router.navigate(['/veiculos'],)
            },error1 => {
                console.log(error1)
            }
        )


    }
}
