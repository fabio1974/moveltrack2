import { Injectable } from '@angular/core';
import {Cliente, Pessoa, Veiculo} from '../model';
import {formatDate} from '@angular/common';
import {Storage} from '@ionic/storage';



@Injectable({
  providedIn: 'root'
})
export class DataService {

  rastreioAutomatico: boolean;
  satelite: boolean;
  pessoaLogada: Pessoa;
  cliente: Cliente
  veiculo: Veiculo
  clientes: Cliente[]
  veiculos: Veiculo[]
  data: string = formatDate(new Date(), "dd/MM/yy", "pt_BR")
  zoom: 16;


  constructor(private storage: Storage) { }

  public  getToken(){
    return  this.storage.get("token")
  }

  public async setToken(token: string){
    await this.storage.set("token",token)
  }


  clear() {
    this.storage.clear()
  }
}

