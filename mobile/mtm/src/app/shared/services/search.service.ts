import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Chip, Cliente, Contrato, Empregado, Location2, Municipio, Page, Veiculo} from '../model';
import {Observable} from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient) { }



  searchClientes(nome){
    const query = `nome=${nome}`;
    return this.http.get(`${environment.apiUrl}/searchClientes?${query}`);
  }

  postVeiculo(veiculo) {
    return this.http.post(`${environment.apiUrl}/veiculos`,veiculo);
  }

  findVeiculosAtivosByCliente(cliente: Cliente){
    return this.http.post(`${environment.apiUrl}/findVeiculosAtivosByCliente`,cliente);
  }

  locationsByImeiDates(imei,inicio,fim) {

    const locationRequest = new Location2()
    locationRequest.imei = imei
    locationRequest.dateLocationInicio = inicio
    locationRequest.dateLocation = fim

    return this.http.post(`${environment.apiUrl}/locationsByImeiDates`,locationRequest);
  }



  /*  searchMotoristas(clienteId){
      const query = `clienteId=${clienteId}`;
      return this.http.get(`${environment.apiUrl}/searchMotoristas?${query}`);
    }*/

 /* searchViagens(cliente,numeroViagem){
    const query = `clienteId=${cliente.id}&numeroViagem=${numeroViagem}`;
    return this.http.get(`${environment.apiUrl}/searchViagens?${query}`);
  }*/


/*  searchMunicipios(nome){
    const query = `nome=${nome}`;
    return this.http.get(`${environment.apiUrl}/searchMunicipios?${query}`);
  }*/


/*
  searchContratos(nome) {
    const query = `nome=${nome}`;
    return this.http.get(`${environment.apiUrl}/searchContratos?${query}`);
  }
*/

  searchEquipamentos(imei) {
    const query = `imei=${imei}`;
    return this.http.get(`${environment.apiUrl}/searchEquipamentos?${query}`);
  }

 /* searchEmpregados(nome) {
    const query = `nome=${nome}`;
    return this.http.get(`${environment.apiUrl}/searchEmpregados?${query}`);
  }*/

/*
  searchChips(iccid) {
    const query = `iccid=${iccid}`;
    return this.http.get(`${environment.apiUrl}/searchChips?${query}`);
  }
*/

  contratosResult
  searchContratos(event) {
    const query = `nome=${event.query}`;
    this.http.get(`${environment.apiUrl}/searchContratos?${query}`).subscribe(
      resp=>{
        this.contratosResult = (resp as Contrato[]).map(
          m=>{
            m.label = `${m.numeroContrato}-${m.cliente.nome}`
            return m
          })
      },
      error1 => {
        console.log('error', error1);
      }
    )
  }





  chipsResult
  searchChips(event) {
    const query = `iccid=${event.query}`;
    this.http.get(`${environment.apiUrl}/searchChips?${query}`).subscribe(
      resp=>{
        this.chipsResult = resp as Chip[]
      },
      error1 => {
        console.log('error', error1);
      }
    )
  }


  veiculosResult
  searchVeiculos(event) {
    const query = `placa=${event.query}`;
    this.http.get(`${environment.apiUrl}/searchVeiculos?${query}`).subscribe(
      resp=>{
        this.veiculosResult = resp as Veiculo[]
      },
      error1 => {
        console.log('error', error1);
      }
    )
  }

 /* clientesResult
  searchClientes(event) {
    const query = `nome=marcelo`;
    this.http.get(`${environment.apiUrl}/searchClientes?${query}`).subscribe(
      resp=>{
        this.clientesResult = resp as Cliente[]
      },
      error1 => {
        console.log('error', error1);
      }
    )
  }*/

  empregadosResult
  searchEmpregados(event) {
    const query = `nome=${event.query}`;
    this.http.get(`${environment.apiUrl}/searchEmpregados?${query}`).subscribe(
      resp=>{
        this.empregadosResult = resp as Empregado[]
      },
      error1 => {
        console.log('error', error1);
      }
    )
  }


  municipiosResult: Municipio[];
  searchMunicipios(event) {
    const query = `nome=${event.query}`;
    this.http.get(`${environment.apiUrl}/searchMunicipios?${query}`).subscribe(
      resp => {
        this.municipiosResult = (resp as Municipio[]).map(
          m=>{
            m.label = `${m.descricao}-${m.uf.sigla}`
            return m
          })
      },
      error => {
        console.log('error', error);
      }
    );
  }





}
