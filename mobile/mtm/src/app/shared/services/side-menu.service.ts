import { Injectable } from '@angular/core';

export const appPages  = [

  {
    title: 'Clientes',
    url: '/clientes',
    icon: 'people',
  },
  {
    title: 'Veículos',
    url: '/veiculos',
    icon: 'car',
  },
 /* {
    title: 'Sinal Atual',
    url: '/home',
    icon: 'locate',
  },*/
 /* {
    title: 'Histórico',
    url: '/historico',
    icon: 'filing',
  },
  {
    title: 'Corta Corrente',
    url: '/corta-corrente',
    icon: 'cut',
  },*/
 /* {
    title: 'Mapa',
    url: '/list',
    icon: 'map',
  },*/
  {
    title: 'Sair',
    url: '/login',
    icon: 'exit',
  }]


@Injectable({
  providedIn: 'root'
})
export class SideMenuService {

  public appPages = appPages

  constructor() {
  }

  removeMenu(url: string) {
    this.appPages = this.appPages.filter(item=> item.url != url)
  }

  resetMenu(){
    this.appPages = appPages
  }
}
