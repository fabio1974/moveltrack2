import { Injectable } from '@angular/core';
import {ToastController} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(public toastController: ToastController) { }

  async presentToast(message,color='default') {
    const toast = await this.toastController.create({
      message,
      duration:4000,
      position:'middle',
      cssClass:'toast-text-center',
      color:color});
    toast.present();
  }

}
