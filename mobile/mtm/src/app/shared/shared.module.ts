import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CpfValidator} from './validators/cpf-validator';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {IonicStorageModule} from '@ionic/storage';
import {HeaderComponent} from './header/header.component';
import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import { Ionic4DatepickerModule } from  '@logisticinfotech/ionic4-datepicker';



@NgModule({
  declarations: [
      CpfValidator,
      HeaderComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    IonicModule,
    Ionic4DatepickerModule,
    RouterModule,


  ],
  exports: [
    CpfValidator,
    HttpClientModule,
    IonicStorageModule,
    HeaderComponent,
    IonicModule,
    Ionic4DatepickerModule,
  ],


})
export class SharedModule { }
