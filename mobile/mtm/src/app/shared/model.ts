import {formatDate} from '@angular/common';

export class User{
    username
    password
}

export class Page {
    pageIndex = 0;
    pageSize = 10;
    list = [];
    totalRecords = 0;
    sortField="id";
    sortDirection="DESC"  //"0" = ASC , "1" = DESC
}




export class Boleto {
    id=null;
    dataVencimento= new Date();
    dataBase=null;
    dataPagamento=null;
    dataRegistroPagamento=null;
    valor=null;
    multa=null;
    juros=null;
    situacao='EMITIDO';
    tipoDeCobranca='COM_REGISTRO';
    tipo='AVULSO';
    dataEmissao= new Date();
    mensagem34=null;
    observacao=null;
    nossoNumero=null;
    carne=null;
    contrato: Contrato = null;
    emissor=null;
    iugu=null;
}

export class Contrato  {
    id = null;
    numeroContrato = null;
    inicio = new Date();
    fim = null;
    diaVencimento = 0;
    vendedor = null;
    pagamentoAnual = false;
    contratoGeraCarne = 'PENDENTE';
    contratoTipo = 'COMODATO';
    mensalidade = 0;
    entrada = 0;
    status = 'ATIVO';
    ultimaAlteracao = null;
    dataBase=null
    veiculos = null
    cliente: Cliente = new Cliente()
    label = null
}

export class Veiculo {

    constructor(id:number=null){
        this.id = id
    }

    id = null
    placa = null
    marcaModelo = null
    motorista = null
    descricao = null
    velocidadeMaxima = null
    equipamento = null
    cor = null
    dataInstalacao = null
    dataDesinstalacao = null
    instalador = null
    contrato: Contrato = new Contrato()
    botaoPanico: Boolean = false
    demo: Boolean = false
    comEscuta: Boolean = false
    comCercaVirtual: Boolean = false
    comBloqueio: Boolean = false
    consumoCombustivel= 0
    tipo = null
    status = 'ATIVO'
}



export class Equipamento {
    id: number
    dataCadastro: Date
    situacao: EquipamentoSituacao
    proprietario: Cliente
    modelo: any//ModeloRastreador
    senha: string
    imei: string
    atrasoGmt: number = 0
    chip: Chip
    comando: number = 0
    observacao: string
    ultimaAlteracao: Date
    possuidor: Empregado
    label
}



export enum EquipamentoSituacao {
    AGUARDANDO_TESTE = 'Aguardando teste',
    ATIVO = 'Ativo',
    COM_DEFEITO = 'Com defeito',
    EM_TESTE = 'Em teste',
    NAO_ENCONTRADO = 'Não encontrado',
    NOVO_CONFIGURADO = 'Novo - Configurado',
    NOVO_NAO_CONFIGURADO = 'Novo - Não configurado',
    OUTRA = 'Outra',
    PRONTO_PARA_REUSO = 'Pronto para reuso',
    SUSPENSO = 'Suspenso'
}



export class Chip {
    id = null
    numero: string = null
    iccid: string = null
    dataCadastro: Date = null
    operadora
    status
}


export class Sms {
    id
    celular: string
    mensagem: string
    imei: string
    tipo: SmsTipo
    status: SmsStatus
    dataUltimaAtualizacao
}

export enum  SmsStatus {
    ESPERANDO, ENVIADO, RECEBIDO, DESCARTADO, ESPERANDO_SOCKET
}

export enum SmsTipo {
    BLOQUEIO, DESBLOQUEIO, REINICIAR, CHECAR, AVISO, DNS
}


export const PessoaStatus = {
    ATIVO: 'ATIVO',
    INATIVO: 'INATIVO'
}

export const ModeloRastreador =[
    {value: 'GT06', label: 'GT06'},
    {value: 'H08', label: 'H08'},
    {value: 'TK103A2', label: 'TK103A2'},
    {value: 'GT06B', label: 'GT06B'},
    {value: 'XT009', label: 'XT009'},
    {value: 'GT06N', label: 'GT06N'},
    {value: 'GT06B2', label: 'GT06B2'},
    {value: 'TR02', label: 'TR02'},
    {value: 'CRX1', label: 'CRX1'},
    {value: 'JV200', label: 'JV200'},
    {value: 'TK06', label: 'TK06'},
    {value: 'ST350_LC2', label: 'ST350_LC2'},
    {value: 'ST350_LC4', label: 'ST350_LC4'},
    {value: 'ST940', label: 'ST940'},
    {value: 'CRXN', label: 'CRXN'},
    {value: 'SPOT_TRACE', label: 'SPOT_TRACE'},
    {value: 'CRX3', label: 'CRX3'},
]



export const DespesaFrotaCategoria = [
    {label: 'MOTORISTA', value: 'MOTORISTA'},
    {label: 'VEÍCULO', value: 'VEICULO'},
    {label: 'VIAGEM', value: 'VIAGEM'}
]

export const DespesaFrotaEspecie = [
    {value: 'COMBUSTIVEL',label: 'COMBUSTÍVEL'},
    {value: 'ESTIVA',label: 'SERV. DE ESTIVA'},
    {value: 'DIARIA',label: 'DIÁRIA'},
    {value: 'IPVA',label: 'IPVA'},
    {value: 'LICENCIAMENTO',label: 'LICENCIAMENTO'},
    {value: 'MANUTENCAO',label: 'MANUTENÇÃO - PEÇAS E SERVIÇOS'},
    {value: 'MULTA_DE_TRANSITO',label: 'MULTA DE TRANSITO'},
    {value: 'OUTROS',label: 'OUTROS'},
    {value: 'SEGURO_OBRIGATORIO',label: 'SEGURO OBRIGATORIO'},
    {value: 'TRABALHISTAS',label: 'SALARIOS E ADICIONAIS TRAB.'}
]

export class Carne {
    id ;
    contrato = null;
    tipoDeCobranca=null;
    dataVencimentoInicio=null;
    dataVencimentoFim=null;
    postagem=null;
    dataEmissao=null;
    valorPrimeiroBoleto=null
}

export class Pessoa {
    id = null
    cpf = null
    cnpj = null
    status = PessoaStatus.ATIVO;
    nome = null
    email = null
    logoFile = 'logo_azul_221_57.png'
    ultimaAlteracao = null
    dataCadastro = null
    dataNascimento = null
    telefoneFixo = null
    celular1 = null
    celular2 = null
    endereco = null
    numero = null
    complemento = null
    municipio = null
    cep = null
    bairro = null
    usuario: Usuario = new Usuario()

    public isCliente():boolean{
        return this.usuario.perfil.tipo === PerfilTipo.CLIENTE_PF ||  this.usuario.perfil.tipo === PerfilTipo.CLIENTE_PJ ||  this.usuario.perfil.tipo === PerfilTipo.CLIENTE_DEMONSTRACAO
    }
}


export class DataSet{
    rastreioAutomatico: boolean
    satelite: boolean = false
    cliente: Cliente
    clientes: Cliente[]
    veiculo: Veiculo
    veiculos: Veiculo[]
    pessoaLogada: Pessoa
    data: string = formatDate(new Date(), "dd/MM/yyyy", "pt_BR")
}

export class Cliente extends Pessoa{

    nomeFantasia = null
    observacao = null
    emailAlarme = null
    //cerca = null
    ultimaCobranca = null
    ultimoLembrete = null
    tipo = 'PF'
}

export class Motorista extends Pessoa {

    constructor(dataCadastro: Date = new Date(),id:number=null){
        super()
        this.dataCadastro = dataCadastro
        this.id = id
    }

    patrao = null
    validadeCnh = null;
    categoriaCnh = null;
}

export class Usuario{
    id = null
    perfil: Perfil = null
    nomeUsuario = null
    ativo = null
    ultimoAcesso = null
    permissoes = []
    senha = null
    email = null
}


export const caxias = {
    "codigo" : 7579,
    "descricao" : "CAXIAS",
    "latitude" : -4.85,
    "longitude" : -43.35,
    "uf" : {
        "id" : 10,
        "descricao" : "Maranhao",
        "sigla" : "MA",
        "horarioVerao" : false,
        "gmt" : -3
    },
    "label" : "CAXIAS-MA",
    "label$backend" : null
}

export class Municipio{
    codigo = null
    descricao = null
    uf = null
    label = `${this.descricao}-${this.uf}`
    latitude: number=null;
    longitude:number=null
}

export class Perfil{
    tipo: PerfilTipo = null
}

export enum  PerfilTipo {
    ADMINISTRADOR = "ADMINISTRADOR",
    FRANQUEADO = "FRANQUEADO",
    CLIENTE_PJ = "CLIENTE_PJ",
    CLIENTE_PF = "CLIENTE_PF",
    CLIENTE_DEMONSTRACAO = "CLIENTE_DEMONSTRACAO",
    VENDEDOR = "VENDEDOR",
    FINANCEIRO = "FINANCEIRO",
    GERENTE_GERAL = "GERENTE_GERAL",
    GERENTE_ADM = "GERENTE_ADM",
    INSTALADOR = "INSTALADOR"

}


export class Viagem {
    id = null
    numeroViagem = null
    descricao = null
    cliente = null
    veiculo = null
    motorista = null
    cidadeOrigem = null
    cidadeDestino = null
    status = null
    partida = null
    saiuDaCerca = null
    entrouNaCerca = null
    chegadaReal = null
    chegadaPrevista = null
    valorDaCarga = 0
    valorDevolucao = 0
    pesoDaCarga = 0
    distanciaPercorrida = 0
    distanciaHodometro = 0
    qtdCidades = 0
    qtdClientes = 0
}


export class DespesaFrota {
    id = null;
    dataDaDespesa = null;
    descricao = null;
    veiculo  ;
    motorista;
    cliente = null;
    viagem ;
    categoria = null;
    especie = null;
    valor = null;
    litros = null;
    dataDePagamento = null;
}

export class Lancamento{
    id = null
    data = null
    valor = null
    operacao = null
    status = null
    houveBaixa = null
    formaPagamento = null
    observacao=null
    ordemDeServico=null
    solicitante=null
    operador=null
}

export class OrdemDeServico{
    id=null
    dataDoServico=null
    cliente=null
    veiculo=null
    observacao=null
    numero=null
    operador=null
    servico=null
    valorDoServico=null
    status=null
}


export class RelatorioFiltro{
    inicio
    fim
    cliente: Cliente
    numeroViagem
    motorista: Motorista
    uf: string;
}

export class LitrosValor {
    litros
    valor
}

export class RelatorioMotoristaPorViagem {
    viagem: Viagem
    despesaCombustivel: LitrosValor
    despesaDiarias
    despesaEstivas
    despesaDiversas
    pesoDaCarga
    diasViagens
}

export const Operacao = {
    CREATE: 'CREATE',
    UPDATE: 'UPDATE',
    SHOW: 'SHOW'
}

export const ContratoGeraCarneStatusOptions = [
    {label:'GERADO', value:'GERADO'},
    {label:'PENDENTE', value:'PENDENTE'}
]

export const AtivoInativo = [
    {label:'ATIVO', value:'ATIVO'},
    {label:'INATIVO', value:'INATIVO'}
]

export const ContratoStatusOptions = [
    {label:'ATIVO', value:'ATIVO'},
    {label:'ENCERRADO', value:'ENCERRADO'},
    {label:'CANCELADO', value:'CANCELADO'},
    {label:'SUSPENSO', value:'SUSPENSO'}
]

export const ViagemStatusOptions = [
    {value:'ABERTA', label:'ABERTA'},
    {value:'PARTIDA_EM_ATRASO', label:'PARTIDA EM ATRASO'},
    {value:'SAINDO', label:'SAIU DA CERCA'},
    {value:'SAIU_DA_CERCA', label:'GERADO'},
    {value:'NA_ESTRADA', label:'NA ESTRADA'},
    {value:'ENTROU_NA_CERCA', label:'ENTROU NA CERCA'},
    {value:'SE_APROXIMANDO', label:'SE APROXIMANDO'},
    {value:'CHEGADA_EM_TEMPO', label:'CHEGADA EM TEMPO'},
    {value:'CHEGADA_EM_ATRASO', label:'CHEGADA EM ATRASO'},
    {value:'ENCERRADA', label:'ENCERRADA'},
]


export class Empregado extends Pessoa{
    salario: number
}


export class Rota{
    origem: Municipio = caxias
    destino: Municipio = null
    itinerario: Municipio[] = []
    polyline: string = null
    id: any;
}



export class Location2 {
    id
    dateLocation
    dateLocationInicio
    latitude
    longitude
    velocidade
    imei
    comando
    satelites
    endereco
    mcc
    bloqueio
    gps
    gsm
    sos
    battery
    volt
    ignition
    alarm
    alarmType
}


export class ConsumoPorVeiculo{
    id
    placa
    marcaModelo
    qtdViagens
    distanciaPercorrida
    distanciaHodometro
    litros
    kml
    kml2
    erro
}

export class RelatorioFrota {
    id
    placa
    motorista
    ano
    mes
    data
    destino
    estado
    qtdViagens
    diasViagens
    qtdCidades
    qtdClientes
    valorDaCarga
    pesoDaCarga
    despesaEstiva
    despesaDiaria
    despesaCombustivel
    despesaTotal
    distanciaPercorrida
    litros
    kml
    valorDevolucao
}


export class RelatorioMensalDespesa {
    ano
    mes
    data
    carga
    combustivel
    manutencao
    estivas
    diarias
    ipva
    transito
    trabalhistas
    outras
    despesa
}
