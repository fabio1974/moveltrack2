#!/usr/bin/env bash
OUTPUT=platforms/android/app/build/outputs/apk/release
KEY=resources/android/key/moveltrackmobile.key
FILENAME=mtm
rm -f $OUTPUT/$FILENAME.apk
rm -f $OUTPUT/app-release-unsigned.apk
#ionic cordova plugin rm cordova-plugin-console
ionic cordova build --release android
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore $KEY -storepass mvt17547 $OUTPUT/app-release-unsigned.apk moveltrackmobile
/Users/barros/Library/Android/sdk/build-tools/29.0.2/zipalign -v 4 $OUTPUT/app-release-unsigned.apk $OUTPUT/$FILENAME.apk
echo "APK criado: $OUTPUT/$FILENAME.apk"
