package br.com.moveltrack.persistence.pojos

import br.com.moveltrack.persistence.domain.Location
import java.util.ArrayList



class ParadaSpot {
    var pontosDeParada: MutableList<Location>

    val isAberta: Boolean
        get() = !pontosDeParada.isEmpty()

    init {
        pontosDeParada = ArrayList<Location>()
    }

    fun abreParada(inicio: Location) {
        pontosDeParada.add(inicio)
    }

    fun mantemParada(meio: Location) {
        pontosDeParada.add(meio)
    }

    fun fechaParada(pontosMapa: MutableList<Location>, fim: Location) {
        pontosDeParada.add(fim)
        val parada = getAverageLocation(pontosDeParada)
        pontosMapa.add(parada)
        pontosDeParada.clear()
    }

    fun fechaEAbreNova(pontosMapa: MutableList<Location>, inicio: Location) {
        val parada = getAverageLocation(pontosDeParada)
        pontosMapa.add(parada)
        pontosDeParada.clear()
        abreParada(inicio)
    }

    fun getAverageLocation(parada: List<Location>): Location {
        //----------------------------------------------------------------------------------------------------------
        //O código comentado abaixo faz a média dos pontos. Esse algoritmo porém não é recomendável em casos de roubo ou furto.
        //----------------------------------------------------------------------------------------------------------
        //if (parada.size <= 0)
          //  return null

        var count = 0
        var latMed = 0.0
        var lonMed = 0.0
        for (location in parada) {
            latMed += location.latitude
            lonMed += location.longitude

            val ignition = location.ignition
            if (ignition != null && ignition == "1")
                count++
        }
        latMed = latMed / parada.size
        lonMed = lonMed / parada.size

        //val firstLoc = parada[0]
        val lastLoc = parada[parada.size - 1]
        val loc = Location()

        loc.ignition = (if (count > parada.size / 2) "1" else "0")
        loc.latitude = latMed
        loc.longitude = lonMed
        loc.velocidade = 0.toDouble()
        loc.dateLocationInicio = parada[0].dateLocationInicio
        loc.dateLocation = parada[parada.size - 1].dateLocation

        loc.alarm = lastLoc.alarm
        loc.alarmType =  lastLoc.alarmType
        loc.battery = lastLoc.battery
        loc.bloqueio = lastLoc.bloqueio
        loc.comando = "STOP"
        loc.gps = lastLoc.gps
        loc.gsm = lastLoc.gsm
        loc.id = lastLoc.id
        loc.imei = lastLoc.imei
        loc.mcc = lastLoc.mcc
        loc.satelites = lastLoc.satelites
        loc.sos = lastLoc.sos
        //loc.setVersion(lastLoc.getVersion());
        loc.volt = lastLoc.volt
        return loc
    }

}
