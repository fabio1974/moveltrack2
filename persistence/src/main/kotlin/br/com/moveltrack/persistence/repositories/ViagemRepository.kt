package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.*
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import java.time.ZonedDateTime

@Repository
interface ViagemRepository: PagingAndSortingRepository<Viagem, Int>, JpaSpecificationExecutor<Viagem> {

    abstract fun findAllByClienteAndMotoristaAndPartidaBetweenAndChegadaRealNotNullOrderByPartida(cliente: Cliente?, motorista: Motorista?, inicio: ZonedDateTime?, fim:ZonedDateTime?): MutableList<Viagem?>
    abstract fun findAllByVeiculoAndPartidaBetweenAndChegadaRealNotNullOrderByPartida(veiculo: Veiculo?, inicio: ZonedDateTime?, fim:ZonedDateTime?): MutableList<Viagem?>

    abstract fun findAllByClienteAndMotoristaAndChegadaRealNotNullAndPartidaBetweenOrderByPartida(
            cliente: Cliente,
            motorista: Motorista,
            inicio: ZonedDateTime,
            fim:ZonedDateTime
    ): MutableList<Viagem?>

    abstract fun findAllByNumeroViagemContaining(nome: String): List<Viagem>
    abstract fun findAllByStatus(status: ViagemStatus): List<Viagem>?
    abstract fun findAllByStatusNotAndChegadaPrevistaBefore(status: ViagemStatus,chegada: ZonedDateTime): List<Viagem>?
    abstract fun findAllByStatusNotAndChegadaPrevistaAfter(status: ViagemStatus,chegada: ZonedDateTime): List<Viagem>?
}
