package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.Uf
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(path="ufs",exported = false)
interface UfRepositoryMy: JpaRepository<Uf, Int> {
    fun findOneById(id: Int?): Uf
}
