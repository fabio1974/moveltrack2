package br.com.moveltrack.persistence.domain

import javax.persistence.*

@Entity
////@Table(name = "carneconteudo")
class CarneConteudo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null

    @Lob
    var conteudo: ByteArray? = null

}
