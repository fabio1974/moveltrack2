package br.com.moveltrack.persistence.domain



import javax.persistence.*
import javax.validation.constraints.NotNull


@Entity
//@Table(name = "uf")
data class Uf(
        @Id val  id: Int? =null,
        @NotNull val descricao: String?=null,
        @NotNull val sigla: String?=null,
        @NotNull val horarioVerao: Boolean?=null,
        @NotNull val gmt: Int?=null)











