package br.com.moveltrack.persistence.domain


enum class Cor constructor(val descricao: String) {
    AMARELA("AMARELA"),
    AZUL("AZUL"),
    AZUL_METALICA("AZUL METÁLICA"),
    BEGE("BEGE"),
    BRANCA("BRANCA"),
    CHUMBO("CHUMBO"),
    CINZA("CINZA"),
    LARANJA("LARANJA"),
    PRATA("PRATA"),
    PRETA("PRETA"),
    ROSA("ROSA"),
    ROSA_METALICA("ROSA METÁLICA"),
    VERDE("VERDE"),
    VERMELHA("VERMELHA"),
    VERMELHA_METALICA("VERMELHA METÁLICA");

    override fun toString(): String {
        return name
    }

}

fun findCorByName(name: String): Cor? {
    return Cor.values().firstOrNull { it.name == name}
}
