package br.com.moveltrack.persistence.domain

import javax.persistence.*
import javax.validation.constraints.NotNull


@Entity
////@Table(name = "geoendereco")
class GeoEndereco (

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null,

    @NotNull
    var latitude: Double? = null,

    @NotNull
    var longitude: Double? = null,

    var endereco: String? = null,

    @NotNull
    var confiavel: Boolean? = null


)
