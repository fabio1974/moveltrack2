package br.com.moveltrack.persistence.pojos

import br.com.moveltrack.persistence.domain.*
import java.time.ZonedDateTime
import java.util.*


class RelatorioFiltro(
        var inicio: ZonedDateTime?,
        var fim: ZonedDateTime?,
        var fimCobranca: ZonedDateTime? = null,
        var cliente: Cliente?,
        var numeroViagem: Int?,
        var motorista: Motorista?,
        var uf: String?,
        var inferior: Int?,
        var superior: Int?,
        var veiculo: Veiculo?,
        var orderBy: String?,
        var boletoStatus: MBoletoStatus?=null,
        var instalador: Empregado?=null
        ) {
}

class RelatorioSinalFiltro(
        var equipamento: Equipamento?=null,
        var chip: Chip?=null,
        var operadora: Operadora?=null,
        var placa: String?=null,
        var veiculoTipo: VeiculoTipo?=null,
        var cliente: Cliente?=null,
        var modeloRastreador: ModeloRastreador?=null,
        var equipamentoSituacao: EquipamentoSituacao?=null,
        var veiculoStatus: VeiculoStatus?=null,
        var contratoStatus: ContratoStatus?=null,
        var atrasoMinimo: Int=0,
        var atrasoMinimoUnidade: AtrasoUnidade?=null,
        var atrasoMaximo: Int=0,
        var atrasoMaximoUnidade: AtrasoUnidade?=null
)

enum class AtrasoUnidade{
        MINUTOS,HORAS,DIAS
}
