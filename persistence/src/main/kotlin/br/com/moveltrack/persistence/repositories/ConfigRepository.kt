package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.Config
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ConfigRepository: JpaRepository<Config, Int>
