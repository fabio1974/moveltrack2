package br.com.moveltrack.persistence.domain

import javax.persistence.*


@Entity
////@Table(name = "cerca")
class Cerca {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null

    var lat1: Double = 0.toDouble()

    var lon1: Double = 0.toDouble()

    var lat2: Double = 0.toDouble()

    var lon2: Double = 0.toDouble()

    var raio: Float = 0.toFloat()

    @Transient
    var zoom: Int = 0
        get() = Math.round(24 - Math.log((1000 * this.raio).toDouble()) / Math.log(2.0)).toInt()


}
