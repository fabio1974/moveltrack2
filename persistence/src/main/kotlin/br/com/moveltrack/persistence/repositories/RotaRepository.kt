package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.Municipio
import br.com.moveltrack.persistence.domain.Rota
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(path="rotas")
interface RotaRepository: JpaRepository<Rota, Int> {
    abstract fun findAll(especification: Specification<Rota>, pageable: Pageable): Page<Rota>
    abstract fun findAllByOrigemAndDestino(origem: Municipio?, destino: Municipio?): List<Rota>?
}
