package br.com.moveltrack.persistence.domain

import javax.persistence.Entity

@Entity
class Empregado(
        var salario: Double = 0.toDouble()
) : Pessoa()
