package br.com.moveltrack.persistence.domain

enum class ContratoStatus {

    ATIVO, SUSPENSO, ENCERRADO, CANCELADO
}
