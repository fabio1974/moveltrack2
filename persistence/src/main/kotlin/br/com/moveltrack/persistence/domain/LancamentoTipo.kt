package br.com.moveltrack.persistence.domain

enum class LancamentoTipo {
    RECEBIMENTO_DE_CLIENTE,
    DEVOLUCAO_DE_DINHEIRO,
    GASTO_DE_MATERIAL,
    VALE
}
