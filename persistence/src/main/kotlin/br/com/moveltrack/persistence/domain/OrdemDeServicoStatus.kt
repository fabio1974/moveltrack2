package br.com.moveltrack.persistence.domain


enum class OrdemDeServicoStatus {
    FECHADA,
    CANCELADA
}
