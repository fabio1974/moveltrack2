package br.com.moveltrack.persistence.domain

enum class SmsStatus {
    ESPERANDO, ENVIADO, RECEBIDO, DESCARTADO, ESPERANDO_SOCKET//, ESPERANDO_SOCKET2
}
