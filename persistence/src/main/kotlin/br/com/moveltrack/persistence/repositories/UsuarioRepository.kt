package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.Usuario
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(path="usuarios")
interface UsuarioRepository: JpaRepository<Usuario, Int> {
    abstract fun findByNomeUsuario(nomeUsuario: String): Usuario?
}
