package br.com.moveltrack.persistence.domain

import java.time.ZonedDateTime

import java.util.Date
import javax.persistence.*

import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@Entity
//@Table(name="location")
class Location (

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null,

    @Column(columnDefinition="DATETIME")
    var dateLocation: ZonedDateTime? = null,

    @Column(columnDefinition="DATETIME")
    var dateLocationInicio: ZonedDateTime? = null,

    @NotNull
    var latitude: Double = 0.toDouble(),

    @NotNull
    var longitude: Double = 0.toDouble(),

    @NotNull
    var velocidade: Double = 0.toDouble(),

    @NotNull
    var imei: String? = null,

    @Size(max = 30)
    var comando: String? = null,

    var satelites: Int = 0,

    @Transient
    var endereco: String? = null,

    var mcc: Int = 0,

    @Size(max = 10)
    var bloqueio: String? = null,

    @Size(max = 10)
    var gps: String? = null,

    @Size(max = 10)
    var gsm: String? = null,

    @Size(max = 10)
    var sos: String? = null,

    @Size(max = 10)
    var battery: String? = null,

    @Size(max = 10)
    var volt: String? = null,

    @Size(max = 10)
    var ignition: String? = null,

    @Size(max = 10)
    var alarm: String? = null,

    @Size(max = 30)
    var alarmType: String? = null,

    var version: Int = 0
)
