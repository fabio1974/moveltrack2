package br.com.moveltrack.persistence.domain

enum class ChipStatus {
    ATIVO,
    INATIVO
}
