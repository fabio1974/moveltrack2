package br.com.moveltrack.persistence.domain

import javax.persistence.*


@Entity
//@Table(name = "config")
class Config  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null

    var multa: Double = 0.toDouble()

    var juros: Double = 0.toDouble()

    var migrado: Boolean = false

    var geoCodeCount: Long = 0.toLong()



}
