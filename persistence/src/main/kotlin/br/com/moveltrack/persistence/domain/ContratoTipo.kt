package br.com.moveltrack.persistence.domain


enum class ContratoTipo {

    COMODATO, AQUISICAO, VISITANTE
}
