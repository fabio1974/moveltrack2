package br.com.moveltrack.persistence.domain

enum class LancamentoStatus {
    ATIVO,
    CANCELADO
}
