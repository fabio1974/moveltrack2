package br.com.moveltrack.persistence.domain

import java.time.ZonedDateTime
import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size
import java.util.Date


@Entity
//@Table(name = "mboleto")
data class MBoleto(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null,

    var dataVencimento: ZonedDateTime? = null,

    var dataPagamento: ZonedDateTime? = null,

    var dataRegistroPagamento: ZonedDateTime? = null,

    var dataEmissao: ZonedDateTime? = null,

    @Transient
    var dataBase: ZonedDateTime? = null,

    @NotNull
    var valor: Double = 0.toDouble(),

    @NotNull
    var multa: Double = 0.toDouble(),

    @NotNull
    var juros: Double = 0.toDouble(),

    @NotNull
    @Enumerated(EnumType.STRING)
    var situacao: MBoletoStatus? = null,

    @NotNull
    @Enumerated(EnumType.STRING)
    var tipoDeCobranca: TipoDeCobranca? = null,

    @NotNull
    @Enumerated(EnumType.STRING)
    var tipo: MBoletoTipo? = null,

    @Size(max = 80)
    var mensagem34: String? = null,

    @Size(max = 300)
    var observacao: String? = null,

    @NotNull
    var nossoNumero: String? = null,

    @ManyToOne
    var carne: Carne? = null,

    @NotNull
    @ManyToOne
    var contrato: Contrato? = null,

    @ManyToOne
    var emissor: Empregado? = null,

    @ManyToOne
    var iugu: Iugu? = null
)
