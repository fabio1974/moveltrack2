package br.com.moveltrack.persistence.domain

enum class HouveBaixa {
    SIM, NAO
}
