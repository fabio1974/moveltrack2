package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.CarneConteudo
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(path="carneConteudos")
interface CarneConteudoRepositoryMy: JpaRepository<CarneConteudo, Int>
