package br.com.moveltrack.persistence.util


import br.com.moveltrack.persistence.domain.MBoleto
import br.com.moveltrack.persistence.util.Utils.DDMMYY_FORMAT
import br.com.moveltrack.persistence.util.Utils.moeda
import com.lowagie.text.*
import com.lowagie.text.pdf.PdfPTable
import com.lowagie.text.pdf.PdfWriter
import com.lowagie.text.pdf.draw.LineSeparator
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat


object BoletoUtils{

    @Throws(DocumentException::class)
    fun getBoletoIuguInBytes(mBoleto: MBoleto): ByteArray {

        println("id recuperado: ${mBoleto.id} - Nome: ${mBoleto.contrato?.cliente?.nome}")

        var os = ByteArrayOutputStream()

        val document = Document(PageSize.A4, 2.toFloat(), 2.toFloat(), 36.toFloat(), 36.toFloat())

        PdfWriter.getInstance(document, os)
        document.open()

        val ls = LineSeparator()
        ls.setLineWidth(2.toFloat())
        ls.setPercentage(120.toFloat())

        document.newPage()
        document.add(Chunk(ls))
        document.add(getBoletoTable(mBoleto))
        document.add(Chunk(ls))

        document.close()
        return os.toByteArray()
    }

    @Throws(DocumentException::class)
    fun getCarneIuguInBytes(mBoletos: List<MBoleto>): ByteArray {

        var os = ByteArrayOutputStream()

        val document = Document(PageSize.A4, 2.toFloat(), 2.toFloat(), 36.toFloat(), 36.toFloat())

        PdfWriter.getInstance(document, os)
        document.open()

        val ls = LineSeparator()
        ls.setLineWidth(2.toFloat())
        ls.setPercentage(120.toFloat())

        var i = 0

        while (i < mBoletos.size) {

            document.newPage()
            document.add(Chunk(ls))
            document.add(getBoletoTable(mBoletos[i++]))
            document.add(Chunk(ls))

            if (i < mBoletos.size) {
                document.add(getBoletoTable(mBoletos[i++]))
                document.add(Chunk(ls))
            }

            if (i < mBoletos.size) {
                document.add(getBoletoTable(mBoletos[i++]))
                document.add(Chunk(ls))
            }
        }
        document.close()
        return os.toByteArray()
    }


    fun getBoletoTable(mBoleto: MBoleto): PdfPTable? {

        if (mBoleto.iugu == null)
            return null

        val table = PdfPTable(58)
        //val sdf = SimpleDateFormat("dd/MM/yyyy")

        table.setHorizontalAlignment(PdfPTable.ALIGN_CENTER)
        table.setWidthPercentage(100.toFloat())

        table.setSpacingBefore(3.toFloat())

        table.addCell(UtilsReport.getCellHeader("RECIBO DO SACADO", 9, Paragraph.ALIGN_CENTER, 8, UtilsReport.FAIRLY_LIGTH_GRAY))
        table.addCell(UtilsReport.getEmptyCell(1))

        table.addCell(UtilsReport.getImageCellFromPath("237.png", 9))
        table.addCell(UtilsReport.getCellBoleto("BANCO BRADESCO S.A.", mBoleto.iugu!!.codigoBarras!!, 39, Paragraph.ALIGN_LEFT))

        table.addCell(UtilsReport.getCellBoleto("NOSSO NÚMERO", mBoleto.nossoNumero!!, 9, Paragraph.ALIGN_LEFT, 8, 8))
        table.addCell(UtilsReport.getEmptyCell(1))
        table.addCell(UtilsReport.getCellBoleto("LOCAL DE PAGAMENTO", "Pagável em qualquer banco até o vencimento. Após vencimento, somente no Bradesco.", 39, Paragraph.ALIGN_LEFT, 8, 9))
        table.addCell(UtilsReport.getCellBoleto("NOSSO NÚMERO", mBoleto.nossoNumero!!, 9, Paragraph.ALIGN_RIGHT, 8, 8))

        table.addCell(UtilsReport.getCellBoleto("VENCIMENTO", mBoleto.dataVencimento!!.format(DDMMYY_FORMAT), 9, Paragraph.ALIGN_LEFT, 8, 9))
        table.addCell(UtilsReport.getEmptyCell(1))
        table.addCell(UtilsReport.getCellBoleto("CEDENTE", "Moveltrack Segurança & Tecnologia Ltda", 28, Paragraph.ALIGN_LEFT, 8, 9))
        table.addCell(UtilsReport.getCellBoleto("CNPJ", "17.547.013/0001-88", 11, Paragraph.ALIGN_LEFT, 8, 9))
        table.addCell(UtilsReport.getCellBoleto("VENCIMENTO",mBoleto.dataVencimento!!.format(DDMMYY_FORMAT), 9, Paragraph.ALIGN_RIGHT, 8, 9))

        val cliente = mBoleto.contrato?.cliente

        table.addCell(UtilsReport.getCellBoleto("VALOR DO DOC", moeda(mBoleto.valor), 9, Paragraph.ALIGN_LEFT, 8, 9))
        table.addCell(UtilsReport.getEmptyCell(1))
        table.addCell(UtilsReport.getCellBoleto("CLIENTE", cliente!!.nome!!, 28, Paragraph.ALIGN_LEFT, 8, 9))
        table.addCell(UtilsReport.getCellBoleto("CPF/CNPJ", if (cliente.cpf != null) cliente.cpf!! else cliente.cnpj!!, 11, Paragraph.ALIGN_LEFT, 8, 9))
        table.addCell(UtilsReport.getCellBoleto("VALOR DO DOC", moeda(mBoleto.valor), 9, Paragraph.ALIGN_RIGHT, 8, 9))

        table.addCell(UtilsReport.getCellBoleto("CEDENTE", "Moveltrack S.& T.", 9, Paragraph.ALIGN_LEFT, 8, 9))
        table.addCell(UtilsReport.getEmptyCell(1))
        val mensagem34 = mBoleto.mensagem34
        table.addCell(UtilsReport.getCellBoleto("DESCRIÇÃO", if (mensagem34 == null) " " else mensagem34, 39, Paragraph.ALIGN_LEFT, 8, 8))

        table.addCell(UtilsReport.getCellBoleto("MULTA/JUROS", "", 9, Paragraph.ALIGN_RIGHT, 8, 9))

        //val c = Calendar.getInstance()
        //c.time = mBoleto.dataVencimento
        //c.add(Calendar.DAY_OF_YEAR, 30)


        table.addCell(UtilsReport.getCellBoleto("CPF/CNPJ SACADO", if (cliente.cpf != null) cliente.cpf!! else cliente.cnpj!!, 9, Paragraph.ALIGN_LEFT, 8, 9))
        table.addCell(UtilsReport.getEmptyCell(1))
        table.addCell(UtilsReport.getCellBoleto("INSTRUÇÕES", "Multa por atraso: 5%. Juros 1% ao mês. Não receber após ${mBoleto.dataVencimento?.plusDays(30)?.format(DDMMYY_FORMAT)}", 39, Paragraph.ALIGN_LEFT, 8, 9))
        table.addCell(UtilsReport.getCellBoleto("VALOR A PAGAR", moeda(mBoleto.valor), 9, Paragraph.ALIGN_RIGHT, 8, 9))

        table.addCell(UtilsReport.getCellBoleto("NOME DO SACADO", cliente.nome!!, 9, Paragraph.ALIGN_LEFT, 8, 5))
        table.addCell(UtilsReport.getEmptyCell(1))
        table.addCell(UtilsReport.getImageCellFromByteArray(mBoleto.iugu?.codigoBarrasImagem!!, 32))
        table.addCell(UtilsReport.getImageCellFromPath("236.png", 16, true, 20))
        return table
    }




}




