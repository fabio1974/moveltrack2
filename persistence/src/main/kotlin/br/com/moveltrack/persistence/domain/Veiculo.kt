package br.com.moveltrack.persistence.domain


import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
//@Table(name = "veiculo")
class Veiculo(

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int? = null,

        var placa: String? = null,

        @NotNull
        var marcaModelo: String? = null,

        var motorista: String? = null,

        var descricao: String? = null,

        var velocidadeMaxima: Double = 0.toDouble(),

        @NotNull
        @Enumerated(EnumType.STRING)
        var cor: Cor? = null,

        @Temporal(TemporalType.TIMESTAMP)
        var dataInstalacao: Date? = null,

        @Temporal(TemporalType.TIMESTAMP)
        var dataDesinstalacao: Date? = null,

        @OneToOne
        var equipamento: Equipamento? = null,

        @ManyToOne
        var instalador: Empregado? = null,

        @ManyToOne(fetch = FetchType.LAZY)
        var contrato: Contrato? = null,

        var botaoPanico: Boolean = false,

        var demo: Boolean = false,

        var comEscuta: Boolean = false,

        var comCercaVirtual: Boolean = false,

        var comBloqueio: Boolean = false,

        var consumoCombustivel: Double = 0.toDouble(),

        @NotNull
        @Enumerated(EnumType.STRING)
        var tipo: VeiculoTipo? = null,

        @Enumerated(EnumType.STRING)
        var status: VeiculoStatus? = null,

        @Transient
        var lastLocation: Location?=null

)


