package br.com.moveltrack.persistence.domain

import java.util.*
import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.Temporal
import javax.persistence.TemporalType
import javax.validation.constraints.Size


@Entity
class Cliente(

        @Size(max = 40)
        var nomeFantasia: String? = null,

        @Size(max = 100)
        var observacao: String? = null,

        var emailAlarme: String? = null,

        @ManyToOne
        var cerca: Cerca? = null,

        @Temporal(TemporalType.TIMESTAMP)
        var ultimaCobranca: Date? = null,

        @Temporal(TemporalType.TIMESTAMP)
        var ultimoLembrete: Date? = null


) : Pessoa()
