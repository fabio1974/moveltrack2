package br.com.moveltrack.persistence.util

import com.lowagie.text.Font
import com.lowagie.text.Paragraph

class MyParagraph(alignment: Int, leading: Float, string: String, font: Font, spacingAfter: Int) : Paragraph(leading, string, font) {

    init {
        super.setAlignment(alignment)
        super.setSpacingAfter(spacingAfter.toFloat())
        //super.setIndentationRight(3f)
    }

}
