package br.com.moveltrack.persistence.pojos

import java.util.*

class RelatorioFrota (
        var id: Int? = null,
        var placa: String? = null,
        var marcaModelo: String? = null,
        var motorista: String? = null,
        var ano: Int = 0,
        var mes: Int = 0,
        var data: Date? = null,
        var destino: String? = null,
        var estado: String? = null,
        var qtdViagens: Int = 0,
        var diasViagens: Double = 0.toDouble(),
        var qtdCidades: Int = 0,
        var qtdClientes: Int = 0,
        var valorDaCarga: Double = 0.toDouble(),
        var pesoDaCarga: Int = 0,
        var despesaEstiva: Double = 0.toDouble(),
        var despesaDiaria: Double = 0.toDouble(),
        var despesaCombustivel: Double = 0.toDouble(),
        var despesaOutras: Double = 0.toDouble(),
        var despesaTotal: Double = 0.toDouble(),
        var distanciaPercorrida: Double = 0.toDouble(),
        var litros: Double = 0.toDouble(),
        var kml: Double = 0.toDouble(),
        var valorDevolucao: Double = 0.toDouble()
)
