package br.com.moveltrack.persistence.domain

enum class MBoletoTipo {
    AVULSO,
    ENTRADA,
    MENSAL
}
