package br.com.moveltrack.persistence.domain

enum class DespesaFrotaEspecie private constructor(val descricao: String) {

    //Veículos
    COMBUSTIVEL("COMBUSTÍVEL"),
    ESTIVA("SERV. DE ESTIVA"),
    DIARIA("DIÁRIA"),
    IPVA("IPVA"),
    LICENCIAMENTO("LICENCIAMENTO"),
    MANUTENCAO("MANUTENÇÃO - PEÇAS E SERVIÇOS"),
    MULTA_DE_TRANSITO("MULTA DE TRANSITO"),
    OUTROS("OUTROS"),
    SEGURO_OBRIGATORIO("SEGURO OBRIGATORIO"),
    TRABALHISTAS("SALARIOS E ADICIONAIS TRAB.");

    override fun toString(): String {
        return name
    }

}
