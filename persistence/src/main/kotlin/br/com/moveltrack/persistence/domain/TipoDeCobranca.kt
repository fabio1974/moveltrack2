package br.com.moveltrack.persistence.domain

enum class TipoDeCobranca {
    COM_REGISTRO, SEM_REGISTRO
}
