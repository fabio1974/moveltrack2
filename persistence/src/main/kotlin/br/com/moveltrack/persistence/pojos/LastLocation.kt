package br.com.moveltrack.persistence.pojos


import br.com.moveltrack.persistence.domain.Cor
import br.com.moveltrack.persistence.domain.VeiculoStatus
import br.com.moveltrack.persistence.domain.VeiculoTipo
import java.sql.Timestamp
import java.time.ZonedDateTime
import javax.persistence.Column
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.Transient
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class
LastLocation(

        var placa: String? = null,

        var marcaModelo: String? = null,

        @Enumerated(EnumType.STRING)
        var cor: Cor? = null,

        @NotNull
        @Enumerated(EnumType.STRING)
        var tipo: VeiculoTipo? = null,

        @Enumerated(EnumType.STRING)
        var status: VeiculoStatus? = null,

        var dateLocation: Timestamp? = null,

        @NotNull
        var latitude: Double = 0.toDouble(),

        @NotNull
        var longitude: Double = 0.toDouble(),

        @NotNull
        var velocidade: Double = 0.toDouble(),

        @NotNull
        var imei: String? = null,

        @Size(max = 30)
        var comando: String? = null,

        var battery: String? = null,

        var gps: String? = null,
        var gsm: String? = null,
        var ignition: String? = null,

        var satelites: Int = 0,

        @Transient
        var endereco: String? = null


        )
