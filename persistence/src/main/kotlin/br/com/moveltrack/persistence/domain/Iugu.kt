package br.com.moveltrack.persistence.domain

import javax.persistence.*


@Entity
//@Table(name="iugu")
class Iugu (
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)  var id: Int? = null,
    var codigoBarras: String? = null,
    var invoiceId: String? = null,
    @Lob var codigoBarrasImagem: ByteArray? = null
)
