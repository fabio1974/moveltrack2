package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.Location
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import java.time.ZonedDateTime

@RepositoryRestResource(path="locations")
interface LocationRepository: JpaRepository<Location, Int> {
    //fun findAllByImeiAndDateLocationAfterAndDateLocationBeforeOrderByDateLocationInicio(imei:String,inicio:ZonedDateTime,fim:ZonedDateTime): List<Location>
    fun findAllByImeiAndDateLocationGreaterThanEqualAndDateLocationLessThanEqualOrderByDateLocationInicio(imei:String, inicio: ZonedDateTime, fim:ZonedDateTime): List<Location>
    fun findFirstByImeiAndDateLocationAfterOrderByDateLocation(imei: String, dateLocation: ZonedDateTime?): Location?
    fun findFirstByImeiAndDateLocationLessThanEqualOrderByDateLocationDesc(imei: String, dateLocation: ZonedDateTime?): Location?
    fun findFirstByImeiAndDateLocationBeforeOrderByDateLocationDesc(imei: String, inicio: ZonedDateTime): Location?
    abstract fun findFirstByImeiOrderByDateLocation(imei: String?): Location?
    abstract fun findFirstByImeiOrderByDateLocationDesc(imei: String?): Location?
    abstract fun findFirstByImeiAndGsmIsNotNullOrderByDateLocationDesc(imei: String?): Location?
    abstract fun deleteAllByDateLocationBefore(dateLocation: ZonedDateTime?): Long
}
