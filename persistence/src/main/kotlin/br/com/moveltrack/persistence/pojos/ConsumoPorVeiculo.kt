package br.com.moveltrack.persistence.pojos

import javax.persistence.Id

class ConsumoPorVeiculo(
    @Id
    var id: Int? = null,
    var placa: String? = null,
    var marcaModelo: String? = null,
    var qtdViagens: Int = 0,
    var distanciaPercorrida: Double = 0.toDouble(),
    var distanciaHodometro: Double = 0.toDouble(),
    var litros: Double = 0.toDouble(),
    var kml: Double = 0.toDouble(),
    var kml2: Double = 0.toDouble(),
    var erro: Double = 0.toDouble()
)
