package br.com.moveltrack.persistence.domain

import java.time.ZonedDateTime
import javax.persistence.*
import kotlin.jvm.Transient

@Entity
data class RelatorioUsoIndevidoParam (

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null,

    @ManyToOne
    var cliente: Cliente?=null,

    var velocidade: Float?=null,

    var segundaInicio: ZonedDateTime?=null,
    var segundaFim: ZonedDateTime?=null,
    var tercaInicio: ZonedDateTime?=null,
    var tercaFim: ZonedDateTime?=null,
    var quartaInicio: ZonedDateTime?=null,
    var quartaFim: ZonedDateTime?=null,
    var quintaInicio: ZonedDateTime?=null,
    var quintaFim: ZonedDateTime?=null,
    var sextaInicio: ZonedDateTime?=null,
    var sextaFim: ZonedDateTime?=null,
    var sabadoInicio: ZonedDateTime?=null,
    var sabadoFim: ZonedDateTime?=null,
    var domingoInicio: ZonedDateTime?=null,
    var domingoFim: ZonedDateTime?=null,

    @Transient
    var inicio: ZonedDateTime? = null,

    @Transient
    var fim: ZonedDateTime? = null,

    @Transient
    var orderBy: String?=null,

    @Transient
    var veiculo: Veiculo?=null




)
