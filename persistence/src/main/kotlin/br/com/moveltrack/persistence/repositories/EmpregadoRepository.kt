package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.Chip
import br.com.moveltrack.persistence.domain.Empregado
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(path="empregados")
interface EmpregadoRepository: PagingAndSortingRepository<Empregado, Int>, JpaSpecificationExecutor<Empregado> {
    abstract fun findByCpf(cpfEmissor: String): Empregado
    abstract fun findAllByNomeContaining(nome: String): List<Empregado>
}
