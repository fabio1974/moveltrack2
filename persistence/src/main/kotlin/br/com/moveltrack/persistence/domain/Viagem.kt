package br.com.moveltrack.persistence.domain

import java.time.ZonedDateTime

import java.util.*
import javax.persistence.*

@Entity
//@Table(name="viagem")
class Viagem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null

    var numeroViagem: Int? = null
    var descricao: String? = null

    @ManyToOne
    var cliente: Cliente? = null

    @ManyToOne
    var veiculo: Veiculo? = null

    @ManyToOne
    var rota: Rota? = null


    @ManyToOne
    var motorista: Motorista? = null


    @ManyToOne
    var cidadeOrigem: Municipio? = null

    @ManyToOne
    var cidadeDestino: Municipio? = null

    @Enumerated(EnumType.STRING)
    var status: ViagemStatus? = null
        set(status) {
            if(id!=null)  println("alterando viagem " + id + " de " + this.status + " para " + status)
            field = status
        }

    var partida: ZonedDateTime? = null

    var saiuDaCerca: ZonedDateTime? = null
        set(saiuDaCerca) {
            if(id!=null) println("saiuDaCerca da viagem " + id + " de " + this.saiuDaCerca + " para " + saiuDaCerca)
            field = saiuDaCerca
        }

    var entrouNaCerca: ZonedDateTime? = null
        set(entrouNaCerca) {
            if(id!=null)  println("entrouNaCerca da viagem " + id + " de " + this.entrouNaCerca + " para " + entrouNaCerca)
            field = entrouNaCerca
        }


    var chegadaReal: ZonedDateTime? = null
        set(chegadaReal) {
            if(id!=null) println("chegadaReal da viagem " + id + " de " + this.chegadaReal + " para " + chegadaReal)
            field = chegadaReal
        }


    var chegadaPrevista: ZonedDateTime? = null

    var valorDaCarga: Double = 0.toDouble()
    var valorDevolucao: Double = 0.toDouble()
    var pesoDaCarga: Int = 0
    var distanciaPercorrida: Double = 0.toDouble()
    var distanciaHodometro: Double = 0.toDouble()
    var qtdCidades: Int = 0
    var qtdClientes: Int = 0

    fun numeroViagemFormatado(): String? {
        return if (this.numeroViagem == null) null else String.format("%05d", numeroViagem!!.toInt())
    }
}
