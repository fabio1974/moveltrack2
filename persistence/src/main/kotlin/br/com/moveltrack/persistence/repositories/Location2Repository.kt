package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.Location2
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import java.time.ZonedDateTime

@RepositoryRestResource(path="locations2")
interface Location2Repository: JpaRepository<Location2, Int> {
    fun findAllByImeiAndDateLocationAfterAndDateLocationBeforeOrderByDateLocationInicio(imei:String, inicio: ZonedDateTime, fim:ZonedDateTime): List<Location2>
    fun findFirstByImeiAndDateLocationBeforeOrderByDateLocationDesc(imei: String, dateLocation: ZonedDateTime?): Location2?
    fun deleteByImeiAndDateLocation(imei: String?, dateLocation: ZonedDateTime?)
    fun findFirstByImeiOrderByDateLocationDesc(imei:String?):Location2?
    abstract fun deleteAllByDateLocationBefore(dateLocation: ZonedDateTime?): Long
}
