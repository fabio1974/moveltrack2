package br.com.moveltrack.persistence.domain



import java.time.ZonedDateTime
import javax.persistence.*
import javax.validation.constraints.Size
import java.util.Date
import kotlin.jvm.Transient

@Entity
class Equipamento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null

    @Column(columnDefinition="DATETIME")
    var dataCadastro: ZonedDateTime? = null

    @Column(columnDefinition="DATETIME")
    var inicioSpot: ZonedDateTime? = null

    @Column(columnDefinition="DATETIME")
    var vencimentoSpot: ZonedDateTime? = null

    @Enumerated(EnumType.STRING)
    var situacao: EquipamentoSituacao? = null

    @ManyToOne
    var proprietario: Cliente? = null

    @Enumerated(EnumType.STRING)
    var modelo: ModeloRastreador? = null

    @Size(max = 6)
    var senha: String? = null

    var imei: String? = null

    var atrasoGmt: Int = 0

    @OneToOne
    var chip: Chip? = null

    var comando: Int = 0

    @Size(max = 150)
    var observacao: String? = null

    @Temporal(TemporalType.TIMESTAMP)
    var ultimaAlteracao: Date? = null

    @ManyToOne
    var possuidor: Empregado? = null

    @Transient
    var label: String? = null


    @Transient
    var lastLocation: Location?=null

    @Transient
    var veiculo: Veiculo?=null



}
