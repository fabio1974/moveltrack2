package br.com.moveltrack.persistence.domain

import java.time.ZonedDateTime
import java.util.*
import javax.persistence.*
import javax.validation.constraints.Size


@Entity
class Lancamento (

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int? = null,

    //@Temporal(TemporalType.TIMESTAMP)
    val data: ZonedDateTime? = null,

    val valor: Double = 0.toDouble(),

    @Enumerated(EnumType.STRING)
    val operacao: LancamentoTipo? = null,

    @Enumerated(EnumType.STRING)
    val status: LancamentoStatus? = null,

    @Enumerated(EnumType.STRING)
    val houveBaixa: HouveBaixa? = null,

    @Enumerated(EnumType.STRING)
    val formaPagamento: LancamentoFormaPagamento? = null,

    @Size(max = 300)
    val observacao: String? = null,

    @ManyToOne
    val ordemDeServico: OrdemDeServico? = null,

    @ManyToOne
    val solicitante: Empregado? = null,

    @ManyToOne
    val operador: Empregado? = null
)
