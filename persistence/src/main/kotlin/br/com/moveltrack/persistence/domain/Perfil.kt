package br.com.moveltrack.persistence.domain


import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
//@Table(name = "perfil")
data class Perfil(
        @Id
        var id: Int? = null,

        @NotNull
        @Enumerated(EnumType.STRING)
        var tipo: PerfilTipo? = null,

        @ManyToMany(fetch = FetchType.EAGER)
        var permissoes: Set<Permissao> = HashSet<Permissao>()
)
