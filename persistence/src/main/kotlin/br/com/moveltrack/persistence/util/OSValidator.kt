package br.com.moveltrack.persistence.util

object OSValidator {

    private val OS = System.getProperty("os.name").toLowerCase()

    val isWindows: Boolean
        get() {
            println(OS)
            return OS.indexOf("win") >= 0

        }

    val isMac: Boolean
        get() {
            println(OS)
            return OS.indexOf("mac") >= 0

        }

    val isUnix: Boolean
        get() {
            println(OS)
            return OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0

        }

    val isSolaris: Boolean
        get() {
            println(OS)
            return OS.indexOf("sunos") >= 0

        }

  /*  @JvmStatic
    fun main(args: Array<String>) {

        println(OS)

        if (isWindows) {
            println("This is Windows")
        } else if (isMac) {
            println("This is Mac")
        } else if (isUnix) {
            println("This is Unix or Linux")
        } else if (isSolaris) {
            println("This is Solaris")
        } else {
            println("Your OS is not support!!")
        }
    }*/

}
