package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.Permissao
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(path="permissaos")
interface PermissaoRepository: JpaRepository<Permissao, Int>{

}
