package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.Carne
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(path="carnes")
interface CarneRepository:  PagingAndSortingRepository<Carne, Int>, JpaSpecificationExecutor<Carne>

