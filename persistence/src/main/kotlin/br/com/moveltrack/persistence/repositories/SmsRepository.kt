package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.Sms
import br.com.moveltrack.persistence.domain.SmsStatus
import br.com.moveltrack.persistence.domain.SmsTipo
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import java.time.ZonedDateTime

@RepositoryRestResource(path="smss",exported = false)
interface SmsRepository: PagingAndSortingRepository<Sms, Int>, JpaSpecificationExecutor<Sms> {
    abstract fun findFirstByCelularAndStatusOrderByDataUltimaAtualizacaoDesc(celular: String,status: SmsStatus): Sms?
    abstract fun findFirstByCelularOrderByDataUltimaAtualizacaoDesc(celular: String): Sms?
    abstract fun findFirstByCelularOrderByIdDesc(celular: String): Sms?
    abstract fun findAllByStatusAndDataUltimaAtualizacaoAfter(status:SmsStatus,dataUltimaAtualizacao: ZonedDateTime): List<Sms>?
    abstract fun findFirstByStatusAndDataUltimaAtualizacaoAfterOrderByDataUltimaAtualizacaoDesc(status:SmsStatus,dataUltimaAtualizacao: ZonedDateTime): Sms?
    abstract fun findFirstByStatusAndDataUltimaAtualizacaoAfterAndTipoNotInOrderByDataUltimaAtualizacaoDesc(status:SmsStatus,dataUltimaAtualizacao: ZonedDateTime,tipos: List<SmsTipo>): Sms?
    abstract fun findAllByStatusAndDataUltimaAtualizacaoAfterOrderByDataUltimaAtualizacaoDesc(status:SmsStatus,dataUltimaAtualizacao: ZonedDateTime): List<Sms>?
}
