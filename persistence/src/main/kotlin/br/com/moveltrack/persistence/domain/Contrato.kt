package br.com.moveltrack.persistence.domain

//import br.com.moveltrack2.backend.persistence.mongodb.domain.Veiculo

import java.time.ZonedDateTime
import java.util.*
import javax.persistence.*


@Entity
////@Table(name = "contrato")
class Contrato(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int? = null,

        var numeroContrato: String? = null,

        @ManyToOne
        var cliente: Cliente? = null,

        var inicio: ZonedDateTime? = null,

        var fim: ZonedDateTime? = null,

        var diaVencimento: Int = 0,

        @ManyToOne
        var vendedor: Pessoa? = null,

        var pagamentoAnual: Boolean = false,

        @Enumerated(EnumType.STRING)
        var contratoGeraCarne: ContratoGeraCarne? = null,

        @Enumerated(EnumType.STRING)
        var contratoTipo: ContratoTipo? = null,

        var mensalidade: Double = 0.toDouble(),

        var entrada: Double = 0.toDouble(),

        @Enumerated(EnumType.STRING)
        var status: ContratoStatus? = null,

        @Temporal(TemporalType.TIMESTAMP)
        var ultimaAlteracao: Date? = null,

        @Transient
        var veiculos: List<Veiculo>? = null,

        @Transient
        var dataBase: ZonedDateTime? = null  //Data de vencimento do ultimo boleto do carnê anterior


) {
    @get:Transient
    val label: String
        get() = "$numeroContrato-${cliente?.nome}"
}
