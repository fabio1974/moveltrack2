package br.com.moveltrack.persistence.domain

enum class PessoaStatus {
    ATIVO,
    INATIVO
}
