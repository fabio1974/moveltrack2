package br.com.moveltrack.persistence.domain

enum class SmsTipo {
    BLOQUEIO, DESBLOQUEIO, REINICIAR, CHECAR, AVISO, DNS
}
