package br.com.moveltrack.persistence.domain

import java.time.ZonedDateTime
import java.util.Date
import javax.persistence.*

import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@Entity
////@Table(name="distanciadiaria")
class DistanciaDiaria(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null,

    var dataComputada: ZonedDateTime? = null,

    @ManyToOne
    var veiculo: Veiculo? = null,

    var distanciaPercorrida: Double = 0.toDouble()
)


