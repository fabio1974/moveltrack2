package br.com.moveltrack.persistence.domain

import javax.persistence.*

@Entity
//@Table(name="rota")
data class Rota (

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null,

    @ManyToOne
    var origem: Municipio? = null,

    @ManyToOne
    var destino: Municipio? = null,

    @ManyToMany(fetch = FetchType.LAZY)
    var itinerario:List<Municipio>? = null, //HashSet<Municipio>()

    @Lob
    var polyline: String? = null,

    var distancia: Double?= null,

    @ManyToOne
    var cliente: Cliente? = null

) {

}
