package br.com.moveltrack.persistence.domain

import java.time.ZonedDateTime
import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@Entity
class Sms (

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null,

    val celular: String? = null,

    @NotNull
    @Size(max = 500)
    val mensagem: String? = null,

    val imei: String? = null,

    @Enumerated(EnumType.STRING)
    val tipo: SmsTipo? = null,

    @NotNull
    @Enumerated(EnumType.STRING)
    var status: SmsStatus? = null,

    @NotNull
    @Column(columnDefinition="DATETIME")
    var dataUltimaAtualizacao: ZonedDateTime? = null

)
