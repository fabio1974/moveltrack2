package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.Municipio
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(path="municipios",exported = false)
interface MunicipioRepositoryMy : JpaRepository<Municipio, Int> {
    abstract fun findByDescricaoContaining(descricao: String?): MutableList<Municipio>
}
