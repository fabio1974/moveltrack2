package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.Empregado
import br.com.moveltrack.persistence.domain.Lancamento
import br.com.moveltrack.persistence.domain.LancamentoTipo
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import java.time.ZonedDateTime

@RepositoryRestResource(path="lancamentos",exported = false)
interface LancamentoRepository: PagingAndSortingRepository<Lancamento, Int>, JpaSpecificationExecutor<Lancamento> {
    abstract fun findAllByOperacaoAndSolicitanteAndDataBetweenOrderByData(tipo:LancamentoTipo?,solicitante:Empregado?,inicio:ZonedDateTime?,fim:ZonedDateTime?): List<Lancamento>?
}
