package br.com.moveltrack.persistence.domain

enum class OrdemDeServicoTipo private constructor(val descricao: String) {

    COBRANCA("Cobrança"),
    INSTALACAO("Instalação Rastreador"),
    RETIRADA("Retirada de Rastreador"),
    MANUTENCAO("Manutenção de Rastreador");

    override fun toString(): String {
        return name
    }

}
