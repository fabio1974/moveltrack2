package br.com.moveltrack.persistence.util

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.regex.Pattern


object Utils {

    val DDMMYY_FORMAT = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    val DD_MM_YY_FORMAT = DateTimeFormatter.ofPattern("dd_MM_yyyy");

    val DOLAR = DecimalFormatSymbols(Locale.US)
    val DINHEIRO_DOLAR = DecimalFormat("¤ ###,###,##0.00", DOLAR)
    val EURO = DecimalFormatSymbols(Locale.GERMANY)
    val DINHEIRO_EURO = DecimalFormat("¤ ###,###,##0.00", EURO)
    val BRAZIL = Locale("pt", "BR")
    val REAL = DecimalFormatSymbols(BRAZIL)
    val DINHEIRO_REAL = DecimalFormat("¤ ###,###,##0.00", REAL)
    val `DINHEIRO_REAL_SEM_R$` = DecimalFormat("###,###,##0.00", REAL)

    fun mascaraDinheiro(valor: Double, moeda: DecimalFormat): String {
        return moeda.format(valor)
    }

    fun moeda(valor: Double): String {
        return DINHEIRO_REAL.format(valor)
    }




    fun mascaraDinheiro(valor: Float, moeda: DecimalFormat): String {
        return moeda.format(valor.toDouble())
    }

    fun validEmail(email: String?): Boolean {
        if(email==null)
            return false
        val p = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,7}$")
        val m = p.matcher(email)
        return m.find()
    }

}
