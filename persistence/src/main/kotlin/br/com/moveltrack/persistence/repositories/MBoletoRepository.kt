package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.*
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import java.time.ZonedDateTime
import java.util.*

@Repository
interface MBoletoRepository: PagingAndSortingRepository<MBoleto, Int>, JpaSpecificationExecutor<MBoleto>, MBoletoRepositoryCustom{
    abstract fun findTopByTipoAndContratoOrderByDataVencimentoDesc(mBoletoTipo: MBoletoTipo, contrato: Contrato): MBoleto?
    fun findAllBySituacaoAndCarne(mBoletoStatus: MBoletoStatus, carne: Carne): List<MBoleto>
    abstract fun findByIugu(iugu:Iugu?): MBoleto?
    abstract fun findAllByCarne(carne: Carne?): List<MBoleto>
    abstract fun findAllByContrato_StatusAndSituacaoAndDataVencimentoBefore(contratoStatus: ContratoStatus, situacao: MBoletoStatus,dataVencimento: ZonedDateTime): List<MBoleto>?
    abstract fun findAllBySituacaoAndDataVencimentoBefore(situacao: MBoletoStatus,dataVencimento: ZonedDateTime): List<MBoleto>?
    abstract fun findAllByContratoAndSituacao(contrato: Contrato,situacao: MBoletoStatus): List<MBoleto>?
    abstract fun countAllByContrato_IdAndSituacao(contratoId: Int,situacao: MBoletoStatus): Int?
    abstract fun findAllBySituacaoAndDataRegistroPagamentoBetweenAndDataPagamentoIsNull(situacao: MBoletoStatus,inicio: ZonedDateTime, fim: ZonedDateTime): List<MBoleto>?
}
