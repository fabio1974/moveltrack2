package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.Empregado
import br.com.moveltrack.persistence.domain.OrdemDeServico
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import java.time.ZonedDateTime

@RepositoryRestResource(path="ordemDeServicos",exported = false)
interface OrdemDeServicoRepository: PagingAndSortingRepository<OrdemDeServico, Int>, JpaSpecificationExecutor<OrdemDeServico> {
    abstract fun findAllByOperadorAndDataDoServicoBetweenOrderByDataDoServico(instalador:Empregado?,inicio:ZonedDateTime?,fim:ZonedDateTime?): List<OrdemDeServico>?
}
