package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.Pessoa
import br.com.moveltrack.persistence.domain.Usuario
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(path="pessoas",exported = false)
interface PessoaRepository: JpaRepository<Pessoa, Int> {
    abstract fun findByUsuario(usuario: Usuario): Pessoa
}
