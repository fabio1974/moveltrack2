package br.com.moveltrack.persistence.domain

enum class VeiculoStatus {
    ATIVO, INATIVO
}

fun findVeiculoStatusByName(name: String): VeiculoStatus? {
    return VeiculoStatus.values().firstOrNull { it.name == name}
}
