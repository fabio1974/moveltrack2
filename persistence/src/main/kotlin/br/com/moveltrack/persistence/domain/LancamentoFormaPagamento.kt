package br.com.moveltrack.persistence.domain

enum class LancamentoFormaPagamento {
    DINHEIRO,
    CARTAO,
    CHEQUE
}
