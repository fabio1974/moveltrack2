package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.DespesaFrota
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
interface DespesaFrotaRepositoryMy: PagingAndSortingRepository<DespesaFrota, Int>, JpaSpecificationExecutor<DespesaFrota> {
}
