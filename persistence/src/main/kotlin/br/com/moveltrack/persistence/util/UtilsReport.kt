package br.com.moveltrack.persistence.util

import java.awt.Color
import java.io.IOException

import javax.servlet.ServletContext

import com.lowagie.text.BadElementException
import com.lowagie.text.Element
import com.lowagie.text.Font
import com.lowagie.text.Image
import com.lowagie.text.Paragraph
import com.lowagie.text.pdf.PdfPCell
import com.lowagie.text.pdf.PdfPTable
import com.lowagie.text.pdf.draw.DottedLineSeparator
import com.lowagie.text.pdf.draw.LineSeparator
import org.springframework.core.io.ClassPathResource
import org.springframework.util.ResourceUtils
import java.io.File


object UtilsReport {

    val FAIRLY_LIGTH_GRAY = Color(210, 210, 210)
    val interTables = 10f


    /*
		    Image image;
		    String filePath = "basepath" + File.separator + "images"+ File.separator + "LOGOTIPO_WEB.png";
			System.out.println(filePath);
			image = Image.getInstance(filePath);
			table.addCell(getImageCell(image,2,Paragraph.ALIGN_CENTER));*///table.addCell(getImageCell(image,2,Paragraph.ALIGN_CENTER));
    val cabecalho: PdfPTable?
        get() {
            try {
                val table = PdfPTable(9)
                table.horizontalAlignment = PdfPTable.ALIGN_LEFT
                table.widthPercentage = 100f

                val c = PdfPCell()
                c.addElement(MyParagraph(Paragraph.ALIGN_CENTER, 12f, "MOVELTRACK RASTREAMENTO", Font(Font.TIMES_ROMAN, 12f), 12))
                c.addElement(MyParagraph(Paragraph.ALIGN_CENTER, 0f, "Av. Visconde do Rio Branco, 3066, sala 04 - Fortaleza/CE", Font(Font.TIMES_ROMAN, 10f), 12))
                c.addElement(MyParagraph(Paragraph.ALIGN_CENTER, 0f, "CNPJ: 17.547.013/0001-88", Font(Font.TIMES_ROMAN, 10f), 12))
                c.addElement(MyParagraph(Paragraph.ALIGN_CENTER, 0f, "Telefones: (85)4105-0145 / (85)98549-7318", Font(Font.TIMES_ROMAN, 10f), 2))
                c.colspan = 5

                table.addCell(c)
                return table
            } catch (e: Exception) {
                e.printStackTrace()
                return null
            }

        }


    fun getInformacoes(titulo: String, linhas: List<String>): PdfPTable? {

        try {
            val table = PdfPTable(1)
            table.horizontalAlignment = PdfPTable.ALIGN_LEFT
            table.widthPercentage = 100f

            val c = PdfPCell()
            c.addElement(MyParagraph(Paragraph.ALIGN_CENTER, 10f, titulo, Font(Font.TIMES_ROMAN, 12f), 14))

            for (i in linhas.indices) {
                c.addElement(MyParagraph(Paragraph.ALIGN_LEFT, 0f, linhas[i], Font(Font.TIMES_ROMAN, 10f), if (i == linhas.size - 1) 2 else 12))
            }

            table.addCell(c)
            return table
        } catch (e: Exception) {
            return null
        }

    }




    fun getMyCell(header: String, content: String, colspan: Int): PdfPCell {
        val c = PdfPCell()
        c.isNoWrap = false
        c.colspan = colspan
        val t = PdfPTable(1)
        t.widthPercentage = 100f
        val c1 = PdfPCell()
        c1.isNoWrap = false
        c1.border = PdfPCell.NO_BORDER
        c1.verticalAlignment = PdfPCell.ALIGN_TOP
        c1.addElement(MyParagraph(Paragraph.ALIGN_LEFT, 7f, header, Font(Font.TIMES_ROMAN, 10f, Font.BOLD), 0))
        c1.addElement(MyParagraph(Paragraph.ALIGN_LEFT, 13f, "   $content", Font(Font.TIMES_ROMAN, 11f, Font.NORMAL), 0))
        t.addCell(c1)
        c.addElement(t)
        return c
    }


    fun getCellBoleto(header: String, content: String, colspan: Int, align: Int): PdfPCell {
        val c = PdfPCell()
        c.isNoWrap = false
        c.colspan = colspan
        val t = PdfPTable(1)
        t.widthPercentage = 100f
        val c1 = PdfPCell()
        c1.isNoWrap = false
        c1.border = PdfPCell.NO_BORDER
        c1.verticalAlignment = PdfPCell.ALIGN_TOP
        c1.addElement(MyParagraph(align, 7f, " $header", Font(Font.TIMES_ROMAN, 9f, Font.NORMAL), 0))
        c1.addElement(MyParagraph(align, 13f, " $content", Font(Font.HELVETICA, 12f, Font.BOLD), 0))
        t.addCell(c1)
        c.addElement(t)
        return c
    }

    fun getCellHeader(content: String, colspan: Int, align: Int, contentSize: Int, bg: Color): PdfPCell {
        val c = PdfPCell()
        c.isNoWrap = false
        c.colspan = colspan
        c.addElement(MyParagraph(align, 14f, " $content", Font(Font.TIMES_ROMAN, contentSize.toFloat(), Font.BOLD), 0))
        c.backgroundColor = bg
        return c
    }


    fun getCellBoleto(header: String, content: String, colspan: Int, align: Int, headerSize: Int, contentSize: Int): PdfPCell {
        val c = PdfPCell()
        c.isNoWrap = false
        c.colspan = colspan
        val t = PdfPTable(1)
        t.widthPercentage = 100f
        val c1 = PdfPCell()
        c1.isNoWrap = false
        c1.border = PdfPCell.NO_BORDER
        c1.verticalAlignment = PdfPCell.ALIGN_TOP
        c1.addElement(MyParagraph(align, 7f, " $header", Font(Font.TIMES_ROMAN, headerSize.toFloat(), Font.NORMAL), 0))
        c1.addElement(MyParagraph(align, 11f, " $content", Font(Font.HELVETICA, contentSize.toFloat(), Font.BOLD), 0))
        t.addCell(c1)
        c.addElement(t)
        return c
    }


    fun getImageCellFromByteArray(byteArray: ByteArray, colspan: Int): PdfPCell {
        val cell = PdfPCell()
        cell.colspan = colspan
        cell.border = PdfPCell.NO_BORDER
        cell.addElement(getFromByteArray(byteArray))
        cell.verticalAlignment = Element.ALIGN_MIDDLE
        return cell
    }

    fun getImageCellFromImage(image: Image, colspan: Int): PdfPCell {
        val cell = PdfPCell()
        cell.colspan = colspan
        cell.addElement(image)
        cell.verticalAlignment = Element.ALIGN_MIDDLE
        return cell
    }

    private fun getFromByteArray(byteArray: ByteArray): Image? {
        var image: Image? = null
        try {
            image = Image.getInstance(byteArray)
        } catch (e: BadElementException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return image
    }


    fun getImageCellFromPath(imagePath: String, colspan: Int): PdfPCell {
        val c = PdfPCell()
        c.isNoWrap = false
        c.colspan = colspan
        c.addElement(getFromPath(imagePath))
        return c
    }

    fun getImageCellFromPath(imagePath: String, colspan: Int, noBorder: Boolean, padding: Int): PdfPCell {
        val c = PdfPCell()
        if (noBorder)
            c.border = PdfPCell.NO_BORDER
        c.setPadding(padding.toFloat())
        c.isNoWrap = false
        c.colspan = colspan
        c.addElement(getFromPath(imagePath))
        return c
    }


    private fun getFromPath(imagePath: String): Image? {
        var image: Image? = null
       // val realPath = servletContext.getRealPath(imagePath)
        try {
            var f = ClassPathResource(imagePath).getInputStream() //ResourceUtils.getFile("classpath:$imagePath")
            image = Image.getInstance(f.readBytes())
        } catch (e: BadElementException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return image
    }


    fun dottedLineSeparator(): Paragraph {
        val p = Paragraph("")
        val line = LineSeparator(1f, 100f, null, Element.ALIGN_CENTER, -10f)
        p.add(line)
        return p
    }





    fun getMyCell2(horizontalAlignment: Int, content: String, colspan: Int, baseColor: Color): PdfPCell {
        val c = PdfPCell()
        c.isNoWrap = false
        c.colspan = colspan
        c.addElement(MyParagraph(horizontalAlignment, 10f, content, Font(Font.TIMES_ROMAN, 10f, Font.BOLD, baseColor), 0))
        return c
    }

    fun getEmptyCell(colspan: Int): PdfPCell {
        val c = PdfPCell()
        c.isNoWrap = false
        c.border = PdfPCell.NO_BORDER
        c.colspan = colspan
        return c
    }

    fun getMyCell2(horizontalAlignment: Int, content: String, colspan: Int, baseColor: Color, bgcolor: Color): PdfPCell {
        val c = PdfPCell()
        c.isNoWrap = false
        c.colspan = colspan
        c.addElement(MyParagraph(horizontalAlignment, 10f, content, Font(Font.TIMES_ROMAN, 10f, Font.BOLD, baseColor), 0))
        c.backgroundColor = bgcolor
        return c
    }


   /* fun getOperadoraShort(o: Operadora?): String {
        if (o == null)
            return ""
        when (o) {
            CLARO -> return "C"
            TIM -> return "T"
            OI -> return "O"
            VIVO -> return "V"
            LINK_CLARO -> return "LC"
            LINK_TIM -> return "LT"
            LINK_VIVO -> return "LV"
            VODAFONE -> return "VO"
            TRANSMEET_VIVO -> return "TV"
            else -> {
            }
        }
        return ""
    }
*/

}

fun cell(horizontalAlignment: Int, content: String, colspan: Int, bgDark: Boolean?=false): PdfPCell {
    val c = PdfPCell()
    c.isNoWrap = false
    c.colspan = colspan
    if(bgDark!!)
        c.backgroundColor = UtilsReport.FAIRLY_LIGTH_GRAY
    c.addElement(MyParagraph(horizontalAlignment, 10f, content, Font(Font.TIMES_ROMAN, 10f, Font.BOLD), 3))
    return c
}

fun cellWithHeader(header: String, content: String, colspan: Int): PdfPCell {
    val c = PdfPCell()
    c.isNoWrap = false
    c.colspan = colspan
    val t = PdfPTable(1)
    t.widthPercentage = 100f
    val c1 = PdfPCell()
    c1.isNoWrap = false
    c1.border = PdfPCell.NO_BORDER
    c1.verticalAlignment = PdfPCell.ALIGN_TOP
    c1.backgroundColor = UtilsReport.FAIRLY_LIGTH_GRAY
    c1.addElement(MyParagraph(Paragraph.ALIGN_LEFT, 7f, header, Font(Font.TIMES_ROMAN, 10f, Font.BOLD), 0))
    c1.addElement(MyParagraph(Paragraph.ALIGN_LEFT, 13f, "   $content", Font(Font.TIMES_ROMAN, 11f, Font.NORMAL), 0))
    t.addCell(c1)
    c.addElement(t)
    return c
}

fun header(title: String, colspan: Int, horizontalAlignment: Int): PdfPCell {
    val f = Font(Font.TIMES_ROMAN, 10f, Font.BOLD)
    val cell = PdfPCell()
    cell.backgroundColor = UtilsReport.FAIRLY_LIGTH_GRAY
    cell.colspan = colspan
    cell.addElement(MyParagraph(horizontalAlignment, 10f, title, f, 3))
    return cell
}
