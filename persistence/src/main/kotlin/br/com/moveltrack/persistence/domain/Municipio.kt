package br.com.moveltrack.persistence.domain

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.validation.constraints.NotNull

@Entity
//@Table(name = "municipio")
data class Municipio(
        @Id val codigo: Int? = null,
        val descricao: String? = null,
        var latitude: Double? = null,
        var longitude: Double? = null,

        @ManyToOne
        val uf:Uf?=null
        ) {

        @Transient
        internal var label: String? = null
        fun getLabel(): String {
                return "$descricao-${uf?.sigla}"
        }
}

