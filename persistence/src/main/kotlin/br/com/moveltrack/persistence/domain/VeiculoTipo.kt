package br.com.moveltrack.persistence.domain


enum class VeiculoTipo private constructor(val descricao: String) {

    MOTOCICLETA("MOTOCICLETA"),
    AUTOMOVEL("AUTOMÓVEL"),
    CAMINHAO("CAMINHÃO"),
    TRATOR("TRATOR"),
    PICKUP("PICKUP");

    override fun toString(): String {
        return name
    }



}

fun findVeiculoTipoByName(name: String): VeiculoTipo? {
    return VeiculoTipo.values().firstOrNull{it.name==name}
}
