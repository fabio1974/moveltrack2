package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.GeoEndereco
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(path="geoenderecos",exported = false)
interface GeoEnderecoRepository: PagingAndSortingRepository<GeoEndereco, Int>, JpaSpecificationExecutor<GeoEndereco> {
}
