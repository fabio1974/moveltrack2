package br.com.moveltrack.persistence.pojos

import br.com.moveltrack.persistence.domain.Viagem


class RelatorioMotoristaPorViagem(
        var viagem: Viagem =Viagem(),
        var despesaCombustivel: LitrosValor=LitrosValor(),
        var despesaDiarias: Double=0.toDouble(),
        var despesaEstivas: Double=0.toDouble(),
        var diasViagens: Double=0.toDouble(),
        var pesoDaCarga: Int= 0,
        var despesaDiversas: Double=0.toDouble()
)
