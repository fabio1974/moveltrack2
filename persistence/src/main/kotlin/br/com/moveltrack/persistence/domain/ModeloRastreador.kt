package br.com.moveltrack.persistence.domain

enum class ModeloRastreador {GT06, H08, TK103A2, GT06B, XT009, GT06N, GT06B2, TR02, CRX1, JV200, TK06, ST350_LC2, ST350_LC4, ST940, CRXN, SPOT_TRACE, CRX3}
