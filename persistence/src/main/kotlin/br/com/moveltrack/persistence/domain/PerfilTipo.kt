package br.com.moveltrack.persistence.domain

enum class PerfilTipo {
    ADMINISTRADOR,
    FRANQUEADO,
    CLIENTE_PJ,
    CLIENTE_PF,
    CLIENTE_DEMONSTRACAO,
    VENDEDOR,
    FINANCEIRO,
    GERENTE_GERAL,
    GERENTE_ADM,
    INSTALADOR
}
