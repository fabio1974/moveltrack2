package br.com.moveltrack.persistence.pojos

import java.util.*

class RelatorioMensalDespesa {
    var ano: Int = 0
    var mes: Int = 0
    var data: Date? = null
    var carga: Double = 0.toDouble()
    var combustivel: Double = 0.toDouble()
    var manutencao: Double = 0.toDouble()
    var estivas: Double = 0.toDouble()
    var diarias: Double = 0.toDouble()
    var ipva: Double = 0.toDouble()
    var transito: Double = 0.toDouble()
    var trabalhistas: Double = 0.toDouble()
    var outras: Double = 0.toDouble()
    var despesa: Double = 0.toDouble()
}
