package br.com.moveltrack.persistence.domain

enum class CategoriaCnh {
    A, B, C, D, E, AB, AC, AD, AE, ACC
}
