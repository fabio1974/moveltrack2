package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.Cliente
import br.com.moveltrack.persistence.domain.Contrato
import br.com.moveltrack.persistence.domain.ContratoGeraCarne
import br.com.moveltrack.persistence.domain.ContratoStatus
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(path="contratos",exported = false)
interface ContratoRepository: PagingAndSortingRepository<Contrato, Int>, JpaSpecificationExecutor<Contrato> {
    fun findByCliente(cliente: Cliente?):Contrato
    fun findAllByStatusAndContratoGeraCarneNot(status: ContratoStatus, pendencia: ContratoGeraCarne): List<Contrato>?
    fun findAllByStatus(status: ContratoStatus): List<Contrato>?
}

