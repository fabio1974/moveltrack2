package br.com.moveltrack.persistence.domain

enum class Operadora {
    CLARO,
    OI,
    TIM,
    VIVO,
    TODAS,
    LINK_CLARO,
    LINK_TIM,
    LINK_VIVO,
    VODAFONE,
    TRANSMEET_VIVO
}
