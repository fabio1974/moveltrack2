package br.com.moveltrack.persistence.domain

import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.ManyToOne
import javax.validation.constraints.NotNull
import java.util.Date


@Entity
class Motorista (

    /*    @NotNull
    @Enumerated(EnumType.STRING)
    private MotoristaStatus status;
*/
    /*	public MotoristaStatus getStatus() {
		return status;
	}

	public void setStatus(MotoristaStatus status) {
		this.status = status;
	}*/

    @ManyToOne
    var patrao: Cliente? = null,

    var validadeCnh: Date? = null,

    @NotNull
    @Enumerated(EnumType.STRING)
    var categoriaCnh: CategoriaCnh? = null
): Pessoa()

