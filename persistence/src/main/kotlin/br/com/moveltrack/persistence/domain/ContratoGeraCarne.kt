package br.com.moveltrack.persistence.domain

enum class ContratoGeraCarne {

    GERADO, PENDENTE

}
