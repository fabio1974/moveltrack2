package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.Equipamento
import br.com.moveltrack.persistence.domain.ModeloRastreador
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(path="equipamentos",exported = false)
interface EquipamentoRepository: PagingAndSortingRepository<Equipamento, Int>, JpaSpecificationExecutor<Equipamento> {
    abstract fun findAllByImeiContaining(imei: String?): List<Equipamento>
    abstract fun findByImei(imei: String?): Equipamento
    abstract fun findAllByModeloAndInicioSpotIsNull(modelo: ModeloRastreador): List<Equipamento>
    abstract fun findAllByModeloAndAtrasoGmtNotAndChipIsNotNull(m: ModeloRastreador,atrasoGmt: Int): List<Equipamento>?
}
