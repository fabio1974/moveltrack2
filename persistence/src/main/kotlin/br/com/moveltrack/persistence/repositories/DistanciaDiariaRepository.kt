package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.DistanciaDiaria
import br.com.moveltrack.persistence.domain.Veiculo
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import java.time.ZonedDateTime
import java.util.*

@RepositoryRestResource(path="distanciaDiarias")
interface DistanciaDiariaRepository: JpaRepository<DistanciaDiaria, Int> {
    abstract fun findAllByVeiculoAndDataComputadaGreaterThanEqualAndDataComputadaLessThanEqual(veiculo: Veiculo, inicio: Date, fim: Date): List<DistanciaDiaria>
    abstract fun findTopByVeiculoAndDataComputadaOrderByDataComputada(veiculo: Veiculo, dataComputada: ZonedDateTime): DistanciaDiaria?
}
