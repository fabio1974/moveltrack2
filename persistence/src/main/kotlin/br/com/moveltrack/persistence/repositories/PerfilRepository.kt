package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.Perfil
import br.com.moveltrack.persistence.domain.PerfilTipo
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(path="perfils")
interface PerfilRepository: JpaRepository<Perfil, Int> {
    abstract fun findByTipo(tipo: PerfilTipo): Perfil
}
