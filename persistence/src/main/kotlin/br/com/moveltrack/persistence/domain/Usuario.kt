package br.com.moveltrack.persistence.domain

import java.util.*
import javax.persistence.*

@Entity
//@Table(name = "usuario")
data class Usuario(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int? = null,

        @Column(length = 50, nullable = false)
        var senha: String? = null,

        @Column(length = 50)
        var email: String? = null,

        @Column(nullable = false)
        var ativo: Boolean = false,

        @Column(length = 25, unique = true, nullable = false)
        var nomeUsuario: String? = null,

        @Temporal(TemporalType.TIMESTAMP)
        var ultimoAcesso: Date? = null,

        @ManyToOne(fetch = FetchType.EAGER)
        var perfil: Perfil? = null,

        @ManyToMany(fetch = FetchType.EAGER)
        var  permissoes:Set<Permissao> = HashSet<Permissao>()

)
