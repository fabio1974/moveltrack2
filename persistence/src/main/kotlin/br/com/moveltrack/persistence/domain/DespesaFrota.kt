package br.com.moveltrack.persistence.domain

import java.util.Date
import javax.persistence.*

import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@Entity
////@Table(name="despesafrota")
class DespesaFrota {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null


    @Temporal(TemporalType.TIMESTAMP)
    var dataDaDespesa: Date? = null


    @Size(max = 30)
    var descricao: String? = null

    @ManyToOne
    var veiculo: Veiculo? = null

    @ManyToOne
    var motorista: Motorista? = null

    @ManyToOne
    var cliente: Cliente? = null

    @ManyToOne
    var viagem: Viagem? = null

    @Enumerated(EnumType.STRING)
    var categoria: DespesaFrotaCategoria? = null

    @Enumerated(EnumType.STRING)
    var especie: DespesaFrotaEspecie? = null

    @NotNull
    var valor: Double = 0.toDouble()

    var litros: Double = 0.toDouble()

    @Temporal(TemporalType.TIMESTAMP)
    var dataDePagamento: Date? = null



}
