package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.Contrato
import br.com.moveltrack.persistence.domain.Equipamento
import br.com.moveltrack.persistence.domain.Veiculo
import br.com.moveltrack.persistence.domain.VeiculoStatus
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
interface VeiculoRepository: PagingAndSortingRepository<Veiculo, Int>, JpaSpecificationExecutor<Veiculo> {
    abstract fun findAllByContratoOrderByPlaca(contrato: Contrato): List<Veiculo>
    abstract fun findAllByContratoAndStatusOrderByPlaca(contrato: Contrato, status: VeiculoStatus): List<Veiculo>
    abstract fun findAllByContratoAndStatus(contrato: Contrato, status: VeiculoStatus): List<Veiculo>
    abstract fun countByContratoAndStatus(contrato: Contrato, ativo: VeiculoStatus): Long
    abstract fun findAllByPlacaContaining(placa: String): List<Veiculo>
    abstract fun findByEquipamento(equipamento: Equipamento): Veiculo?

}
