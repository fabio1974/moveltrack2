package br.com.moveltrack.persistence.domain

enum class ViagemStatus private constructor(val descricao: String) {

    ABERTA("ABERTA"),
    PARTIDA_EM_ATRASO("PARTIDA EM ATRASO"),
    SAINDO("SAINDO"),
    SAIU_DA_CERCA("SAIU DA CERCA"),
    NA_ESTRADA("NA ESTRADA"),
    ENTROU_NA_CERCA("ENTROU NA CERCA"),
    SE_APROXIMANDO("SE APROXIMANDO"),
    CHEGADA_EM_TEMPO("CHEGADA EM TEMPO"),
    CHEGADA_EM_ATRASO("CHEGADA EM ATRASO"),
    ENCERRADA("ENCERRADA");

    override fun toString(): String {
        return name
    }

}
