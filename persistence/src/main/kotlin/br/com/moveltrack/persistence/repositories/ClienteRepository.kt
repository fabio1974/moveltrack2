package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.Cliente
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(path="clientes",exported = false)
interface ClienteRepository: PagingAndSortingRepository<Cliente, Int>, JpaSpecificationExecutor<Cliente> {
    abstract fun findByNomeContaining(nome: String?): MutableList<Cliente>
}
