package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.TipoDeCobranca
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

class MBoletoRepositoryImpl constructor(@PersistenceContext var em: EntityManager): MBoletoRepositoryCustom {
    override fun getProximoNossoNumero(tipoDeCobranca: TipoDeCobranca): String {
        val sql:String = "select max(substring(nossoNumero,3)) from mboleto ";
        val query = em.createNativeQuery(sql)
        var proximo: Long = 0

        try {
            val ultimo = query.getSingleResult() as String
            proximo = java.lang.Long.parseLong(ultimo.substring(2)) + 1
        } catch (e: Exception) {
            println(e.message)
        }
        return (if (tipoDeCobranca == TipoDeCobranca.COM_REGISTRO) 14 else 24).toString() + String.format("%015d", proximo)
    }
}
