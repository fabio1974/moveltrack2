package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.TipoDeCobranca

interface MBoletoRepositoryCustom{
    fun getProximoNossoNumero(tipoDeCobranca: TipoDeCobranca): String
}
