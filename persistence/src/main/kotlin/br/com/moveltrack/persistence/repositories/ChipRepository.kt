package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.Chip
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource


@RepositoryRestResource(path="chips",exported = false)
interface ChipRepository: PagingAndSortingRepository<Chip, Int>, JpaSpecificationExecutor<Chip> {
    abstract fun findByIccidContaining(iccid: String?): List<Chip>
}

