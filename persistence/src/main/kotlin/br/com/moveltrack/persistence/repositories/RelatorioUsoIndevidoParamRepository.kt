package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.Cliente
import br.com.moveltrack.persistence.domain.RelatorioUsoIndevidoParam
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface RelatorioUsoIndevidoParamRepository: JpaRepository<RelatorioUsoIndevidoParam, Int> {
    abstract fun findAllByCliente(cliente: Cliente?): MutableList<RelatorioUsoIndevidoParam>

}
