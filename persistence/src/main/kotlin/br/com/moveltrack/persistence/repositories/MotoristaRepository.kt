package br.com.moveltrack.persistence.repositories

import br.com.moveltrack.persistence.domain.Cliente
import br.com.moveltrack.persistence.domain.Motorista
import br.com.moveltrack.persistence.domain.PessoaStatus
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import java.util.*

@RepositoryRestResource(path="motoristas",exported = false)
interface MotoristaRepository: PagingAndSortingRepository<Motorista, Int>, JpaSpecificationExecutor<Motorista> {
    abstract fun findByNomeContaining(nome: String?): MutableList<Motorista>
    abstract fun findAllByNomeContaining(nome: String): List<Motorista>
    abstract fun findAllByPatraoEqualsAndValidadeCnhLessThanEqualAndStatusEqualsOrderByValidadeCnh(cliente: Cliente, limite: Date?, status: PessoaStatus): MutableList<Motorista>?
}
