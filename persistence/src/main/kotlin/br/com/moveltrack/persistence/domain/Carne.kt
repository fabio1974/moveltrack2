package br.com.moveltrack.persistence.domain


import java.time.ZonedDateTime
import java.util.Date
import javax.persistence.*
import javax.validation.constraints.NotNull


@Entity
////@Table(name = "carne")
class Carne(

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int? = null,

        @NotNull
        @ManyToOne
        var contrato: Contrato? = null,

        @NotNull
        @Enumerated(EnumType.STRING)
        var tipoDeCobranca: TipoDeCobranca? = null,

        @ManyToOne
        var conteudo: CarneConteudo? = null,

        var quantidadeBoleto: Int = 0,

        //private double mensalidade;

        var dataVencimentoInicio: ZonedDateTime? = null,

        var dataVencimentoFim: ZonedDateTime? = null,

        var dataReferencia: ZonedDateTime? = null,

        var postagem: ZonedDateTime? = null,

        var dataEmissao: ZonedDateTime? = null,

        @Transient
        var valorPrimeiroBoleto: Double? = null
)
