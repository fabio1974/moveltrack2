package br.com.moveltrack.persistence.domain

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
//@Table(name = "permissao")
data class Permissao(@Id var id:Int?=null, var descricao: String? = null)
