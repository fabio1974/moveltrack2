package br.com.moveltrack.persistence.domain

import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size
import kotlin.jvm.Transient


@Entity
class Chip {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null

    @NotNull
    @Size(max = 30)
    var numero: String? = null

    @Size(max = 30)
    var iccid: String? = null

    @Temporal(TemporalType.TIMESTAMP)
    var dataCadastro: Date? = null

    @NotNull
    @Enumerated(EnumType.STRING)
    var operadora: Operadora? = null

    @Enumerated(EnumType.STRING)
    var status: ChipStatus? = ChipStatus.ATIVO

    @Transient
    var label: String?=null


}
