package br.com.moveltrack.persistence.domain


enum class EquipamentoSituacao private constructor(val descricao: String) {

    AGUARDANDO_TESTE("Aguardando teste"),
    ATIVO("Ativo"),
    COM_DEFEITO("Com defeito"),
    EM_TESTE("Em teste"),
    NAO_ENCONTRADO("Não encontrado"),
    NOVO_CONFIGURADO("Novo - Configurado"),
    NOVO_NAO_CONFIGURADO("Novo - Não configurado"),
    OUTRA("Outra"),
    PRONTO_PARA_REUSO("Pronto para reuso"),
    SUSPENSO("Suspenso");

    override fun toString(): String {
        return name
    }


}
