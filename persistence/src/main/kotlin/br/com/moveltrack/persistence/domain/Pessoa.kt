package br.com.moveltrack.persistence.domain

import java.util.Date
import javax.persistence.*

import javax.validation.constraints.NotNull
import javax.validation.constraints.Size


@Entity
//@Table(name = "pessoa")
public open class Pessoa(

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int? = null,

        @Size(max = 25)
        var cpf: String? = null,

        @Column(unique = true)
        @Size(max = 25)
        var cnpj: String? = null,

        @Enumerated(EnumType.STRING)
        var status: PessoaStatus? = null,

        @NotNull
        @Size(max = 60)
        var nome: String? = null,

        var email: String? = null,

        var logoFile: String? = null,

        var ultimaAlteracao: Date? = null,

        var dataCadastro: Date? = null,

        var dataNascimento: Date? = null,

        @Size(max = 40)
        var telefoneFixo: String? = null,

        @Size(max = 40)
        var celular1: String? = null,

        @Size(max = 40)
        var celular2: String? = null,

        @NotNull
        @Size(max = 60)
        var endereco: String? = null,

        @NotNull
        @Size(max = 5)
        var numero: String? = null,

        @Size(max = 40)
        var complemento: String? = null,

        @ManyToOne
        var municipio: Municipio? = null,

        @Size(max = 30)
        var cep: String? = null,

        @NotNull
        var bairro: String? = null,

        @ManyToOne
        var usuario: Usuario? = null
)
