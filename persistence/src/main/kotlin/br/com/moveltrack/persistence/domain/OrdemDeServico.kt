package br.com.moveltrack.persistence.domain

import java.time.ZonedDateTime
import java.util.Date

import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.Temporal
import javax.persistence.TemporalType
import javax.validation.constraints.Size

@Entity
class OrdemDeServico(

        @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null,

        //@Temporal(TemporalType.TIMESTAMP)
    val dataDoServico: ZonedDateTime? = null,

        @ManyToOne
    val cliente: Cliente? = null,

        @ManyToOne
    val veiculo: Veiculo? = null,

        @Size(max = 150)
    val observacao: String? = null,

        @Size(max = 150)
    var numero: String? = null,

        @ManyToOne
    val operador: Empregado? = null,

        @Enumerated(EnumType.STRING)
    val servico: OrdemDeServicoTipo? = null,

        val valorDoServico: Double = 0.toDouble(),

        @Enumerated(EnumType.STRING)
    val status: OrdemDeServicoStatus? = null

)
