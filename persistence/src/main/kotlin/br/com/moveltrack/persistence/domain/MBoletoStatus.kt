package br.com.moveltrack.persistence.domain

enum class MBoletoStatus {
    EMITIDO,
    VENCIDO,
    PAGAMENTO_EFETUADO,
    CANCELADO,
    PROTESTADO
}
