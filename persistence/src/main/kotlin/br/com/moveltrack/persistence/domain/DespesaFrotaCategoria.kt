package br.com.moveltrack.persistence.domain

enum class DespesaFrotaCategoria private constructor(val descricao: String) {

    MOTORISTA("MOTORISTA"),
    VEICULO("VEÍCULO"),
    VIAGEM("VIAGEM");

    override fun toString(): String {
        return name
    }

}
